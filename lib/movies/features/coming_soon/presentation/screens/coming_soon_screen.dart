import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/presentation/controllers/coming_soon_bloc.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/presentation/controllers/coming_soon_events.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../components/coming_soon_component.dart';
import '../controllers/coming_soon_states.dart';

class ComingSoonScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<ComingSoonBloc>()
        ..add(GetAllComingSoonMoviesEvent(
          page: 1,
          language: FunctionsManager.getLangCode(context),
        )),
      child: Scaffold(
          backgroundColor: ColorsManager.kEerieBlack,
          appBar: AppBar(
            elevation: SizesManager.s0,
            backgroundColor: ColorsManager.kEerieBlack,
            title: Text(
              FunctionsManager.translateText(
                  text: StringsManager.comingSoonString, context: context),
              style: getBoldTextStyle(
                fontSize: FontSizeManager.size16,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
          ),
          body: const ComingSoonContent()),
    );
  }
}

class ComingSoonContent extends StatefulWidget {
  const ComingSoonContent({Key? key}) : super(key: key);

  @override
  State<ComingSoonContent> createState() => _ComingSoonContentState();
}

class _ComingSoonContentState extends State<ComingSoonContent> {
  final scrollController = ScrollController();
  int page = 1;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(_listener);
  }

  _listener() {
    if (scrollController.position.maxScrollExtent ==
        scrollController.position.pixels) {
      page = page + 1;
      BlocProvider.of<ComingSoonBloc>(context).add(GetAllComingSoonMoviesEvent(
          page: page, language: FunctionsManager.getLangCode(context)));
    }
  }

  @override
  void dispose() {
    scrollController
      ..removeListener(_listener)
      ..dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ComingSoonBloc, ComingSoonState>(
      builder: (context, state) {
        switch (state.requestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return state.comingSoon.isEmpty
                ? BaseEmptyWidget(
                    emptyMessage: StringsManager.noComingSoonFilmsString,
                  )
                : SingleChildScrollView(
                    key: const Key('topRatedScrollView'),
                    controller: scrollController,
                    child: Column(
                      children: [
                        FadeInUp(
                          from: SizesManager.s20,
                          duration: const Duration(
                            milliseconds: ConstsManager.fadeAnimationDuration,
                          ),
                          child: ListView.separated(
                            padding: EdgeInsets.symmetric(
                              vertical:
                                  getHeight(inputHeight: PaddingManager.p16),
                              horizontal:
                                  getWidth(inputWidth: PaddingManager.p8),
                            ),
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              return index >= state.comingSoon.length
                                  ? BaseLoadingIndicator()
                                  : ComingSoonComponent(
                                      comingSoon: state.comingSoon[index]);
                            },
                            separatorBuilder: (context, index) => SizedBox(
                              height: getHeight(
                                inputHeight: SizesManager.s20,
                              ),
                            ),
                            itemCount: state.hasReachedMax
                                ? state.comingSoon.length
                                : state.comingSoon.length + 1,
                          ),
                        ),
                      ],
                    ),
                  );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorComingSoonMessage,
                onPressFunction: () {
                  BlocProvider.of<ComingSoonBloc>(context)
                    ..add(GetAllComingSoonMoviesEvent(
                        page: page,
                        language: FunctionsManager.getLangCode(context)));
                });
        }
      },
    );
  }
}
