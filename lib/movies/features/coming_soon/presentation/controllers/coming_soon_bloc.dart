import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/domain/usecases/get_coming_soon_usecase.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/presentation/controllers/coming_soon_events.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/presentation/controllers/coming_soon_states.dart';

class ComingSoonBloc extends Bloc<ComingSoonEvents, ComingSoonState> {
  final GetComingSoonMoviesUseCase getComingSoonUseCase;
  ComingSoonBloc({required this.getComingSoonUseCase})
      : super(ComingSoonState()) {
    on<GetAllComingSoonMoviesEvent>(
      (event, emit) async {
        if (state.hasReachedMax) return;
        try {
          if (state.requestStates == RequestStates.loading) {
            final response = await getComingSoonUseCase(
                ComingSoonParameters(page: event.page , language: event.language));
            response.fold((l) {
              emit(
                state.copyWith(
                  requestStates: RequestStates.error,
                  errorComingSoonMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                      state.copyWith(
                        requestStates: RequestStates.success,
                        comingSoon: r,
                        hasReachedMax: false,
                      ),
                    );
            });
          } else {
            final response = await getComingSoonUseCase(
                ComingSoonParameters(page: event.page , language: event.language));
            print("length is ${state.comingSoon.length}");
            response.fold((l) {
              emit(
                state.copyWith(
                  requestStates: RequestStates.error,
                  errorComingSoonMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                      state.copyWith(
                        requestStates: RequestStates.success,
                        comingSoon: [...state.comingSoon, ...r],
                        hasReachedMax: false,
                      ),
                    );
            });
          }
        } catch (e) {
          emit(
            state.copyWith(
              requestStates: RequestStates.error,
              errorComingSoonMessage: e.toString(),
            ),
          );
        }
      },
      transformer: droppable(),
    );
  }
}
