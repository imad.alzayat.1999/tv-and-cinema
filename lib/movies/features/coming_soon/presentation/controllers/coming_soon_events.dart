import 'package:equatable/equatable.dart';

abstract class ComingSoonEvents extends Equatable{

}

class GetAllComingSoonMoviesEvent extends ComingSoonEvents{
  final int page;
  final String language;

  GetAllComingSoonMoviesEvent({required this.page , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [page , language];
}