import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';

import '../../domain/entities/coming_soon.dart';

class ComingSoonState extends Equatable {
  final bool hasReachedMax;
  final RequestStates requestStates;
  final List<ComingSoon> comingSoon;
  final String errorComingSoonMessage;

  ComingSoonState({
    this.hasReachedMax = false,
    this.requestStates = RequestStates.loading,
    this.comingSoon = const [],
    this.errorComingSoonMessage = '',
  });

  ComingSoonState copyWith({
    bool? hasReachedMax,
    RequestStates? requestStates,
    List<ComingSoon>? comingSoon,
    String? errorComingSoonMessage,
  }) {
    return ComingSoonState(
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      requestStates: requestStates ?? this.requestStates,
      comingSoon: comingSoon ?? this.comingSoon,
      errorComingSoonMessage:
          errorComingSoonMessage ?? this.errorComingSoonMessage,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        hasReachedMax,
        requestStates,
        comingSoon,
        errorComingSoonMessage,
      ];
}

