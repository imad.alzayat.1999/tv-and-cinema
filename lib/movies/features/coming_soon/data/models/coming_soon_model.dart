import 'package:intl/intl.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/domain/entities/coming_soon.dart';

class ComingSoonModel extends ComingSoon {
  ComingSoonModel(
      {required int? id,
      required String? movieImage,
      required String? movieTitle,
      required num? voteAvg,
      required DateTime? movieReleaseDate})
      : super(
            id: id,
            movieImage: movieImage,
            movieTitle: movieTitle,
            voteAvg: voteAvg,
            movieReleaseDate: movieReleaseDate,
  );

  factory ComingSoonModel.fromJson(Map<String, dynamic> json) => ComingSoonModel(
    id: json["id"],
    movieImage: json["backdrop_path"],
    movieTitle: json["title"],
    voteAvg: json["vote_average"],
    movieReleaseDate: DateFormat.y().parse(json["release_date"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "backdrop_path": movieImage,
    "title": movieTitle,
    "vote_average": voteAvg,
    "release_date": DateFormat.y().format(movieReleaseDate!)
  };
}
