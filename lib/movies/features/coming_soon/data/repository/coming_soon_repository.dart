import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/data/datasources/coming_soon_remote_datasource.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/domain/entities/coming_soon.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/domain/repository/base_coming_soon_repository.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/domain/usecases/get_coming_soon_usecase.dart';

import '../../../../../core/network/network_info.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../datasources/coming_soon_local_datasource.dart';

class ComingSoonRepository extends BaseComingSoonRepository {
  final BaseComingSoonRemoteDataSource baseComingSoonRemoteDataSource;
  final BaseComingSoonLocalDataSource baseComingSoonLocalDataSource;
  final NetworkInfo networkInfo;

  ComingSoonRepository({
    required this.baseComingSoonRemoteDataSource,
    required this.baseComingSoonLocalDataSource,
    required this.networkInfo,
  });
  @override
  Future<Either<Failure, List<ComingSoon>>> getComingSoonMovies(
      ComingSoonParameters comingSoonParameters) async {
    if(await networkInfo.isConnected){
      try {
        final result = await baseComingSoonRemoteDataSource
            .getComingSoonMovies(comingSoonParameters);
        baseComingSoonLocalDataSource.cacheInfo(result, ConstsManager.CACHE_COMING_SOON_LIST);
        return Right(result);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message));
      }
    }else{
      try {
        final localData = await baseComingSoonLocalDataSource
            .getCachedInfo(ConstsManager.CACHE_COMING_SOON_LIST);
        return Right(localData);
      } on LocalDatabaseException catch (e) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }
}
