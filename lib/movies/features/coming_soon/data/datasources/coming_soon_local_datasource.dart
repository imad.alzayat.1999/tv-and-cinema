import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/data/models/coming_soon_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/services/services_locator.dart';

abstract class BaseComingSoonLocalDataSource{
  Future<List<ComingSoonModel>> getCachedInfo(String infoKey);
  Future<Unit> cacheInfo(List<ComingSoonModel> comingSoonMovies, String infoKey);
}
class ComingSoonLocalDataSource extends BaseComingSoonLocalDataSource{
  @override
  Future<Unit> cacheInfo(List<ComingSoonModel> comingSoonMovies, String infoKey) {
    List comingSoonModelsToJson = comingSoonMovies
        .map<Map<String, dynamic>>((comingSoonMovie) => comingSoonMovie.toJson())
        .toList();
    getIt<SharedPreferences>().setString(infoKey, json.encode(comingSoonModelsToJson));
    print("Info Cached for is:  " + json.encode(comingSoonModelsToJson));
    return Future.value(unit);
  }

  @override
  Future<List<ComingSoonModel>> getCachedInfo(String infoKey) {
    final jsonString = getIt<SharedPreferences>().getString(infoKey);
    if (jsonString != null) {
      List decodeJsonData = json.decode(jsonString);
      List<ComingSoonModel> jsonToComingSoonModels = decodeJsonData
          .map<ComingSoonModel>((jsonComingSoonModel) => ComingSoonModel.fromJson(jsonComingSoonModel))
          .toList();
      return Future.value(jsonToComingSoonModels);
    } else {
      throw LocalDatabaseException(message: '');
    }
  }

}