import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/data/models/coming_soon_model.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/domain/usecases/get_coming_soon_usecase.dart';

abstract class BaseComingSoonRemoteDataSource {
  Future<List<ComingSoonModel>> getComingSoonMovies(
      ComingSoonParameters comingSoonParameters);
}

class ComingSoonRemoteDataSource extends BaseComingSoonRemoteDataSource {
  final ApiConsumer apiConsumer;

  ComingSoonRemoteDataSource({required this.apiConsumer});
  @override
  Future<List<ComingSoonModel>> getComingSoonMovies(
      ComingSoonParameters comingSoonParameters) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getComingSoonMoviesEndPoint,
      queryParameters: {
        "page": comingSoonParameters.page,
        "language": comingSoonParameters.language,
      },
    );
    return List<ComingSoonModel>.from((response["results"] as List).map(
      (e) => ComingSoonModel.fromJson(e),
    ));
  }
}
