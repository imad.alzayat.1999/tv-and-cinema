import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/domain/entities/coming_soon.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/domain/repository/base_coming_soon_repository.dart';

class GetComingSoonMoviesUseCase extends BaseUseCase<List<ComingSoon> , ComingSoonParameters>{
  final BaseComingSoonRepository baseComingSoonRepository;

  GetComingSoonMoviesUseCase({required this.baseComingSoonRepository});
  @override
  Future<Either<Failure, List<ComingSoon>>> call(ComingSoonParameters parameters) async{
    return await baseComingSoonRepository.getComingSoonMovies(parameters);
  }
}

class ComingSoonParameters extends Equatable{
  final int page;
  final String language;

  const ComingSoonParameters({required this.page , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [page , language];
}