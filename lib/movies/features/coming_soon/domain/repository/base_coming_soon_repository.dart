import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/domain/usecases/get_coming_soon_usecase.dart';

import '../../../../../core/error/failure.dart';
import '../entities/coming_soon.dart';

abstract class BaseComingSoonRepository{
  Future<Either<Failure , List<ComingSoon>>> getComingSoonMovies(ComingSoonParameters comingSoonParameters);
}