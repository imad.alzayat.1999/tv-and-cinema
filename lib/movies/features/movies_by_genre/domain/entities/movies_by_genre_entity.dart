import 'package:equatable/equatable.dart';

class MoviesByGenreEntity extends Equatable {
  final String? movieImage;
  final int? id;
  final DateTime? movieReleaseDate;
  final num? voteAvg;
  final String? title;

  MoviesByGenreEntity({
    required this.movieImage,
    required this.id,
    required this.movieReleaseDate,
    required this.voteAvg,
    required this.title,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [movieImage , id , movieReleaseDate , voteAvg , title];
}
