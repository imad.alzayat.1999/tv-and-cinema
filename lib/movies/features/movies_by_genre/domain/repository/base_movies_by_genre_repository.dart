import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/domain/entities/movies_by_genre_entity.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/domain/usecases/movies_by_genre_usecase.dart';

import '../../../../../core/error/failure.dart';

abstract class BaseMoviesByGenreRepository {
  Future<Either<Failure, List<MoviesByGenreEntity>>> getMoviesByGenre(
      MoviesByGenreParameters moviesByGenreParameters);
}
