import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/domain/entities/movies_by_genre_entity.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/domain/repository/base_movies_by_genre_repository.dart';

class MoviesByGenreUseCase extends BaseUseCase<List<MoviesByGenreEntity> , MoviesByGenreParameters>{
  final BaseMoviesByGenreRepository baseMoviesByGenreRepository;

  MoviesByGenreUseCase({required this.baseMoviesByGenreRepository});
  @override
  Future<Either<Failure, List<MoviesByGenreEntity>>> call(MoviesByGenreParameters parameters) async{
    return await baseMoviesByGenreRepository.getMoviesByGenre(parameters);
  }
}

class MoviesByGenreParameters extends Equatable{
  final int page;
  final String movieGenre;
  final String language;

  MoviesByGenreParameters({required this.page, required this.movieGenre , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [page , movieGenre , language];
}