import 'package:intl/intl.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/domain/entities/movies_by_genre_entity.dart';

class MoviesByGenreModel extends MoviesByGenreEntity {
  MoviesByGenreModel(
      {required String? movieImage,
      required int? id,
      required DateTime? movieReleaseDate,
      required num? voteAvg,
      required String? title})
      : super(
          movieImage: movieImage,
          id: id,
          movieReleaseDate: movieReleaseDate,
          voteAvg: voteAvg,
          title: title,
        );

  factory MoviesByGenreModel.fromJson(Map<String, dynamic> json) =>
      MoviesByGenreModel(
          movieImage: json["backdrop_path"],
          id: json["id"],
          movieReleaseDate: DateTime.parse(json["release_date"]),
          voteAvg: json["vote_average"],
          title: json["title"]);

  Map<String, dynamic> toJson() => {
        "backdrop_path": movieImage,
        "id": id,
        "release_date": DateFormat.y().format(movieReleaseDate!),
        "vote_average": voteAvg,
        "title": title,
      };
}
