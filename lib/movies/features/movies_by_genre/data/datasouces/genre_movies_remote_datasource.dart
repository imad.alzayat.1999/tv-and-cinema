import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/data/models/movies_by_genre_model.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/domain/usecases/movies_by_genre_usecase.dart';

abstract class BaseGenreMoviesRemoteDataSource{
  Future<List<MoviesByGenreModel>> getMoviesByGenre(MoviesByGenreParameters moviesByGenreParameters);
}

class GenreMoviesRemoteDataSource extends BaseGenreMoviesRemoteDataSource{
  final ApiConsumer apiConsumer;

  GenreMoviesRemoteDataSource({required this.apiConsumer});

  @override
  Future<List<MoviesByGenreModel>> getMoviesByGenre(MoviesByGenreParameters moviesByGenreParameters) async{
    final response = await apiConsumer.get(path: ApiConstance.discoverMoviesEndPoint , queryParameters: {
      "page" : moviesByGenreParameters.page,
      "with_genres" : moviesByGenreParameters.movieGenre,
      "language" : moviesByGenreParameters.language,
    });
    return List<MoviesByGenreModel>.from((response["results"] as List).map(
          (e) => MoviesByGenreModel.fromJson(e),
    ));
  }
}