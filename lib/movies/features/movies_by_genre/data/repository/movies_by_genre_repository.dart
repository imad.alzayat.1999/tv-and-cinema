import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/domain/entities/movies_by_genre_entity.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/domain/repository/base_movies_by_genre_repository.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/domain/usecases/movies_by_genre_usecase.dart';

import '../datasouces/genre_movies_remote_datasource.dart';

class MoviesByGenreRepository extends BaseMoviesByGenreRepository {
  final BaseGenreMoviesRemoteDataSource baseGenreMoviesRemoteDataSource;

  MoviesByGenreRepository({required this.baseGenreMoviesRemoteDataSource});

  @override
  Future<Either<Failure, List<MoviesByGenreEntity>>> getMoviesByGenre(
      MoviesByGenreParameters moviesByGenreParameters) async{
    try{
      final result = await baseGenreMoviesRemoteDataSource.getMoviesByGenre(moviesByGenreParameters);
      return Right(result);
    }on ServerException catch(failure){
      return Left(ServerFailure(failure.message));
    }
  }
}
