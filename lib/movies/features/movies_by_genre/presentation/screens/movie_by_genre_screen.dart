import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/presentation/components/movie_by_genre_component.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/presentation/controller/movies_by_genre_bloc.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/presentation/controller/movies_by_genre_events.dart';

import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../../domain/entities/movies_by_genre_entity.dart';
import '../controller/movies_by_genre_states.dart';

class MovieByGenreScreen extends StatefulWidget {
  final String movieGenre;
  final String genreId;

  const MovieByGenreScreen({
    Key? key,
    required this.movieGenre,
    required this.genreId,
  }) : super(key: key);

  @override
  State<MovieByGenreScreen> createState() => _MovieByGenreScreenState();
}

class _MovieByGenreScreenState extends State<MovieByGenreScreen> {
  String _getGenreName(String name, BuildContext context) {
    switch (name) {
      case "Action":
        return FunctionsManager.translateText(
            text: StringsManager.actionString, context: context);
      case "Adventure":
        return FunctionsManager.translateText(
            text: StringsManager.adventureString, context: context);
      case "Animation":
        return FunctionsManager.translateText(
            text: StringsManager.animationString, context: context);
      case "Comedy":
        return FunctionsManager.translateText(
            text: StringsManager.comedyString, context: context);
      case "Crime":
        return FunctionsManager.translateText(
            text: StringsManager.crimeString, context: context);
      case "Documentary":
        return FunctionsManager.translateText(
            text: StringsManager.documentaryString, context: context);
      case "Drama":
        return FunctionsManager.translateText(
            text: StringsManager.dramaString, context: context);
      case "Family":
        return FunctionsManager.translateText(
            text: StringsManager.familyString, context: context);
      case "Fantasy":
        return FunctionsManager.translateText(
            text: StringsManager.fantasyString, context: context);
      case "History":
        return FunctionsManager.translateText(
            text: StringsManager.historyString, context: context);
      case "Horror":
        return FunctionsManager.translateText(
            text: StringsManager.horrorString, context: context);
      case "Music":
        return FunctionsManager.translateText(
            text: StringsManager.musicString, context: context);
      case "Mystery":
        return FunctionsManager.translateText(
            text: StringsManager.mystreyString, context: context);
      case "Romance":
        return FunctionsManager.translateText(
            text: StringsManager.romanceString, context: context);
      case "Science Fiction":
        return FunctionsManager.translateText(
            text: StringsManager.scienceFictionString, context: context);
      case "TV Movie":
        return FunctionsManager.translateText(
            text: StringsManager.tvMoviesString, context: context);
      case "Thriller":
        return FunctionsManager.translateText(
            text: StringsManager.thrillerString, context: context);
      case "War":
        return FunctionsManager.translateText(
            text: StringsManager.warString, context: context);
      default:
        return FunctionsManager.translateText(
            text: StringsManager.otherString, context: context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<MoviesByGenreBloc>()
        ..add(
          GetAllMoviesByGenreEvent(
              page: 1,
              movieGenre: widget.genreId,
              language: FunctionsManager.getLangCode(context)),
        ),
      child: Scaffold(
        backgroundColor: ColorsManager.kEerieBlack,
        appBar: AppBar(
          elevation: SizesManager.s0,
          backgroundColor: ColorsManager.kEerieBlack,
          title: Text(
            _getGenreName(widget.movieGenre, context),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size16,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
        ),
        body: MovieByGenreContent(genreId: widget.genreId),
      ),
    );
  }
}

class MovieByGenreContent extends StatefulWidget {
  final String genreId;
  const MovieByGenreContent({Key? key, required this.genreId})
      : super(key: key);

  @override
  State<MovieByGenreContent> createState() => _MovieByGenreContentState();
}

class _MovieByGenreContentState extends State<MovieByGenreContent> {
  List<MoviesByGenreEntity> moviesByGenre = [];
  final scrollController = ScrollController();
  int page = 1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    scrollController.addListener(_listener);
  }

  _listener() {
    if (scrollController.position.maxScrollExtent ==
        scrollController.position.pixels) {
      page = page + 1;
      BlocProvider.of<MoviesByGenreBloc>(context).add(GetAllMoviesByGenreEvent(
          page: page,
          movieGenre: widget.genreId,
          language: FunctionsManager.getLangCode(context)));
    }
  }

  @override
  void dispose() {
    scrollController
      ..removeListener(_listener)
      ..dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MoviesByGenreBloc, MoviesByGenreStates>(
      builder: (context, state) {
        switch (state.moviesByGenreRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return state.moviesByGenre.isEmpty
                ? BaseEmptyWidget(emptyMessage: StringsManager.noMoviesByGenreString)
                : SingleChildScrollView(
                    key: const Key('topRatedScrollView'),
                    controller: scrollController,
                    child: Column(
                      children: [
                        FadeInUp(
                          from: SizesManager.s20,
                          duration: const Duration(
                            milliseconds: ConstsManager.fadeAnimationDuration,
                          ),
                          child: ListView.separated(
                            padding: EdgeInsets.symmetric(
                              vertical:
                                  getHeight(inputHeight: PaddingManager.p16),
                              horizontal:
                                  getWidth(inputWidth: PaddingManager.p8),
                            ),
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              if (index >= state.moviesByGenre.length) {
                                return BaseLoadingIndicator();
                              }
                              return MovieByGenreComponent(
                                  moviesByGenre: state.moviesByGenre[index]);
                            },
                            separatorBuilder: (context, index) => SizedBox(
                              height: getHeight(inputHeight: SizesManager.s20),
                            ),
                            itemCount: state.hasReachedMax
                                ? state.moviesByGenre.length
                                : state.moviesByGenre.length + 1,
                          ),
                        ),
                      ],
                    ),
                  );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorMoviesByGenreMessage,
                onPressFunction: () {
                  BlocProvider.of<MoviesByGenreBloc>(context)
                    ..add(GetAllMoviesByGenreEvent(
                        page: page,
                        movieGenre: widget.genreId,
                        language: FunctionsManager.getLangCode(context)));
                });
        }
      },
    );
  }
}
