import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';

import '../../domain/entities/movies_by_genre_entity.dart';

class MoviesByGenreStates extends Equatable {
  final bool hasReachedMax;
  final RequestStates moviesByGenreRequestStates;
  final List<MoviesByGenreEntity> moviesByGenre;
  final String errorMoviesByGenreMessage;

  MoviesByGenreStates({
    this.hasReachedMax = false,
    this.moviesByGenreRequestStates = RequestStates.loading,
    this.moviesByGenre = const [],
    this.errorMoviesByGenreMessage = '',
  });

  MoviesByGenreStates copyWith({
    bool? hasReachedMax,
    RequestStates? moviesByGenreRequestStates,
    List<MoviesByGenreEntity>? moviesByGenre,
    String? errorMoviesByGenreMessage,
  }) {
    return MoviesByGenreStates(
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      errorMoviesByGenreMessage:
          errorMoviesByGenreMessage ?? this.errorMoviesByGenreMessage,
      moviesByGenreRequestStates:
          moviesByGenreRequestStates ?? this.moviesByGenreRequestStates,
      moviesByGenre: moviesByGenre ?? this.moviesByGenre,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        hasReachedMax,
        errorMoviesByGenreMessage,
        moviesByGenreRequestStates,
        moviesByGenre,
      ];
}
