import 'package:equatable/equatable.dart';

abstract class MoviesByGenreEvents extends Equatable{}

class GetAllMoviesByGenreEvent extends MoviesByGenreEvents{
  final int page;
  final String movieGenre;
  final String language;

  GetAllMoviesByGenreEvent({required this.page, required this.movieGenre , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [page , movieGenre , language];
}

