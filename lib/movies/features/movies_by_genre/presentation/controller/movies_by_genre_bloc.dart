import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/presentation/controller/movies_by_genre_events.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/presentation/controller/movies_by_genre_states.dart';

import '../../../../../core/utils/request_states.dart';
import '../../domain/usecases/movies_by_genre_usecase.dart';

class MoviesByGenreBloc extends Bloc<MoviesByGenreEvents, MoviesByGenreStates> {
  final MoviesByGenreUseCase moviesByGenreUseCase;
  MoviesByGenreBloc({required this.moviesByGenreUseCase})
      : super(MoviesByGenreStates()) {
    on<GetAllMoviesByGenreEvent>(
      (event, emit) async {
        if (state.hasReachedMax) return;
        try {
          if (state.moviesByGenreRequestStates == RequestStates.loading) {
            final response = await moviesByGenreUseCase(
              MoviesByGenreParameters(
                page: event.page,
                movieGenre: event.movieGenre,
                language: event.language,
              ),
            );
            response.fold((l) {
              emit(
                state.copyWith(
                  moviesByGenreRequestStates: RequestStates.error,
                  errorMoviesByGenreMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                      state.copyWith(
                        moviesByGenreRequestStates: RequestStates.success,
                        moviesByGenre: r,
                        hasReachedMax: false,
                      ),
                    );
            });
          } else {
            final response = await moviesByGenreUseCase(
              MoviesByGenreParameters(
                page: event.page,
                movieGenre: event.movieGenre,
                language: event.language,
              ),
            );
            response.fold((l) {
              emit(
                state.copyWith(
                  moviesByGenreRequestStates: RequestStates.error,
                  errorMoviesByGenreMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                      state.copyWith(
                        moviesByGenreRequestStates: RequestStates.success,
                        moviesByGenre: [...state.moviesByGenre, ...r],
                        hasReachedMax: false,
                      ),
                    );
            });
          }
        } catch (e) {
          emit(
            state.copyWith(
              moviesByGenreRequestStates: RequestStates.error,
              errorMoviesByGenreMessage: e.toString(),
            ),
          );
        }
      },
      transformer: droppable(),
    );
  }
}
