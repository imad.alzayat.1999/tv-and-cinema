import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/movies/features/search/domain/repository/base_search_repository.dart';
import '../entities/result_entity.dart';

class SearchForMoviesUseCase extends BaseUseCase<List<ResultEntity> , SearchParameters>{
  final BaseSearchRepository baseSearchRepository;

  SearchForMoviesUseCase({required this.baseSearchRepository});

  @override
  Future<Either<Failure, List<ResultEntity>>> call(SearchParameters parameters) async{
    return await baseSearchRepository.getSearchResults(parameters);
  }
}
class SearchParameters extends Equatable{
  final int page;
  final String query;
  final String language;

  SearchParameters({required this.page, required this.query , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [page , query , language];
}