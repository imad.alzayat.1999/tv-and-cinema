import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/movies/features/search/domain/entities/result_entity.dart';
import 'package:movie_app_clean_architecture/movies/features/search/domain/usecases/search_for_movies_usecase.dart';

abstract class BaseSearchRepository{
  Future<Either<Failure , List<ResultEntity>>> getSearchResults(SearchParameters searchParameters);
}