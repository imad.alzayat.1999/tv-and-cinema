import 'package:equatable/equatable.dart';

class ResultEntity extends Equatable {
  final int? id;
  final num? voteAvg;
  final String? movieImage;
  final String? movieTitle;
  final DateTime? releaseDate;

  ResultEntity({
    required this.id,
    required this.voteAvg,
    required this.movieImage,
    required this.movieTitle,
    required this.releaseDate,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [id , voteAvg , movieImage , movieTitle , releaseDate];
}
