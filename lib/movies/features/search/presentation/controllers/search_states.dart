import 'package:equatable/equatable.dart';
import '../../../../../core/utils/request_states.dart';
import '../../domain/entities/result_entity.dart';

class SearchStates extends Equatable{
  final bool hasReachedMax;
  final RequestStates searchRequestStates;
  final List<ResultEntity> resMovies;
  final String errorSearchMessage;

  SearchStates({
    this.hasReachedMax = false,
    this.searchRequestStates = RequestStates.loading,
    List<ResultEntity>? resMovies,
    this.errorSearchMessage = '',
  }) : resMovies = resMovies ?? [];

  SearchStates copyWith({
    bool? hasReachedMax,
    RequestStates? searchRequestStates,
    List<ResultEntity>? resMovies,
    String? errorSearchMessage,
  }) {
    return SearchStates(
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      errorSearchMessage: errorSearchMessage ?? this.errorSearchMessage,
      resMovies: resMovies ?? this.resMovies,
      searchRequestStates: searchRequestStates ?? this.searchRequestStates,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
    hasReachedMax,
    searchRequestStates,
    resMovies,
    errorSearchMessage,
  ];
}