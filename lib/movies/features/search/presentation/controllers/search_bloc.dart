import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/movies/features/search/presentation/controllers/search_events.dart';
import 'package:movie_app_clean_architecture/movies/features/search/presentation/controllers/search_states.dart';

import '../../../../../core/utils/request_states.dart';
import '../../domain/usecases/search_for_movies_usecase.dart';

class SearchBloc extends Bloc<SearchEvents, SearchStates> {
  final SearchForMoviesUseCase searchForMoviesUseCase;
  bool isLoadingMore = false;
  SearchBloc({required this.searchForMoviesUseCase})
      : super(SearchStates()) {
    on<GetResultsEvent>((event, emit) async {
      if (state.hasReachedMax) return;
      try {
        if (state.searchRequestStates == RequestStates.loading) {
          final response = await searchForMoviesUseCase(
            SearchParameters(page: event.page , query: event.query , language: event.language),
          );
          response.fold((l) {
            emit(
              state.copyWith(
                searchRequestStates: RequestStates.error,
                errorSearchMessage: l.message,
              ),
            );
          }, (r) {
            r.isEmpty
                ? emit(state.copyWith(hasReachedMax: true))
                : emit(
              state.copyWith(
                searchRequestStates: RequestStates.success,
                resMovies: r,
                hasReachedMax: false,
              ),
            );
          });
        } else {
          final response = await searchForMoviesUseCase(
            SearchParameters(page: event.page , query: event.query , language: event.language),
          );
          print("length is ${state.resMovies.length}");
          response.fold((l) {
            emit(
              state.copyWith(
                searchRequestStates: RequestStates.error,
                errorSearchMessage: l.message,
              ),
            );
          }, (r) {
            r.isEmpty
                ? emit(state.copyWith(hasReachedMax: true))
                : emit(
              state.copyWith(
                searchRequestStates: RequestStates.success,
                resMovies: [...state.resMovies, ...r],
                hasReachedMax: false,
              ),
            );
          });
        }
      } catch (e) {
        emit(
          state.copyWith(
            searchRequestStates: RequestStates.error,
            errorSearchMessage: e.toString(),
          ),
        );
      }
    } , transformer: droppable());
  }
}
