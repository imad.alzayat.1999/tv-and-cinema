import 'package:equatable/equatable.dart';

abstract class SearchEvents extends Equatable{

}

class GetResultsEvent extends SearchEvents{
  final String query;
  final String language;
  int page;

  GetResultsEvent({required this.query , required this.page , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [query , page , language];
}