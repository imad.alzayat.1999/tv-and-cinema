import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_icon.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import 'package:movie_app_clean_architecture/movies/features/search/presentation/components/search_text_field.dart';
import 'package:movie_app_clean_architecture/movies/features/search/presentation/controllers/search_bloc.dart';
import 'package:movie_app_clean_architecture/movies/features/search/presentation/controllers/search_events.dart';
import 'package:movie_app_clean_architecture/movies/features/search/presentation/controllers/search_states.dart';

import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../components/result_component.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<SearchBloc>(),
      child: Scaffold(
        backgroundColor: ColorsManager.kEerieBlack,
        appBar: AppBar(
          elevation: SizesManager.s0,
          backgroundColor: ColorsManager.kEerieBlack,
          title: Text(
            FunctionsManager.translateText(
                text: StringsManager.searchString, context: context),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size16,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
        ),
        body: SearchContent(),
      ),
    );
  }
}

class SearchContent extends StatefulWidget {
  const SearchContent({Key? key}) : super(key: key);

  @override
  State<SearchContent> createState() => _SearchContentState();
}

class _SearchContentState extends State<SearchContent> {
  bool _isSearched = false;
  ScrollController? _scrollController = ScrollController();
  ScrollController? _mainScrollController = ScrollController();
  var searchController = TextEditingController();
  int page = 1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scrollController!.addListener(_listener);
  }

  _listener() {
    if (_scrollController!.position.maxScrollExtent ==
        _scrollController!.position.pixels) {
      page = page + 1;
      BlocProvider.of<SearchBloc>(context).add(GetResultsEvent(
          query: searchController.text,
          page: page,
          language: FunctionsManager.getLangCode(context)));
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _scrollController!
      ..removeListener(_listener)
      ..dispose();
  }

  /// widget to search here
  Widget _hintToSearch() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BaseIcon(
            iconHeight: SizesManager.s125,
            iconPath: IconsManager.searchIcon,
          ),
          Text(
            FunctionsManager.translateText(
                text: StringsManager.searchHerePleaseString, context: context),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size20,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
        ],
      ),
    );
  }

  /// widget to get the result
  Widget _getTheResult() {
    return BlocBuilder<SearchBloc, SearchStates>(
      builder: (context, state) {
        switch (state.searchRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return FadeInUp(
              from: SizesManager.s20,
              duration: const Duration(
                milliseconds: ConstsManager.fadeAnimationDuration,
              ),
              child: state.resMovies.isEmpty
                  ? BaseEmptyWidget(
                      emptyMessage: StringsManager.noResultsString)
                  : ListView.separated(
                      padding: EdgeInsets.symmetric(
                        vertical: getHeight(inputHeight: PaddingManager.p16),
                        horizontal: getWidth(inputWidth: PaddingManager.p8),
                      ),
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        if (index >= state.resMovies.length) {
                          return BaseLoadingIndicator();
                        }
                        return ResultComponent(
                            resultEntity: state.resMovies[index]);
                      },
                      separatorBuilder: (context, index) => SizedBox(
                          height: getHeight(inputHeight: SizesManager.s20)),
                      itemCount: context.read<SearchBloc>().isLoadingMore
                          ? state.resMovies.length
                          : state.resMovies.length + 1,
                    ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorSearchMessage,
                onPressFunction: () {
                  BlocProvider.of<SearchBloc>(context).add(GetResultsEvent(
                      query: searchController.text,
                      page: page,
                      language: FunctionsManager.getLangCode(context)));
                });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    page = 1;
    return SingleChildScrollView(
      controller: _isSearched ? _scrollController : _mainScrollController,
      child: Column(
        children: [
          SearchTextField(
            textEditingController: searchController,
            onChange: (value) {
              if (value.isEmpty) {
                setState(() {
                  _isSearched = false;
                });
              } else {
                setState(() {
                  _isSearched = true;
                  BlocProvider.of<SearchBloc>(context).state.resMovies.clear();
                  BlocProvider.of<SearchBloc>(context)
                    ..add(
                      GetResultsEvent(
                          query: value,
                          page: 1,
                          language: FunctionsManager.getLangCode(context)),
                    );
                });
              }
            },
          ),
          _isSearched ? _getTheResult() : _hintToSearch(),
        ],
      ),
    );
  }
}
