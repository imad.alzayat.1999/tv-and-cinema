import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/movies/features/search/domain/entities/result_entity.dart';

import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';

class ResultComponent extends StatelessWidget {
  final ResultEntity resultEntity;

  const ResultComponent({Key? key, required this.resultEntity})
      : super(key: key);

  /// get movie image
  Widget getMovieImage({required String? image}) {
    return SizedBox(
      width: getWidth(inputWidth: SizesManager.s125),
      height: getHeight(inputHeight: SizesManager.s150),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(SizesManager.s16),
        child: FadeInImage.assetNetwork(
          image: FunctionsManager.showAPIImage(image),
          placeholder: ImagesManager.logoImage,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          Routes.movieDetailsRoute,
          arguments: DetailsRouteParameters(id: resultEntity.id!),
        );
      },
      child: Card(
        color: ColorsManager.kDimGray,
        elevation: SizesManager.s4,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(SizesManager.s16),
        ),
        child: Row(
          children: [
            getMovieImage(image: resultEntity.movieImage),
            SizedBox(width: getWidth(inputWidth: SizesManager.s20)),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    resultEntity.movieTitle!,
                    style: getBoldTextStyle(
                      fontSize: FontSizeManager.size16,
                      letterSpacing: 0,
                      context: context,
                    ),
                  ),
                  SizedBox(height: getHeight(inputHeight: SizesManager.s16)),
                  Row(
                    children: [
                      const BaseIcon(
                        iconHeight: SizesManager.s20,
                        iconPath: IconsManager.calendarIcon,
                      ),
                      SizedBox(width: getWidth(inputWidth: SizesManager.s10)),
                      Text(
                        FunctionsManager.formatDate(resultEntity.releaseDate),
                        style: getMediumTextStyle(
                          fontSize: FontSizeManager.size14,
                          letterSpacing: SizesManager.s0,
                          context: context,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(width: getWidth(inputWidth: SizesManager.s10)),
            Padding(
              padding: const EdgeInsets.all(PaddingManager.p8),
              child: Row(
                children: [
                  const BaseIcon(
                    iconHeight: SizesManager.s20,
                    iconPath: IconsManager.starIcon,
                    iconColor: ColorsManager.voteColor,
                  ),
                  SizedBox(width: getWidth(inputWidth: SizesManager.s5)),
                  Text(
                    resultEntity.voteAvg.toString(),
                    style: getRegularTextStyle(
                      fontSize: FontSizeManager.size14,
                      letterSpacing: 0,
                      context: context,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
