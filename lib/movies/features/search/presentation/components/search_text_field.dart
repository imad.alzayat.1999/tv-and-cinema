import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_icon.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../../../core/utils/resources/assets_manager.dart';

class SearchTextField extends StatelessWidget {
  final TextEditingController textEditingController;
  final void Function(String)? onChange;
  const SearchTextField({
    Key? key,
    required this.onChange,
    required this.textEditingController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: getHeight(inputHeight: PaddingManager.p8),
        horizontal: getWidth(inputWidth: PaddingManager.p16),
      ),
      child: TextFormField(
        controller: textEditingController,
        onChanged: onChange,
        style: getRegularTextStyle(
          fontSize: FontSizeManager.size14,
          letterSpacing: SizesManager.s0,
          context: context,
        ),
        decoration: InputDecoration(
          prefixIcon: IconButton(
            onPressed: null,
            icon: BaseIcon(
              iconPath: IconsManager.searchIcon,
              iconHeight: SizesManager.s20,
            ),
          ),
          hintText: FunctionsManager.translateText(text: StringsManager.searchHerePleaseString, context: context),
          hintStyle: getMediumTextStyle(
            fontSize: FontSizeManager.size14,
            letterSpacing: SizesManager.s0,
            context: context,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(SizesManager.s8),
            borderSide: BorderSide(
              width: SizesManager.s1,
              color: ColorsManager.kBlack,
            ),
          ),
        ),
      ),
    );
  }
}
