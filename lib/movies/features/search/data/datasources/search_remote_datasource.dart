import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import 'package:movie_app_clean_architecture/movies/features/search/data/models/result_model.dart';
import 'package:movie_app_clean_architecture/movies/features/search/domain/usecases/search_for_movies_usecase.dart';

abstract class BaseSearchRemoteDataSource {
  Future<List<ResultModel>> getSearchResults(SearchParameters searchParameters);
}

class SearchRemoteDataSource extends BaseSearchRemoteDataSource {
  final ApiConsumer apiConsumer;

  SearchRemoteDataSource({required this.apiConsumer});

  @override
  Future<List<ResultModel>> getSearchResults(
      SearchParameters searchParameters) async {
    final response = await apiConsumer.get(
      path: ApiConstance.searchForMovieEndPoint,
      queryParameters: {
        "api_key": ApiConstance.apiKey,
        "page": searchParameters.page,
        "query": searchParameters.query,
        "language": searchParameters.language,
      },
    );
    return List<ResultModel>.from((response["results"] as List).map(
      (e) => ResultModel.fromJson(e),
    ));
  }
}
