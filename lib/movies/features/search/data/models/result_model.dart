import 'package:intl/intl.dart';
import 'package:movie_app_clean_architecture/movies/features/search/domain/entities/result_entity.dart';

class ResultModel extends ResultEntity {
  ResultModel({
    required int? id,
    required num? voteAvg,
    required String? movieImage,
    required String? movieTitle,
    required DateTime? releaseDate,
  }) : super(
          id: id,
          voteAvg: voteAvg,
          movieImage: movieImage,
          movieTitle: movieTitle,
          releaseDate: releaseDate,
        );

  factory ResultModel.fromJson(Map<String, dynamic> json) => ResultModel(
        id: json["id"],
        voteAvg: json["vote_average"],
        movieImage: json["backdrop_path"],
        movieTitle: json["title"],
        releaseDate: json["release_date"] == "" || json["release_date"] == null ? DateTime.now() :DateTime.parse(json["release_date"]),
      );

  Map<String , dynamic> toJson() => {
    "id" : id,
    "vote_average":voteAvg,
    "backdrop_path":movieImage,
    "title":movieTitle,
    "release_date":DateFormat.y().format(releaseDate!),
  };
}
