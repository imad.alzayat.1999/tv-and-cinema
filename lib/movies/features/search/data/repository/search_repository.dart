import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/movies/features/search/data/datasources/search_remote_datasource.dart';
import 'package:movie_app_clean_architecture/movies/features/search/domain/entities/result_entity.dart';
import 'package:movie_app_clean_architecture/movies/features/search/domain/repository/base_search_repository.dart';
import 'package:movie_app_clean_architecture/movies/features/search/domain/usecases/search_for_movies_usecase.dart';

class SearchRepository extends BaseSearchRepository{
  final BaseSearchRemoteDataSource baseSearchRemoteDataSource;

  SearchRepository({required this.baseSearchRemoteDataSource});

  @override
  Future<Either<Failure, List<ResultEntity>>> getSearchResults(SearchParameters searchParameters) async{
    try{
      final result = await baseSearchRemoteDataSource.getSearchResults(searchParameters);
      return Right(result);
    }on ServerException catch(failure){
      return Left(ServerFailure(failure.message));
    }
  }
}