import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_navigation_drawer.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/shared_features/home/home_movies/presentation/screens/movies_screen.dart';
import 'package:movie_app_clean_architecture/shared_features/home/home_tv/presentation/screens/tv_screen.dart';

import 'core/config/language_config/app_localizations.dart';
import 'core/config/routes_config/routes_config.dart';
import 'core/utils/global_widgets/base_icon.dart';
import 'core/utils/resources/assets_manager.dart';
import 'core/utils/resources/colors_manager.dart';
import 'core/utils/resources/values_mananger.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  int _currentIndex = 0;

  List<Widget> pages = [
    MainMoviesScreen(),
    TvScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: ColorsManager.kEerieBlack,
      drawer: BaseNavigationDrawer(),
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            if (scaffoldKey.currentState!.isDrawerOpen) {
              scaffoldKey.currentState!.closeDrawer();
            } else {
              scaffoldKey.currentState!.openDrawer();
            }
          },
          icon: BaseIcon(
            iconPath: IconsManager.menuIcon,
            iconHeight: SizesManager.s20,
            iconColor: ColorsManager.kWhite,
          ),
        ),
        elevation: SizesManager.s0,
        backgroundColor: ColorsManager.kEerieBlack,
      ),
      body: pages[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: ColorsManager.kEerieBlack,
        selectedItemColor: ColorsManager.kWhite,
        selectedLabelStyle: getRegularTextStyle(
          fontSize: FontSizeManager.size14,
          letterSpacing: SizesManager.s0,
          context: context,
        ),
        unselectedLabelStyle: getRegularTextStyle(
          fontSize: FontSizeManager.size14,
          letterSpacing: SizesManager.s0,
          context: context,
        ),
        unselectedItemColor: ColorsManager.kWhite.withOpacity(0.5),
        currentIndex: _currentIndex,
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.all(PaddingManager.p5),
              child: BaseIcon(
                iconColor: _currentIndex == 0
                    ? ColorsManager.kWhite
                    : ColorsManager.kWhite.withOpacity(0.5),
                iconHeight: SizesManager.s20,
                iconPath: IconsManager.movieIcon,
              ),
            ),
            label: AppLocalizations.of(context)!
                .translate(StringsManager.movieString),
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: const EdgeInsets.all(PaddingManager.p5),
              child: BaseIcon(
                iconColor: _currentIndex == 1
                    ? ColorsManager.kWhite
                    : ColorsManager.kWhite.withOpacity(0.5),
                iconHeight: SizesManager.s20,
                iconPath: IconsManager.tvIcon,
              ),
            ),
            label: AppLocalizations.of(context)!
                .translate(StringsManager.tvString),
          ),
        ],
      ),
    );
  }
}
