import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movie_app_clean_architecture/core/services/services_locator.dart';
import 'package:movie_app_clean_architecture/shared_features/language/presentation/controller/language_cubit.dart';
import 'package:movie_app_clean_architecture/shared_features/language/presentation/controller/language_state.dart';
import 'bloc_observer.dart';
import 'core/config/language_config/app_localizations_setup.dart';
import 'core/config/routes_config/routes_config.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await ServicesLocator().init();
  Bloc.observer = AppBlocObserver();
  runApp(Phoenix(child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<LanguageCubit>()..getSavedLang(),
      child: BlocBuilder<LanguageCubit, LanguageState>(
        buildWhen: (previousState, currentState) {
          return previousState != currentState;
        },
        builder: (context, state) {
          return ScreenUtilInit(
            designSize: const Size(390, 844),
            minTextAdapt: true,
            splitScreenMode: true,
            builder: (context , child) {
              return MaterialApp(
                onGenerateRoute: AppRoute.onGenerateRoute,
                initialRoute: Routes.initRoute,
                locale: state.locale,
                debugShowCheckedModeBanner: false,
                supportedLocales: AppLocalizationsSetup.supportedLocales,
                localeResolutionCallback:
                AppLocalizationsSetup.localeResolutionCallback,
                localizationsDelegates:
                AppLocalizationsSetup.localizationsDelegates,
              );
            },
          );
        },
      ),
    );
  }
}