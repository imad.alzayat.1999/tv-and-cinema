import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class ApiInterceptors extends Interceptor{
  /// Fire A Request
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    debugPrint("Request[${options.method}] => Path:[${options.path}]");
    options.headers["Content-Type"] = "application/json";
    super.onRequest(options, handler);
  }

  /// Success
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    debugPrint("Response[${response.statusCode}] => Path:[${response.requestOptions.path}]");
    super.onResponse(response, handler);
  }

  /// Failure
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    debugPrint("Error[${err.response!.statusCode}] => Path:[${err.requestOptions.path}]");
    super.onError(err, handler);
  }
}