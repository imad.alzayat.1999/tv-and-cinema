import 'dart:convert';
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/api/api_interceptors.dart';
import 'package:movie_app_clean_architecture/core/api/status_code.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';

import '../network/api_constance.dart';
import '../services/services_locator.dart';

class DioConsumer implements ApiConsumer {
  final Dio client;

  DioConsumer({required this.client}) {
    (client.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    client.options
      ..baseUrl = ApiConstance.baseUrl
      ..responseType = ResponseType.plain
      ..connectTimeout = 60000
      ..sendTimeout = 60000
      ..contentType = "application/json";
    client.interceptors.add(getIt<ApiInterceptors>());
    if (kDebugMode) {
      client.interceptors.add(getIt<LogInterceptor>());
    }
  }

  @override
  Future delete({
    required String path,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? body,
  }) async {
    try{
      final response = await client.delete(
        path,
        queryParameters: queryParameters,
        data: body,
      );
      return _handleResponse(response);
    } on DioError catch(error){
      _handleException(error);
    }
  }

  @override
  Future get({
    required String path,
    Map<String, dynamic>? queryParameters,
  }) async {
    try{
      final response = await client.get(path, queryParameters: queryParameters);
      return _handleResponse(response);
    }on DioError catch(error){
      _handleException(error);
    }
  }

  @override
  Future post({
    required String path,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? body,
  }) async {
    try{
      final response = await client.post(
        path,
        queryParameters: queryParameters,
        data: body,
      );
      return _handleResponse(response);
    }on DioError catch(error){
      _handleException(error);
    }
  }

  @override
  Future put({
    required String path,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? body,
  }) async {
    try{
      final response = await client.put(
        path,
        queryParameters: queryParameters,
        data: body,
      );
      return _handleResponse(response);
    }on DioError catch(error){
      _handleException(error);
    }
  }

  /// response handling
  _handleResponse(Response<dynamic> response) {
    final responseJson = jsonDecode(response.data.toString());
    return responseJson;
  }

  /// exception handling
  _handleException(DioError error) {
    switch (error.type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.receiveTimeout:
        throw ServerException(message: StringsManager.serverFailureString);
      case DioErrorType.response:
        switch (error.response?.statusCode) {
          case StatusCode.methodNotAllowed:
            throw ServerException(message: StringsManager.serverFailureString);
          case StatusCode.badRequest:
            throw ServerException(message: StringsManager.serverFailureString);
          case StatusCode.unAuthorized:
            throw ServerException(message: StringsManager.serverFailureString);
          case StatusCode.forbidden:
            throw ServerException(message: StringsManager.serverFailureString);
          case StatusCode.notFound:
            throw ServerException(message: StringsManager.serverFailureString);
          case StatusCode.conflict:
            throw ServerException(message: StringsManager.serverFailureString);
          case StatusCode.internalServerError:
            throw ServerException(message: StringsManager.serverFailureString);
        }
        break;
      case DioErrorType.cancel:
        break;
      case DioErrorType.other:
        throw ServerException(message: StringsManager.serverFailureString);
    }
  }
}
