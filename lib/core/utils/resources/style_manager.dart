import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';

import '../../config/language_config/app_localizations.dart';

TextStyle getTextStyle({
  required double fontSize,
  required FontWeight fontWeight,
  required double letterSpacing,
  required TextDecoration textDecoration,
  required Color color,
  required BuildContext context,
}) {
  if (AppLocalizations.of(context)!.isEnLocale) {
    return GoogleFonts.lato(
      fontSize: FontSizeManager.getResponsiveFontSize(fontSize),
      fontWeight: fontWeight,
      letterSpacing: letterSpacing,
      decoration: textDecoration,
      color: color,
    );
  } else {
    return GoogleFonts.tajawal(
      fontSize: FontSizeManager.getResponsiveFontSize(fontSize),
      fontWeight: fontWeight,
      letterSpacing: letterSpacing,
      decoration: textDecoration,
      color: color,
    );
  }
}

/// light weight
TextStyle getLightTextStyle({
  required double fontSize,
  required double letterSpacing,
  required BuildContext context,
  Color color = ColorsManager.kWhite,
  TextDecoration decoration = TextDecoration.none,
}) {
  return getTextStyle(
      fontSize: fontSize,
      fontWeight: FontWeightManager.lightFontWeight,
      letterSpacing: letterSpacing,
      color: color,
      textDecoration: decoration,
      context: context);
}

/// regular weight
TextStyle getRegularTextStyle({
  required double fontSize,
  required double letterSpacing,
  required BuildContext context,
  Color color = ColorsManager.kWhite,
  TextDecoration decoration = TextDecoration.none,
}) {
  return getTextStyle(
      fontSize: fontSize,
      fontWeight: FontWeightManager.regularFontWeight,
      letterSpacing: letterSpacing,
      color: color,
      textDecoration: decoration,
      context: context);
}

/// medium weight
TextStyle getMediumTextStyle({
  required double fontSize,
  required double letterSpacing,
  required BuildContext context,
  Color color = ColorsManager.kWhite,
  TextDecoration decoration = TextDecoration.none,
}) {
  return getTextStyle(
      fontSize: fontSize,
      fontWeight: FontWeightManager.mediumFontWeight,
      letterSpacing: letterSpacing,
      color: color,
      textDecoration: decoration,
      context: context);
}

/// semi bold weight
TextStyle getSemiBoldTextStyle({
  required double fontSize,
  required double letterSpacing,
  required BuildContext context,
  Color color = ColorsManager.kWhite,
  TextDecoration decoration = TextDecoration.none,
}) {
  return getTextStyle(
      fontSize: fontSize,
      fontWeight: FontWeightManager.semiBoldFontWeight,
      letterSpacing: letterSpacing,
      color: color,
      textDecoration: decoration,
      context: context);
}

/// bold weight
TextStyle getBoldTextStyle({
  required double fontSize,
  required double letterSpacing,
  required BuildContext context,
  Color color = ColorsManager.kWhite,
  TextDecoration decoration = TextDecoration.none,
}) {
  return getTextStyle(
      fontSize: fontSize,
      fontWeight: FontWeightManager.boldFontWeight,
      letterSpacing: letterSpacing,
      color: color,
      textDecoration: decoration,
      context: context);
}
