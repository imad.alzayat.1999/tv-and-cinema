class PaddingManager {
  static const double p0 = 0;
  static const double p2 = 2;
  static const double p5 = 5;
  static const double p8 = 8;
  static const double p12 = 12;
  static const double p16 = 16;
  static const double p20 = 20;
  static const double p24 = 24;
  static const double p40 = 40;
  static const double p150 = 150;
}

class SizesManager {
  static const double s0 = 0;
  static const double s0_15 = 0.15;
  static const double s1 = 1;
  static const double s1_2 = 1.2;
  static const double s4 = 4;
  static const double s5 = 5;
  static const double s8 = 8;
  static const double s10 = 10;
  static const double s16 = 16;
  static const double s20 = 20;
  static const double s25 = 25;
  static const double s30 = 30;
  static const double s35 = 35;
  static const double s40 = 40;
  static const double s50 = 50;
  static const double s60 = 60;
  static const double s63 = 63;
  static const double s70 = 70;
  static const double s100 = 100;
  static const double s103 = 103;
  static const double s120 = 120;
  static const double s125 = 125;
  static const double s130 = 130;
  static const double s141 = 141;
  static const double s150 = 150;
  static const double s170 = 170;
  static const double s175 = 175;
  static const double s180 = 180;
  static const double s200 = 200;
  static const double s206 = 206;
  static const double s221 = 221;
  static const double s240 = 240;
  static const double s250 = 250;
  static const double s262 = 262;
  static const double s285 = 285;
  static const double s300 = 300;
  static const double s319 = 319;
  static const double s500 = 500;
}

class MarginManager {
  static const double m4 = 4;
  static const double m8 = 8;
  static const double m10 = 10;
  static const double m16 = 16;
  static const double m24 = 24;
}
