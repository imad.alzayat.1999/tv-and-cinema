import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../shared_features/on_board/domain/entities/on_board_entity.dart';


class ConstsManager {
  /// durations
  static const int splashRoutingDuration = 1;
  static const int carouselAnimationDuration = 500;
  static const int carouselAutoPlayIntervalDuration = 3;
  static const int autoPlayAnimationDuration = 800;
  static const int fadeAnimationDuration = 500;
  static const int onBoardTransitionDuration = 300;
  static const int snackBarDuration = 5;

  /// aspect ratios
  static const double recommendationsAspectRatio = 0.7;
  static const double actorImagesAspectRatio = 0.65;

  /// lang codes
  static const String englishCode = "en-US";
  static const String arabicCode = 'ar-SY';

  /// on board items
  static List<OnBoardEntity> onBoardItems = [
    OnBoardEntity(
      imageUrl: ImagesManager.onBoard1Image,
      title: StringsManager.onBoardString,
      subTitle: StringsManager.onBoardSubTitleString,
      widthOfImage: SizesManager.s285,
      heightOfImage: SizesManager.s221,
      distance: SizesManager.s63,
    ),
    OnBoardEntity(
      imageUrl: ImagesManager.onBoard2Image,
      title: StringsManager.onBoard2String,
      subTitle: StringsManager.onBoardSubTitleString,
      widthOfImage: SizesManager.s262,
      heightOfImage: SizesManager.s206,
      distance: SizesManager.s70,
    ),
    OnBoardEntity(
      imageUrl: ImagesManager.onBoard3Image,
      title: StringsManager.onBoard2String,
      subTitle: StringsManager.onBoardSubTitleString,
      widthOfImage: SizesManager.s319,
      heightOfImage: SizesManager.s141,
      distance: SizesManager.s103,
    ),
  ];

  /// grid delegates
  static const recommendationsGridDelegates =
      SliverGridDelegateWithFixedCrossAxisCount(
    mainAxisSpacing: 8.0,
    crossAxisSpacing: 8.0,
    childAspectRatio: ConstsManager.recommendationsAspectRatio,
    crossAxisCount: 3,
  );
  static const actorImagesGridDelegates =
      SliverGridDelegateWithFixedCrossAxisCount(
    mainAxisSpacing: 8.0,
    crossAxisSpacing: 8.0,
    childAspectRatio: ConstsManager.recommendationsAspectRatio,
    crossAxisCount: 3,
  );

  /// local storage keys
  static const String CACHE_NOW_PLAYING = "CACHE_NOW_PLAYING";
  static const String CACHE_POPULAR = "CACHE_POPULAR";
  static const String CACHE_TOP_RATED = "CACHE_TOP_RATED";
  static const String CACHE_COMING_SOON = "CACHE_COMING_SOON";
  static const String CACHE_LATEST = "CACHE_LATEST";
  static const String CACHE_REVIEWS = "CACHE_REVIEWS";
  static const String CACHE_DETAILS = "CACHE_DETAILS";
  static const String CACHE_RECOMMENDED = "CACHE_RECOMMENDED";
  static const String CACHE_CREDITS = "CACHE_CREDITS";
  static const String CACHE_SIMILAR = "CACHE_SIMILAR";
  static const String CACHE_TRAILER = "CACHE_TRAILER";
  static const String CACHE_POPULAR_LIST = "CACHE_POPULAR_LIST";
  static const String CACHE_COMING_SOON_LIST = "CACHE_COMING_SOON_LIST";
  static const String CACHE_TOP_RATED_LIST = "CACHE_TOP_RATED_LIST";
  static const String CACHE_ACTOR_DETAILS = "CACHE_ACTOR_DETAILS";
  static const String CACHE_ACTOR_IMAGES = "CACHE_ACTOR_IMAGES";
  static const String CACHE_ACTOR_MOVIES = "CACHE_ACTOR_MOVIES";
  static const String LANG_KEY = "LANG_KEY";

  /// language keys
  static const String en = "en";
  static const String ar = "ar";

  /// main image url
  static const String baseImageUrl = 'https://image.tmdb.org/t/p/w500';

  /// other
  static const String dummyReviewProfileName =
      "https://images.unsplash.com/photo-1595769816263-9b910be24d5f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1479&q=80";
  static const String movieHeadersImage =
      "https://cdn.pixabay.com/photo/2019/11/07/20/48/cinema-4609877_1280.jpg";
  static const String actionImage =
      "https://cdn.pixabay.com/photo/2017/12/31/11/55/hand-3052115_1280.jpg";
  static const String adventureImage =
      "https://cdn.pixabay.com/photo/2016/08/01/20/13/girl-1561989_1280.jpg";
  static const String animationImage =
      "https://cdn.pixabay.com/photo/2014/10/31/17/41/dancing-dave-minion-510835_1280.jpg";
  static const String comedyImage =
      "https://cdn.pixabay.com/photo/2017/08/16/22/29/silly-2649321_1280.jpg";
  static const String crimeImage =
      "https://cdn.pixabay.com/photo/2016/01/09/23/03/fist-1131143_1280.jpg";
  static const String documentaryImage =
      "https://cdn.pixabay.com/photo/2017/05/08/08/22/portrait-2294754_1280.jpg";
  static const String dramaImage =
      "https://cdn.pixabay.com/photo/2016/07/01/22/34/people-1492052_1280.jpg";
  static const String familyImage =
      "https://cdn.pixabay.com/photo/2018/01/24/19/49/people-3104635_1280.jpg";
  static const String fantasyImage =
      "https://cdn.pixabay.com/photo/2018/01/12/10/19/fantasy-3077928_1280.jpg";
  static const String historyImage =
      "https://cdn.pixabay.com/photo/2016/08/15/08/22/greece-1594689_1280.jpg";
  static const String horrorImage =
      "https://cdn.pixabay.com/photo/2017/10/13/14/15/fantasy-2847724_1280.jpg";
  static const String musicImage =
      "https://cdn.pixabay.com/photo/2014/05/21/15/18/musician-349790_1280.jpg";
  static const String mysteryImage =
      "https://cdn.pixabay.com/photo/2017/09/15/02/22/fantasy-2750995_1280.jpg";
  static const String romanceImage =
      "https://cdn.pixabay.com/photo/2014/10/22/18/05/couple-498484_1280.jpg";
  static const String scienceFictionImage =
      "https://cdn.pixabay.com/photo/2017/05/10/19/29/robot-2301646_1280.jpg";
  static const String tvMovieImage =
      "https://cdn.pixabay.com/photo/2015/09/16/23/02/video-943569_1280.jpg";
  static const String thrillerImage =
      "https://cdn.pixabay.com/photo/2020/01/17/18/28/killer-4773702_1280.jpg";
  static const String warImage =
      "https://cdn.pixabay.com/photo/2014/10/02/06/34/war-469503_1280.jpg";
  static const String kidsImage =
      "https://cdn.pixabay.com/photo/2015/06/22/08/38/children-817368_1280.jpg";
  static const String newsImage =
      "https://cdn.pixabay.com/photo/2015/05/31/12/12/coffee-791439_1280.jpg";
  static const String realityImage =
      "https://cdn.pixabay.com/photo/2017/04/08/09/01/youth-2212762_1280.jpg";
  static const String soapImage =
      "https://cdn.pixabay.com/photo/2017/06/14/23/15/soap-bubble-2403673_1280.jpg";
  static const String talkImage =
      "https://cdn.pixabay.com/photo/2017/07/31/11/21/people-2557396_1280.jpg";
  static const String warAndPoliticsImage =
      "https://cdn.pixabay.com/photo/2020/03/31/11/06/crowd-4987226_1280.jpg";
  static String getYoutubeUrl(String key) =>
      "https://www.youtube.com/watch?v=$key";
  static DateTime nowDate = DateTime.now();
}
