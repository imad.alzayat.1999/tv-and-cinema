class ImagesManager {
  static const String mainImagesPath = "assets/images";

  ///images
  static const String splashImage = "$mainImagesPath/splash.png";
  static const String logoImage = "$mainImagesPath/logo.png";
  static const String onBoard1Image = "$mainImagesPath/on_board1.svg";
  static const String onBoard2Image = "$mainImagesPath/on_board2.svg";
  static const String onBoard3Image = "$mainImagesPath/on_board3.svg";
  static const String cinemaImage = "$mainImagesPath/cinema.jpg";
}
class IconsManager{
  static const String mainIconsPath = "assets/icons";

  ///icons
  static const String clockIcon = "$mainIconsPath/clock.svg";
  static const String starIcon = "$mainIconsPath/star.svg";
  static const String calendarIcon = "$mainIconsPath/calendar.svg";
  static const String playButtonIcon = "$mainIconsPath/play.svg";
  static const String searchIcon = "$mainIconsPath/search.svg";
  static const String genreIcon = "$mainIconsPath/comedy.svg";
  static const String menuIcon = "$mainIconsPath/menu.svg";
  static const String menuRightIcon = "$mainIconsPath/menu_right.svg";
  static const String englishIcon = "$mainIconsPath/english.svg";
  static const String arabicIcon = "$mainIconsPath/syria.svg";
  static const String refreshIcon = "$mainIconsPath/refresh.svg";
  static const String tvIcon = "$mainIconsPath/tv.svg";
  static const String movieIcon = "$mainIconsPath/movie.svg";
  static const String languageIcon = "$mainIconsPath/language.svg";
}
