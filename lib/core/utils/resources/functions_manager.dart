import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../config/language_config/app_localizations.dart';

class FunctionsManager {
  /// get movies image
  static String imageUrl(String path) => '${ConstsManager.baseImageUrl}$path';

  /// show movie genres
  static String showGenres(List<dynamic> genres) {
    String result = '';
    for (var genre in genres) {
      result += '${genre.name}, ';
    }
    if (result.isEmpty) {
      return result;
    }
    return result.substring(0, result.length - 2);
  }

  /// show movie genres
  static String showSpokenLanguages(List<dynamic> spokenLanguages) {
    String result = '';
    for (var spokenLanguage in spokenLanguages) {
      result += '${spokenLanguage.name}, ';
    }
    if (result.isEmpty) {
      return result;
    }
    return result.substring(0, result.length - 2);
  }

  /// show API image
  static String showAPIImage(String? image) {
    if (image != null) {
      if (image.contains("https")) {
        return image.substring(1, image.length);
      } else {
        return FunctionsManager.imageUrl(image);
      }
    } else {
      return ConstsManager.dummyReviewProfileName;
    }
  }

  /// launch external url
  static launchExternalUrl(String url) async {
    final Uri _url = Uri.parse(url);
    if (!await launchUrl(_url)) throw 'Could not launch $_url';
  }

  /// calculate age of actor
  static int getAgeOfActor(
      {required DateTime birth, required DateTime deathOrToday}) {
    int birthDate = int.parse(DateFormat.y().format(birth));
    int deathOfTodayDate = int.parse(DateFormat.y().format(deathOrToday));
    int age = deathOfTodayDate - birthDate;
    return age;
  }

  /// rich text widget
  static Widget richTextWidget({
    required String title,
    required String? text,
    required BuildContext context,
  }) {
    return RichText(
      text: TextSpan(
        text: title,
        style: getSemiBoldTextStyle(
          fontSize: FontSizeManager.size14,
          letterSpacing: SizesManager.s0,
          context: context
        ),
        children: <TextSpan>[
          TextSpan(
            text: " ${text ?? ""}",
            style: getRegularTextStyle(
              fontSize: FontSizeManager.size14,
              letterSpacing: SizesManager.s0,
              context: context
            ),
          )
        ],
      ),
    );
  }

  static String getActorGender(int gender , BuildContext context) {
    if (gender == 2) {
      return AppLocalizations.of(context)!.translate(StringsManager.maleString)!;
    } else {
      return AppLocalizations.of(context)!.translate(StringsManager.femaleString)!;
    }
  }

  static String formatDate(DateTime? dateTime){
    if(dateTime == null){
      return DateFormat.y().format(DateTime.now());
    }
    return DateFormat.y().format(dateTime);
  }

  static String getLangCode(BuildContext context){
    if(AppLocalizations.of(context)!.isEnLocale){
      return ConstsManager.englishCode;
    }else{
      return ConstsManager.arabicCode;
    }
  }

  static String showDuration(int? runtime){
    if(runtime == null){
      return '0m';
    }else{
      final int hours = runtime ~/ 60;
      final int minutes = runtime % 60;

      if (hours > 0) {
        return '${hours}h ${minutes}m';
      } else {
        return '${minutes}m';
      }
    }
  }

  static String translateText({required String text , required BuildContext context}){
    return AppLocalizations.of(context)!.translate(text).toString();
  }
}
