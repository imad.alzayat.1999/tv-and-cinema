import 'dart:ui';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class FontWeightManager{
  static const FontWeight lightFontWeight = FontWeight.w300;
  static const FontWeight regularFontWeight = FontWeight.w400;
  static const FontWeight mediumFontWeight = FontWeight.w500;
  static const FontWeight semiBoldFontWeight = FontWeight.w600;
  static const FontWeight boldFontWeight = FontWeight.w700;
}

class FontSizeManager{
  static const double size10 = 10;
  static const double size12 = 12;
  static const double size14 = 14;
  static const double size16 = 16;
  static const double size19 = 19;
  static const double size20 = 20;
  static const double size23 = 23;

  static double getResponsiveFontSize(double fontSize){
    return fontSize.sp;
  }
}