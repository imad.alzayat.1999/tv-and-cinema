class StringsManager {
  static const String appName = "movie_app";

  /// home page strings
  static const String nowPlayingString = "now_playing";
  static const String popularString = "popular";
  static const String seeMoreString = "see_more";
  static const String topRatedString = "top_rated";
  static const String comingSoonString = "coming_soon";
  static const String airingTodayString = "airing_today";

  /// details page strings
  static const String genresString = "genres";
  static const String reviewString = "reviews";
  static const String creditsString = "credits";
  static const String similarMoviesString = "similar_movies";
  static const String spokenLanguagesString = "spoken_languages";
  static const String trailersString = "trailers";
  static const String recommendedString = "recommended";
  static const String seasonsString = "seasons";
  static const String moreLikeThisString = "more_like_this";

  /// actor page strings
  static const String personalInformationString = "personal_information";
  static const String birthDateString = "birthdate";
  static const String deathDateString = "death_date";
  static const String birthPlaceString = "birth_place";
  static const String ageString = "age";
  static const String genderString = "gender";
  static const String overviewString = "overview";
  static const String actorImagesString = "actor_images";
  static const String maleString = "male";
  static const String femaleString = "female";
  static const String actorFilmsString = "actor_films";
  static const String actorSeriesString = "actor_series";

  /// search page strings
  static const String searchHerePleaseString = "search_here_please";
  static const String searchString = "search";
  static const String emptyDataString = "empty_data";

  /// navigation drawer strings
  static const String languageString = "language";
  static const String typeOfMoviesString = "type_of_movies";
  static const String typeOfTvString = "type_of_tv";

  /// genres page strings
  static const String genreString = "genres";
  static const String actionString = "action";
  static const String adventureString = "adventure";
  static const String animationString = "animation";
  static const String comedyString = "comedy";
  static const String crimeString = "crime";
  static const String documentaryString = "documentary";
  static const String dramaString = "drama";
  static const String familyString = "family";
  static const String fantasyString = "fantasy";
  static const String horrorString = "horror";
  static const String historyString = "history";
  static const String musicString = "music";
  static const String mystreyString = "mystery";
  static const String romanceString = "romance";
  static const String tvMoviesString = "tv_movies";
  static const String warString = "war";
  static const String thrillerString = "thriller";
  static const String scienceFictionString = "science_fiction";
  static const String otherString = "other";
  static const String actionAndAdventureString = "action_and_adventure";
  static const String scifiAndFantasyString = "sci_fi_and_fantasy";
  static const String kidsString = "kids";
  static const String newsString = "news";
  static const String realityString = "reality";
  static const String soapString = "soap";
  static const String talkString = "talk";
  static const String warAndPoliticsString = "war_and_politics";

  /// language screen
  static const String englishString = "english";
  static const String arabicString = "arabic";
  static const String chooseLangString = "choose_your_language";

  /// on board screen
  static const String onBoardString = "title1";
  static const String onBoard2String = "title2";
  static const String onBoardSubTitleString =
      "subtitle";
  static const String getStartedString = "get_started";
  static const String nextString = "next";
  static const String skipString = "skip";

  /// main screen
  static const String movieString = "movie";
  static const String tvString = "tv";

  /// season details screen
  static const String episodesString = "episodes";
  static const String seasonVideosString = "season_videos";

  /// episode details screen
  static const String episodeActorsString = "episode_actors";
  static const String episodeNumberString = "episode_number";
  static const String seasonNumberString = "season_number";

  /// success , failure messages
  static const String localDataBaseFailureString = "Error in database";
  static const String serverFailureString =
      "An error occurred , please try again later";

  /// empty messages
  static const String noRecommendationsFilmString = "no_recommendations_films";
  static const String noCreditsFilmString = "no_credits_films";
  static const String noReviewsFilmString = "no_reviews_films";
  static const String noSimilarFilmString = "no_similar_films";
  static const String noTrailersFilmString = "no_trailer_films";
  static const String noRecommendationsTvString = "no_recommendations_series";
  static const String noCreditsTvString = "no_credits_series";
  static const String noReviewsTvString = "no_reviews_series";
  static const String noSimilarTvString = "no_similar_series";
  static const String noSeasonsTvString = "no_seasons_series";
  static const String noOverviewString = "no_overview";
  static const String noActorMoviesString = "no_actor_movies";
  static const String noActorImagesString = "no_actor_images";
  static const String noActorSeriesString = "no_actor_series";
  static const String noComingSoonFilmsString = "no_coming_soon_films";
  static const String noPopularFilmsString = "no_popular_films";
  static const String noTopRatedFilmsString = "no_top_rated_films";
  static const String noNowPlayingFilmsString = "no_now_playing_films";
  static const String noMoviesByGenreString = "no_movies_by_genre";
  static const String noGenresString = "no_genres";
  static const String noResultsString = "no_results";
  static const String noRouteFoundString = "no_route_found";
}
