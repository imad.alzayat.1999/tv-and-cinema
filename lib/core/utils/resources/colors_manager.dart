import 'package:flutter/material.dart';

class ColorsManager {
  static const Color kWhite = Colors.white;
  static const Color kBlack = Colors.black;
  static const Color kTransparent = Colors.transparent;
  static const Color kEerieBlack = Color(0xff15141F);
  static const Color kDarkGunmetal = Color(0xff211F30);
  static const Color kVeryLightBlue = Color(0xff6964FF);
  static const Color kDimGray = Color(0xff6D5D6E);
  static const Color voteColor = Color(0xffF1C40F);
  static const Color kGreyX11 = Color(0xffBCBCBC);
}
