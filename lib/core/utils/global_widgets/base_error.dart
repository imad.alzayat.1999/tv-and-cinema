import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import 'base_icon.dart';

class BaseError extends StatelessWidget {
  final String errorMessage;
  final void Function()? onPressFunction;
  const BaseError({
    Key? key,
    required this.errorMessage,
    required this.onPressFunction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            errorMessage,
            style: getRegularTextStyle(
              fontSize: FontSizeManager.size16,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s10)),
          IconButton(
            onPressed: onPressFunction,
            icon: BaseIcon(
              iconHeight: SizesManager.s20,
              iconPath: IconsManager.refreshIcon,
            ),
          ),
        ],
      ),
    );
  }
}
