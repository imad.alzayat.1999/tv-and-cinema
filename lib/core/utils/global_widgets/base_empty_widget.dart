import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

class BaseEmptyWidget extends StatelessWidget {
  final String emptyMessage;
  const BaseEmptyWidget({Key? key, required this.emptyMessage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        FunctionsManager.translateText(text: emptyMessage, context: context),
        textAlign: TextAlign.center,
        style: getRegularTextStyle(
          fontSize: FontSizeManager.size16,
          letterSpacing: SizesManager.s0,
          context: context,
        ),
      ),
    );
  }
}
