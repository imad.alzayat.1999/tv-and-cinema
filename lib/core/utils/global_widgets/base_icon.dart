import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';

class BaseIcon extends StatelessWidget {
  final double iconHeight;
  final String iconPath;
  final Color iconColor;
  const BaseIcon({
    Key? key,
    required this.iconHeight,
    required this.iconPath,
    this.iconColor = ColorsManager.kWhite
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getHeight(inputHeight: iconHeight),
      child: SvgPicture.asset(iconPath, color: iconColor),
    );
  }
}
