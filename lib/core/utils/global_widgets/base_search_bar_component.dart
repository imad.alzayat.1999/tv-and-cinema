import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_icon.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';

import '../resources/values_mananger.dart';

class BaseSearchBarComponent extends StatelessWidget {
  final void Function()? onPressed;

  const BaseSearchBarComponent({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getHeight(inputHeight: SizesManager.s63),
      child: TextFormField(
        onTap: onPressed,
        readOnly: true,
        decoration: InputDecoration(
          filled: true,
          prefixIcon: Padding(
            padding: const EdgeInsets.all(PaddingManager.p12),
            child: BaseIcon(
              iconPath: IconsManager.searchIcon,
              iconHeight: SizesManager.s16,
              iconColor: ColorsManager.kWhite,
            ),
          ),
          fillColor: ColorsManager.kDarkGunmetal,
          contentPadding: EdgeInsets.zero,
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(SizesManager.s25),
          ),
        ),
      ),
    );
  }
}
