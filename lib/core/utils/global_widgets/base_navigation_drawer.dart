import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_icon.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../config/language_config/app_localizations.dart';
import '../../config/routes_config/routes_config.dart';

class BaseNavigationDrawer extends StatelessWidget {
  const BaseNavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: ColorsManager.kEerieBlack,
      child: ListView(
        children: [
          SizedBox(
            height: getHeight(inputHeight: SizesManager.s300),
            child: DrawerHeader(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(ImagesManager.cinemaImage)),
                ),
                child: Container()),
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, Routes.genresRoute);
            },
            child: ListTile(
              leading: SizedBox(
                width: getHeight(inputHeight: SizesManager.s30),
                height: getHeight(inputHeight: SizesManager.s30),
                child: SvgPicture.asset(
                  IconsManager.genreIcon,
                  color: ColorsManager.kWhite,
                ),
              ),
              title: Text(
                AppLocalizations.of(context)!
                    .translate(StringsManager.typeOfMoviesString)
                    .toString(),
                style: getRegularTextStyle(
                    fontSize: FontSizeManager.size14,
                    letterSpacing: SizesManager.s0,
                    context: context),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, Routes.tvSeriesRoute);
            },
            child: ListTile(
              leading: SizedBox(
                width: getHeight(inputHeight: SizesManager.s30),
                height: getHeight(inputHeight: SizesManager.s30),
                child: SvgPicture.asset(
                  IconsManager.tvIcon,
                  color: ColorsManager.kWhite,
                ),
              ),
              title: Text(
                AppLocalizations.of(context)!
                    .translate(StringsManager.typeOfTvString)
                    .toString(),
                style: getRegularTextStyle(
                    fontSize: FontSizeManager.size14,
                    letterSpacing: SizesManager.s0,
                    context: context),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamed(
                context,
                Routes.languageRoute,
                arguments: LanguageRouteParameters(
                    isEnglish: AppLocalizations.of(context)!.isEnLocale),
              );
            },
            child: ListTile(
              leading: SizedBox(
                width: getHeight(inputHeight: SizesManager.s30),
                height: getHeight(inputHeight: SizesManager.s30),
                child: SvgPicture.asset(
                  IconsManager.languageIcon,
                  color: ColorsManager.kWhite,
                ),
              ),
              title: Text(
                AppLocalizations.of(context)!
                    .translate(StringsManager.languageString)
                    .toString(),
                style: getRegularTextStyle(
                  fontSize: FontSizeManager.size14,
                  letterSpacing: SizesManager.s0,
                  context: context,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
