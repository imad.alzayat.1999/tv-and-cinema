import 'package:flutter/material.dart';

class BaseLoadingIndicator extends StatelessWidget {
  const BaseLoadingIndicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(child: CircularProgressIndicator());
  }
}
