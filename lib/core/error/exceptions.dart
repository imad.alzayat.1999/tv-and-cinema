import 'package:movie_app_clean_architecture/core/network/error_message_model.dart';

class ServerException implements Exception{
  final String message;

  ServerException({required this.message});
}

class LocalDatabaseException implements Exception{
  final String message;

  LocalDatabaseException({required this.message});

}