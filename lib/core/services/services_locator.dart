import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:movie_app_clean_architecture/core/api/api_interceptors.dart';
import 'package:movie_app_clean_architecture/core/api/dio_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/network_info.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/data/datasources/coming_soon_local_datasource.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/data/datasources/coming_soon_remote_datasource.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/data/repository/coming_soon_repository.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/domain/repository/base_coming_soon_repository.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/presentation/controllers/coming_soon_bloc.dart';
import 'package:movie_app_clean_architecture/movies/features/movies_by_genre/presentation/controller/movies_by_genre_bloc.dart';
import 'package:movie_app_clean_architecture/movies/features/search/data/datasources/search_remote_datasource.dart';
import 'package:movie_app_clean_architecture/movies/features/search/data/repository/search_repository.dart';
import 'package:movie_app_clean_architecture/movies/features/search/domain/repository/base_search_repository.dart';
import 'package:movie_app_clean_architecture/movies/features/search/domain/usecases/search_for_movies_usecase.dart';
import 'package:movie_app_clean_architecture/movies/features/search/presentation/controllers/search_bloc.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/domain/usecases/get_actor_images_usecase.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/domain/usecases/get_actor_series_usecase.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_movies/domain/usecases/get_movie_trailers_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/data/datasouces/airing_today_remote_datasource.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/data/repository/airing_today_repository.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/domain/repository/base_airing_today_repository.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/domain/usecases/get_airing_today_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/presentation/controller/airing_today_bloc.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/data/datasources/episode_details_remote_datasource.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/data/repository/episode_details_repository.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/repository/base_episode_details_repository.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/usecases/get_episode_details_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/presentation/controller/episode_details_bloc.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/data/datasource/search_series_remote_datasource.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/data/repository/search_series_repository.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/domain/usecase/search_for_series_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/presentation/controllers/search_series_bloc.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/data/datasources/season_details_remote_datasource.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/repository/base_season_details_repository.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/usecases/get_season_details_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/usecases/get_season_videos_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/presentation/controller/season_details_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../movies/features/movies_by_genre/data/datasouces/genre_movies_remote_datasource.dart';
import '../../movies/features/movies_by_genre/data/repository/movies_by_genre_repository.dart';
import '../../movies/features/movies_by_genre/domain/repository/base_movies_by_genre_repository.dart';
import '../../movies/features/movies_by_genre/domain/usecases/movies_by_genre_usecase.dart';
import '../../shared_features/actor/data/datasources/actor_details_local_datasource.dart';
import '../../shared_features/actor/data/datasources/actor_details_remote_datasource.dart';
import '../../shared_features/actor/data/repository/actor_repository.dart';
import '../../shared_features/actor/domain/repository/base_actor_repository.dart';
import '../../shared_features/actor/domain/usecases/get_actor_details_usecase.dart';
import '../../shared_features/actor/domain/usecases/get_actor_movies_usecase.dart';
import '../../shared_features/actor/presentation/controllers/actor_bloc.dart';
import '../../shared_features/details/details_movies/data/datasources/details_local_datasource.dart';
import '../../shared_features/details/details_movies/data/datasources/details_remote_datasource.dart';
import '../../shared_features/details/details_movies/data/repository/movie_details_repository.dart';
import '../../shared_features/details/details_movies/domain/repository/base_movie_details_repository.dart';
import '../../shared_features/details/details_movies/domain/usecases/get_movie_credits_usecase.dart';
import '../../shared_features/details/details_movies/domain/usecases/get_movie_details_usecase.dart';
import '../../shared_features/details/details_movies/domain/usecases/get_movie_reviews.dart';
import '../../shared_features/details/details_movies/domain/usecases/get_recommended_movies_usecase.dart';
import '../../shared_features/details/details_movies/domain/usecases/get_similar_movies_usecase.dart';
import '../../shared_features/details/details_movies/presentation/controllers/movie_details_bloc.dart';
import '../../shared_features/details/details_tv/data/datasources/tv_details_remote_datasource.dart';
import '../../shared_features/details/details_tv/data/repository/tv_details_repository.dart';
import '../../shared_features/details/details_tv/domain/repository/base_tv_details_repository.dart';
import '../../shared_features/details/details_tv/domain/usecases/get_credits_tv_usecase.dart';
import '../../shared_features/details/details_tv/domain/usecases/get_tv_details_usecase.dart';
import '../../shared_features/details/details_tv/domain/usecases/get_tv_recommended_usecase.dart';
import '../../shared_features/details/details_tv/domain/usecases/get_tv_reviews_usecase.dart';
import '../../shared_features/details/details_tv/domain/usecases/get_tv_similar_usecase.dart';
import '../../shared_features/details/details_tv/presentation/controller/tv_details_bloc.dart';
import '../../shared_features/genres/genre_tv/data/datasources/tv_genres_remote_datasource.dart';
import '../../shared_features/genres/genre_tv/data/repository/tv_genres_repository.dart';
import '../../shared_features/genres/genre_tv/domain/repository/base_tv_genres_repository.dart';
import '../../shared_features/genres/genre_tv/domain/usecases/get_tv_genres_usecase.dart';
import '../../shared_features/genres/genre_tv/presentation/controllers/bloc/tv_genres_bloc.dart';
import '../../shared_features/genres/genres_movies/data/datasources/genres_remote_datasource.dart';
import '../../shared_features/genres/genres_movies/data/repository/genres_repository.dart';
import '../../shared_features/genres/genres_movies/domain/repository/base_genres_repository.dart';
import '../../shared_features/genres/genres_movies/domain/usecases/get_movie_genres_usecase.dart';
import '../../shared_features/genres/genres_movies/presentation/controllers/genres_bloc.dart';
import '../../shared_features/home/home_movies/data/datasources/home_local_datasource.dart';
import '../../shared_features/home/home_movies/data/datasources/home_remote_datasource.dart';
import '../../shared_features/home/home_movies/data/repository/movie_repository.dart';
import '../../shared_features/home/home_movies/domain/repository/base_movie_repository.dart';
import '../../shared_features/home/home_movies/domain/usecases/get_coming_soon_usecase.dart';
import '../../shared_features/home/home_movies/domain/usecases/get_now_playing_usecase.dart';
import '../../shared_features/home/home_movies/domain/usecases/get_popular_movies_usecase.dart';
import '../../shared_features/home/home_movies/domain/usecases/get_top_rated_movies_usecase.dart';
import '../../shared_features/home/home_movies/presentation/controllers/movie_bloc.dart';
import '../../shared_features/home/home_tv/data/datasource/home_tv_remote_datasource.dart';
import '../../shared_features/home/home_tv/data/repository/home_tv_repository.dart';
import '../../shared_features/home/home_tv/domain/repository/base_home_tv_repository.dart';
import '../../shared_features/home/home_tv/domain/usecases/get_airing_today_tv_usecase.dart';
import '../../shared_features/home/home_tv/domain/usecases/get_on_the_air_tv_usecase.dart';
import '../../shared_features/home/home_tv/domain/usecases/get_popular_tv_usecase.dart';
import '../../shared_features/home/home_tv/domain/usecases/get_top_rated_tv_usecase.dart';
import '../../shared_features/home/home_tv/presentation/controller/tv_bloc.dart';
import '../../shared_features/language/data/datasources/language_local_datasource.dart';
import '../../shared_features/language/data/repository/language_repository.dart';
import '../../shared_features/language/domain/repository/base_language_repository.dart';
import '../../shared_features/language/domain/usecases/change_language_usecase.dart';
import '../../shared_features/language/domain/usecases/get_saved_language_usecase.dart';
import '../../shared_features/language/presentation/controller/language_cubit.dart';
import '../../shared_features/popular/popular_movies/data/datasource/popular_movies_local_datasource.dart';
import '../../shared_features/popular/popular_movies/data/datasource/popular_movies_remote_datasource.dart';
import '../../shared_features/popular/popular_movies/data/repository/popular_repository.dart';
import '../../shared_features/popular/popular_movies/domain/repository/base_popular_repository.dart';
import '../../shared_features/popular/popular_movies/domain/usecase/get_popular_movies_usecase.dart';
import '../../shared_features/popular/popular_movies/presentation/controllers/popular_bloc.dart';
import '../../shared_features/popular/popular_tv/data/datasources/popular_tv_remote_datasource.dart';
import '../../shared_features/popular/popular_tv/data/repository/popular_tv_repository.dart';
import '../../shared_features/popular/popular_tv/domain/repository/base_popular_tv_repository.dart';
import '../../shared_features/popular/popular_tv/domain/usecases/get_popular_tv_usecase.dart';
import '../../shared_features/popular/popular_tv/presentation/controller/tv_popular_bloc.dart';
import '../../shared_features/top_rated/top_rated_movies/data/datasources/top_rated_local_datasource.dart';
import '../../shared_features/top_rated/top_rated_movies/data/datasources/top_rated_remote_datasource.dart';
import '../../shared_features/top_rated/top_rated_movies/data/repository/top_rated_repository.dart';
import '../../shared_features/top_rated/top_rated_movies/domain/repository/base_top_rated_repository.dart';
import '../../shared_features/top_rated/top_rated_movies/domain/usecase/get_top_rated_usecase.dart';
import '../../shared_features/top_rated/top_rated_movies/presentation/controllers/top_rated_bloc.dart';
import '../../shared_features/top_rated/top_rated_tv/data/datasources/top_rated_tv_remote_datasource.dart';
import '../../shared_features/top_rated/top_rated_tv/data/repository/top_rated_tv_repository.dart';
import '../../shared_features/top_rated/top_rated_tv/domain/repository/base_top_rated_tv_repository.dart';
import '../../shared_features/top_rated/top_rated_tv/domain/usecases/get_top_rated_usecase.dart';
import '../../shared_features/top_rated/top_rated_tv/presentation/controllers/top_rated_tv_bloc.dart';
import '../../tv/features/search_series/domain/repository/base_search_series_repository.dart';
import '../../tv/features/season_details/data/repository/season_details_repository.dart';
import '../../tv/features/tv_series_by_genre/data/datasources/series_by_genre_remote_datasource.dart';
import '../../tv/features/tv_series_by_genre/data/repository/series_by_genre_repository.dart';
import '../../tv/features/tv_series_by_genre/domain/repository/base_series_by_genre_repository.dart';
import '../../tv/features/tv_series_by_genre/domain/usecases/get_genres_tv_usecase.dart';
import '../../tv/features/tv_series_by_genre/presentation/controller/genres_tv_bloc.dart';
import '../api/api_consumer.dart';
import '../../movies/features/coming_soon/domain/usecases/get_coming_soon_usecase.dart';

final getIt = GetIt.instance;

class ServicesLocator {
  Future<void> init() async {
    /// API
    getIt.registerLazySingleton(() => Dio());
    getIt.registerLazySingleton(() => ApiInterceptors());
    getIt.registerLazySingleton(() => LogInterceptor(
        request: true,
        requestBody: true,
        requestHeader: true,
        responseBody: true,
        responseHeader: true,
        error: true));
    getIt
        .registerLazySingleton<ApiConsumer>(() => DioConsumer(client: getIt()));

    /// Other
    getIt.registerLazySingleton<InternetConnectionChecker>(
        () => InternetConnectionChecker());
    getIt.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImplementation(getIt()),
    );

    /// local storage (Shared Preferences)
    final sharedPreferences = await SharedPreferences.getInstance();
    getIt.registerLazySingleton(() => sharedPreferences);

    /// Remote Data Sources
    getIt.registerLazySingleton<BaseHomeRemoteDataSource>(
        () => HomeRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseDetailsRemoteDataSource>(
        () => DetailsRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseTopRatedRemoteDataSource>(
        () => TopRatedRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BasePopularMoviesRemoteDataSource>(
        () => PopularMoviesRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseComingSoonRemoteDataSource>(
        () => ComingSoonRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseActorDetailsRemoteDataSource>(
        () => ActorDetailsRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseSearchRemoteDataSource>(
        () => SearchRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseGenresRemoteDataSources>(
        () => GenresRemoteDataSources(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseGenreMoviesRemoteDataSource>(
        () => GenreMoviesRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseTvHomeRemoteDataSource>(
        () => HomeTvRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseTvDetailsRemoteDataSource>(
        () => TvDetailsRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseTvGenresRemoteDataSource>(
        () => TvGenresRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseSeriesByGenreRemoteDataSource>(
        () => SeriesByGenreRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseSeasonDetailsRemoteDataSource>(
        () => SeasonDetailsRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BasePopularTvRemoteDataSource>(
        () => PopularTvRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseAiringTodayRemoteDataSource>(
        () => AiringTodayRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseTopRatedTvRemoteDataSource>(
        () => TopRatedTvRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseEpisodeDetailsRemoteDataSource>(
        () => EpisodeDetailsRemoteDataSource(apiConsumer: getIt()));
    getIt.registerLazySingleton<BaseSearchSeriesRemoteDataSource>(
        () => SearchSeriesRemoteDataSource(apiConsumer: getIt()));

    /// Local Data Sources
    getIt.registerLazySingleton<BaseHomeLocalDataSource>(
        () => HomeLocalDataSource());
    getIt.registerLazySingleton<BaseDetailsLocalDataSource>(
        () => DetailsLocalDataSource());
    getIt.registerLazySingleton<BasePopularMoviesLocalDataSource>(
        () => PopularMoviesLocalDataSource());
    getIt.registerLazySingleton<BaseComingSoonLocalDataSource>(
        () => ComingSoonLocalDataSource());
    getIt.registerLazySingleton<BaseTopRatedLocalDataSource>(
        () => TopRatedLocalDataSource());
    getIt.registerLazySingleton<BaseLanguageLocalDataSource>(
      () => LanguageLocalDataSource(
        sharedPreferences: getIt(),
      ),
    );
    getIt.registerLazySingleton<BaseActorDetailsLocalDataSource>(
      () => ActorDetailsLocalDataSource(),
    );

    ///Repositories
    getIt.registerLazySingleton<BaseMovieRepository>(
      () => MovieRepository(
        baseHomeRemoteDataSource: getIt(),
        baseHomeLocalDataSource: getIt(),
        networkInfo: getIt(),
      ),
    );
    getIt.registerLazySingleton<BaseMovieDetailsRepository>(
      () => MovieDetailsRepository(
        baseDetailsRemoteDataSource: getIt(),
        baseDetailsLocalDataSource: getIt(),
        networkInfo: getIt(),
      ),
    );
    getIt.registerLazySingleton<BaseTopRatedRepository>(
      () => TopRatedRepository(
        baseTopRatedRemoteDataSource: getIt(),
        baseTopRatedLocalDataSource: getIt(),
        networkInfo: getIt(),
      ),
    );
    getIt.registerLazySingleton<BasePopularRepository>(
      () => PopularRepository(
        basePopularMoviesRemoteDataSource: getIt(),
        networkInfo: getIt(),
        basePopularMoviesLocalDataSource: getIt(),
      ),
    );
    getIt.registerLazySingleton<BaseComingSoonRepository>(
      () => ComingSoonRepository(
        baseComingSoonRemoteDataSource: getIt(),
        networkInfo: getIt(),
        baseComingSoonLocalDataSource: getIt(),
      ),
    );
    getIt.registerLazySingleton<BaseActorRepository>(
      () => ActorRepository(
        baseActorDetailsRemoteDataSource: getIt(),
        baseActorDetailsLocalDataSource: getIt(),
        networkInfo: getIt(),
      ),
    );
    getIt.registerLazySingleton<BaseSearchRepository>(
        () => SearchRepository(baseSearchRemoteDataSource: getIt()));
    getIt.registerLazySingleton<BaseGenresRepository>(
        () => GenresRepository(baseGenresRemoteDataSources: getIt()));
    getIt.registerLazySingleton<BaseMoviesByGenreRepository>(() =>
        MoviesByGenreRepository(baseGenreMoviesRemoteDataSource: getIt()));
    getIt.registerLazySingleton<BaseLanguageRepository>(
        () => LanguageRepository(baseLanguageLocalDataSource: getIt()));
    getIt.registerLazySingleton<BaseTvHomeRepository>(
        () => HomeTvRepository(baseTvHomeRemoteDataSource: getIt()));
    getIt.registerLazySingleton<BaseTvDetailsRepository>(
        () => TvDetailsRepository(baseTvDetailsRemoteDataSource: getIt()));
    getIt.registerLazySingleton<BaseTvGenresRepository>(
        () => TvGenresRepository(baseTvGenresRemoteDataSource: getIt()));
    getIt.registerLazySingleton<BaseSeriesByGenreRepository>(() =>
        SeriesByGenreRepository(baseSeriesByGenreRemoteDataSource: getIt()));
    getIt.registerLazySingleton<BaseSeasonDetailsRepository>(() =>
        SeasonDetailsRepository(baseSeasonDetailsRemoteDataSource: getIt()));
    getIt.registerLazySingleton<BasePopularTvRepository>(
        () => PopularTvRepository(basePopularTvRemoteDataSource: getIt()));
    getIt.registerLazySingleton<BaseAiringTodayRepository>(
        () => AiringTodayRepository(baseAiringTodayRemoteDataSource: getIt()));
    getIt.registerLazySingleton<BaseTopRatedTvRepository>(
        () => TopRatedTvRepository(baseTopRatedTvRemoteDataSource: getIt()));
    getIt.registerLazySingleton<BaseEpisodeDetailsRepository>(() =>
        EpisodeDetailsRepository(baseEpisodeDetailsRemoteDataSource: getIt()));
    getIt.registerLazySingleton<BaseSearchSeriesRepository>(() =>
        SearchSeriesRepository(baseSearchSeriesRemoteDataSource: getIt()));

    ///Use Cases
    getIt.registerLazySingleton<GetTopPlayingUseCase>(
      () => GetTopPlayingUseCase(getIt()),
    );
    getIt.registerLazySingleton<GetPopularMoviesUseCase>(
      () => GetPopularMoviesUseCase(getIt()),
    );
    getIt.registerLazySingleton<GetTopRatedMoviesUseCase>(
      () => GetTopRatedMoviesUseCase(getIt()),
    );
    getIt.registerLazySingleton<GetComingSoonUseCase>(
      () => GetComingSoonUseCase(baseMovieRepository: getIt()),
    );
    getIt.registerLazySingleton<GetRecommendedMoviesUseCase>(
      () => GetRecommendedMoviesUseCase(baseMovieDetailsRepository: getIt()),
    );
    getIt.registerLazySingleton<GetMovieDetailsUseCase>(
      () => GetMovieDetailsUseCase(baseMovieDetailsRepository: getIt()),
    );
    getIt.registerLazySingleton<GetMovieReviews>(
      () => GetMovieReviews(baseMovieDetailsRepository: getIt()),
    );
    getIt.registerLazySingleton<GetMovieCreditsUseCase>(
      () => GetMovieCreditsUseCase(baseMovieDetailsRepository: getIt()),
    );
    getIt.registerLazySingleton<GetSimilarMoviesUseCase>(
      () => GetSimilarMoviesUseCase(baseMovieDetailsRepository: getIt()),
    );
    getIt.registerLazySingleton<GetMovieTrailersUseCase>(
      () => GetMovieTrailersUseCase(baseMovieDetailsRepository: getIt()),
    );
    getIt.registerLazySingleton<GetTopRatedUseCase>(
        () => GetTopRatedUseCase(baseTopRatedRepository: getIt()));
    getIt.registerLazySingleton<GetPopularUseCase>(
        () => GetPopularUseCase(basePopularRepository: getIt()));
    getIt.registerLazySingleton<GetComingSoonMoviesUseCase>(
        () => GetComingSoonMoviesUseCase(baseComingSoonRepository: getIt()));
    getIt.registerLazySingleton<GetActorDetailsUseCase>(
        () => GetActorDetailsUseCase(baseActorRepository: getIt()));
    getIt.registerLazySingleton<GetActorImagesUseCase>(
        () => GetActorImagesUseCase(baseActorRepository: getIt()));
    getIt.registerLazySingleton<GetActorMoviesUseCase>(
        () => GetActorMoviesUseCase(baseActorRepository: getIt()));
    getIt.registerLazySingleton<SearchForMoviesUseCase>(
        () => SearchForMoviesUseCase(baseSearchRepository: getIt()));
    getIt.registerLazySingleton<GetMovieGenresUseCase>(
        () => GetMovieGenresUseCase(baseGenresRepository: getIt()));
    getIt.registerLazySingleton<MoviesByGenreUseCase>(
        () => MoviesByGenreUseCase(baseMoviesByGenreRepository: getIt()));
    getIt.registerLazySingleton<ChangeLanguageUseCase>(
        () => ChangeLanguageUseCase(baseLanguageRepository: getIt()));
    getIt.registerLazySingleton<GetSavedLanguageUseCase>(
        () => GetSavedLanguageUseCase(baseLanguageRepository: getIt()));
    getIt.registerLazySingleton<GetOnTheAirTvUseCase>(
        () => GetOnTheAirTvUseCase(baseTvHomeRepository: getIt()));
    getIt.registerLazySingleton<GetPopularTvUseCase>(
        () => GetPopularTvUseCase(baseTvHomeRepository: getIt()));
    getIt.registerLazySingleton<GetAiringTodayTvUseCase>(
        () => GetAiringTodayTvUseCase(baseTvHomeRepository: getIt()));
    getIt.registerLazySingleton<GetTopRatedTvUseCase>(
        () => GetTopRatedTvUseCase(baseTvHomeRepository: getIt()));
    getIt.registerLazySingleton<GetTvDetailsUseCase>(
        () => GetTvDetailsUseCase(baseTvDetailsRepository: getIt()));
    getIt.registerLazySingleton<GetTvRecommendedUseCase>(
        () => GetTvRecommendedUseCase(baseTvDetailsRepository: getIt()));
    getIt.registerLazySingleton<GetTvSimilarUseCase>(
        () => GetTvSimilarUseCase(baseTvDetailsRepository: getIt()));
    getIt.registerLazySingleton<GetCreditsTvUseCase>(
        () => GetCreditsTvUseCase(baseTvDetailsRepository: getIt()));
    getIt.registerLazySingleton<GetTvReviewsUseCase>(
        () => GetTvReviewsUseCase(baseTvDetailsRepository: getIt()));
    getIt.registerLazySingleton<GetTvGenresUseCase>(
        () => GetTvGenresUseCase(baseTvGenresRepository: getIt()));
    getIt.registerLazySingleton<GetGenresTvUseCase>(
        () => GetGenresTvUseCase(baseSeriesByGenreRepository: getIt()));
    getIt.registerLazySingleton<GetSeasonDetailsUseCase>(
        () => GetSeasonDetailsUseCase(baseSeasonDetailsRepository: getIt()));
    getIt.registerLazySingleton<GetTvPopularUseCase>(
        () => GetTvPopularUseCase(basePopularTvRepository: getIt()));
    getIt.registerLazySingleton<GetAiringTodayUseCase>(
        () => GetAiringTodayUseCase(baseAiringTodayRepository: getIt()));
    getIt.registerLazySingleton<GetTvTopRatedUseCase>(
        () => GetTvTopRatedUseCase(baseTopRatedTvRepository: getIt()));
    getIt.registerLazySingleton<GetSeasonVideosUseCase>(
        () => GetSeasonVideosUseCase(baseSeasonDetailsRepository: getIt()));
    getIt.registerLazySingleton<GetEpisodeDetailsUseCase>(
        () => GetEpisodeDetailsUseCase(baseEpisodeDetailsRepository: getIt()));
    getIt.registerLazySingleton<GetActorSeriesUseCase>(
        () => GetActorSeriesUseCase(baseActorRepository: getIt()));
    getIt.registerLazySingleton<SearchSeriesUseCase>(
        () => SearchSeriesUseCase(baseSearchSeriesRepository: getIt()));

    ///Bloc
    getIt.registerFactory(
      () => MovieBloc(
        getTopPlayingUseCase: getIt(),
        getPopularMoviesUseCase: getIt(),
        getTopRatedMoviesUseCase: getIt(),
        getComingSoonUseCase: getIt(),
      ),
    );

    getIt.registerFactory(
      () => MovieDetailsBloc(
        getRecommendedMoviesUseCase: getIt(),
        getMovieDetailsUseCase: getIt(),
        getMovieReviews: getIt(),
        getMovieCreditsUseCase: getIt(),
        getSimilarMoviesUseCase: getIt(),
        getMovieTrailersUseCase: getIt(),
      ),
    );
    getIt.registerFactory(() => TopRatedBloc(getTopRatedUseCase: getIt()));
    getIt.registerFactory(() => PopularBloc(getPopularMoviesUseCase: getIt()));
    getIt.registerFactory(() => ComingSoonBloc(getComingSoonUseCase: getIt()));
    getIt.registerFactory(
      () => ActorBloc(
          getActorDetailsUseCase: getIt(),
          getActorImagesUseCase: getIt(),
          getActorMoviesUseCase: getIt(),
          getActorSeriesUseCase: getIt()),
    );
    getIt.registerFactory(() => SearchBloc(searchForMoviesUseCase: getIt()));
    getIt.registerFactory(() => GenresBloc(getMovieGenresUseCase: getIt()));
    getIt.registerFactory(
        () => MoviesByGenreBloc(moviesByGenreUseCase: getIt()));
    getIt.registerFactory<LanguageCubit>(
      () => LanguageCubit(
        getSavedLanguageUseCase: getIt(),
        changeLanguageUseCase: getIt(),
      ),
    );
    getIt.registerFactory(
      () => TvBloc(
        getOnTheAirTvUseCase: getIt(),
        getPopularTvUseCase: getIt(),
        getAiringTodayTvUseCase: getIt(),
        getTopRatedTvUseCase: getIt(),
      ),
    );
    getIt.registerFactory(
      () => TvDetailsBloc(
        getTvDetailsUseCase: getIt(),
        getTvRecommendedUseCase: getIt(),
        getTvSimilarUseCase: getIt(),
        getCreditsTvUseCase: getIt(),
        getTvReviewsUseCase: getIt(),
      ),
    );
    getIt.registerFactory(() => TvGenresBloc(getTvGenresUseCase: getIt()));
    getIt.registerFactory(() => GenresTvBloc(getGenresTvUseCase: getIt()));
    getIt.registerFactory(
      () => SeasonDetailsBloc(
        getSeasonDetailsUseCase: getIt(),
        getSeasonVideosUseCase: getIt(),
      ),
    );
    getIt.registerFactory(() => TvPopularBloc(getTvPopularUseCase: getIt()));
    getIt
        .registerFactory(() => AiringTodayBloc(getAiringTodayUseCase: getIt()));
    getIt.registerFactory(() => TopRatedTvBloc(getTopRatedTvUseCase: getIt()));
    getIt.registerFactory(
        () => EpisodeDetailsBloc(getEpisodeDetailsUseCase: getIt()));
    getIt.registerFactory(() => SearchSeriesBloc(searchSeriesUseCase: getIt()));
  }
}
