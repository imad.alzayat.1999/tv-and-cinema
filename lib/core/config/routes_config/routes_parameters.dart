import 'package:equatable/equatable.dart';

class DetailsRouteParameters extends Equatable {
  final int id;

  DetailsRouteParameters({required this.id});

  @override
  // TODO: implement props
  List<Object?> get props => [id];
}

class SeasonDetailsRouteParameters extends Equatable {
  final int tvId;
  final int seasonId;

  SeasonDetailsRouteParameters({required this.tvId, required this.seasonId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId, seasonId];
}

class LanguageRouteParameters extends Equatable {
  final bool isEnglish;

  LanguageRouteParameters({required this.isEnglish});

  @override
  // TODO: implement props
  List<Object?> get props => [isEnglish];
}

class ByGenreRouteParameters extends Equatable {
  final int id;
  final String name;

  ByGenreRouteParameters({required this.id, required this.name});

  @override
  // TODO: implement props
  List<Object?> get props => [id, name];
}

class EpisodeDetailsRouteParameters extends Equatable {
  final int tvId;
  final int seasonNumber;
  final int episodeNumber;

  EpisodeDetailsRouteParameters({
    required this.tvId,
    required this.seasonNumber,
    required this.episodeNumber,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [tvId , seasonNumber , episodeNumber];
}
