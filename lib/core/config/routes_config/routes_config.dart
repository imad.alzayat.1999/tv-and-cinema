import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import 'package:movie_app_clean_architecture/movies/features/coming_soon/presentation/screens/coming_soon_screen.dart';
import 'package:movie_app_clean_architecture/movies/features/search/presentation/screens/search_screen.dart';
import 'package:movie_app_clean_architecture/splash_screen.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/presentation/screens/airing_today_screen.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/presentation/screens/episode_details_screen.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/presentation/screens/search_series_screen.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/presentation/screens/season_details_screen.dart';
import '../../../main_screen.dart';
import '../../../movies/features/movies_by_genre/presentation/screens/movie_by_genre_screen.dart';
import '../../../shared_features/actor/presentation/screens/actor_details_screen.dart';
import '../../../shared_features/details/details_movies/presentation/screens/movie_detail_screen.dart';
import '../../../shared_features/details/details_tv/presentation/screen/tv_details_screen.dart';
import '../../../shared_features/genres/genre_tv/presentation/controllers/routing_parameters/tv_genre_routing_parameters.dart';
import '../../../shared_features/genres/genre_tv/presentation/screens/tv_genres_screen.dart';
import '../../../shared_features/genres/genres_movies/presentation/screens/genres_screen.dart';
import '../../../shared_features/home/home_movies/presentation/screens/movies_screen.dart';
import '../../../shared_features/home/home_tv/presentation/screens/tv_screen.dart';
import '../../../shared_features/language/presentation/screens/language_screen.dart';
import '../../../shared_features/on_board/presentation/screens/on_board_screen.dart';
import '../../../shared_features/popular/popular_movies/presentation/screens/popular_movies_screen.dart';
import '../../../shared_features/popular/popular_tv/presentation/screens/popular_tv_screen.dart';
import '../../../shared_features/top_rated/top_rated_movies/presentation/screens/top_rated_screen.dart';
import '../../../shared_features/top_rated/top_rated_tv/presentation/screens/top_rated_tv_screen.dart';
import '../../../tv/features/tv_series_by_genre/presentation/screens/genres_tv_screen.dart';

class Routes {
  static const String initRoute = "/";
  static const String onBoardingRoute = "/on_board";
  static const String mainRoute = "/main";
  static const String tvRoute = "/tv";
  static const String moviesRoute = "/movies";
  static const String tvDetailsRoute = "/tv/details";
  static const String genresTvRoute = "/tv/genres";
  static const String searchMoviesRoute = "/search/movies";
  static const String seasonDetailsRoute = "/tv/season/details";
  static const String movieDetailsRoute = "/movie/details";
  static const String genresRoute = "/movie/genres";
  static const String tvSeriesRoute = "/tv/series/genres";
  static const String languageRoute = "/language";
  static const String actorDetailsRoute = "/actor/details";
  static const String movieByGenreRoute = "/movie/by_genre";
  static const String popularRoute = "/movie/popular";
  static const String topRatedRoute = "/movie/top_rated";
  static const String comingSoonRoute = "/movie/coming_soon";
  static const String tvPopularRoute = "/tv/popular";
  static const String airingTodayRoute = "/tv/airing_today";
  static const String topRatedTvRoute = "/tv/top_rated";
  static const String episodeDetailsRoute = "/tv/season/episode/details";
  static const String searchSeriesRoute = "/search/series";
}

class AppRoute {
  static Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.initRoute:
        return MaterialPageRoute(builder: (context) => SplashScreen());
      case Routes.onBoardingRoute:
        return MaterialPageRoute(builder: (context) => OnBoardScreen());
      case Routes.mainRoute:
        return MaterialPageRoute(builder: (context) => MainScreen());
      case Routes.tvRoute:
        return MaterialPageRoute(builder: (context) => TvScreen());
      case Routes.moviesRoute:
        return MaterialPageRoute(builder: (context) => MainMoviesScreen());
      case Routes.tvDetailsRoute:
        final args = settings.arguments as DetailsRouteParameters;
        return MaterialPageRoute(
            builder: (context) => TvDetailsScreen(tvId: args.id));
      case Routes.genresTvRoute:
        final args = settings.arguments as SeriesByGenreRoutingParameter;
        return MaterialPageRoute(
          builder: (context) => GenresTvScreen(
            tvGenre: args.genre,
            genreId: args.genreId,
          ),
        );
      case Routes.searchMoviesRoute:
        return MaterialPageRoute(builder: (context) => SearchScreen());
      case Routes.seasonDetailsRoute:
        final args = settings.arguments as SeasonDetailsRouteParameters;
        return MaterialPageRoute(
          builder: (context) => SeasonDetailsScreen(
            tvId: args.tvId,
            seasonNumber: args.seasonId,
          ),
        );
      case Routes.movieDetailsRoute:
        final args = settings.arguments as DetailsRouteParameters;
        return MaterialPageRoute(
            builder: (context) => MovieDetailScreen(id: args.id));
      case Routes.genresRoute:
        return MaterialPageRoute(builder: (context) => GenresScreen());
      case Routes.tvSeriesRoute:
        return MaterialPageRoute(builder: (context) => TvGenresScreen());
      case Routes.languageRoute:
        final args = settings.arguments as LanguageRouteParameters;
        return MaterialPageRoute(
            builder: (context) => LanguageScreen(isEnLocale: args.isEnglish));
      case Routes.languageRoute:
        final args = settings.arguments as DetailsRouteParameters;
        return MaterialPageRoute(
            builder: (context) => ActorDetailsScreen(actorId: args.id));
      case Routes.movieByGenreRoute:
        final args = settings.arguments as ByGenreRouteParameters;
        return MaterialPageRoute(
          builder: (context) => MovieByGenreScreen(
            movieGenre: args.name,
            genreId: args.id.toString(),
          ),
        );
      case Routes.popularRoute:
        return MaterialPageRoute(builder: (context) => PopularMoviesScreen());
      case Routes.topRatedRoute:
        return MaterialPageRoute(builder: (context) => TopRatedScreen());
      case Routes.comingSoonRoute:
        return MaterialPageRoute(builder: (context) => ComingSoonScreen());
      case Routes.actorDetailsRoute:
        final args = settings.arguments as DetailsRouteParameters;
        return MaterialPageRoute(
            builder: (context) => ActorDetailsScreen(actorId: args.id));
      case Routes.tvPopularRoute:
        return MaterialPageRoute(
            builder: (_) => Scaffold(body: PopularTvScreen()));
      case Routes.airingTodayRoute:
        return MaterialPageRoute(
            builder: (_) => Scaffold(body: AiringTodayScreen()));
      case Routes.topRatedTvRoute:
        return MaterialPageRoute(builder: (_) => TopRatedTvScreen());
      case Routes.episodeDetailsRoute:
        final args = settings.arguments as EpisodeDetailsRouteParameters;
        return MaterialPageRoute(
          builder: (_) => EpisodeDetailsScreen(
            episodeDetailsRouteParameters: args,
          ),
        );
      case Routes.searchSeriesRoute:
        return MaterialPageRoute(builder: (_) => SearchSeriesScreen());
      default:
        return undefinedRoute();
    }
  }

  static Route<dynamic> undefinedRoute() {
    return MaterialPageRoute(
        builder: ((context) => Scaffold(
              body: Center(
                child: Text(
                  FunctionsManager.translateText(text: StringsManager.noRouteFoundString, context: context),
                  style: getRegularTextStyle(
                      fontSize: FontSizeManager.size16,
                      letterSpacing: SizesManager.s0,
                      context: context),
                ),
              ),
            )));
  }
}
