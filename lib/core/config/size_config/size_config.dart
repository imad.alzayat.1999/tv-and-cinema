import 'package:flutter_screenutil/flutter_screenutil.dart';

double getHeight({required double inputHeight}) {
  return inputHeight.h;
}

double getWidth({required double inputWidth}) {
  return inputWidth.w;
}