class ApiConstance {
  static const String baseUrl = "https://api.themoviedb.org/3";
  static const String apiKey = "0cc45c303f03d65a979b070499939285";

  static const String getNowPlayingEndPoint =
      "$baseUrl/movie/now_playing?api_key=$apiKey";
  static const String getPopularMoviesEndPoint =
      "$baseUrl/movie/popular?api_key=$apiKey";
  static const String getTopRatedMoviesEndPoint =
      "$baseUrl/movie/top_rated?api_key=$apiKey";
  static const String getComingSoonMoviesEndPoint =
      "$baseUrl/movie/upcoming?api_key=$apiKey";

  static String getMovieDetailsEndPoint(int movieId) =>
      "$baseUrl/movie/$movieId?api_key=$apiKey";

  static String getMovieReviewsEndPoint(int movieId) =>
      "$baseUrl/movie/$movieId/reviews?api_key=$apiKey";

  static String getMovieCreditsEndPoint(int movieId) =>
      "$baseUrl/movie/$movieId/credits?api_key=$apiKey";

  static String getSimilarMoviesEndPoint(int movieId) =>
      "$baseUrl/movie/$movieId/similar?api_key=$apiKey";

  static String getMoviesTrailersEndPoint(int movieId) =>
      "$baseUrl/movie/$movieId/videos?api_key=$apiKey";

  static String getRecommendedMoviesEndPoint(int movieId) =>
      "$baseUrl/movie/$movieId/recommendations?api_key=$apiKey";

  static String getActorInformationEndPoint(int actorId) =>
      "$baseUrl/person/$actorId?api_key=$apiKey";

  static String getActorImagesEndPoint(int actorId) =>
      "$baseUrl/person/$actorId/images?api_key=$apiKey";

  static String getActorMoviesEndPoint(int actorId) =>
      "$baseUrl/person/$actorId/movie_credits?api_key=$apiKey";

  static String searchForMovieEndPoint = "$baseUrl/search/movie";

  static String getMovieGenresEndPoint =
      "$baseUrl/genre/movie/list?api_key=$apiKey";

  static String discoverMoviesEndPoint =
      "$baseUrl/discover/movie?api_key=$apiKey";

  static String getOnTheAirTvEndPoint =
      "$baseUrl/tv/on_the_air?api_key=$apiKey";

  static String geTvPopularEndPoint = "$baseUrl/tv/popular?api_key=$apiKey";

  static String getAiringTodayTvEndPoint =
      "$baseUrl/tv/airing_today?api_key=$apiKey";

  static String getTopRatedTvEndPoint = "$baseUrl/tv/top_rated?api_key=$apiKey";

  static String getTvByIdEndPoint(int tvId) =>
      "$baseUrl/tv/$tvId?api_key=$apiKey";

  static String getTvRecommendedByIdEndPoint(int tvId) =>
      "$baseUrl/tv/$tvId/recommendations?api_key=$apiKey";

  static String getTvSimilarByIdEndPoint(int tvId) =>
      "$baseUrl/tv/$tvId/similar?api_key=$apiKey";

  static String getTvCreditsByIdEndPoint(int tvId) =>
      "$baseUrl/tv/$tvId/credits?api_key=$apiKey";

  static String getTvReviewByIdEndPoint(int tvId) =>
      "$baseUrl/tv/$tvId/reviews?api_key=$apiKey";

  static String getTvGenresEndPoint = "$baseUrl/genre/tv/list?api_key=$apiKey";

  static String getTvSeriesByGenreEndPoint =
      "$baseUrl/discover/tv?api_key=$apiKey";

  static String getSeasonDetailsEndPoint({
    required int tvId,
    required int seasonId,
  }) =>
      "$baseUrl/tv/$tvId/season/$seasonId?api_key=$apiKey";

  static String getSeasonVideosEndPoint({
    required int tvId,
    required int seasonId,
  }) =>
      "$baseUrl/tv/$tvId/season/$seasonId/videos?api_key=$apiKey";

  static String popularTvEndPoint = "$baseUrl/tv/popular?api_key=$apiKey";

  static String airingTodayTvEndPoint =
      "$baseUrl/tv/airing_today?api_key=$apiKey";

  static String topRatedTvEndPoint = "$baseUrl/tv/top_rated?api_key=$apiKey";

  static String getEpisodeDetailsEndPoint({
    required int tvId,
    required int seasonId,
    required int episodeId,
  }) =>
      "$baseUrl/tv/$tvId/season/$seasonId/episode/$episodeId?api_key=$apiKey";

  static String getActorSeriesEndPoint({required int actorId}) =>
      "$baseUrl/person/$actorId/tv_credits?api_key=$apiKey";
  static String getSeriesResultEndPoint = "$baseUrl/search/tv?api_key=$apiKey";
}
