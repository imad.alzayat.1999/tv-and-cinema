import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/domain/entities/tv_result.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/domain/usecase/search_for_series_usecase.dart';

abstract class BaseSearchSeriesRepository {
  Future<Either<Failure, List<TvSeriesResult>>> searchForTvSeries({
    required SearchSeriesParameters searchSeriesParameters,
  });
}
