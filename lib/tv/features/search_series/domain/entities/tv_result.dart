import 'package:equatable/equatable.dart';

class TvSeriesResult extends Equatable {
  final String? backdropPath;
  final String? name;
  final String? overview;
  final DateTime? firstAirDate;
  final num? voteAvg;
  final int id;

  TvSeriesResult({
    required this.backdropPath,
    required this.name,
    required this.overview,
    required this.firstAirDate,
    required this.voteAvg,
    required this.id,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [id , voteAvg , firstAirDate , overview , name , backdropPath];
}
