import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/domain/entities/tv_result.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/domain/repository/base_search_series_repository.dart';


class SearchSeriesUseCase extends BaseUseCase<List<TvSeriesResult> , SearchSeriesParameters>{
  final BaseSearchSeriesRepository baseSearchSeriesRepository;

  SearchSeriesUseCase({required this.baseSearchSeriesRepository});

  @override
  Future<Either<Failure, List<TvSeriesResult>>> call(SearchSeriesParameters parameters) async{
    return await baseSearchSeriesRepository.searchForTvSeries(searchSeriesParameters: parameters);
  }
}

class SearchSeriesParameters extends Equatable {
  final String query;
  final int page;
  final String language;

  SearchSeriesParameters({
    required this.query,
    required this.page,
    required this.language,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [page , query , language];
}
