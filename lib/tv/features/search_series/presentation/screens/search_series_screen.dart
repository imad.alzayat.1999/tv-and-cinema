import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/presentation/components/series_result_component.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/presentation/controllers/search_series_events.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/presentation/controllers/search_series_states.dart';

import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_empty_widget.dart';
import '../../../../../core/utils/global_widgets/base_error.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../../../../../movies/features/search/presentation/components/search_text_field.dart';
import '../controllers/search_series_bloc.dart';

class SearchSeriesScreen extends StatelessWidget {
  const SearchSeriesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<SearchSeriesBloc>(),
      child: Scaffold(
        backgroundColor: ColorsManager.kEerieBlack,
        appBar: AppBar(
          elevation: SizesManager.s0,
          backgroundColor: ColorsManager.kEerieBlack,
          title: Text(
            FunctionsManager.translateText(
                text: StringsManager.searchString, context: context),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size16,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
        ),
        body: SearchSeriesContent(),
      ),
    );
  }
}

class SearchSeriesContent extends StatefulWidget {
  const SearchSeriesContent({Key? key}) : super(key: key);

  @override
  State<SearchSeriesContent> createState() => _SearchSeriesContentState();
}

class _SearchSeriesContentState extends State<SearchSeriesContent> {
  var _searchController = TextEditingController();
  var _searchScrollController = ScrollController();
  var _mainController = ScrollController();
  int page = 1;
  bool _isSearched = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _searchScrollController.addListener(_searchListener);
  }

  _searchListener() {
    if(_searchScrollController.position.maxScrollExtent ==
        _searchScrollController.position.pixels){
      page = page + 1;
      BlocProvider.of<SearchSeriesBloc>(context).add(GetTvSeriesEvent(
          query: _searchController.text,
          page: page,
          language: FunctionsManager.getLangCode(context)));
    }
  }

  /// widget to search here
  Widget _hintToSearch() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BaseIcon(
            iconHeight: SizesManager.s125,
            iconPath: IconsManager.searchIcon,
          ),
          Text(
            FunctionsManager.translateText(
                text: StringsManager.searchHerePleaseString, context: context),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size20,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
        ],
      ),
    );
  }

  /// widget to get the result
  Widget _getTheResult() {
    return BlocBuilder<SearchSeriesBloc, SearchSeriesStates>(
      builder: (context, state) {
        switch (state.searchSeriesRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return FadeInUp(
              from: SizesManager.s20,
              duration: const Duration(
                milliseconds: ConstsManager.fadeAnimationDuration,
              ),
              child: state.tvSeries.isEmpty
                  ? BaseEmptyWidget(
                      emptyMessage: StringsManager.noResultsString)
                  : ListView.separated(
                      padding: EdgeInsets.symmetric(
                        vertical: getHeight(inputHeight: PaddingManager.p16),
                        horizontal: getWidth(inputWidth: PaddingManager.p8),
                      ),
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        if (index >= state.tvSeries.length) {
                          return BaseLoadingIndicator();
                        }
                        return SeriesResultComponent(
                            tvSeriesResult: state.tvSeries[index]);
                      },
                      separatorBuilder: (context, index) => SizedBox(
                          height: getHeight(inputHeight: SizesManager.s20)),
                      itemCount: context.read<SearchSeriesBloc>().isLoading
                          ? state.tvSeries.length
                          : state.tvSeries.length + 1,
                    ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorSearchSeriesMessage,
                onPressFunction: () {
                  BlocProvider.of<SearchSeriesBloc>(context).add(
                      GetTvSeriesEvent(
                          query: _searchController.text,
                          page: page,
                          language: FunctionsManager.getLangCode(context)));
                });
        }
      },
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _searchScrollController
      ..removeListener(_searchListener)
      ..dispose();
  }

  @override
  Widget build(BuildContext context) {
    page = 1;
    return SingleChildScrollView(
      controller: _isSearched ? _searchScrollController : _mainController,
      child: Column(
        children: [
          SearchTextField(
            textEditingController: _searchController,
            onChange: (value) {
              if (value.isEmpty) {
                setState(() {
                  _isSearched = false;
                });
              } else {
                setState(() {
                  _isSearched = true;
                  BlocProvider.of<SearchSeriesBloc>(context)
                      .state
                      .tvSeries
                      .clear();
                  BlocProvider.of<SearchSeriesBloc>(context)
                    ..add(
                      GetTvSeriesEvent(
                          query: value,
                          page: 1,
                          language: FunctionsManager.getLangCode(context)),
                    );
                });
              }
            },
          ),
          _isSearched ? _getTheResult() : _hintToSearch(),
        ],
      ),
    );
  }
}
