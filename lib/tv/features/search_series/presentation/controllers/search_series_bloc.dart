import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/presentation/controllers/search_series_events.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/presentation/controllers/search_series_states.dart';

import '../../../../../core/utils/request_states.dart';
import '../../domain/usecase/search_for_series_usecase.dart';

class SearchSeriesBloc extends Bloc<SearchSeriesEvents , SearchSeriesStates>{
  final SearchSeriesUseCase searchSeriesUseCase;
  bool isLoading = false;
  SearchSeriesBloc({required this.searchSeriesUseCase}) : super(SearchSeriesStates()){
    on<GetTvSeriesEvent>((event, emit) async {
      if (state.hasReachedMax) return;
      try {
        if (state.searchSeriesRequestStates == RequestStates.loading) {
          final response = await searchSeriesUseCase(
            SearchSeriesParameters(page: event.page , query: event.query , language: event.language),
          );
          response.fold((l) {
            emit(
              state.copyWith(
                searchSeriesRequestStates: RequestStates.error,
                errorSearchSeriesMessage: l.message,
              ),
            );
          }, (r) {
            r.isEmpty
                ? emit(state.copyWith(hasReachedMax: true))
                : emit(
              state.copyWith(
                searchSeriesRequestStates: RequestStates.success,
                tvSeries: r,
                hasReachedMax: false,
              ),
            );
          });
        } else {
          final response = await searchSeriesUseCase(
            SearchSeriesParameters(page: event.page , query: event.query , language: event.language),
          );
          print("length is ${state.tvSeries.length}");
          response.fold((l) {
            emit(
              state.copyWith(
                searchSeriesRequestStates: RequestStates.error,
                errorSearchSeriesMessage: l.message,
              ),
            );
          }, (r) {
            r.isEmpty
                ? emit(state.copyWith(hasReachedMax: true))
                : emit(
              state.copyWith(
                searchSeriesRequestStates: RequestStates.success,
                tvSeries: [...state.tvSeries, ...r],
                hasReachedMax: false,
              ),
            );
          });
        }
      } catch (e) {
        emit(
          state.copyWith(
            searchSeriesRequestStates: RequestStates.error,
            errorSearchSeriesMessage: e.toString(),
          ),
        );
      }
    } , transformer: droppable());
  }
}