import 'package:equatable/equatable.dart';

abstract class SearchSeriesEvents extends Equatable {}

class GetTvSeriesEvent extends SearchSeriesEvents {
  final String query;
  final String language;
  final int page;

  GetTvSeriesEvent({
    required this.query,
    required this.language,
    required this.page,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [query , language , page];
}
