import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';

import '../../domain/entities/tv_result.dart';

class SearchSeriesStates extends Equatable {
  final String errorSearchSeriesMessage;
  final RequestStates searchSeriesRequestStates;
  final bool hasReachedMax;
  final List<TvSeriesResult> tvSeries;

  SearchSeriesStates({
    this.errorSearchSeriesMessage = '',
    this.searchSeriesRequestStates = RequestStates.loading,
    this.hasReachedMax = false,
    List<TvSeriesResult>? tvSeries,
  }) : tvSeries = tvSeries ?? [];

  SearchSeriesStates copyWith({
    String? errorSearchSeriesMessage,
    RequestStates? searchSeriesRequestStates,
    bool? hasReachedMax,
    List<TvSeriesResult>? tvSeries,
  }) {
    return SearchSeriesStates(
        errorSearchSeriesMessage:
            errorSearchSeriesMessage ?? this.errorSearchSeriesMessage,
        searchSeriesRequestStates:
            searchSeriesRequestStates ?? this.searchSeriesRequestStates,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        tvSeries: tvSeries ?? this.tvSeries);
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        hasReachedMax,
        errorSearchSeriesMessage,
        searchSeriesRequestStates,
        tvSeries,
      ];
}
