import 'package:movie_app_clean_architecture/tv/features/search_series/domain/entities/tv_result.dart';

class TvSeriesResultModel extends TvSeriesResult {
  TvSeriesResultModel({
    required String? backdropPath,
    required String? name,
    required String? overview,
    required DateTime? firstAirDate,
    required num? voteAvg,
    required int id,
  }) : super(
          backdropPath: backdropPath,
          name: name,
          overview: overview,
          firstAirDate: firstAirDate,
          voteAvg: voteAvg,
          id: id,
        );

  factory TvSeriesResultModel.fromJson(Map<String, dynamic> json) =>
      TvSeriesResultModel(
        backdropPath: json["backdrop_path"],
        name: json["name"],
        overview: json["overview"],
        firstAirDate: json["first_air_date"] == ""
            ? DateTime.now()
            : DateTime.parse(json["first_air_date"]),
        voteAvg: json["vote_average"],
        id: json["id"],
      );
}
