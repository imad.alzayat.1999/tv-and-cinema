import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/data/datasource/search_series_remote_datasource.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/domain/entities/tv_result.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/domain/repository/base_search_series_repository.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/domain/usecase/search_for_series_usecase.dart';

class SearchSeriesRepository extends BaseSearchSeriesRepository{
  final BaseSearchSeriesRemoteDataSource baseSearchSeriesRemoteDataSource;

  SearchSeriesRepository({required this.baseSearchSeriesRemoteDataSource});
  @override
  Future<Either<Failure, List<TvSeriesResult>>> searchForTvSeries({required SearchSeriesParameters searchSeriesParameters}) async{
    try{
      final result = await baseSearchSeriesRemoteDataSource.searchTvSeries(searchSeriesParameters: searchSeriesParameters);
      return Right(result);
    }on ServerException catch(e){
      return Left(ServerFailure(e.message));
    }
  }
}