import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/data/models/tv_series_result_model.dart';
import 'package:movie_app_clean_architecture/tv/features/search_series/domain/usecase/search_for_series_usecase.dart';

abstract class BaseSearchSeriesRemoteDataSource {
  Future<List<TvSeriesResultModel>> searchTvSeries(
      {required SearchSeriesParameters searchSeriesParameters});
}

class SearchSeriesRemoteDataSource extends BaseSearchSeriesRemoteDataSource {
  final ApiConsumer apiConsumer;

  SearchSeriesRemoteDataSource({required this.apiConsumer});

  @override
  Future<List<TvSeriesResultModel>> searchTvSeries(
      {required SearchSeriesParameters searchSeriesParameters}) async {
    final response = await apiConsumer
        .get(path: ApiConstance.getSeriesResultEndPoint, queryParameters: {
      "page": searchSeriesParameters.page,
      "language": searchSeriesParameters.language,
      "query": searchSeriesParameters.query
    });
    return List<TvSeriesResultModel>.from((response["results"] as List).map(
      (e) => TvSeriesResultModel.fromJson(e),
    ));
  }
}
