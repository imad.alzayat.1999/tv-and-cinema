import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/domain/entities/airing_today_entity.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/domain/repository/base_airing_today_repository.dart';


class GetAiringTodayUseCase extends BaseUseCase<List<AiringTodayEntity> , AiringTodayParameters>{
  final BaseAiringTodayRepository baseAiringTodayRepository;

  GetAiringTodayUseCase({required this.baseAiringTodayRepository});

  @override
  Future<Either<Failure, List<AiringTodayEntity>>> call(AiringTodayParameters parameters) async{
    return await baseAiringTodayRepository.getAiringToday(airingTodayParameters: parameters);
  }
}
class AiringTodayParameters extends Equatable{
  final int page;

  AiringTodayParameters({required this.page});

  @override
  // TODO: implement props
  List<Object?> get props => [page];
}