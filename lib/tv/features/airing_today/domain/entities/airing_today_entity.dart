import 'package:equatable/equatable.dart';

class AiringTodayEntity extends Equatable {
  final String? backdropPath;
  final String? name;
  final String? overview;
  final String? firstAirDate;
  final num? voteAvg;
  final int id;

  AiringTodayEntity({
    required this.backdropPath,
    required this.name,
    required this.overview,
    required this.firstAirDate,
    required this.voteAvg,
    required this.id,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [id , backdropPath , name , overview , firstAirDate , voteAvg];


}
