import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/domain/entities/airing_today_entity.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/domain/usecases/get_airing_today_usecase.dart';

abstract class BaseAiringTodayRepository {
  Future<Either<Failure, List<AiringTodayEntity>>> getAiringToday(
      {required AiringTodayParameters airingTodayParameters});
}
