import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/domain/usecases/get_airing_today_usecase.dart';

import '../models/airing_today_model.dart';

abstract class BaseAiringTodayRemoteDataSource {
  Future<List<AiringTodayModel>> getAiringToday(
      {required AiringTodayParameters airingTodayParameters});
}

class AiringTodayRemoteDataSource extends BaseAiringTodayRemoteDataSource {
  final ApiConsumer apiConsumer;

  AiringTodayRemoteDataSource({required this.apiConsumer});

  @override
  Future<List<AiringTodayModel>> getAiringToday(
      {required AiringTodayParameters airingTodayParameters}) async {
    final response = await apiConsumer
        .get(path: ApiConstance.airingTodayTvEndPoint, queryParameters: {
      "page": airingTodayParameters.page,
    });
    return List<AiringTodayModel>.from((response["results"] as List).map(
      (e) => AiringTodayModel.fromJson(e),
    ));
  }
}
