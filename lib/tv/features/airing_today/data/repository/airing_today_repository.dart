import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/data/datasouces/airing_today_remote_datasource.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/domain/entities/airing_today_entity.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/domain/repository/base_airing_today_repository.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/domain/usecases/get_airing_today_usecase.dart';

class AiringTodayRepository extends BaseAiringTodayRepository{
  final BaseAiringTodayRemoteDataSource baseAiringTodayRemoteDataSource;

  AiringTodayRepository({required this.baseAiringTodayRemoteDataSource});
  @override
  Future<Either<Failure, List<AiringTodayEntity>>> getAiringToday({required AiringTodayParameters airingTodayParameters}) async{
    try{
      final result = await baseAiringTodayRemoteDataSource.getAiringToday(airingTodayParameters: airingTodayParameters);
      return Right(result);
    }on ServerException catch(e){
      return Left(ServerFailure(e.message));
    }
  }

}