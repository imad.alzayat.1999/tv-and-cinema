import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';

import '../../domain/entities/airing_today_entity.dart';

class AiringTodayStates extends Equatable {
  final String errorAiringTodayMessage;
  final List<AiringTodayEntity> listOfAiringToday;
  final RequestStates airingTodayRequestStates;
  final bool hasReachedMax;

  AiringTodayStates({
    this.errorAiringTodayMessage = '',
    this.listOfAiringToday = const [],
    this.airingTodayRequestStates = RequestStates.loading,
    this.hasReachedMax = false,
  });

  AiringTodayStates copyWith({
    String? errorAiringTodayMessage,
    List<AiringTodayEntity>? listOfAiringToday,
    RequestStates? airingTodayRequestStates,
    bool? hasReachedMax,
  }) {
    return AiringTodayStates(
      errorAiringTodayMessage:
          errorAiringTodayMessage ?? this.errorAiringTodayMessage,
      listOfAiringToday: listOfAiringToday ?? this.listOfAiringToday,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      airingTodayRequestStates:
          airingTodayRequestStates ?? this.airingTodayRequestStates,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        hasReachedMax,
        airingTodayRequestStates,
        listOfAiringToday,
        errorAiringTodayMessage
      ];
}
