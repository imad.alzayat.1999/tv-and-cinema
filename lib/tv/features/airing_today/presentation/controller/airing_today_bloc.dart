import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/presentation/controller/airing_today_events.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/presentation/controller/airing_today_states.dart';

import '../../../../../core/utils/request_states.dart';
import '../../domain/usecases/get_airing_today_usecase.dart';

class AiringTodayBloc extends Bloc<AiringTodayEvents , AiringTodayStates>{
  final GetAiringTodayUseCase getAiringTodayUseCase;
  AiringTodayBloc({required this.getAiringTodayUseCase}) : super(AiringTodayStates()){
    on<GetAiringTodayEvent>(
          (event, emit) async {
        if (state.hasReachedMax) return;
        try {
          if (state.airingTodayRequestStates == RequestStates.loading) {
            final response = await getAiringTodayUseCase(
                AiringTodayParameters(page: event.page));
            response.fold((l) {
              emit(
                state.copyWith(
                  airingTodayRequestStates: RequestStates.error,
                  errorAiringTodayMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                state.copyWith(
                  airingTodayRequestStates: RequestStates.success,
                  listOfAiringToday: r,
                  hasReachedMax: false,
                ),
              );
            });
          } else {
            final response = await getAiringTodayUseCase(
                AiringTodayParameters(page: event.page));
            response.fold((l) {
              emit(
                state.copyWith(
                  airingTodayRequestStates: RequestStates.error,
                  errorAiringTodayMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                state.copyWith(
                  airingTodayRequestStates: RequestStates.success,
                  listOfAiringToday: [...state.listOfAiringToday, ...r],
                  hasReachedMax: false,
                ),
              );
            });
          }
        } catch (e) {
          emit(
            state.copyWith(
              airingTodayRequestStates: RequestStates.error,
              errorAiringTodayMessage: e.toString(),
            ),
          );
        }
      },
      transformer: droppable(),
    );
  }

}