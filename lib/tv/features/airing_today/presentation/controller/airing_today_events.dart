import 'package:equatable/equatable.dart';

abstract class AiringTodayEvents extends Equatable{

}

class GetAiringTodayEvent extends AiringTodayEvents{
  final int page;

  GetAiringTodayEvent({required this.page});

  @override
  // TODO: implement props
  List<Object?> get props => [page];
}