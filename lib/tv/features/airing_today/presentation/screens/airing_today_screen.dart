import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/presentation/components/airing_today_item.dart';
import 'package:movie_app_clean_architecture/tv/features/airing_today/presentation/controller/airing_today_events.dart';
import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_error.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../controller/airing_today_bloc.dart';
import '../controller/airing_today_states.dart';

class AiringTodayScreen extends StatelessWidget {
  const AiringTodayScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          getIt<AiringTodayBloc>()..add(GetAiringTodayEvent(page: 1)),
      child: Scaffold(
          appBar: AppBar(
            elevation: SizesManager.s0,
            backgroundColor: ColorsManager.kEerieBlack,
            title: Text(
              AppLocalizations.of(context)!
                  .translate(StringsManager.airingTodayString)
                  .toString(),
              style: getBoldTextStyle(
                fontSize: FontSizeManager.size16,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
          ),
          body: AiringTodayContent()),
    );
  }
}

class AiringTodayContent extends StatefulWidget {
  const AiringTodayContent({Key? key}) : super(key: key);

  @override
  State<AiringTodayContent> createState() => _AiringTodayContentState();
}

class _AiringTodayContentState extends State<AiringTodayContent> {
  final scrollController = ScrollController();
  int page = 1;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(_listener);
  }

  _listener() {
    if (scrollController.position.maxScrollExtent ==
        scrollController.position.pixels) {
      page = page + 1;
      BlocProvider.of<AiringTodayBloc>(context)
          .add(GetAiringTodayEvent(page: page));
    }
  }

  @override
  void dispose() {
    scrollController
      ..removeListener(_listener)
      ..dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AiringTodayBloc, AiringTodayStates>(
      builder: (context, state) {
        switch (state.airingTodayRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return SingleChildScrollView(
              key: const Key('topRatedScrollView'),
              controller: scrollController,
              child: Column(
                children: [
                  FadeInUp(
                    from: SizesManager.s20,
                    duration: const Duration(
                      milliseconds: ConstsManager.fadeAnimationDuration,
                    ),
                    child: ListView.separated(
                      padding: EdgeInsets.symmetric(
                        vertical: getHeight(inputHeight: PaddingManager.p16),
                        horizontal: getWidth(inputWidth: PaddingManager.p8),
                      ),
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return index >= state.listOfAiringToday.length
                            ? BaseLoadingIndicator()
                            : AiringTodayItem(
                                airingToday: state.listOfAiringToday[index]);
                      },
                      separatorBuilder: (context, index) => SizedBox(
                          height: getHeight(inputHeight: SizesManager.s20)),
                      itemCount: state.hasReachedMax
                          ? state.listOfAiringToday.length
                          : state.listOfAiringToday.length + 1,
                    ),
                  ),
                ],
              ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorAiringTodayMessage,
                onPressFunction: () {
                  BlocProvider.of<AiringTodayBloc>(context)
                    ..add(GetAiringTodayEvent(page: page));
                });
        }
      },
    );
  }
}
