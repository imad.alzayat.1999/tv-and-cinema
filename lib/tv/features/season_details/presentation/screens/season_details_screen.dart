import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/presentation/components/season_videos_component.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/presentation/controller/season_details_bloc.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/presentation/controller/season_details_states.dart';

import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../components/season_episode_component.dart';
import '../controller/season_details_events.dart';

class SeasonDetailsScreen extends StatefulWidget {
  final int tvId;
  final int seasonNumber;
  const SeasonDetailsScreen({
    Key? key,
    required this.tvId,
    required this.seasonNumber,
  }) : super(key: key);

  @override
  State<SeasonDetailsScreen> createState() => _SeasonDetailsScreenState();
}

class _SeasonDetailsScreenState extends State<SeasonDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<SeasonDetailsBloc>()
        ..add(GetSeasonDetailsEvent(
          tvId: widget.tvId,
          seasonId: widget.seasonNumber,
        ))
        ..add(GetSeasonVideosEvent(
          tvId: widget.tvId,
          seasonId: widget.seasonNumber,
        )),
      child: Scaffold(
        backgroundColor: ColorsManager.kEerieBlack,
        body: SeasonDetailsContent(
          tvId: widget.tvId,
          seasonNumber: widget.seasonNumber,
        ),
      ),
    );
  }
}

class SeasonDetailsContent extends StatelessWidget {
  final int tvId;
  final int seasonNumber;
  const SeasonDetailsContent({
    Key? key,
    required this.tvId,
    required this.seasonNumber,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SeasonDetailsBloc, SeasonDetailsStates>(
      builder: (context, state) {
        switch (state.seasonDetailsRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return CustomScrollView(
              key: const Key('movieDetailScrollView'),
              shrinkWrap: true,
              slivers: [
                SliverAppBar(
                  leading: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: Icon(Icons.arrow_back),
                  ),
                  pinned: true,
                  backgroundColor: ColorsManager.kEerieBlack,
                  expandedHeight: getHeight(inputHeight: SizesManager.s250),
                  flexibleSpace: FlexibleSpaceBar(
                    background: FadeIn(
                      duration: const Duration(
                        milliseconds: ConstsManager.fadeAnimationDuration,
                      ),
                      child: ShaderMask(
                        shaderCallback: (rect) {
                          return LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              ColorsManager.kTransparent,
                              ColorsManager.kBlack,
                              ColorsManager.kBlack,
                              ColorsManager.kTransparent,
                            ],
                            stops: [0.0, 0.5, 1.0, 1.0],
                          ).createShader(
                            Rect.fromLTRB(0.0, 0.0, rect.width, rect.height),
                          );
                        },
                        blendMode: BlendMode.dstIn,
                        child: FadeInImage.assetNetwork(
                          width: MediaQuery.of(context).size.width,
                          placeholder: ImagesManager.logoImage,
                          image: FunctionsManager.showAPIImage(
                              state.seasonDetails!.posterPath),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: FadeInUp(
                    from: SizesManager.s20,
                    duration: const Duration(
                      milliseconds: ConstsManager.fadeAnimationDuration,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(PaddingManager.p16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            state.seasonDetails!.name.toString(),
                            style: getBoldTextStyle(
                              fontSize: FontSizeManager.size23,
                              letterSpacing: SizesManager.s1_2,
                              context: context,
                            ),
                          ),
                          SizedBox(
                            height: getHeight(inputHeight: SizesManager.s8),
                          ),
                          Row(
                            children: [
                              Container(
                                padding: const EdgeInsets.symmetric(
                                  vertical: PaddingManager.p2,
                                  horizontal: PaddingManager.p8,
                                ),
                                decoration: BoxDecoration(
                                  color:
                                      ColorsManager.kDimGray,
                                  borderRadius:
                                      BorderRadius.circular(SizesManager.s4),
                                ),
                                child: Text(
                                  FunctionsManager.formatDate(
                                      state.seasonDetails!.airDate),
                                  style: getMediumTextStyle(
                                    color: ColorsManager.kWhite,
                                    fontSize: FontSizeManager.size16,
                                    letterSpacing: SizesManager.s0,
                                    context: context,
                                  ),
                                ),
                              ),
                              SizedBox(
                                  width:
                                      getWidth(inputWidth: SizesManager.s16)),
                              Row(
                                children: [
                                  const BaseIcon(
                                    iconHeight: SizesManager.s20,
                                    iconPath: IconsManager.clockIcon,
                                  ),
                                  SizedBox(
                                      width: getWidth(
                                          inputWidth: SizesManager.s4)),
                                  Text(
                                    state.seasonDetails!.seasonNumber
                                        .toString(),
                                    style: getMediumTextStyle(
                                      fontSize: FontSizeManager.size16,
                                      letterSpacing: SizesManager.s1_2,
                                      context: context,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                              height: getHeight(inputHeight: SizesManager.s20)),
                          Text(
                            state.seasonDetails!.overview.toString(),
                            style: getMediumTextStyle(
                              fontSize: FontSizeManager.size14,
                              letterSpacing: SizesManager.s1_2,
                              context: context,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(
                      PaddingManager.p16,
                      PaddingManager.p0,
                      PaddingManager.p16,
                      PaddingManager.p24,
                    ),
                    child: Column(
                      children: [
                        Align(
                          alignment: AppLocalizations.of(context)!.isEnLocale
                              ? Alignment.centerLeft
                              : Alignment.centerRight,
                          child: Text(
                            AppLocalizations.of(context)!
                                .translate(StringsManager.seasonVideosString)!,
                            style: getBoldTextStyle(
                              fontSize: FontSizeManager.size20,
                              letterSpacing: SizesManager.s0,
                              context: context,
                            ),
                          ),
                        ),
                        SeasonVideosComponent(
                          seasonNumber: seasonNumber,
                          tvId: tvId,
                          tvSeriesPoster: state.seasonDetails!.posterPath,
                        ),
                      ],
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(
                      PaddingManager.p16,
                      PaddingManager.p0,
                      PaddingManager.p16,
                      PaddingManager.p24,
                    ),
                    child: Column(
                      children: [
                        Align(
                          alignment: AppLocalizations.of(context)!.isEnLocale
                              ? Alignment.centerLeft
                              : Alignment.centerRight,
                          child: Text(
                            AppLocalizations.of(context)!
                                .translate(StringsManager.episodesString)!,
                            style: getBoldTextStyle(
                              fontSize: FontSizeManager.size20,
                              letterSpacing: SizesManager.s0,
                              context: context,
                            ),
                          ),
                        ),
                        SeasonEpisodeComponent(
                          tvId: tvId,
                          seasonNumber: seasonNumber,
                          episodes: state.seasonDetails!.episodes,
                        ),
                      ],
                    ),
                  ),
                ),
                // SliverToBoxAdapter(
                //   child: Padding(
                //     padding: const EdgeInsets.fromLTRB(
                //       PaddingManager.p16,
                //       PaddingManager.p0,
                //       PaddingManager.p16,
                //       PaddingManager.p24,
                //     ),
                //     child: Column(
                //       children: [
                //         Align(
                //           alignment: getAlignmentByLanguage(),
                //           child: Text(
                //             AppLocalizations.of(context)!
                //                 .translate(StringsManager.reviewString)!,
                //             style: getBoldTextStyle(
                //               fontSize: FontSizeManager.size20,
                //               letterSpacing: SizesManager.s0,
                //               context: context,
                //             ),
                //           ),
                //         ),
                //         const Reviews(),
                //       ],
                //     ),
                //   ),
                // ),
                // SliverToBoxAdapter(
                //   child: Padding(
                //     padding: const EdgeInsets.fromLTRB(
                //       PaddingManager.p16,
                //       PaddingManager.p0,
                //       PaddingManager.p16,
                //       PaddingManager.p24,
                //     ),
                //     child: Column(
                //       children: [
                //         Align(
                //           alignment: getAlignmentByLanguage(),
                //           child: Text(
                //             AppLocalizations.of(context)!
                //                 .translate(StringsManager.trailersString)!,
                //             style: getBoldTextStyle(
                //               fontSize: FontSizeManager.size20,
                //               letterSpacing: SizesManager.s0,
                //               context: context,
                //             ),
                //           ),
                //         ),
                //         Trailers(moviePoster: state.movieDetails!.backdropPath),
                //       ],
                //     ),
                //   ),
                // ),
                // SliverToBoxAdapter(
                //   child: Padding(
                //     padding: const EdgeInsets.fromLTRB(
                //       PaddingManager.p16,
                //       PaddingManager.p0,
                //       PaddingManager.p16,
                //       PaddingManager.p24,
                //     ),
                //     child: Column(
                //       children: [
                //         Align(
                //           alignment: getAlignmentByLanguage(),
                //           child: Text(
                //             AppLocalizations.of(context)!
                //                 .translate(StringsManager.creditsString)!,
                //             style: getBoldTextStyle(
                //               fontSize: FontSizeManager.size20,
                //               letterSpacing: SizesManager.s0,
                //               context: context,
                //             ),
                //           ),
                //         ),
                //         const Credits(),
                //       ],
                //     ),
                //   ),
                // ),
              ],
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorSeasonDetailsMessage,
                onPressFunction: () {
                  BlocProvider.of<SeasonDetailsBloc>(context)
                    ..add(
                      GetSeasonDetailsEvent(tvId: tvId, seasonId: seasonNumber),
                    );
                });
        }
      },
    );
  }
}
