import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../domain/entities/episode_info_entity.dart';

class SeasonEpisodeComponent extends StatelessWidget {
  final List<EpisodeInfoEntity> episodes;
  final int tvId;
  final int seasonNumber;
  const SeasonEpisodeComponent({
    Key? key,
    required this.episodes,
    required this.seasonNumber,
    required this.tvId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) => _getEpisode(episodes[index], context),
      separatorBuilder: (context, index) => SizedBox(
        height: getHeight(
          inputHeight: SizesManager.s10,
        ),
      ),
      itemCount: episodes.length,
    );
  }

  Widget _getEpisode(EpisodeInfoEntity episode, BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        Routes.episodeDetailsRoute,
        arguments: EpisodeDetailsRouteParameters(
          tvId: tvId,
          seasonNumber: seasonNumber,
          episodeNumber: episode.episodeNumber!,
        ),
      ),
      child: SizedBox(
        height: getHeight(inputHeight: SizesManager.s180),
        child: Card(
          elevation: SizesManager.s0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(SizesManager.s10)),
          color: ColorsManager.kDimGray,
          child: Row(
            children: [
              SizedBox(
                height: getHeight(inputHeight: SizesManager.s180),
                width: getWidth(inputWidth: SizesManager.s125),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(SizesManager.s10),
                  child: FadeInImage.assetNetwork(
                    image: FunctionsManager.showAPIImage(episode.stillPath),
                    placeholder: ImagesManager.logoImage,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(
                width: getWidth(inputWidth: SizesManager.s10),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: getWidth(inputWidth: SizesManager.s200),
                      child: Text(
                        episode.name.toString(),
                        maxLines: 2,
                        style: getSemiBoldTextStyle(
                            fontSize: FontSizeManager.size14,
                            letterSpacing: SizesManager.s0,
                            context: context),
                      ),
                    ),
                    SizedBox(
                      height: getHeight(inputHeight: SizesManager.s10),
                    ),
                    SizedBox(
                      width: getWidth(inputWidth: SizesManager.s200),
                      child: Text(
                        episode.overview.toString(),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 3,
                        style: getRegularTextStyle(
                            fontSize: FontSizeManager.size12,
                            letterSpacing: SizesManager.s0,
                            context: context),
                      ),
                    ),
                    SizedBox(
                      height: getHeight(inputHeight: SizesManager.s10),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            const BaseIcon(
                              iconHeight: SizesManager.s20,
                              iconPath: IconsManager.clockIcon,
                            ),
                            SizedBox(
                                width: getWidth(inputWidth: SizesManager.s4)),
                            Text(
                              FunctionsManager.showDuration(episode.runtime),
                              style: getMediumTextStyle(
                                fontSize: FontSizeManager.size16,
                                letterSpacing: SizesManager.s1_2,
                                context: context,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            const BaseIcon(
                              iconHeight: SizesManager.s20,
                              iconPath: IconsManager.starIcon,
                              iconColor: ColorsManager.voteColor,
                            ),
                            SizedBox(
                                width: getWidth(inputWidth: SizesManager.s4)),
                            Text(
                              (episode.voteAvg! / 2).toStringAsFixed(1),
                              style: getMediumTextStyle(
                                fontSize: FontSizeManager.size16,
                                letterSpacing: SizesManager.s1_2,
                                context: context,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: getHeight(inputHeight: SizesManager.s10),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                        vertical: getHeight(inputHeight: PaddingManager.p2),
                        horizontal: getWidth(inputWidth: PaddingManager.p8),
                      ),
                      decoration: BoxDecoration(
                        color: ColorsManager.kVeryLightBlue,
                        borderRadius: BorderRadius.circular(SizesManager.s4),
                      ),
                      child: Text(
                        FunctionsManager.formatDate(episode.airDate),
                        style: getMediumTextStyle(
                          fontSize: FontSizeManager.size16,
                          letterSpacing: SizesManager.s0,
                          context: context,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
