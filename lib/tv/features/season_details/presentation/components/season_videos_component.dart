import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/entities/season_videos/season_video_entity.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/presentation/controller/season_details_bloc.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/presentation/controller/season_details_events.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/presentation/controller/season_details_states.dart';

import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/utils/global_widgets/base_error.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';

class SeasonVideosComponent extends StatefulWidget {
  final int tvId;
  final int seasonNumber;
  final String? tvSeriesPoster;

  const SeasonVideosComponent({
    Key? key,
    required this.seasonNumber,
    required this.tvId,
    required this.tvSeriesPoster,
  }) : super(key: key);

  @override
  State<SeasonVideosComponent> createState() => _SeasonVideosComponentState();
}

class _SeasonVideosComponentState extends State<SeasonVideosComponent> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SeasonDetailsBloc, SeasonDetailsStates>(
        builder: (context, state) {
      switch (state.seasonVideosRequestStates) {
        case RequestStates.loading:
          return BaseLoadingIndicator();
        case RequestStates.success:
          return state.seasonVideos.isEmpty
              ? Container()
              : FadeInUp(
                  from: SizesManager.s20,
                  duration: const Duration(
                    milliseconds: ConstsManager.fadeAnimationDuration,
                  ),
                  child: SizedBox(
                    height: getHeight(inputHeight: SizesManager.s120),
                    child: ListView.separated(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) => _getTrailer(
                        video: state.seasonVideos[index],
                      ),
                      separatorBuilder: (context, index) => SizedBox(
                          width: getWidth(inputWidth: SizesManager.s16)),
                      itemCount: state.seasonVideos.length,
                    ),
                  ),
                );
        case RequestStates.error:
          return BaseError(
              errorMessage: state.errorSeasonVideosMessage,
              onPressFunction: () {
                BlocProvider.of<SeasonDetailsBloc>(context)
                  ..add(
                    GetSeasonVideosEvent(
                        tvId: widget.tvId, seasonId: widget.seasonNumber),
                  );
              });
      }
    });
  }

  Widget _getTrailer({required SeasonVideoEntity video}) {
    return GestureDetector(
      onTap: () {
        FunctionsManager.launchExternalUrl(
          ConstsManager.getYoutubeUrl(video.key.toString()),
        );
      },
      child: Stack(
        alignment: Alignment.center,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(SizesManager.s10),
            child: Container(
              color: ColorsManager.kBlack,
              width: getWidth(inputWidth: SizesManager.s200),
              child: Opacity(
                opacity: 0.6,
                child: FadeInImage.assetNetwork(
                  image: FunctionsManager.showAPIImage(widget.tvSeriesPoster),
                  placeholder: ImagesManager.logoImage,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          const BaseIcon(
            iconHeight: SizesManager.s50,
            iconPath: IconsManager.playButtonIcon,
            iconColor: ColorsManager.kWhite,
          )
        ],
      ),
    );
  }
}
