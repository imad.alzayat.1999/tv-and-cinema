import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/presentation/controller/season_details_events.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/presentation/controller/season_details_states.dart';

import '../../domain/usecases/get_season_details_usecase.dart';
import '../../domain/usecases/get_season_videos_usecase.dart';

class SeasonDetailsBloc extends Bloc<SeasonDetailsEvents, SeasonDetailsStates> {
  final GetSeasonDetailsUseCase getSeasonDetailsUseCase;
  final GetSeasonVideosUseCase getSeasonVideosUseCase;

  SeasonDetailsBloc({
    required this.getSeasonDetailsUseCase,
    required this.getSeasonVideosUseCase,
  }) : super(SeasonDetailsStates()) {
    on<GetSeasonDetailsEvent>((event, emit) async {
      final failureOrSeasonDetails = await getSeasonDetailsUseCase(
        SeasonDetailsParameters(tvId: event.tvId, seasonId: event.seasonId),
      );
      failureOrSeasonDetails.fold((l) {
        emit(
          state.copyWith(
            errorSeasonDetailsMessage: l.message,
            seasonDetailsRequestStates: RequestStates.error,
          ),
        );
      }, (r) {
        emit(
          state.copyWith(
            seasonDetails: r,
            seasonDetailsRequestStates: RequestStates.success,
          ),
        );
      });
    });
    on<GetSeasonVideosEvent>((event, emit) async {
      final failureOrSeasonVideos = await getSeasonVideosUseCase(
        SeasonVideosParameters(tvId: event.tvId, seasonId: event.seasonId),
      );
      failureOrSeasonVideos.fold((l) {
        emit(
          state.copyWith(
            errorSeasonVideosMessage: l.message,
            seasonVideosRequestStates: RequestStates.error,
          ),
        );
      }, (r) {
        emit(
          state.copyWith(
            seasonVideos: r,
            seasonVideosRequestStates: RequestStates.success,
          ),
        );
      });
    });
  }
}
