import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/entities/season_details_entity.dart';

import '../../domain/entities/season_videos/season_video_entity.dart';

class SeasonDetailsStates extends Equatable {
  final String errorSeasonDetailsMessage;
  final SeasonDetailsEntity? seasonDetails;
  final RequestStates seasonDetailsRequestStates;

  final String errorSeasonVideosMessage;
  final List<SeasonVideoEntity> seasonVideos;
  final RequestStates seasonVideosRequestStates;

  SeasonDetailsStates({
    this.errorSeasonDetailsMessage = '',
    this.seasonDetails,
    this.seasonDetailsRequestStates = RequestStates.loading,
    this.errorSeasonVideosMessage = '',
    this.seasonVideos = const [],
    this.seasonVideosRequestStates = RequestStates.loading,
  });

  SeasonDetailsStates copyWith({
    String? errorSeasonDetailsMessage,
    SeasonDetailsEntity? seasonDetails,
    RequestStates? seasonDetailsRequestStates,
    String? errorSeasonVideosMessage,
    List<SeasonVideoEntity>? seasonVideos,
    RequestStates? seasonVideosRequestStates,
  }) {
    return SeasonDetailsStates(
      errorSeasonDetailsMessage:
          errorSeasonDetailsMessage ?? this.errorSeasonDetailsMessage,
      seasonDetails: seasonDetails ?? this.seasonDetails,
      seasonDetailsRequestStates:
          seasonDetailsRequestStates ?? this.seasonDetailsRequestStates,
      errorSeasonVideosMessage:
          errorSeasonVideosMessage ?? this.errorSeasonVideosMessage,
      seasonVideos: seasonVideos ?? this.seasonVideos,
      seasonVideosRequestStates:
          seasonVideosRequestStates ?? this.seasonVideosRequestStates,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        errorSeasonDetailsMessage,
        seasonDetails,
        seasonDetailsRequestStates,
        errorSeasonVideosMessage,
        seasonVideos,
        seasonVideosRequestStates,
      ];
}
