import 'package:equatable/equatable.dart';

abstract class SeasonDetailsEvents extends Equatable{

}

class GetSeasonDetailsEvent extends SeasonDetailsEvents{
  final int tvId;
  final int seasonId;

  GetSeasonDetailsEvent({required this.tvId, required this.seasonId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId , seasonId];
}

class GetSeasonVideosEvent extends SeasonDetailsEvents{
  final int tvId;
  final int seasonId;

  GetSeasonVideosEvent({required this.tvId, required this.seasonId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId , seasonId];
}