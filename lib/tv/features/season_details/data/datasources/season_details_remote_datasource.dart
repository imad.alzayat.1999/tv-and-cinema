import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/data/models/season_videos/season_video_model.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/usecases/get_season_details_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/usecases/get_season_videos_usecase.dart';

import '../models/season_details_model.dart';

abstract class BaseSeasonDetailsRemoteDataSource {
  Future<SeasonDetailsModel> getSeasonDetails(
      {required SeasonDetailsParameters seasonDetailsParameters});
  Future<List<SeasonVideoModel>> getSeasonVideos(
      {required SeasonVideosParameters seasonVideosParameters});
}

class SeasonDetailsRemoteDataSource extends BaseSeasonDetailsRemoteDataSource {
  final ApiConsumer apiConsumer;

  SeasonDetailsRemoteDataSource({required this.apiConsumer});

  @override
  Future<SeasonDetailsModel> getSeasonDetails(
      {required SeasonDetailsParameters seasonDetailsParameters}) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getSeasonDetailsEndPoint(
        tvId: seasonDetailsParameters.tvId,
        seasonId: seasonDetailsParameters.seasonId,
      ),
    );
    return SeasonDetailsModel.fromJson(response);
  }

  @override
  Future<List<SeasonVideoModel>> getSeasonVideos(
      {required SeasonVideosParameters seasonVideosParameters}) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getSeasonVideosEndPoint(
        tvId: seasonVideosParameters.tvId,
        seasonId: seasonVideosParameters.seasonId,
      ),
    );
    return List<SeasonVideoModel>.from((response["results"] as List).map(
          (e) => SeasonVideoModel.fromJson(e),
    ));
  }
}
