import 'package:movie_app_clean_architecture/tv/features/season_details/data/models/episode_info_model.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/entities/episode_info_entity.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/entities/season_details_entity.dart';

class SeasonDetailsModel extends SeasonDetailsEntity {
  SeasonDetailsModel({
    required int? id,
    required int? seasonNumber,
    required String? name,
    required String? overview,
    required String? posterPath,
    required DateTime? airDate,
    required List<EpisodeInfoEntity> episodes,
  }) : super(
          id: id,
          seasonNumber: seasonNumber,
          name: name,
          overview: overview,
          posterPath: posterPath,
          airDate: airDate,
          episodes: episodes,
        );

  factory SeasonDetailsModel.fromJson(Map<String, dynamic> json) =>
      SeasonDetailsModel(
        id: json["id"],
        seasonNumber: json["season_number"],
        name: json["name"],
        overview: json["overview"],
        posterPath: json["poster_path"],
        airDate: DateTime.parse(json["air_date"]),
        episodes: List<EpisodeInfoModel>.from(
          json["episodes"].map((x) => EpisodeInfoModel.fromJson(x)),
        ),
      );
}
