import 'package:movie_app_clean_architecture/tv/features/season_details/domain/entities/episode_info_entity.dart';

class EpisodeInfoModel extends EpisodeInfoEntity {
  EpisodeInfoModel({
    required int? id,
    required int? episodeNumber,
    required String? name,
    required String? overview,
    required num? voteAvg,
    required String? stillPath,
    required int? runtime,
    required DateTime? airDate,
  }) : super(
          id: id,
          episodeNumber: episodeNumber,
          name: name,
          overview: overview,
          voteAvg: voteAvg,
          stillPath: stillPath,
          runtime: runtime,
          airDate: airDate,
        );

  factory EpisodeInfoModel.fromJson(Map<String, dynamic> json) =>
      EpisodeInfoModel(
        id: json["id"],
        episodeNumber: json["episode_number"],
        name: json["name"],
        overview: json["overview"],
        voteAvg: json["vote_average"],
        stillPath: json["still_path"],
        runtime: json["runtime"],
        airDate: json["air_date"] == null ? null : DateTime.parse(json["air_date"]),
      );
}
