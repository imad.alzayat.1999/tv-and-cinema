import 'package:movie_app_clean_architecture/tv/features/season_details/domain/entities/season_videos/season_video_entity.dart';

class SeasonVideoModel extends SeasonVideoEntity {
  SeasonVideoModel({required String? name, required String? key}) : super(name: name, key: key);

  factory SeasonVideoModel.fromJson(Map<String, dynamic> json) =>
      SeasonVideoModel(name: json["name"], key: json["key"]);
}
