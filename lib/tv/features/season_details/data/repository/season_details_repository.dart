import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/data/datasources/season_details_remote_datasource.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/data/models/season_details_model.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/data/models/season_videos/season_video_model.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/repository/base_season_details_repository.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/usecases/get_season_details_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/usecases/get_season_videos_usecase.dart';

class SeasonDetailsRepository extends BaseSeasonDetailsRepository {
  final BaseSeasonDetailsRemoteDataSource baseSeasonDetailsRemoteDataSource;

  SeasonDetailsRepository({required this.baseSeasonDetailsRemoteDataSource});
  @override
  Future<Either<Failure, SeasonDetailsModel>> getSeasonDetails(
      {required SeasonDetailsParameters seasonDetailsParameters}) async {
    try {
      final result = await baseSeasonDetailsRemoteDataSource.getSeasonDetails(
          seasonDetailsParameters: seasonDetailsParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<SeasonVideoModel>>> getSeasonVideos(
      {required SeasonVideosParameters seasonVideosParameters}) async {
    try {
      final result = await baseSeasonDetailsRemoteDataSource.getSeasonVideos(
          seasonVideosParameters: seasonVideosParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }
}
