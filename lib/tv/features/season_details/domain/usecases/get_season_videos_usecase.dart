import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/entities/season_videos/season_video_entity.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/repository/base_season_details_repository.dart';


class GetSeasonVideosUseCase extends BaseUseCase<List<SeasonVideoEntity> , SeasonVideosParameters>{
  final BaseSeasonDetailsRepository baseSeasonDetailsRepository;

  GetSeasonVideosUseCase({required this.baseSeasonDetailsRepository});
  @override
  Future<Either<Failure, List<SeasonVideoEntity>>> call(SeasonVideosParameters parameters) async{
    return await baseSeasonDetailsRepository.getSeasonVideos(seasonVideosParameters: parameters);
  }
}


class SeasonVideosParameters extends Equatable{
  final int tvId;
  final int seasonId;

  SeasonVideosParameters({required this.tvId, required this.seasonId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId , seasonId];
}