import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/entities/season_details_entity.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/repository/base_season_details_repository.dart';

class GetSeasonDetailsUseCase extends BaseUseCase<SeasonDetailsEntity , SeasonDetailsParameters>{
  final BaseSeasonDetailsRepository baseSeasonDetailsRepository;

  GetSeasonDetailsUseCase({required this.baseSeasonDetailsRepository});
  @override
  Future<Either<Failure, SeasonDetailsEntity>> call(SeasonDetailsParameters parameters) async{
    return await baseSeasonDetailsRepository.getSeasonDetails(seasonDetailsParameters: parameters);
  }
}

class SeasonDetailsParameters extends Equatable{
  final int tvId;
  final int seasonId;

  SeasonDetailsParameters({required this.tvId, required this.seasonId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId , seasonId];
}