import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/data/models/season_details_model.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/data/models/season_videos/season_video_model.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/usecases/get_season_details_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/season_details/domain/usecases/get_season_videos_usecase.dart';

abstract class BaseSeasonDetailsRepository {
  Future<Either<Failure, SeasonDetailsModel>> getSeasonDetails({
    required SeasonDetailsParameters seasonDetailsParameters,
  });

  Future<Either<Failure, List<SeasonVideoModel>>> getSeasonVideos({
    required SeasonVideosParameters seasonVideosParameters,
  });
}
