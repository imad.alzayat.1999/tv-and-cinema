import 'package:equatable/equatable.dart';

import 'episode_info_entity.dart';

class SeasonDetailsEntity extends Equatable {
  final int? id;
  final int? seasonNumber;
  final String? name;
  final String? overview;
  final String? posterPath;
  final DateTime? airDate;
  final List<EpisodeInfoEntity> episodes;

  SeasonDetailsEntity({
    required this.id,
    required this.seasonNumber,
    required this.name,
    required this.overview,
    required this.posterPath,
    required this.airDate,
    required this.episodes,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [id , seasonNumber , name , overview , posterPath , airDate , episodes];
}
