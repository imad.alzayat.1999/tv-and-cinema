import 'package:equatable/equatable.dart';

class EpisodeInfoEntity extends Equatable {
  final int? id;
  final int? episodeNumber;
  final String? name;
  final String? overview;
  final num? voteAvg;
  final String? stillPath;
  final int? runtime;
  final DateTime? airDate;

  EpisodeInfoEntity({
    required this.id,
    required this.episodeNumber,
    required this.name,
    required this.overview,
    required this.voteAvg,
    required this.stillPath,
    required this.runtime,
    required this.airDate,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        id,
        episodeNumber,
        name,
        overview,
        voteAvg,
        stillPath,
        runtime,
        airDate,
      ];
}
