import 'package:equatable/equatable.dart';

class SeasonVideoEntity extends Equatable{
  final String? name;
  final String? key;

  SeasonVideoEntity({required this.name, required this.key});

  @override
  // TODO: implement props
  List<Object?> get props => [name , key];
}