import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/entities/episode_actors/episode_actor_entity.dart';

class EpisodeActorModel extends EpisodeActorEntity {
  EpisodeActorModel({
    required int actorId,
    required String? name,
    required String? profilePath,
    required String? department,
  }) : super(
          actorId: actorId,
          name: name,
          profilePath: profilePath,
          department: department,
        );

  factory EpisodeActorModel.fromJson(Map<String, dynamic> json) =>
      EpisodeActorModel(
        actorId: json["id"],
        name: json["name"],
        profilePath: json["profile_path"],
        department: json["department"],
      );

  Map<String, dynamic> toJson() => {
        "id": actorId,
        "name": name,
        "profile_path": profilePath,
        "department": department,
      };
}
