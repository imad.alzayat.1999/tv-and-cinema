import 'package:movie_app_clean_architecture/tv/features/episode_details/data/models/episode_actors/episode_actor_model.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/entities/episode_actors/episode_actor_entity.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/entities/episode_details_entity.dart';

class EpisodeDetailsModel extends EpisodeDetailsEntity {
  EpisodeDetailsModel({
    required DateTime? airDate,
    required String? name,
    required String? overview,
    required int id,
    required List<EpisodeActorEntity> episodeActors,
    required int? seasonNumber,
    required int? episodeNumber,
    required String? stillPath,
    required num? voteAvg,
  }) : super(
          airDate: airDate,
          name: name,
          overview: overview,
          id: id,
          episodeActors: episodeActors,
          seasonNumber: seasonNumber,
          episodeNumber: episodeNumber,
          stillPath: stillPath,
          voteAvg: voteAvg,
        );

  factory EpisodeDetailsModel.fromJson(Map<String, dynamic> json) => EpisodeDetailsModel(
    airDate: DateTime.parse(json["air_date"]),
    episodeActors: List<EpisodeActorModel>.from(json["crew"].map((x) => EpisodeActorModel.fromJson(x))),
    episodeNumber: json["episode_number"],
    name: json["name"],
    overview: json["overview"],
    id: json["id"],
    seasonNumber: json["season_number"],
    stillPath: json["still_path"],
    voteAvg: json["vote_average"],
  );

  Map<String, dynamic> toJson() => {
    "air_date": "${airDate!.year.toString().padLeft(4, '0')}-${airDate!.month.toString().padLeft(2, '0')}-${airDate!.day.toString().padLeft(2, '0')}",
    "crew": List<dynamic>.from(episodeActors.map((x) => x)),
    "episode_number": episodeNumber,
    "name": name,
    "overview": overview,
    "id": id,
    "season_number": seasonNumber,
    "still_path": stillPath,
    "vote_average": voteAvg,
  };
}
