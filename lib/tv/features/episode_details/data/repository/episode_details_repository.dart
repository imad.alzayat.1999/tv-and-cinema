import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/data/datasources/episode_details_remote_datasource.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/entities/episode_details_entity.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/repository/base_episode_details_repository.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/usecases/get_episode_details_usecase.dart';

class EpisodeDetailsRepository extends BaseEpisodeDetailsRepository {
  final BaseEpisodeDetailsRemoteDataSource baseEpisodeDetailsRemoteDataSource;

  EpisodeDetailsRepository({required this.baseEpisodeDetailsRemoteDataSource});

  @override
  Future<Either<Failure, EpisodeDetailsEntity>> getEpisodeDetails({
    required EpisodeDetailsParameters episodeDetailsParameters,
  }) async {
    try {
      final result = await baseEpisodeDetailsRemoteDataSource.getEpisodeDetails(
        episodeDetailsParameters: episodeDetailsParameters,
      );
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }
}
