import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/usecases/get_episode_details_usecase.dart';

import '../models/episode_details_model.dart';

abstract class BaseEpisodeDetailsRemoteDataSource {
  Future<EpisodeDetailsModel> getEpisodeDetails({
    required EpisodeDetailsParameters episodeDetailsParameters,
  });
}

class EpisodeDetailsRemoteDataSource extends BaseEpisodeDetailsRemoteDataSource {
  final ApiConsumer apiConsumer;

  EpisodeDetailsRemoteDataSource({required this.apiConsumer});
  @override
  Future<EpisodeDetailsModel> getEpisodeDetails({
    required EpisodeDetailsParameters episodeDetailsParameters,
  }) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getEpisodeDetailsEndPoint(
        tvId: episodeDetailsParameters.tvId,
        seasonId: episodeDetailsParameters.seasonId,
        episodeId: episodeDetailsParameters.episodeId,
      ),
    );

    return EpisodeDetailsModel.fromJson(response);
  }
}
