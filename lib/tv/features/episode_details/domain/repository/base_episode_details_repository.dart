import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/entities/episode_details_entity.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/usecases/get_episode_details_usecase.dart';

abstract class BaseEpisodeDetailsRepository {
  Future<Either<Failure, EpisodeDetailsEntity>> getEpisodeDetails({
    required EpisodeDetailsParameters episodeDetailsParameters,
  });
}
