import 'package:equatable/equatable.dart';

class EpisodeActorEntity extends Equatable {
  final int actorId;
  final String? name;
  final String? profilePath;
  final String? department;

  EpisodeActorEntity({
    required this.actorId,
    required this.name,
    required this.profilePath,
    required this.department,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [actorId , name , profilePath , department];
}
