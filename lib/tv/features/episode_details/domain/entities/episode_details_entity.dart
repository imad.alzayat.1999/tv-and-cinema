import 'package:equatable/equatable.dart';

import 'episode_actors/episode_actor_entity.dart';

class EpisodeDetailsEntity extends Equatable {
  final DateTime? airDate;
  final String? name;
  final String? overview;
  final int id;
  final List<EpisodeActorEntity> episodeActors;
  final int? seasonNumber;
  final int? episodeNumber;
  final String? stillPath;
  final num? voteAvg;

  EpisodeDetailsEntity({
    required this.airDate,
    required this.name,
    required this.overview,
    required this.id,
    required this.episodeActors,
    required this.seasonNumber,
    required this.episodeNumber,
    required this.stillPath,
    required this.voteAvg,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        airDate,
        name,
        overview,
        id,
        episodeActors,
        episodeNumber,
        seasonNumber,
        stillPath,
        voteAvg,
      ];
}
