import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/entities/episode_details_entity.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/repository/base_episode_details_repository.dart';


class GetEpisodeDetailsUseCase extends BaseUseCase<EpisodeDetailsEntity , EpisodeDetailsParameters>{
  final BaseEpisodeDetailsRepository baseEpisodeDetailsRepository;

  GetEpisodeDetailsUseCase({required this.baseEpisodeDetailsRepository});

  @override
  Future<Either<Failure, EpisodeDetailsEntity>> call(EpisodeDetailsParameters parameters) async{
    return await baseEpisodeDetailsRepository.getEpisodeDetails(episodeDetailsParameters: parameters);
  }
}

class EpisodeDetailsParameters extends Equatable {
  final int tvId;
  final int seasonId;
  final int episodeId;

  EpisodeDetailsParameters({
    required this.tvId,
    required this.seasonId,
    required this.episodeId,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [tvId , seasonId , episodeId];
}
