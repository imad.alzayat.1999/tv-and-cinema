import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/presentation/components/credits_episode_component.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/presentation/controller/episode_details_bloc.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/presentation/controller/episode_details_events.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/presentation/controller/episode_details_states.dart';

import '../../../../../core/config/routes_config/routes_parameters.dart';
import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';

class EpisodeDetailsScreen extends StatelessWidget {
  final EpisodeDetailsRouteParameters episodeDetailsRouteParameters;
  const EpisodeDetailsScreen({
    Key? key,
    required this.episodeDetailsRouteParameters,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<EpisodeDetailsBloc>()
        ..add(
          GetEpisodeDetailsEvent(
            tvId: episodeDetailsRouteParameters.tvId,
            seasonId: episodeDetailsRouteParameters.seasonNumber,
            episodeId: episodeDetailsRouteParameters.episodeNumber,
          ),
        ),
      child: Scaffold(
        backgroundColor: ColorsManager.kEerieBlack,
        body: EpisodeDetailsContent(
          episodeDetailsRouteParameters: episodeDetailsRouteParameters,
        ),
      ),
    );
  }
}

class EpisodeDetailsContent extends StatefulWidget {
  final EpisodeDetailsRouteParameters episodeDetailsRouteParameters;
  const EpisodeDetailsContent({
    Key? key,
    required this.episodeDetailsRouteParameters,
  }) : super(key: key);

  @override
  State<EpisodeDetailsContent> createState() => _EpisodeDetailsContentState();
}

class _EpisodeDetailsContentState extends State<EpisodeDetailsContent> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EpisodeDetailsBloc, EpisodeDetailsStates>(
      builder: (context, states) {
        switch (states.episodeDetailsRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return CustomScrollView(
              physics: const NeverScrollableScrollPhysics(),
              key: const Key('movieDetailScrollView'),
              shrinkWrap: true,
              slivers: [
                SliverAppBar(
                  pinned: true,
                  backgroundColor: ColorsManager.kEerieBlack,
                  expandedHeight: getHeight(inputHeight: SizesManager.s250),
                  flexibleSpace: FlexibleSpaceBar(
                    background: FadeIn(
                      duration: const Duration(
                        milliseconds: ConstsManager.fadeAnimationDuration,
                      ),
                      child: ShaderMask(
                        shaderCallback: (rect) {
                          return const LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              ColorsManager.kTransparent,
                              ColorsManager.kBlack,
                              ColorsManager.kBlack,
                              ColorsManager.kTransparent,
                            ],
                            stops: [0.0, 0.5, 1.0, 1.0],
                          ).createShader(
                            Rect.fromLTRB(0.0, 0.0, rect.width, rect.height),
                          );
                        },
                        blendMode: BlendMode.dstIn,
                        child: FadeInImage.assetNetwork(
                          width: MediaQuery.of(context).size.width,
                          image: FunctionsManager.showAPIImage(
                              states.episodeDetails!.stillPath),
                          placeholder: ImagesManager.logoImage,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: FadeInUp(
                    from: SizesManager.s20,
                    duration: const Duration(
                      milliseconds: ConstsManager.fadeAnimationDuration,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(PaddingManager.p16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            states.episodeDetails!.name.toString(),
                            style: getBoldTextStyle(
                              fontSize: FontSizeManager.size23,
                              letterSpacing: SizesManager.s1_2,
                              context: context,
                            ),
                          ),
                          SizedBox(
                              height: getHeight(inputHeight: SizesManager.s8)),
                          Row(
                            children: [
                              Container(
                                padding: const EdgeInsets.symmetric(
                                  vertical: PaddingManager.p2,
                                  horizontal: PaddingManager.p8,
                                ),
                                decoration: BoxDecoration(
                                  color: ColorsManager.kVeryLightBlue,
                                  borderRadius:
                                      BorderRadius.circular(SizesManager.s4),
                                ),
                                child: Text(
                                  FunctionsManager.formatDate(
                                      states.episodeDetails!.airDate),
                                  style: getMediumTextStyle(
                                    color: ColorsManager.kBlack,
                                    fontSize: FontSizeManager.size16,
                                    letterSpacing: SizesManager.s0,
                                    context: context,
                                  ),
                                ),
                              ),
                              SizedBox(
                                  width:
                                      getWidth(inputWidth: SizesManager.s16)),
                              Row(
                                children: [
                                  const BaseIcon(
                                    iconHeight: SizesManager.s16,
                                    iconPath: IconsManager.starIcon,
                                    iconColor: ColorsManager.voteColor,
                                  ),
                                  SizedBox(
                                      width: getWidth(
                                          inputWidth: SizesManager.s4)),
                                  Text(
                                    (states.episodeDetails!.voteAvg! / 2)
                                        .toStringAsFixed(1),
                                    style: getMediumTextStyle(
                                      fontSize: FontSizeManager.size16,
                                      letterSpacing: SizesManager.s1_2,
                                      context: context,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                              height: getHeight(inputHeight: SizesManager.s20)),
                          Text(
                            states.episodeDetails!.overview.toString(),
                            style: getMediumTextStyle(
                              fontSize: FontSizeManager.size14,
                              letterSpacing: SizesManager.s1_2,
                              context: context,
                            ),
                          ),
                          SizedBox(
                              height: getHeight(inputHeight: SizesManager.s20)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              FunctionsManager.richTextWidget(
                                title: AppLocalizations.of(context)!
                                    .translate(
                                        StringsManager.episodeNumberString)
                                    .toString(),
                                text: states.episodeDetails!.episodeNumber
                                    .toString(),
                                context: context,
                              ),
                              FunctionsManager.richTextWidget(
                                title: AppLocalizations.of(context)!
                                    .translate(
                                        StringsManager.seasonNumberString)
                                    .toString(),
                                text: states.episodeDetails!.seasonNumber
                                    .toString(),
                                context: context,
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: FadeInUp(
                    from: SizesManager.s20,
                    duration: const Duration(
                      milliseconds: ConstsManager.fadeAnimationDuration,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(
                        PaddingManager.p16,
                        PaddingManager.p0,
                        PaddingManager.p16,
                        PaddingManager.p24,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .translate(StringsManager.episodeActorsString)
                                .toString(),
                            style: getBoldTextStyle(
                                fontSize: FontSizeManager.size16,
                                letterSpacing: SizesManager.s0,
                                context: context),
                          ),
                          CreditsEpisodeComponent(
                            episodeActors: states.episodeDetails!.episodeActors,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                // SliverToBoxAdapter(
                //   child: FadeInUp(
                //     from: SizesManager.s20,
                //     duration: const Duration(
                //       milliseconds: ConstsManager.fadeAnimationDuration,
                //     ),
                //     child: ReviewsTv(tvId: widget.tvId),
                //   ),
                // ),
              ],
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: states.errorEpisodeDetailsMessage,
                onPressFunction: () {
                  BlocProvider.of<EpisodeDetailsBloc>(context)
                    ..add(
                      GetEpisodeDetailsEvent(
                        tvId: widget.episodeDetailsRouteParameters.tvId,
                        seasonId:
                            widget.episodeDetailsRouteParameters.seasonNumber,
                        episodeId:
                            widget.episodeDetailsRouteParameters.episodeNumber,
                      ),
                    );
                });
        }
      },
    );
  }
}
