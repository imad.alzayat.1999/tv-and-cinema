import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../domain/entities/episode_actors/episode_actor_entity.dart';

class CreditsEpisodeComponent extends StatelessWidget {
  final List<EpisodeActorEntity> episodeActors;
  const CreditsEpisodeComponent({Key? key, required this.episodeActors})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getHeight(inputHeight: SizesManager.s200),
      child: ListView.separated(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) =>
            getEpisodeActor(episodeActors[index], context),
        separatorBuilder: (context, index) => SizedBox(
          width: getWidth(
            inputWidth: SizesManager.s10,
          ),
        ),
        itemCount: episodeActors.length,
      ),
    );
  }

  Widget getEpisodeActor(
      EpisodeActorEntity episodeActor, BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: getHeight(inputHeight: SizesManager.s120),
          height: getHeight(inputHeight: SizesManager.s120),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(2000),
            child: FadeInImage.assetNetwork(
              image: FunctionsManager.showAPIImage(episodeActor.profilePath),
              placeholder: ImagesManager.logoImage,
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(height: getHeight(inputHeight: SizesManager.s16)),
        Text(
          episodeActor.name.toString(),
          style: getBoldTextStyle(
            fontSize: FontSizeManager.size14,
            letterSpacing: SizesManager.s0,
            context: context,
          ),
        ),
      ],
    );
  }
}
