import 'package:equatable/equatable.dart';

abstract class EpisodeDetailsEvents extends Equatable{

}

class GetEpisodeDetailsEvent extends EpisodeDetailsEvents{
  final int tvId;
  final int seasonId;
  final int episodeId;

  GetEpisodeDetailsEvent({required this.tvId, required this.seasonId, required this.episodeId});
  @override
  // TODO: implement props
  List<Object?> get props => [tvId , seasonId , episodeId];
}