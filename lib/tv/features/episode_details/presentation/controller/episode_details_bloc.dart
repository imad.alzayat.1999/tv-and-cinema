import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/presentation/controller/episode_details_events.dart';

import '../../domain/usecases/get_episode_details_usecase.dart';
import 'episode_details_states.dart';

class EpisodeDetailsBloc
    extends Bloc<EpisodeDetailsEvents, EpisodeDetailsStates> {
  final GetEpisodeDetailsUseCase getEpisodeDetailsUseCase;
  EpisodeDetailsBloc({required this.getEpisodeDetailsUseCase})
      : super(EpisodeDetailsStates()) {
    on<GetEpisodeDetailsEvent>((event, emit) async {
      final failureOrEpisodeDetails = await getEpisodeDetailsUseCase(
        EpisodeDetailsParameters(
          tvId: event.tvId,
          seasonId: event.seasonId,
          episodeId: event.episodeId,
        ),
      );
      failureOrEpisodeDetails.fold((l) {
        emit(
          state.copyWith(
            errorEpisodeDetailsMessage: l.message,
            episodeDetailsRequestStates: RequestStates.error,
          ),
        );
      }, (r) {
        emit(
          state.copyWith(
              episodeDetailsRequestStates: RequestStates.success,
              episodeDetails: r),
        );
      });
    });
  }
}
