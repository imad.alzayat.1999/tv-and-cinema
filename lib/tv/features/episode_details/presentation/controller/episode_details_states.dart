import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import 'package:movie_app_clean_architecture/tv/features/episode_details/domain/entities/episode_details_entity.dart';

class EpisodeDetailsStates extends Equatable {
  final String errorEpisodeDetailsMessage;
  final EpisodeDetailsEntity? episodeDetails;
  final RequestStates episodeDetailsRequestStates;

  EpisodeDetailsStates({
    this.errorEpisodeDetailsMessage = '',
    this.episodeDetails,
    this.episodeDetailsRequestStates = RequestStates.loading,
  });

  EpisodeDetailsStates copyWith({
    String? errorEpisodeDetailsMessage,
    EpisodeDetailsEntity? episodeDetails,
    RequestStates? episodeDetailsRequestStates,
  }) {
    return EpisodeDetailsStates(
      episodeDetails: episodeDetails ?? this.episodeDetails,
      episodeDetailsRequestStates:
          episodeDetailsRequestStates ?? this.episodeDetailsRequestStates,
      errorEpisodeDetailsMessage:
          errorEpisodeDetailsMessage ?? this.errorEpisodeDetailsMessage,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        errorEpisodeDetailsMessage,
        episodeDetailsRequestStates,
        episodeDetails,
      ];
}
