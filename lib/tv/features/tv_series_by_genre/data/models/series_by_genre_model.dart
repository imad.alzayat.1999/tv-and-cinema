import '../../domain/entities/series_by_genre_entity.dart';

class SeriesByGenreModel extends SeriesByGenreEntity {
  SeriesByGenreModel({
    required String? backdropPath,
    required String? name,
    required int? id,
    required String? firstAirDate,
    required num? voteAvg,
  }) : super(
          backdropPath: backdropPath,
          name: name,
          id: id,
          firstAirDate: firstAirDate,
          voteAvg: voteAvg,
        );

  factory SeriesByGenreModel.fromJson(Map<String, dynamic> json) =>
      SeriesByGenreModel(
        backdropPath: json["backdrop_path"],
        name: json["name"],
        id: json["id"],
        firstAirDate: json["first_air_date"],
        voteAvg: json["vote_average"],
      );
}
