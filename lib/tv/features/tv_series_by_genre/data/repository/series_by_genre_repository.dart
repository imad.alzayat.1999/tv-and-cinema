import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import '../../domain/entities/series_by_genre_entity.dart';
import '../../domain/repository/base_series_by_genre_repository.dart';
import '../../domain/usecases/get_genres_tv_usecase.dart';
import '../datasources/series_by_genre_remote_datasource.dart';

class SeriesByGenreRepository extends BaseSeriesByGenreRepository {
  final BaseSeriesByGenreRemoteDataSource baseSeriesByGenreRemoteDataSource;

  SeriesByGenreRepository({required this.baseSeriesByGenreRemoteDataSource});

  @override
  Future<Either<Failure, List<SeriesByGenreEntity>>> getSeriesByGenre(
      {required GenresTvParameters genresTvParameters}) async {
    try {
      final result = await baseSeriesByGenreRemoteDataSource.getSeriesByGenre(
          genresTvParameters: genresTvParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }
}
