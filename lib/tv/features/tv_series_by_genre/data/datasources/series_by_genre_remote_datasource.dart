import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import '../../domain/usecases/get_genres_tv_usecase.dart';
import '../models/series_by_genre_model.dart';

abstract class BaseSeriesByGenreRemoteDataSource {
  Future<List<SeriesByGenreModel>> getSeriesByGenre(
      {required GenresTvParameters genresTvParameters});
}

class SeriesByGenreRemoteDataSource extends BaseSeriesByGenreRemoteDataSource {
  final ApiConsumer apiConsumer;

  SeriesByGenreRemoteDataSource({required this.apiConsumer});

  @override
  Future<List<SeriesByGenreModel>> getSeriesByGenre(
      {required GenresTvParameters genresTvParameters}) async {
    final response = await apiConsumer
        .get(path: ApiConstance.getTvSeriesByGenreEndPoint, queryParameters: {
      "page": genresTvParameters.page,
      "with_genres": genresTvParameters.genre,
    });

    return List<SeriesByGenreModel>.from(
        (response["results"] as List).map((e) => SeriesByGenreModel.fromJson(e)));
  }
}
