import 'package:equatable/equatable.dart';

class SeriesByGenreEntity extends Equatable {
  final String? backdropPath;
  final String? name;
  final int? id;
  final String? firstAirDate;
  final num? voteAvg;

  SeriesByGenreEntity({
    required this.backdropPath,
    required this.name,
    required this.id,
    required this.firstAirDate,
    required this.voteAvg,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [backdropPath , name , id , firstAirDate , voteAvg];
}
