import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import '../entities/series_by_genre_entity.dart';
import '../usecases/get_genres_tv_usecase.dart';

abstract class BaseSeriesByGenreRepository {
  Future<Either<Failure, List<SeriesByGenreEntity>>> getSeriesByGenre(
      {required GenresTvParameters genresTvParameters});
}
