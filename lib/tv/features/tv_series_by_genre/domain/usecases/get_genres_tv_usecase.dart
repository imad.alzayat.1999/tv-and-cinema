import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entities/series_by_genre_entity.dart';
import '../repository/base_series_by_genre_repository.dart';

class GetGenresTvUseCase extends BaseUseCase<List<SeriesByGenreEntity> , GenresTvParameters>{
  final BaseSeriesByGenreRepository baseSeriesByGenreRepository;

  GetGenresTvUseCase({required this.baseSeriesByGenreRepository});
  @override
  Future<Either<Failure, List<SeriesByGenreEntity>>> call(GenresTvParameters parameters) async{
    return await baseSeriesByGenreRepository.getSeriesByGenre(genresTvParameters: parameters);
  }
}

class GenresTvParameters extends Equatable{
  final int page;
  final String genre;

  GenresTvParameters({required this.page, required this.genre});

  @override
  // TODO: implement props
  List<Object?> get props => [page , genre];
}