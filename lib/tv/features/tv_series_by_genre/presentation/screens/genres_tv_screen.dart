import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../components/tv_genre_component.dart';
import '../controller/genres_tv_bloc.dart';
import '../controller/genres_tv_events.dart';
import '../controller/genres_tv_states.dart';

class GenresTvScreen extends StatefulWidget {
  final int genreId;
  final String tvGenre;

  const GenresTvScreen({
    Key? key,
    required this.genreId,
    required this.tvGenre,
  }) : super(key: key);

  @override
  State<GenresTvScreen> createState() => _GenresTvScreenState();
}

class _GenresTvScreenState extends State<GenresTvScreen> {
  String _getGenreName(String? name, BuildContext context) {
    switch (name) {
      case "Action & Adventure":
        return AppLocalizations.of(context)!
            .translate(StringsManager.actionAndAdventureString)!;
      case "Animation":
        return AppLocalizations.of(context)!
            .translate(StringsManager.animationString)!;
      case "Comedy":
        return AppLocalizations.of(context)!
            .translate(StringsManager.comedyString)!;
      case "Crime":
        return AppLocalizations.of(context)!
            .translate(StringsManager.crimeString)!;
      case "Documentary":
        return AppLocalizations.of(context)!
            .translate(StringsManager.documentaryString)!;
      case "Drama":
        return AppLocalizations.of(context)!
            .translate(StringsManager.dramaString)!;
      case "Family":
        return AppLocalizations.of(context)!
            .translate(StringsManager.familyString)!;
      case "Sci-Fi & Fantasy":
        return AppLocalizations.of(context)!
            .translate(StringsManager.scifiAndFantasyString)!;
      case "Kids":
        return AppLocalizations.of(context)!
            .translate(StringsManager.kidsString)!;
      case "News":
        return AppLocalizations.of(context)!
            .translate(StringsManager.newsString)!;
      case "Reality":
        return AppLocalizations.of(context)!
            .translate(StringsManager.realityString)!;
      case "Mystery":
        return AppLocalizations.of(context)!
            .translate(StringsManager.mystreyString)!;
      case "Soap":
        return AppLocalizations.of(context)!
            .translate(StringsManager.soapString)!;
      case "Talk":
        return AppLocalizations.of(context)!
            .translate(StringsManager.talkString)!;
      case "War & Politics":
        return AppLocalizations.of(context)!
            .translate(StringsManager.warAndPoliticsString)!;
      default:
        return AppLocalizations.of(context)!
            .translate(StringsManager.otherString)!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<GenresTvBloc>()
        ..add(
          GetTvByGenreEvent(
            page: 1,
            genre: widget.genreId.toString(),
          ),
        ),
      child: Scaffold(
        backgroundColor: ColorsManager.kEerieBlack,
        appBar: AppBar(
          elevation: SizesManager.s0,
          backgroundColor: ColorsManager.kEerieBlack,
          title: Text(
            _getGenreName(widget.tvGenre, context),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size16,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
        ),
        body: GenresTvContent(genreId: widget.genreId),
      ),
    );
  }
}

class GenresTvContent extends StatefulWidget {
  final int genreId;
  const GenresTvContent({Key? key, required this.genreId}) : super(key: key);

  @override
  State<GenresTvContent> createState() => _GenresTvContentState();
}

class _GenresTvContentState extends State<GenresTvContent> {
  final scrollController = ScrollController();
  int page = 1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    scrollController.addListener(_listener);
  }

  _listener() {
    if (scrollController.position.maxScrollExtent ==
        scrollController.position.pixels) {
      page = page + 1;
      BlocProvider.of<GenresTvBloc>(context)
        ..add(GetTvByGenreEvent(page: page, genre: widget.genreId.toString()));
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    scrollController
      ..removeListener(_listener)
      ..dispose();
  }

  @override
  Widget build(BuildContext context) {
    page = 1;
    return BlocBuilder<GenresTvBloc, GenresTvStates>(
      builder: (context, state) {
        switch (state.tvByGenreRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return SingleChildScrollView(
              key: const Key('topRatedScrollView'),
              controller: scrollController,
              child: Column(
                children: [
                  FadeInUp(
                    from: SizesManager.s20,
                    duration: const Duration(
                      milliseconds: ConstsManager.fadeAnimationDuration,
                    ),
                    child: ListView.separated(
                      padding: EdgeInsets.symmetric(
                        vertical: getHeight(inputHeight: PaddingManager.p16),
                        horizontal: getWidth(inputWidth: PaddingManager.p8),
                      ),
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        if (index >= state.listOfSeries.length) {
                          return BaseLoadingIndicator();
                        }
                        return TvGenreComponent(
                            seriesByGenreEntity: state.listOfSeries[index]);
                      },
                      separatorBuilder: (context, index) => SizedBox(
                        height: getHeight(
                          inputHeight: SizesManager.s20,
                        ),
                      ),
                      itemCount: state.hasReachedMax
                          ? state.listOfSeries.length
                          : state.listOfSeries.length + 1,
                    ),
                  ),
                ],
              ),
            );
          case RequestStates.error:
            return Center(child: Text(state.errorTvByGenreMessage));
        }
      },
    );
  }
}
