import 'package:equatable/equatable.dart';

abstract class GenresTvEvents extends Equatable{

}

class GetTvByGenreEvent extends GenresTvEvents{
  final int page;
  final String genre;

  GetTvByGenreEvent({required this.page , required this.genre});
  @override
  // TODO: implement props
  List<Object?> get props => [page , genre];
}