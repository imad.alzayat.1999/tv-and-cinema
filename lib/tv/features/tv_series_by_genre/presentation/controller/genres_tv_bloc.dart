import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../core/utils/request_states.dart';
import '../../domain/usecases/get_genres_tv_usecase.dart';
import 'genres_tv_events.dart';
import 'genres_tv_states.dart';

class GenresTvBloc extends Bloc<GenresTvEvents , GenresTvStates>{
  final GetGenresTvUseCase getGenresTvUseCase;
  GenresTvBloc({required this.getGenresTvUseCase}) : super(GenresTvStates()){
    on<GetTvByGenreEvent>((event, emit)async{
      if (state.hasReachedMax) return;
      try {
        if (state.tvByGenreRequestStates == RequestStates.loading) {
          final response = await getGenresTvUseCase(
            GenresTvParameters(
              page: event.page,
              genre: event.genre,
            ),
          );
          response.fold((l) {
            emit(
              state.copyWith(
                tvByGenreRequestStates: RequestStates.error,
                errorTvByGenreMessage: l.message,
              ),
            );
          }, (r) {
            r.isEmpty
                ? emit(state.copyWith(hasReachedMax: true))
                : emit(
              state.copyWith(
                tvByGenreRequestStates: RequestStates.success,
                listOfSeries: r,
                hasReachedMax: false,
              ),
            );
          });
        } else {
          final response = await getGenresTvUseCase(
            GenresTvParameters(
              page: event.page,
              genre: event.genre,
            ),
          );
          response.fold((l) {
            emit(
              state.copyWith(
                tvByGenreRequestStates: RequestStates.error,
                errorTvByGenreMessage: l.message,
              ),
            );
          }, (r) {
            r.isEmpty
                ? emit(state.copyWith(hasReachedMax: true))
                : emit(
              state.copyWith(
                tvByGenreRequestStates: RequestStates.success,
                listOfSeries: [...state.listOfSeries, ...r],
                hasReachedMax: false,
              ),
            );
          });
        }
      } catch (e) {
        emit(
          state.copyWith(
            tvByGenreRequestStates: RequestStates.error,
            errorTvByGenreMessage: e.toString(),
          ),
        );
      }
    });
  }

}