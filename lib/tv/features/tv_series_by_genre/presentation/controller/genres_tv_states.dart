import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';

import '../../domain/entities/series_by_genre_entity.dart';

class GenresTvStates extends Equatable {
  final String errorTvByGenreMessage;
  final List<SeriesByGenreEntity> listOfSeries;
  final RequestStates tvByGenreRequestStates;
  final bool hasReachedMax;

  GenresTvStates({
    this.errorTvByGenreMessage = '',
    this.listOfSeries = const [],
    this.tvByGenreRequestStates = RequestStates.loading,
    this.hasReachedMax = false,
  });

  GenresTvStates copyWith({
    String? errorTvByGenreMessage,
    List<SeriesByGenreEntity>? listOfSeries,
    RequestStates? tvByGenreRequestStates,
    bool? hasReachedMax,
  }) {
    return GenresTvStates(
      errorTvByGenreMessage:
          errorTvByGenreMessage ?? this.errorTvByGenreMessage,
      listOfSeries: listOfSeries ?? this.listOfSeries,
      tvByGenreRequestStates:
          tvByGenreRequestStates ?? this.tvByGenreRequestStates,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        hasReachedMax,
        tvByGenreRequestStates,
        listOfSeries,
        errorTvByGenreMessage,
      ];
}
