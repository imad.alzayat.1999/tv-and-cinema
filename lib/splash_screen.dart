import 'dart:async';
import 'package:flutter/material.dart';
import 'core/config/routes_config/routes_config.dart';
import 'core/utils/resources/consts_manager.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);
  static const String routeName = Routes.initRoute;

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _goToNextScreen();
  }

  /// routing function
  _goToNextScreen() {
    Timer(const Duration(seconds: ConstsManager.splashRoutingDuration), () {
      Navigator.pushNamedAndRemoveUntil(context, Routes.onBoardingRoute, (route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Image.asset("assets/images/splash.png"),
      ),
    );
  }
}
