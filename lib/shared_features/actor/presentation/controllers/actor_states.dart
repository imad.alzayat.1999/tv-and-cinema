import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import '../../domain/entities/actor/actor_entity.dart';
import '../../domain/entities/actor_image/actor_images_entity.dart';
import '../../domain/entities/actor_movies/actor_movies.dart';
import '../../domain/entities/actor_series/actor_series_entity.dart';

class ActorStates extends Equatable {
  final String errorActorDetailsMessage;
  final ActorEntity? actorEntity;
  final RequestStates actorDetailsRequestStates;
  final String errorActorImagesMessage;
  final ActorImagesEntity? actorImagesEntity;
  final RequestStates actorImagesRequestStates;
  final ActorMovies? actorMovies;
  final String errorActorMoviesMessage;
  final RequestStates actorMoviesRequestStates;
  final String errorActorSeriesMessage;
  final RequestStates actorSeriesRequestStates;
  final List<ActorSeriesEntity> actorSeries;

  ActorStates({
    this.errorActorDetailsMessage = '',
    this.actorEntity,
    this.actorDetailsRequestStates = RequestStates.loading,
    this.errorActorImagesMessage = '',
    this.actorImagesEntity,
    this.actorImagesRequestStates = RequestStates.loading,
    this.actorMovies,
    this.actorMoviesRequestStates = RequestStates.loading,
    this.errorActorMoviesMessage = '',
    this.actorSeries = const [],
    this.actorSeriesRequestStates = RequestStates.loading,
    this.errorActorSeriesMessage = '',
  });

  ActorStates copyWith({
    String? errorActorDetailsMessage,
    ActorEntity? actorEntity,
    RequestStates? actorDetailsRequestStates,
    String? errorActorImagesMessage,
    ActorImagesEntity? actorImagesEntity,
    RequestStates? actorImagesRequestStates,
    ActorMovies? actorMovies,
    String? errorActorMoviesMessage,
    RequestStates? actorMoviesRequestStates,
    List<ActorSeriesEntity>? actorSeries,
    RequestStates? actorSeriesRequestStates,
    String? errorActorSeriesMessage,
  }) {
    return ActorStates(
      errorActorDetailsMessage:
          errorActorDetailsMessage ?? this.errorActorDetailsMessage,
      actorDetailsRequestStates:
          actorDetailsRequestStates ?? this.actorDetailsRequestStates,
      actorEntity: actorEntity ?? this.actorEntity,
      actorImagesEntity: actorImagesEntity ?? this.actorImagesEntity,
      actorImagesRequestStates:
          actorImagesRequestStates ?? this.actorImagesRequestStates,
      errorActorImagesMessage:
          errorActorImagesMessage ?? this.errorActorImagesMessage,
      actorMovies: actorMovies ?? this.actorMovies,
      actorMoviesRequestStates:
          actorMoviesRequestStates ?? this.actorMoviesRequestStates,
      errorActorMoviesMessage:
          errorActorMoviesMessage ?? this.errorActorMoviesMessage,
      actorSeries: actorSeries ?? this.actorSeries,
      actorSeriesRequestStates:
          actorSeriesRequestStates ?? this.actorSeriesRequestStates,
      errorActorSeriesMessage:
          errorActorSeriesMessage ?? this.errorActorSeriesMessage,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        errorActorDetailsMessage,
        actorEntity,
        actorDetailsRequestStates,
        errorActorImagesMessage,
        actorImagesEntity,
        actorImagesRequestStates,
        actorMovies,
        actorMoviesRequestStates,
        errorActorMoviesMessage,
        actorSeries,
        actorSeriesRequestStates,
        errorActorSeriesMessage,
      ];
}
