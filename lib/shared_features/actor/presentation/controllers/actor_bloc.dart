import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';

import '../../domain/usecases/get_actor_details_usecase.dart';
import '../../domain/usecases/get_actor_images_usecase.dart';
import '../../domain/usecases/get_actor_movies_usecase.dart';
import '../../domain/usecases/get_actor_series_usecase.dart';
import 'actor_events.dart';
import 'actor_states.dart';

class ActorBloc extends Bloc<ActorEvents, ActorStates> {
  final GetActorDetailsUseCase getActorDetailsUseCase;
  final GetActorImagesUseCase getActorImagesUseCase;
  final GetActorMoviesUseCase getActorMoviesUseCase;
  final GetActorSeriesUseCase getActorSeriesUseCase;
  ActorBloc({
    required this.getActorDetailsUseCase,
    required this.getActorImagesUseCase,
    required this.getActorMoviesUseCase,
    required this.getActorSeriesUseCase,
  }) : super(ActorStates()) {
    on<GetActorDetailsEvent>((event, emit) async {
      final failureOfActorDetails = await getActorDetailsUseCase(
          ActorDetailsParameters(actorId: event.actorId, lang: event.lang));
      failureOfActorDetails.fold(
        (l) => emit(
          state.copyWith(
            errorActorDetailsMessage: l.message,
            actorDetailsRequestStates: RequestStates.error,
          ),
        ),
        (r) => emit(
          state.copyWith(
            actorEntity: r,
            actorDetailsRequestStates: RequestStates.success,
          ),
        ),
      );
    });

    on<GetActorImagesEvent>((event, emit) async {
      final failureOfActorImages = await getActorImagesUseCase(
          ActorImagesParameters(actorId: event.actorId));
      failureOfActorImages.fold(
        (l) => emit(
          state.copyWith(
            errorActorImagesMessage: l.message,
            actorImagesRequestStates: RequestStates.error,
          ),
        ),
        (r) => emit(
          state.copyWith(
            actorImagesEntity: r,
            actorImagesRequestStates: RequestStates.success,
          ),
        ),
      );
    });

    on<GetActorMoviesEvent>((event, emit) async {
      final failureOfActorMovies = await getActorMoviesUseCase(
          ActorMoviesParameters(actorId: event.actorId, lang: event.lang));
      failureOfActorMovies.fold(
        (l) => emit(
          state.copyWith(
            errorActorMoviesMessage: l.message,
            actorMoviesRequestStates: RequestStates.error,
          ),
        ),
        (r) => emit(
          state.copyWith(
            actorMovies: r,
            actorMoviesRequestStates: RequestStates.success,
          ),
        ),
      );
    });

    on<GetActorSeriesEvent>((event, emit) async {
      final failureOrActorSeries = await getActorSeriesUseCase(
        ActorSeriesParameters(
          actorId: event.actorId,
          lang: event.lang,
        ),
      );
      failureOrActorSeries.fold(
        (l) => emit(
          state.copyWith(
            errorActorSeriesMessage: l.message,
            actorSeriesRequestStates: RequestStates.error,
          ),
        ),
        (r) => emit(
          state.copyWith(
            actorSeries: r,
            actorSeriesRequestStates: RequestStates.success,
          ),
        ),
      );
    });
  }
}
