import 'package:equatable/equatable.dart';

abstract class ActorEvents extends Equatable{

}

class GetActorDetailsEvent extends ActorEvents{
  final int actorId;
  final String lang;

  GetActorDetailsEvent({required this.actorId , required this.lang});
  @override
  // TODO: implement props
  List<Object?> get props => [actorId , lang];
}
class GetActorImagesEvent extends ActorEvents{
  final int actorId;

  GetActorImagesEvent({required this.actorId});
  @override
  // TODO: implement props
  List<Object?> get props => [actorId];
}
class GetActorMoviesEvent extends ActorEvents{
  final int actorId;
  final String lang;

  GetActorMoviesEvent({required this.actorId , required this.lang});
  @override
  // TODO: implement props
  List<Object?> get props => [actorId , lang];
}
class GetActorSeriesEvent extends ActorEvents{
  final int actorId;
  final String lang;

  GetActorSeriesEvent({required this.actorId, required this.lang});

  @override
  // TODO: implement props
  List<Object?> get props => [actorId , lang];
}