import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/presentation/components/actor_series.dart';

import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../components/actor_images.dart';
import '../components/actor_movies.dart';
import '../components/personal_information.dart';
import '../controllers/actor_bloc.dart';
import '../controllers/actor_events.dart';
import '../controllers/actor_states.dart';

class ActorDetailsScreen extends StatelessWidget {
  final int actorId;
  const ActorDetailsScreen({Key? key, required this.actorId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<ActorBloc>()
        ..add(GetActorDetailsEvent(
            actorId: actorId, lang: FunctionsManager.getLangCode(context)))
        ..add(GetActorImagesEvent(actorId: actorId))
        ..add(GetActorMoviesEvent(
            actorId: actorId, lang: FunctionsManager.getLangCode(context)))
        ..add(GetActorSeriesEvent(
            actorId: actorId, lang: FunctionsManager.getLangCode(context))),
      child: Scaffold(
        backgroundColor: ColorsManager.kEerieBlack,
        body: ActorDetailsContent(actorId: actorId),
      ),
    );
  }
}

class ActorDetailsContent extends StatefulWidget {
  final int actorId;
  const ActorDetailsContent({Key? key, required this.actorId})
      : super(key: key);

  @override
  State<ActorDetailsContent> createState() => _ActorDetailsContentState();
}

class _ActorDetailsContentState extends State<ActorDetailsContent> {
  Alignment getAlignmentByLanguage() {
    if (AppLocalizations.of(context)!.isEnLocale) {
      return Alignment.topLeft;
    } else {
      return Alignment.topRight;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActorBloc, ActorStates>(
      builder: (context, states) {
        switch (states.actorDetailsRequestStates) {
          case RequestStates.loading:
            return const BaseLoadingIndicator();
          case RequestStates.success:
            return CustomScrollView(
              key: const Key('actorDetailsScrollView'),
              shrinkWrap: true,
              slivers: [
                SliverAppBar(
                  pinned: true,
                  expandedHeight: getHeight(inputHeight: SizesManager.s300),
                  backgroundColor: ColorsManager.kEerieBlack,
                  flexibleSpace: FlexibleSpaceBar(
                    title: Text(states.actorEntity!.name!),
                    background: FadeIn(
                      duration: const Duration(
                        milliseconds: ConstsManager.fadeAnimationDuration,
                      ),
                      child: ShaderMask(
                        shaderCallback: (rect) {
                          return const LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              ColorsManager.kTransparent,
                              ColorsManager.kBlack,
                              ColorsManager.kBlack,
                              ColorsManager.kTransparent,
                            ],
                            stops: [0.0, 0.5, 1.0, 1.0],
                          ).createShader(
                            Rect.fromLTRB(0.0, 0.0, rect.width, rect.height),
                          );
                        },
                        blendMode: BlendMode.dstIn,
                        child: Container(
                          color: ColorsManager.kBlack.withOpacity(0.6),
                          child: FadeInImage.assetNetwork(
                            placeholder: ImagesManager.logoImage,
                            width: MediaQuery.of(context).size.width,
                            image: FunctionsManager.showAPIImage(
                                states.actorEntity!.profilePath),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: FadeInUp(
                    from: SizesManager.s20,
                    duration: const Duration(
                        milliseconds: ConstsManager.fadeAnimationDuration),
                    child: Padding(
                      padding: const EdgeInsets.all(PaddingManager.p16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          PersonalInformation(
                            deathDate: states.actorEntity!.deathDate,
                            birthDate: states.actorEntity!.birthDate,
                            placeOfBirth: states.actorEntity!.placeOfBirth,
                            gender: states.actorEntity!.gender,
                          ),
                          SizedBox(
                              height: getHeight(inputHeight: SizesManager.s40)),
                          Text(
                            FunctionsManager.translateText(
                                text: StringsManager.overviewString,
                                context: context),
                            style: getBoldTextStyle(
                              fontSize: FontSizeManager.size20,
                              letterSpacing: SizesManager.s0,
                              context: context,
                            ),
                          ),
                          SizedBox(
                              height: getHeight(
                            inputHeight: SizesManager.s30,
                          )),
                          states.actorEntity!.biography!.isEmpty
                              ? BaseEmptyWidget(
                                  emptyMessage: StringsManager.noOverviewString)
                              : Text(
                                  states.actorEntity!.biography!,
                                  style: getRegularTextStyle(
                                    fontSize: FontSizeManager.size14,
                                    letterSpacing: SizesManager.s0,
                                    context: context,
                                  ),
                                ),
                        ],
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: PaddingManager.p16),
                    child: Column(
                      children: [
                        Align(
                          alignment: getAlignmentByLanguage(),
                          child: Text(
                            FunctionsManager.translateText(
                                text: StringsManager.actorImagesString,
                                context: context),
                            style: getBoldTextStyle(
                              fontSize: FontSizeManager.size20,
                              letterSpacing: SizesManager.s0,
                              context: context,
                            ),
                          ),
                        ),
                        ActorImages(actorId: widget.actorId),
                      ],
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: PaddingManager.p16),
                    child: Column(
                      children: [
                        Align(
                          alignment: getAlignmentByLanguage(),
                          child: Text(
                            FunctionsManager.translateText(
                                text: StringsManager.actorFilmsString,
                                context: context),
                            style: getBoldTextStyle(
                              fontSize: FontSizeManager.size20,
                              letterSpacing: SizesManager.s0,
                              context: context,
                            ),
                          ),
                        ),
                        ActorMovies(actorId: widget.actorId),
                        SizedBox(
                            height: getHeight(inputHeight: SizesManager.s100))
                      ],
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: PaddingManager.p16),
                    child: Column(
                      children: [
                        Align(
                          alignment: getAlignmentByLanguage(),
                          child: Text(
                            FunctionsManager.translateText(
                                text: StringsManager.actorSeriesString,
                                context: context),
                            style: getBoldTextStyle(
                              fontSize: FontSizeManager.size20,
                              letterSpacing: SizesManager.s0,
                              context: context,
                            ),
                          ),
                        ),
                        ActorSeries(actorId: widget.actorId),
                        SizedBox(
                            height: getHeight(inputHeight: SizesManager.s100))
                      ],
                    ),
                  ),
                ),
              ],
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: states.errorActorDetailsMessage,
                onPressFunction: () {
                  BlocProvider.of<ActorBloc>(context)
                    ..add(GetActorDetailsEvent(
                        actorId: widget.actorId,
                        lang: FunctionsManager.getLangCode(context)));
                });
        }
      },
    );
  }
}
