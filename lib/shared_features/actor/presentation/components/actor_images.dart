import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../domain/entities/actor_image/actor_image_entity.dart';
import '../controllers/actor_bloc.dart';
import '../controllers/actor_events.dart';
import '../controllers/actor_states.dart';

class ActorImages extends StatelessWidget {
  final int actorId;
  const ActorImages({Key? key, required this.actorId}) : super(key: key);

  /// get actor image
  Widget getActorImage(ActorImageEntity actor) {
    return AspectRatio(
      aspectRatio: actor.imageAspectRatio!.toDouble(),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(SizesManager.s20),
        child: FadeInImage.assetNetwork(
          image: FunctionsManager.imageUrl(actor.imagePath!),
          placeholder: ImagesManager.logoImage,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActorBloc, ActorStates>(
      builder: (context, states) {
        switch (states.actorImagesRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return FadeInUp(
              from: SizesManager.s20,
              duration: Duration(
                milliseconds: ConstsManager.fadeAnimationDuration,
              ),
              child: states.actorImagesEntity!.listOfActorImages.isEmpty
                  ? BaseEmptyWidget(emptyMessage: StringsManager.noActorImagesString)
                  : GridView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate: ConstsManager.actorImagesGridDelegates,
                      itemBuilder: (context, index) => getActorImage(
                          states.actorImagesEntity!.listOfActorImages[index]),
                      shrinkWrap: true,
                      itemCount:
                          states.actorImagesEntity!.listOfActorImages.length,
                    ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: states.errorActorImagesMessage,
                onPressFunction: () {
                  BlocProvider.of<ActorBloc>(context)
                    ..add(GetActorImagesEvent(actorId: actorId));
                });
        }
      },
    );
  }
}
