import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';

import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../../domain/entities/actor_movies/actor_movie.dart';
import '../controllers/actor_bloc.dart';
import '../controllers/actor_events.dart';
import '../controllers/actor_states.dart';

class ActorMovies extends StatelessWidget {
  final int actorId;
  const ActorMovies({Key? key, required this.actorId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActorBloc, ActorStates>(
      builder: (context, state) {
        switch (state.actorMoviesRequestStates) {
          case RequestStates.loading:
            return const BaseLoadingIndicator();
          case RequestStates.success:
            return FadeInUp(
              from: SizesManager.s20,
              duration: const Duration(
                milliseconds: ConstsManager.fadeAnimationDuration,
              ),
              child: SizedBox(
                height: getHeight(inputHeight: SizesManager.s240),
                child: state.actorMovies!.listOfActorMovies.isEmpty
                    ? BaseEmptyWidget(emptyMessage: StringsManager.noActorMoviesString)
                    : ListView.separated(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemBuilder: (context, index) => GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(
                              context,
                              Routes.movieDetailsRoute,
                              arguments: DetailsRouteParameters(
                                  id: state.actorMovies!
                                      .listOfActorMovies[index].id!),
                            );
                          },
                          child: getActorMovie(
                            actorMovie:
                                state.actorMovies!.listOfActorMovies[index],
                            context: context,
                          ),
                        ),
                        separatorBuilder: (context, index) => SizedBox(
                            width: getWidth(inputWidth: SizesManager.s25)),
                        itemCount: state.actorMovies!.listOfActorMovies.length,
                      ),
              ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorActorMoviesMessage,
                onPressFunction: () {
                  BlocProvider.of<ActorBloc>(context)
                    ..add(GetActorMoviesEvent(
                        actorId: actorId,
                        lang: FunctionsManager.getLangCode(context)));
                });
        }
      },
    );
  }

  /// actor movie item
  Widget getActorMovie(
      {required ActorMovie actorMovie, required BuildContext context}) {
    return SizedBox(
      width: getWidth(inputWidth: SizesManager.s200),
      child: Column(
        children: [
          SizedBox(
            height: getHeight(inputHeight: SizesManager.s150),
            width: getWidth(inputWidth: SizesManager.s200),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(SizesManager.s10),
              child: FadeInImage.assetNetwork(
                image: FunctionsManager.showAPIImage(actorMovie.movieImage),
                placeholder: ImagesManager.logoImage,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s10)),
          Expanded(
            child: Text(
              actorMovie.title!,
              style: getBoldTextStyle(
                fontSize: FontSizeManager.size16,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  const BaseIcon(
                    iconHeight: SizesManager.s20,
                    iconPath: IconsManager.calendarIcon,
                  ),
                  SizedBox(width: getWidth(inputWidth: SizesManager.s4)),
                  Text(
                    FunctionsManager.formatDate(actorMovie!.releaseDate!),
                    style: getRegularTextStyle(
                      fontSize: FontSizeManager.size14,
                      letterSpacing: SizesManager.s0,
                      context: context,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  const BaseIcon(
                    iconHeight: SizesManager.s20,
                    iconPath: IconsManager.starIcon,
                    iconColor: ColorsManager.voteColor,
                  ),
                  SizedBox(width: getWidth(inputWidth: SizesManager.s4)),
                  Text(
                    (actorMovie!.voteAvg!).toStringAsFixed(1),
                    style: getRegularTextStyle(
                      fontSize: FontSizeManager.size14,
                      letterSpacing: SizesManager.s0,
                      context: context,
                    ),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
