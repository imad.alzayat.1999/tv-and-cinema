import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';

import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/utils/resources/values_mananger.dart';

class PersonalInformation extends StatelessWidget {
  final DateTime? birthDate;
  final DateTime? deathDate;
  final String? placeOfBirth;
  final int? gender;

  const PersonalInformation({
    Key? key,
    required this.deathDate,
    required this.birthDate,
    required this.placeOfBirth,
    required this.gender,
  }) : super(key: key);

  /// get personal information state
  Widget _getPersonalInformationState({required BuildContext context}) {
    if (deathDate != null) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            FunctionsManager.translateText(
                text: StringsManager.personalInformationString,
                context: context),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size20,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s30)),
          FunctionsManager.richTextWidget(
            title: FunctionsManager.translateText(
                text: StringsManager.birthDateString, context: context),
            text: FunctionsManager.formatDate(birthDate),
            context: context,
          ),
          SizedBox(
            height: getHeight(inputHeight: SizesManager.s20),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FunctionsManager.richTextWidget(
                title: FunctionsManager.translateText(
                    text: StringsManager.deathDateString, context: context),
                text: FunctionsManager.formatDate(deathDate),
                context: context,
              ),
              FunctionsManager.richTextWidget(
                title: FunctionsManager.translateText(
                    text: StringsManager.ageString, context: context),
                text: FunctionsManager.getAgeOfActor(
                        birth: birthDate!, deathOrToday: deathDate!)
                    .toString(),
                context: context,
              ),
            ],
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s20)),
          FunctionsManager.richTextWidget(
            title: FunctionsManager.translateText(
                text: StringsManager.birthPlaceString, context: context),
            text: placeOfBirth,
            context: context,
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s20)),
          FunctionsManager.richTextWidget(
            title: FunctionsManager.translateText(
                text: StringsManager.genderString, context: context),
            text: FunctionsManager.getActorGender(gender!, context),
            context: context,
          ),
        ],
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            FunctionsManager.translateText(
                text: StringsManager.personalInformationString,
                context: context),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size20,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s30)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FunctionsManager.richTextWidget(
                title: FunctionsManager.translateText(
                    text: StringsManager.birthDateString, context: context),
                text: FunctionsManager.formatDate(birthDate),
                context: context,
              ),
              FunctionsManager.richTextWidget(
                title: FunctionsManager.translateText(
                    text: StringsManager.ageString, context: context),
                text: FunctionsManager.getAgeOfActor(
                        birth: birthDate!, deathOrToday: ConstsManager.nowDate)
                    .toString(),
                context: context,
              ),
            ],
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s20)),
          FunctionsManager.richTextWidget(
            title: FunctionsManager.translateText(
                text: StringsManager.birthPlaceString, context: context),
            text: placeOfBirth ?? "",
            context: context,
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s20)),
          FunctionsManager.richTextWidget(
            title: FunctionsManager.translateText(
                text: StringsManager.genderString, context: context),
            text: FunctionsManager.getActorGender(gender!, context),
            context: context,
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return _getPersonalInformationState(context: context);
  }
}
