import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/presentation/controllers/actor_bloc.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/presentation/controllers/actor_events.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/presentation/controllers/actor_states.dart';

import '../../../../core/config/routes_config/routes_parameters.dart';
import '../../../../core/config/size_config/size_config.dart';
import '../../../../core/utils/global_widgets/base_empty_widget.dart';
import '../../../../core/utils/global_widgets/base_error.dart';
import '../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../core/utils/request_states.dart';
import '../../../../core/utils/resources/assets_manager.dart';
import '../../../../core/utils/resources/colors_manager.dart';
import '../../../../core/utils/resources/consts_manager.dart';
import '../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../core/utils/resources/strings_mananger.dart';
import '../../../../core/utils/resources/style_manager.dart';
import '../../../../core/utils/resources/values_mananger.dart';
import '../../domain/entities/actor_series/actor_series_entity.dart';

class ActorSeries extends StatelessWidget {
  final int actorId;

  const ActorSeries({
    Key? key,
    required this.actorId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActorBloc, ActorStates>(
      builder: (context, state) {
        switch (state.actorSeriesRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return FadeInUp(
              from: SizesManager.s20,
              duration: const Duration(
                milliseconds: ConstsManager.fadeAnimationDuration,
              ),
              child: SizedBox(
                height: getHeight(inputHeight: SizesManager.s240),
                child: state.actorSeries.isEmpty
                    ? BaseEmptyWidget(
                        emptyMessage: StringsManager.noActorSeriesString)
                    : ListView.separated(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemBuilder: (context, index) => getActorSeries(
                          actorSeries: state.actorSeries[index],
                          context: context,
                        ),
                        separatorBuilder: (context, index) => SizedBox(
                            width: getWidth(inputWidth: SizesManager.s25)),
                        itemCount: state.actorSeries.length,
                      ),
              ),
            );
          case RequestStates.error:
            return BaseError(
              errorMessage: state.errorActorSeriesMessage,
              onPressFunction: () {
                BlocProvider.of<ActorBloc>(context)
                  ..add(
                    GetActorSeriesEvent(
                      actorId: actorId,
                      lang: FunctionsManager.getLangCode(context),
                    ),
                  );
              },
            );
        }
      },
    );
  }

  /// actor movie item
  Widget getActorSeries(
      {required ActorSeriesEntity actorSeries, required BuildContext context}) {
    return SizedBox(
      width: getWidth(inputWidth: SizesManager.s200),
      child: Column(
        children: [
          SizedBox(
            height: getHeight(inputHeight: SizesManager.s150),
            width: getWidth(inputWidth: SizesManager.s200),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(SizesManager.s10),
              child: FadeInImage.assetNetwork(
                image: FunctionsManager.showAPIImage(actorSeries.backdropPath),
                placeholder: ImagesManager.logoImage,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s10)),
          Expanded(
            child: Text(
              actorSeries.name.toString(),
              style: getBoldTextStyle(
                fontSize: FontSizeManager.size16,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  const BaseIcon(
                    iconHeight: SizesManager.s20,
                    iconPath: IconsManager.calendarIcon,
                  ),
                  SizedBox(width: getWidth(inputWidth: SizesManager.s4)),
                  Text(
                    FunctionsManager.formatDate(actorSeries.firstAirDate),
                    style: getRegularTextStyle(
                      fontSize: FontSizeManager.size14,
                      letterSpacing: SizesManager.s0,
                      context: context,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  const BaseIcon(
                    iconHeight: SizesManager.s20,
                    iconPath: IconsManager.starIcon,
                    iconColor: ColorsManager.voteColor,
                  ),
                  SizedBox(width: getWidth(inputWidth: SizesManager.s4)),
                  Text(
                    (actorSeries.voteAvg!).toStringAsFixed(1),
                    style: getRegularTextStyle(
                      fontSize: FontSizeManager.size14,
                      letterSpacing: SizesManager.s0,
                      context: context,
                    ),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
