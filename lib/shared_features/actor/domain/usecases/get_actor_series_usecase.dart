import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/domain/entities/actor_series/actor_series_entity.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/domain/repository/base_actor_repository.dart';

class GetActorSeriesUseCase extends BaseUseCase<List<ActorSeriesEntity> , ActorSeriesParameters>{
  final BaseActorRepository baseActorRepository;

  GetActorSeriesUseCase({required this.baseActorRepository});
  @override
  Future<Either<Failure, List<ActorSeriesEntity>>> call(ActorSeriesParameters parameters) async{
    return await baseActorRepository.getActorSeries(parameters);
  }
}

class ActorSeriesParameters extends Equatable{
  final int actorId;
  final String lang;

  ActorSeriesParameters({required this.actorId , required this.lang});

  @override
  // TODO: implement props
  List<Object?> get props => [actorId , lang];
}