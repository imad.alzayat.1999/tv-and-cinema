import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entities/actor/actor_entity.dart';
import '../repository/base_actor_repository.dart';

class GetActorDetailsUseCase extends BaseUseCase<ActorEntity , ActorDetailsParameters>{
  final BaseActorRepository baseActorRepository;

  GetActorDetailsUseCase({required this.baseActorRepository});

  @override
  Future<Either<Failure, ActorEntity>> call(ActorDetailsParameters parameters) async{
    return await baseActorRepository.getActorDetails(parameters);
  }
}

class ActorDetailsParameters extends Equatable{
  final int actorId;
  final String lang;

  ActorDetailsParameters({required this.actorId , required this.lang});

  @override
  // TODO: implement props
  List<Object?> get props => [actorId , lang];
}