import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../entities/actor_movies/actor_movies.dart';
import '../repository/base_actor_repository.dart';

class GetActorMoviesUseCase extends BaseUseCase<ActorMovies , ActorMoviesParameters>{
  final BaseActorRepository baseActorRepository;

  GetActorMoviesUseCase({required this.baseActorRepository});

  @override
  Future<Either<Failure, ActorMovies>> call(ActorMoviesParameters parameters) async{
    return await baseActorRepository.getActorMovies(parameters);
  }
}
class ActorMoviesParameters extends Equatable{
  final int actorId;
  final String lang;

  ActorMoviesParameters({required this.actorId , required this.lang});

  @override
  // TODO: implement props
  List<Object?> get props => [actorId , lang];
}