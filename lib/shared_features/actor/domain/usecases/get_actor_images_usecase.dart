import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entities/actor_image/actor_images_entity.dart';
import '../repository/base_actor_repository.dart';

class GetActorImagesUseCase extends BaseUseCase<ActorImagesEntity , ActorImagesParameters>{
  final BaseActorRepository baseActorRepository;

  GetActorImagesUseCase({required this.baseActorRepository});
  @override
  Future<Either<Failure, ActorImagesEntity>> call(ActorImagesParameters parameters) async{
    return await baseActorRepository.getActorImages(parameters);
  }
}

class ActorImagesParameters extends Equatable{
  final int actorId;

  ActorImagesParameters({required this.actorId});

  @override
  // TODO: implement props
  List<Object?> get props => [actorId];
}