import 'package:equatable/equatable.dart';

class ActorSeriesEntity extends Equatable {
  final String? originalName;
  final String? character;
  final String? name;
  final String? backdropPath;
  final num? voteAvg;
  final DateTime? firstAirDate;

  ActorSeriesEntity({
    required this.originalName,
    required this.character,
    required this.name,
    required this.backdropPath,
    required this.voteAvg,
    required this.firstAirDate,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        originalName,
        character,
        name,
        backdropPath,
        voteAvg,
        firstAirDate,
      ];
}
