import 'package:equatable/equatable.dart';

import 'actor_movie.dart';

class ActorMovies extends Equatable{
  final List<ActorMovie> listOfActorMovies;

  ActorMovies({required this.listOfActorMovies});

  @override
  // TODO: implement props
  List<Object?> get props => [listOfActorMovies];
}