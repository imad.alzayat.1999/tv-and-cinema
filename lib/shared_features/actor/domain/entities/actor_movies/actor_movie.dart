import 'package:equatable/equatable.dart';

class ActorMovie extends Equatable {
  final String? movieImage;
  final int? id;
  final String? title;
  final DateTime? releaseDate;
  final String? character;
  final num? voteAvg;

  ActorMovie({
    required this.movieImage,
    required this.id,
    required this.title,
    required this.releaseDate,
    required this.character,
    required this.voteAvg,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [movieImage, id, title, releaseDate, character , voteAvg,];
}
