import 'package:equatable/equatable.dart';

import 'actor_image_entity.dart';

class ActorImagesEntity extends Equatable{
  final List<ActorImageEntity> listOfActorImages;

  ActorImagesEntity({required this.listOfActorImages});

  @override
  // TODO: implement props
  List<Object?> get props => [listOfActorImages];
}