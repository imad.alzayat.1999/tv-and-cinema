import 'package:equatable/equatable.dart';

class ActorImageEntity extends Equatable{
  final num? imageAspectRatio;
  final String? imagePath;

  ActorImageEntity({required this.imageAspectRatio, required this.imagePath});

  @override
  // TODO: implement props
  List<Object?> get props => [imageAspectRatio , imagePath];
}