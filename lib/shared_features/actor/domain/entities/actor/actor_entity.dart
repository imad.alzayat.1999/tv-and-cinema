import 'package:equatable/equatable.dart';

class ActorEntity extends Equatable {
  final String? biography;
  final String? name;
  final String? profilePath;
  final String? knownForDepartment;
  final DateTime? birthDate;
  final DateTime? deathDate;
  final int? gender;
  final String? placeOfBirth;
  final int? id;
  final num? popularity;

  ActorEntity({
    required this.biography,
    required this.name,
    required this.profilePath,
    required this.knownForDepartment,
    required this.birthDate,
    required this.deathDate,
    required this.gender,
    required this.placeOfBirth,
    required this.id,
    required this.popularity,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        biography,
        name,
        profilePath,
        knownForDepartment,
        birthDate,
        deathDate,
        gender,
        placeOfBirth,
        id,
        popularity
      ];
}
