import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/domain/entities/actor_series/actor_series_entity.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/domain/usecases/get_actor_series_usecase.dart';
import '../entities/actor/actor_entity.dart';
import '../entities/actor_image/actor_images_entity.dart';
import '../entities/actor_movies/actor_movies.dart';
import '../usecases/get_actor_details_usecase.dart';
import '../usecases/get_actor_images_usecase.dart';
import '../usecases/get_actor_movies_usecase.dart';

abstract class BaseActorRepository{
  Future<Either<Failure , ActorEntity>> getActorDetails(ActorDetailsParameters actorDetailsParameters);
  Future<Either<Failure , ActorImagesEntity>> getActorImages(ActorImagesParameters actorImagesParameters);
  Future<Either<Failure , ActorMovies>> getActorMovies(ActorMoviesParameters actorMoviesParameters);
  Future<Either<Failure , List<ActorSeriesEntity>>> getActorSeries(ActorSeriesParameters actorSeriesParameters);
}