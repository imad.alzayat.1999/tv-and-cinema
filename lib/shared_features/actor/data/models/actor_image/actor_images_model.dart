import '../../../domain/entities/actor_image/actor_image_entity.dart';
import '../../../domain/entities/actor_image/actor_images_entity.dart';
import 'actor_image_model.dart';

class ActorImagesModel extends ActorImagesEntity {
  ActorImagesModel({required List<ActorImageEntity> listOfActorImages})
      : super(listOfActorImages: listOfActorImages);

  factory ActorImagesModel.fromJson(Map<String, dynamic> json) =>
      ActorImagesModel(
        listOfActorImages: List<ActorImageModel>.from(
          json["profiles"].map((x) => ActorImageModel.fromJson(x)),
        ),
      );

  Map<String, dynamic> toJson() => {
        "profiles": List<dynamic>.from(listOfActorImages.map((x) => x)),
      };
}
