import '../../../domain/entities/actor_image/actor_image_entity.dart';

class ActorImageModel extends ActorImageEntity {
  ActorImageModel({required num? imageAspectRatio, required String? imagePath})
      : super(imageAspectRatio: imageAspectRatio, imagePath: imagePath);

  factory ActorImageModel.fromJson(Map<String, dynamic> json) =>
      ActorImageModel(imageAspectRatio: json["aspect_ratio"], imagePath: json["file_path"]);

  Map<String , dynamic> toJson() => {
    "aspect_ratio" : imageAspectRatio,
    "file_path":imagePath,
  };
}
