import 'package:movie_app_clean_architecture/shared_features/actor/domain/entities/actor_series/actor_series_entity.dart';

class ActorSeriesModel extends ActorSeriesEntity {
  ActorSeriesModel({
    required String? originalName,
    required String? character,
    required String? name,
    required String? backdropPath,
    required num? voteAvg,
    required DateTime? firstAirDate,
  }) : super(
          originalName: originalName,
          character: character,
          name: name,
          backdropPath: backdropPath,
          voteAvg: voteAvg,
          firstAirDate: firstAirDate,
        );

  factory ActorSeriesModel.fromJson(Map<String, dynamic> json) =>
      ActorSeriesModel(
        originalName: json["original_name"],
        character: json["character"] ,
        name: json["name"],
        backdropPath: json["backdrop_path"],
        voteAvg: json["vote_average"],
        firstAirDate: json["first_air_date"] == '' ? DateTime.now() : DateTime.parse(json["first_air_date"]),
      );
}
