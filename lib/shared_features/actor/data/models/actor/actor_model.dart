import 'package:intl/intl.dart';
import '../../../domain/entities/actor/actor_entity.dart';

class ActorModel extends ActorEntity {
  ActorModel({
    required String? biography,
    required String? name,
    required String? profilePath,
    required String? knownForDepartment,
    required DateTime? birthDate,
    required DateTime? deathDate,
    required int? gender,
    required String? placeOfBirth,
    required int? id,
    required num? popularity,
  }) : super(
          biography: biography,
          name: name,
          profilePath: profilePath,
          knownForDepartment: knownForDepartment,
          birthDate: birthDate,
          deathDate: deathDate,
          gender: gender,
          placeOfBirth: placeOfBirth,
          id: id,
          popularity: popularity,
        );

  factory ActorModel.fromJson(Map<String, dynamic> json) => ActorModel(
        biography: json["biography"],
        name: json["name"],
        profilePath: json["profile_path"],
        knownForDepartment: json["known_for_department"],
        birthDate: DateTime.parse(json["birthday"]),
        deathDate: json["deathday"] == null ? null : DateTime.parse(json["deathday"]),
        gender: json["gender"],
        placeOfBirth: json["place_of_birth"],
        id: json["id"],
        popularity: json["popularity"],
      );

  Map<String, dynamic> toJson() => {
        "biography": biography,
        "name": name,
        "profile_path": profilePath,
        "known_for_department": knownForDepartment,
        "gender": gender,
        "place_of_birth": placeOfBirth,
        "id": id,
        "popularity": popularity,
        "birthday": DateFormat.yMMMMd('en_US').format(birthDate!),
        "deathday": DateFormat.yMMMMd('en_US').format(deathDate!),
      };
}
