import 'package:intl/intl.dart';
import '../../../domain/entities/actor_movies/actor_movie.dart';

class ActorMovieModel extends ActorMovie {
  ActorMovieModel({
    required String? movieImage,
    required int? id,
    required String? title,
    required DateTime? releaseDate,
    required String? character,
    required num? voteAvg,
  }) : super(
          movieImage: movieImage,
          id: id,
          title: title,
          releaseDate: releaseDate,
          character: character,
          voteAvg: voteAvg,
        );

  factory ActorMovieModel.fromJson(Map<String, dynamic> json) =>
      ActorMovieModel(
          movieImage: json["backdrop_path"],
          id: json["id"],
          title: json["title"],
          releaseDate: json["release_date"] == "" ? DateTime.now() :DateTime.parse(json["release_date"]),
          character: json["character"],
          voteAvg: json["vote_average"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "backdrop_path": movieImage,
        "title": title,
        "release_date": DateFormat.y().format(releaseDate!),
        "character": character,
        "vote_average": voteAvg,
      };
}
