import '../../../domain/entities/actor_movies/actor_movie.dart';
import '../../../domain/entities/actor_movies/actor_movies.dart';
import 'actor_movie_model.dart';

class ActorMoviesModel extends ActorMovies {
  ActorMoviesModel({required List<ActorMovie> listOfActorMovies})
      : super(listOfActorMovies: listOfActorMovies);

  factory ActorMoviesModel.fromJson(Map<String, dynamic> json) =>
      ActorMoviesModel(
        listOfActorMovies: List<ActorMovieModel>.from(
          json["cast"].map((x) => ActorMovieModel.fromJson(x)),
        ),
      );

  Map<String, dynamic> toJson() =>
      {"cast": List<dynamic>.from(listOfActorMovies.map((x) => x))};
}
