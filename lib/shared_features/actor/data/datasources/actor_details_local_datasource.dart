import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../../core/services/services_locator.dart';
import '../models/actor/actor_model.dart';
import '../models/actor_image/actor_images_model.dart';
import '../models/actor_movies/actor_movies_model.dart';

abstract class BaseActorDetailsLocalDataSource {
  Future<Unit> cacheActorDetails(ActorModel actor);
  Future<ActorModel> getCachedActorDetails();
  Future<Unit> cacheActorImages(ActorImagesModel actorImages);
  Future<ActorImagesModel> getCachedActorImages();
  Future<Unit> cacheActorMovies(ActorMoviesModel actorMovies);
  Future<ActorMoviesModel> getCachedActorMovies();
}

class ActorDetailsLocalDataSource extends BaseActorDetailsLocalDataSource {
  @override
  Future<Unit> cacheActorDetails(ActorModel actor) {
    getIt<SharedPreferences>().setString(
        ConstsManager.CACHE_ACTOR_DETAILS, jsonEncode(actor.toJson()));
    return Future.value(unit);
  }

  @override
  Future<ActorModel> getCachedActorDetails() {
    final cachedData =
        getIt<SharedPreferences>().getString(ConstsManager.CACHE_ACTOR_DETAILS);
    final decoded = jsonDecode(cachedData!);
    return Future.value(ActorModel.fromJson(decoded));
  }

  @override
  Future<Unit> cacheActorImages(ActorImagesModel actorImages) {
    getIt<SharedPreferences>().setString(
        ConstsManager.CACHE_ACTOR_IMAGES, jsonEncode(actorImages.toJson()));
    return Future.value(unit);
  }

  @override
  Future<ActorImagesModel> getCachedActorImages() {
    final cachedData = getIt<SharedPreferences>().getString(ConstsManager.CACHE_ACTOR_IMAGES);
    final decoded = jsonDecode(cachedData!);
    return Future.value(ActorImagesModel.fromJson(decoded));
  }

  @override
  Future<Unit> cacheActorMovies(ActorMoviesModel actorMovies) {
    getIt<SharedPreferences>().setString(
        ConstsManager.CACHE_ACTOR_MOVIES, jsonEncode(actorMovies.toJson()));
    return Future.value(unit);
  }

  @override
  Future<ActorMoviesModel> getCachedActorMovies() {
    final cachedData = getIt<SharedPreferences>().getString(ConstsManager.CACHE_ACTOR_MOVIES);
    final decoded = jsonDecode(cachedData!);
    return Future.value(ActorMoviesModel.fromJson(decoded));
  }
}
