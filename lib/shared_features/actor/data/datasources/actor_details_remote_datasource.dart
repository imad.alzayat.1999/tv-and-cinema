import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/domain/usecases/get_actor_series_usecase.dart';

import '../../domain/usecases/get_actor_details_usecase.dart';
import '../../domain/usecases/get_actor_images_usecase.dart';
import '../../domain/usecases/get_actor_movies_usecase.dart';
import '../models/actor/actor_model.dart';
import '../models/actor_image/actor_images_model.dart';
import '../models/actor_movies/actor_movies_model.dart';
import '../models/actor_series/actor_series_model.dart';

abstract class BaseActorDetailsRemoteDataSource {
  Future<ActorModel> getActorDetails(
      ActorDetailsParameters actorDetailsParameters);
  Future<ActorImagesModel> getActorImages(
      ActorImagesParameters actorImagesParameters);
  Future<ActorMoviesModel> getActorMovies(
      ActorMoviesParameters actorMoviesParameters);
  Future<List<ActorSeriesModel>> getActorSeries(
      ActorSeriesParameters actorSeriesParameters);
}

class ActorDetailsRemoteDataSource extends BaseActorDetailsRemoteDataSource {
  final ApiConsumer apiConsumer;

  ActorDetailsRemoteDataSource({required this.apiConsumer});

  @override
  Future<ActorModel> getActorDetails(
      ActorDetailsParameters actorDetailsParameters) async {
    final response = await apiConsumer.get(
        path: ApiConstance.getActorInformationEndPoint(
          actorDetailsParameters.actorId,
        ),
        queryParameters: {
          "language": actorDetailsParameters.lang,
        });
    return ActorModel.fromJson(response);
  }

  @override
  Future<ActorImagesModel> getActorImages(
      ActorImagesParameters actorImagesParameters) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getActorImagesEndPoint(
        actorImagesParameters.actorId,
      ),
    );

    return ActorImagesModel.fromJson(response);
  }

  @override
  Future<ActorMoviesModel> getActorMovies(
      ActorMoviesParameters actorMoviesParameters) async {
    final response = await apiConsumer.get(
        path: ApiConstance.getActorMoviesEndPoint(
          actorMoviesParameters.actorId,
        ),
        queryParameters: {
          "language": actorMoviesParameters.lang,
        });
    return ActorMoviesModel.fromJson(response);
  }

  @override
  Future<List<ActorSeriesModel>> getActorSeries(
      ActorSeriesParameters actorSeriesParameters) async {
    final response = await apiConsumer.get(
        path: ApiConstance.getActorSeriesEndPoint(
          actorId: actorSeriesParameters.actorId,
        ),
        queryParameters: {
          "language": actorSeriesParameters.lang,
        });
    return List<ActorSeriesModel>.from(
        (response["cast"] as List).map((e) => ActorSeriesModel.fromJson(e)));
  }
}
