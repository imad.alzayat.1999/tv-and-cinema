import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/domain/entities/actor_series/actor_series_entity.dart';
import 'package:movie_app_clean_architecture/shared_features/actor/domain/usecases/get_actor_series_usecase.dart';
import '../../../../../core/network/network_info.dart';
import '../../domain/entities/actor/actor_entity.dart';
import '../../domain/entities/actor_image/actor_images_entity.dart';
import '../../domain/entities/actor_movies/actor_movies.dart';
import '../../domain/repository/base_actor_repository.dart';
import '../../domain/usecases/get_actor_details_usecase.dart';
import '../../domain/usecases/get_actor_images_usecase.dart';
import '../../domain/usecases/get_actor_movies_usecase.dart';
import '../datasources/actor_details_local_datasource.dart';
import '../datasources/actor_details_remote_datasource.dart';

class ActorRepository extends BaseActorRepository {
  final BaseActorDetailsRemoteDataSource baseActorDetailsRemoteDataSource;
  final BaseActorDetailsLocalDataSource baseActorDetailsLocalDataSource;
  final NetworkInfo networkInfo;

  ActorRepository({
    required this.baseActorDetailsRemoteDataSource,
    required this.baseActorDetailsLocalDataSource,
    required this.networkInfo,
  });
  @override
  Future<Either<Failure, ActorEntity>> getActorDetails(
      ActorDetailsParameters actorDetailsParameters) async {
    try {
      final result = await baseActorDetailsRemoteDataSource
          .getActorDetails(actorDetailsParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, ActorImagesEntity>> getActorImages(
      ActorImagesParameters actorImagesParameters) async {
    try {
      final result = await baseActorDetailsRemoteDataSource
          .getActorImages(actorImagesParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, ActorMovies>> getActorMovies(
      ActorMoviesParameters actorMoviesParameters) async {
    try {
      final result = await baseActorDetailsRemoteDataSource
          .getActorMovies(actorMoviesParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<ActorSeriesEntity>>> getActorSeries(ActorSeriesParameters actorSeriesParameters) async{
    try{
      final result = await baseActorDetailsRemoteDataSource.getActorSeries(actorSeriesParameters);
      return Right(result);
    } on ServerException catch (e){
      return Left(ServerFailure(e.message));
    }
  }
}
