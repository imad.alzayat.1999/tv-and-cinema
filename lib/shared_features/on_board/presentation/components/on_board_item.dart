import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../../core/config/size_config/size_config.dart';
import '../../domain/entities/on_board_entity.dart';

class OnBoardItem extends StatelessWidget {
  final OnBoardEntity onBoardEntity;
  const OnBoardItem({Key? key, required this.onBoardEntity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(
          onBoardEntity.imageUrl,
          width: getWidth(inputWidth: onBoardEntity.widthOfImage),
          height: getHeight(inputHeight: onBoardEntity.heightOfImage),
        ),
        SizedBox(height: getHeight(inputHeight: onBoardEntity.distance)),
        Text(
          FunctionsManager.translateText(
              text: onBoardEntity.title, context: context),
          style: getRegularTextStyle(
            context: context,
            fontSize: FontSizeManager.size16,
            letterSpacing: SizesManager.s0_15,
          ),
        ),
        SizedBox(height: getHeight(inputHeight: SizesManager.s20)),
        Text(
          FunctionsManager.translateText(
              text: onBoardEntity.subTitle, context: context),
          textAlign: TextAlign.center,
          style: getRegularTextStyle(
            context: context,
            fontSize: FontSizeManager.size12,
            letterSpacing: SizesManager.s0_15,
          ),
        ),
      ],
    );
  }
}
