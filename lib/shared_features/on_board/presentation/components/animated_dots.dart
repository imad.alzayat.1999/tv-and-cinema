import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../../core/config/size_config/size_config.dart';
import '../../../../core/utils/resources/colors_manager.dart';
import '../../../../core/utils/resources/consts_manager.dart';

class AnimatedDots extends StatelessWidget {
  final int currentIndex;
  final int elementIndex;
  const AnimatedDots(
      {Key? key, required this.currentIndex, required this.elementIndex})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(
        milliseconds: ConstsManager.onBoardTransitionDuration,
      ),
      height: getHeight(inputHeight: SizesManager.s10),
      width: getHeight(inputHeight: SizesManager.s10),
      margin: const EdgeInsets.only(right: MarginManager.m10),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: currentIndex == elementIndex
            ? ColorsManager.kVeryLightBlue
            : ColorsManager.kVeryLightBlue.withOpacity(0.5),
      ),
    );
  }
}
