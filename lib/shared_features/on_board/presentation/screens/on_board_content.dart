import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../../core/config/routes_config/routes_config.dart';
import '../../../../core/config/size_config/size_config.dart';
import '../../../../core/utils/resources/strings_mananger.dart';
import '../components/animated_dots.dart';
import '../components/on_board_item.dart';

class OnBoardContent extends StatefulWidget {
  const OnBoardContent({Key? key}) : super(key: key);

  @override
  State<OnBoardContent> createState() => _OnBoardContentState();
}

class _OnBoardContentState extends State<OnBoardContent> {
  PageController? _pageController;
  int _currentIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController(initialPage: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          flex: 4,
          child: PageView.builder(
            onPageChanged: (index) {
              setState(() {
                _currentIndex = index;
              });
            },
            controller: _pageController,
            itemBuilder: (context, index) => OnBoardItem(
              onBoardEntity: ConstsManager.onBoardItems[index],
            ),
            itemCount: ConstsManager.onBoardItems.length,
          ),
        ),
        Expanded(
          child: Column(
            children: [
              _currentIndex == ConstsManager.onBoardItems.length - 1
                  ? TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, Routes.mainRoute);
                      },
                      child: Text(
                        FunctionsManager.translateText(
                            text: StringsManager.getStartedString,
                            context: context),
                        textAlign: TextAlign.center,
                        style: getBoldTextStyle(
                          fontSize: FontSizeManager.size16,
                          letterSpacing: SizesManager.s1,
                          context: context,
                        ),
                      ),
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, Routes.mainRoute);
                          },
                          child: Text(
                            FunctionsManager.translateText(
                                text: StringsManager.skipString,
                                context: context),
                            style: getBoldTextStyle(
                              fontSize: FontSizeManager.size16,
                              letterSpacing: SizesManager.s1,
                              context: context,
                            ),
                          ),
                        ),
                        TextButton(
                          onPressed: () => _pageController!.nextPage(
                            duration: const Duration(
                              milliseconds:
                                  ConstsManager.onBoardTransitionDuration,
                            ),
                            curve: Curves.easeInOut,
                          ),
                          child: Text(
                            FunctionsManager.translateText(
                                text: StringsManager.nextString,
                                context: context),
                            style: getBoldTextStyle(
                              fontSize: FontSizeManager.size16,
                              letterSpacing: SizesManager.s1,
                              context: context,
                            ),
                          ),
                        ),
                      ],
                    ),
              SizedBox(height: getHeight(inputHeight: SizesManager.s40)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(
                  ConstsManager.onBoardItems.length,
                  (index) => AnimatedDots(
                    currentIndex: _currentIndex,
                    elementIndex: index,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
