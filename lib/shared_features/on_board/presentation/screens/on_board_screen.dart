import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../../core/config/size_config/size_config.dart';
import 'on_board_content.dart';



class OnBoardScreen extends StatefulWidget {
  const OnBoardScreen({Key? key}) : super(key: key);

  @override
  State<OnBoardScreen> createState() => _OnBoardScreenState();
}

class _OnBoardScreenState extends State<OnBoardScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsManager.kEerieBlack,
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: getWidth(inputWidth: PaddingManager.p40),
          vertical: getHeight(inputHeight: PaddingManager.p20),
        ),
        child: const OnBoardContent(),
      ),
    );
  }
}
