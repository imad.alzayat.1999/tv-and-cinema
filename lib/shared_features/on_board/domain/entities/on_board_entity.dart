class OnBoardEntity {
  final String imageUrl;
  final String title;
  final String subTitle;
  final double widthOfImage;
  final double heightOfImage;
  final double distance;

  OnBoardEntity({
    required this.imageUrl,
    required this.title,
    required this.subTitle,
    required this.widthOfImage,
    required this.heightOfImage,
    required this.distance,
  });
}
