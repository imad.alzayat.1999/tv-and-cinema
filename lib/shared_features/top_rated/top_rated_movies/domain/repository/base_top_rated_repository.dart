import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import '../entities/top_rated.dart';
import '../usecase/get_top_rated_usecase.dart';

abstract class BaseTopRatedRepository{
  Future<Either<Failure , List<TopRated>>> getTopRatedMovies(TopRatedMoviesParameters topRatedMoviesParameters);
}