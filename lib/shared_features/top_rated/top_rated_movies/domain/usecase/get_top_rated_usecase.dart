import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../entities/top_rated.dart';
import '../repository/base_top_rated_repository.dart';

class GetTopRatedUseCase extends BaseUseCase<List<TopRated> , TopRatedMoviesParameters>{
  final BaseTopRatedRepository baseTopRatedRepository;

  GetTopRatedUseCase({required this.baseTopRatedRepository});

  @override
  Future<Either<Failure, List<TopRated>>> call(TopRatedMoviesParameters parameters) async{
    return await baseTopRatedRepository.getTopRatedMovies(parameters);
  }
}
class TopRatedMoviesParameters extends Equatable{
  final int page;
  final String language;

  const TopRatedMoviesParameters({required this.page , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [page , language];
}