import 'package:equatable/equatable.dart';

class TopRated extends Equatable {
  final int? id;
  final String? movieImage;
  final String? movieTitle;
  final num? voteAvg;
  final DateTime? movieReleaseDate;

  const TopRated({
    required this.id,
    required this.movieImage,
    required this.movieTitle,
    required this.voteAvg,
    required this.movieReleaseDate,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [id , movieImage , movieTitle , voteAvg , movieReleaseDate];
}
