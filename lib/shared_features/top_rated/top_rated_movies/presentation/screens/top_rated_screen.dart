import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_icon.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_error.dart';
import '../../../../../core/utils/request_states.dart';
import '../components/top_rated_component.dart';
import '../controllers/top_rated_bloc.dart';
import '../controllers/top_rated_events.dart';
import '../controllers/top_rated_states.dart';

class TopRatedScreen extends StatelessWidget {
  const TopRatedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<TopRatedBloc>()
        ..add(GetAllTopRatedEvent(
            page: 1, language: FunctionsManager.getLangCode(context))),
      child: Scaffold(
        backgroundColor: ColorsManager.kEerieBlack,
        appBar: AppBar(
          elevation: SizesManager.s0,
          backgroundColor: ColorsManager.kEerieBlack,
          title: Text(
            FunctionsManager.translateText(
                text: StringsManager.topRatedString, context: context),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size16,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
        ),
        body: const TopRatedContent(),
      ),
    );
  }
}

class TopRatedContent extends StatefulWidget {
  const TopRatedContent({Key? key}) : super(key: key);

  @override
  State<TopRatedContent> createState() => _TopRatedContentState();
}

class _TopRatedContentState extends State<TopRatedContent> {
  final scrollController = ScrollController();
  int page = 1;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(_listener);
  }

  @override
  void dispose() {
    scrollController
      ..removeListener(_listener)
      ..dispose();
    super.dispose();
  }

  _listener() {
    if (scrollController.position.maxScrollExtent ==
        scrollController.position.pixels) {
      page = page + 1;
      BlocProvider.of<TopRatedBloc>(context).add(GetAllTopRatedEvent(
          page: page, language: FunctionsManager.getLangCode(context)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TopRatedBloc, TopRatedStates>(
      builder: (context, state) {
        switch (state.requestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return state.topRated.isEmpty
                ? BaseEmptyWidget(
                    emptyMessage: StringsManager.noTopRatedFilmsString)
                : SingleChildScrollView(
                    key: const Key('topRatedScrollView'),
                    controller: scrollController,
                    child: Column(
                      children: [
                        FadeInUp(
                          from: SizesManager.s20,
                          duration: const Duration(
                            milliseconds: ConstsManager.fadeAnimationDuration,
                          ),
                          child: ListView.separated(
                            padding: EdgeInsets.symmetric(
                              vertical:
                                  getHeight(inputHeight: PaddingManager.p16),
                              horizontal:
                                  getWidth(inputWidth: PaddingManager.p8),
                            ),
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              if (index >= state.topRated.length) {
                                return BaseLoadingIndicator();
                              }
                              return TopRatedComponent(
                                  topRated: state.topRated[index]);
                            },
                            separatorBuilder: (context, index) => SizedBox(
                                height:
                                    getHeight(inputHeight: SizesManager.s20)),
                            itemCount: state.hasReachedMax
                                ? state.topRated.length
                                : state.topRated.length + 1,
                          ),
                        ),
                      ],
                    ),
                  );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorTopRatedMessage,
                onPressFunction: () {
                  BlocProvider.of<TopRatedBloc>(context).add(
                      GetAllTopRatedEvent(
                          page: page,
                          language: FunctionsManager.getLangCode(context)));
                });
        }
      },
    );
  }
}
