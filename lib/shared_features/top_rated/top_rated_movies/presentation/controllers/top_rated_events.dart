import 'package:equatable/equatable.dart';

abstract class TopRatedEvents extends Equatable{

}
class GetAllTopRatedEvent extends TopRatedEvents{
  final int page;
  final String language;

  GetAllTopRatedEvent({required this.page , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [page , language];
}