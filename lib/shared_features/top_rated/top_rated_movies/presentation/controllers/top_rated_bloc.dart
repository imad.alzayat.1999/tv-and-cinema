import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/shared_features/top_rated/top_rated_movies/presentation/controllers/top_rated_events.dart';
import 'package:movie_app_clean_architecture/shared_features/top_rated/top_rated_movies/presentation/controllers/top_rated_states.dart';

import '../../../../../core/utils/request_states.dart';
import '../../domain/usecase/get_top_rated_usecase.dart';

class TopRatedBloc extends Bloc<TopRatedEvents, TopRatedStates> {
  final GetTopRatedUseCase getTopRatedUseCase;

  TopRatedBloc({
    required this.getTopRatedUseCase,
  }) : super(TopRatedStates()) {
    on<GetAllTopRatedEvent>(
      (event, emit) async {
        if (state.hasReachedMax) return;
        try {
          if (state.requestStates == RequestStates.loading) {
            final response = await getTopRatedUseCase(
              TopRatedMoviesParameters(page: event.page , language: event.language),
            );
            response.fold((l) {
              emit(
                state.copyWith(
                  requestStates: RequestStates.error,
                  errorTopRatedMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                      state.copyWith(
                        requestStates: RequestStates.success,
                        topRated: r,
                        hasReachedMax: false,
                      ),
                    );
            });
          } else {
            final response = await getTopRatedUseCase(
              TopRatedMoviesParameters(page: event.page , language: event.language),
            );
            print("length is ${state.topRated.length}");
            response.fold((l) {
              emit(
                state.copyWith(
                  requestStates: RequestStates.error,
                  errorTopRatedMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                      state.copyWith(
                        requestStates: RequestStates.success,
                        topRated: [...state.topRated, ...r],
                        hasReachedMax: false,
                      ),
                    );
            });
          }
        } catch (e) {
          emit(
            state.copyWith(
              requestStates: RequestStates.error,
              errorTopRatedMessage: e.toString(),
            ),
          );
        }
      },
      transformer: droppable(),
    );
  }
}
