import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import '../../domain/entities/top_rated.dart';

class TopRatedStates extends Equatable {
  final bool hasReachedMax;
  final RequestStates requestStates;
  final List<TopRated> topRated;
  final String errorTopRatedMessage;

  TopRatedStates({
    this.hasReachedMax = false,
    this.requestStates = RequestStates.loading,
    this.topRated = const [],
    this.errorTopRatedMessage = '',
  });

  TopRatedStates copyWith({
    bool? hasReachedMax,
    RequestStates? requestStates,
    List<TopRated>? topRated,
    String? errorTopRatedMessage,
  }) {
    return TopRatedStates(
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      requestStates: requestStates ?? this.requestStates,
      topRated: topRated ?? this.topRated,
      errorTopRatedMessage: errorTopRatedMessage ?? this.errorTopRatedMessage,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        hasReachedMax,
        requestStates,
        topRated,
        errorTopRatedMessage,
      ];
}