import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';

import '../../../../../core/error/failure.dart';
import '../../../../../core/network/network_info.dart';
import '../../domain/entities/top_rated.dart';
import '../../domain/repository/base_top_rated_repository.dart';
import '../../domain/usecase/get_top_rated_usecase.dart';
import '../datasources/top_rated_local_datasource.dart';
import '../datasources/top_rated_remote_datasource.dart';

class TopRatedRepository extends BaseTopRatedRepository {
  final BaseTopRatedRemoteDataSource baseTopRatedRemoteDataSource;
  final BaseTopRatedLocalDataSource baseTopRatedLocalDataSource;
  final NetworkInfo networkInfo;

  TopRatedRepository({
    required this.baseTopRatedRemoteDataSource,
    required this.baseTopRatedLocalDataSource,
    required this.networkInfo,
  });
  @override
  Future<Either<Failure, List<TopRated>>> getTopRatedMovies(
      TopRatedMoviesParameters topRatedMoviesParameters) async {
    if (await networkInfo.isConnected) {
      try {
        final response = await baseTopRatedRemoteDataSource
            .getTopRatedMovies(topRatedMoviesParameters);
        baseTopRatedLocalDataSource.cacheInfo(
            response, ConstsManager.CACHE_TOP_RATED_LIST);
        return Right(response);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message));
      }
    } else {
      try {
        final result = await baseTopRatedLocalDataSource
            .getCachedInfo(ConstsManager.CACHE_TOP_RATED_LIST);
        return Right(result);
      } on LocalDatabaseException catch (failure) {
        return Left(DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }
}
