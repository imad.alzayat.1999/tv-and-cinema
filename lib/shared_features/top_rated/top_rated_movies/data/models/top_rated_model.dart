import 'package:intl/intl.dart';
import '../../domain/entities/top_rated.dart';

class TopRatedModel extends TopRated {
  const TopRatedModel({
    required int? id,
    required String? movieImage,
    required String? movieTitle,
    required num? voteAvg,
    required DateTime? movieReleaseDate,
  }) : super(
          id: id,
          movieImage: movieImage,
          movieTitle: movieTitle,
          voteAvg: voteAvg,
          movieReleaseDate: movieReleaseDate,
        );

  factory TopRatedModel.fromJson(Map<String, dynamic> json) => TopRatedModel(
        id: json["id"],
        movieImage: json["backdrop_path"],
        movieTitle: json["title"],
        voteAvg: json["vote_average"],
        movieReleaseDate: DateFormat.y().parse(json["release_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "backdrop_path": movieImage,
        "title": movieTitle,
        "vote_average": voteAvg,
        "release_date": DateFormat.y().format(movieReleaseDate!)
      };
}
