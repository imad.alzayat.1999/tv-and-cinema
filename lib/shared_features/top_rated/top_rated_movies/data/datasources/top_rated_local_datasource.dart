import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/services/services_locator.dart';
import '../models/top_rated_model.dart';

abstract class BaseTopRatedLocalDataSource{
  Future<List<TopRatedModel>> getCachedInfo(String infoKey);
  Future<Unit> cacheInfo(List<TopRatedModel> topRatedMovies, String infoKey);
}
class TopRatedLocalDataSource extends BaseTopRatedLocalDataSource{
  @override
  Future<Unit> cacheInfo(List<TopRatedModel> topRatedMovies, String infoKey) {
    List comingSoonModelsToJson = topRatedMovies
        .map<Map<String, dynamic>>((topRatedMovie) => topRatedMovie.toJson())
        .toList();
    getIt<SharedPreferences>().setString(infoKey, json.encode(comingSoonModelsToJson));
    print("Info Cached for is:  " + json.encode(comingSoonModelsToJson));
    return Future.value(unit);
  }

  @override
  Future<List<TopRatedModel>> getCachedInfo(String infoKey) {
    final jsonString = getIt<SharedPreferences>().getString(infoKey);
    if (jsonString != null) {
      List decodeJsonData = json.decode(jsonString);
      List<TopRatedModel> jsonToComingSoonModels = decodeJsonData
          .map<TopRatedModel>((jsonComingSoonModel) => TopRatedModel.fromJson(jsonComingSoonModel))
          .toList();
      return Future.value(jsonToComingSoonModels);
    } else {
      throw LocalDatabaseException(message: '');
    }
  }

}