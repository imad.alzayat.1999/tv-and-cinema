import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';

import '../../domain/usecase/get_top_rated_usecase.dart';
import '../models/top_rated_model.dart';

abstract class BaseTopRatedRemoteDataSource {
  Future<List<TopRatedModel>> getTopRatedMovies(
      TopRatedMoviesParameters topRatedMoviesParameters);
}

class TopRatedRemoteDataSource extends BaseTopRatedRemoteDataSource {
  final ApiConsumer apiConsumer;

  TopRatedRemoteDataSource({required this.apiConsumer});
  @override
  Future<List<TopRatedModel>> getTopRatedMovies(
      TopRatedMoviesParameters topRatedMoviesParameters) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getTopRatedMoviesEndPoint,
      queryParameters: {
        "page": topRatedMoviesParameters.page,
        "language": topRatedMoviesParameters.language,
      },
    );
    return List<TopRatedModel>.from((response["results"] as List).map(
      (e) => TopRatedModel.fromJson(e),
    ));
  }
}
