import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';

import '../../domain/usecases/get_top_rated_usecase.dart';
import '../models/top_rated_tv_model.dart';

abstract class BaseTopRatedTvRemoteDataSource{
  Future<List<TopRatedTvModel>> getTopRated({required TopRatedParameters topRatedParameters});
}

class TopRatedTvRemoteDataSource extends BaseTopRatedTvRemoteDataSource{
  final ApiConsumer apiConsumer;

  TopRatedTvRemoteDataSource({required this.apiConsumer});

  @override
  Future<List<TopRatedTvModel>> getTopRated({required TopRatedParameters topRatedParameters}) async{
    final response = await apiConsumer.get(path: ApiConstance.getTopRatedTvEndPoint , queryParameters: {
      "page" : topRatedParameters.page,
    });
    return List<TopRatedTvModel>.from((response["results"] as List).map(
          (e) => TopRatedTvModel.fromJson(e),
    ));
  }
}