import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';

import '../../domain/entities/top_rated_tv_entity.dart';
import '../../domain/repository/base_top_rated_tv_repository.dart';
import '../../domain/usecases/get_top_rated_usecase.dart';
import '../datasources/top_rated_tv_remote_datasource.dart';

class TopRatedTvRepository extends BaseTopRatedTvRepository{
  final BaseTopRatedTvRemoteDataSource baseTopRatedTvRemoteDataSource;

  TopRatedTvRepository({required this.baseTopRatedTvRemoteDataSource});
  @override
  Future<Either<Failure, List<TopRatedTvEntity>>> getTopRatedTv({required TopRatedParameters topRatedParameters}) async{
    try{
      final result = await baseTopRatedTvRemoteDataSource.getTopRated(topRatedParameters: topRatedParameters);
      return Right(result);
    }on ServerException catch(e){
      return Left(ServerFailure(e.message));
    }
  }

}