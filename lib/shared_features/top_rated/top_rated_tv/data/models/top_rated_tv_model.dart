import '../../domain/entities/top_rated_tv_entity.dart';

class TopRatedTvModel extends TopRatedTvEntity {
  TopRatedTvModel(
      {required String? backdropPath,
      required String? name,
      required String? overview,
      required String? firstAirDate,
      required num? voteAvg,
      required int id})
      : super(
            backdropPath: backdropPath,
            name: name,
            overview: overview,
            firstAirDate: firstAirDate,
            voteAvg: voteAvg,
            id: id);

  factory TopRatedTvModel.fromJson(Map<String, dynamic> json) =>
      TopRatedTvModel(
        backdropPath: json["backdrop_path"],
        name: json["name"],
        overview: json["overview"],
        firstAirDate: json["first_air_date"],
        voteAvg: json["vote_average"],
        id: json["id"],
      );
}
