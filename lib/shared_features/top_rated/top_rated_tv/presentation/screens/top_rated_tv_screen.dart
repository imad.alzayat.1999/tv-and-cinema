import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';

import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_error.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../components/top_rated_tv_item.dart';
import '../controllers/top_rated_tv_bloc.dart';
import '../controllers/top_rated_tv_events.dart';
import '../controllers/top_rated_tv_states.dart';

class TopRatedTvScreen extends StatelessWidget {
  const TopRatedTvScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          getIt<TopRatedTvBloc>()..add(GetTopRatedEvent(page: 1)),
      child: Scaffold(
        backgroundColor: ColorsManager.kEerieBlack,
        appBar: AppBar(
          elevation: SizesManager.s0,
          backgroundColor: ColorsManager.kEerieBlack,
          title: Text(
            AppLocalizations.of(context)!
                .translate(StringsManager.topRatedString)
                .toString(),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size16,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
        ),
        body: TopRatedTvContent(),
      ),
    );
  }
}

class TopRatedTvContent extends StatefulWidget {
  const TopRatedTvContent({Key? key}) : super(key: key);

  @override
  State<TopRatedTvContent> createState() => _TopRatedTvContentState();
}

class _TopRatedTvContentState extends State<TopRatedTvContent> {
  final _scrollController = ScrollController();
  int page = 1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scrollController.addListener(_listener);
  }

  _listener() {
    if (_scrollController.position.maxScrollExtent ==
        _scrollController.position.pixels) {
      page = page + 1;
      BlocProvider.of<TopRatedTvBloc>(context)
          .add(GetTopRatedEvent(page: page));
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _scrollController
      ..removeListener(_listener)
      ..dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TopRatedTvBloc, TopRatedTvStates>(
      builder: (context, state) {
        switch (state.topRatedTvRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return SingleChildScrollView(
              key: const Key('topRatedScrollView'),
              controller: _scrollController,
              child: Column(
                children: [
                  FadeInUp(
                    from: SizesManager.s20,
                    duration: const Duration(
                      milliseconds: ConstsManager.fadeAnimationDuration,
                    ),
                    child: state.listOfTopRated.isEmpty
                        ? BaseEmptyWidget(emptyMessage: StringsManager.noTopRatedFilmsString)
                        : ListView.separated(
                            padding: EdgeInsets.symmetric(
                              vertical:
                                  getHeight(inputHeight: PaddingManager.p16),
                              horizontal:
                                  getWidth(inputWidth: PaddingManager.p8),
                            ),
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              return index >= state.listOfTopRated.length
                                  ? BaseLoadingIndicator()
                                  : TopRatedTvItem(
                                      topRated: state.listOfTopRated[index]);
                            },
                            separatorBuilder: (context, index) => SizedBox(
                              height: getHeight(
                                inputHeight: SizesManager.s20,
                              ),
                            ),
                            itemCount: state.hasReachedMax
                                ? state.listOfTopRated.length
                                : state.listOfTopRated.length + 1,
                          ),
                  ),
                ],
              ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorTopRatedMessage,
                onPressFunction: () {
                  BlocProvider.of<TopRatedTvBloc>(context)
                    ..add(GetTopRatedEvent(page: page));
                });
        }
      },
    );
  }
}
