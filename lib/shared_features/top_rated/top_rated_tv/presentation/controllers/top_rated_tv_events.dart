import 'package:equatable/equatable.dart';

abstract class TopRatedTvEvents extends Equatable{

}

class GetTopRatedEvent extends TopRatedTvEvents{
  final int page;

  GetTopRatedEvent({required this.page});

  @override
  // TODO: implement props
  List<Object?> get props => [page];
}