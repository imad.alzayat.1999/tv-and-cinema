import 'package:equatable/equatable.dart';

import '../../../../../core/utils/request_states.dart';
import '../../domain/entities/top_rated_tv_entity.dart';

class TopRatedTvStates extends Equatable {
  final String errorTopRatedMessage;
  final List<TopRatedTvEntity> listOfTopRated;
  final RequestStates topRatedTvRequestStates;
  final bool hasReachedMax;

  TopRatedTvStates({
    this.errorTopRatedMessage = '',
    this.listOfTopRated = const [],
    this.topRatedTvRequestStates = RequestStates.loading,
    this.hasReachedMax = false,
  });

  TopRatedTvStates copyWith({
    String? errorTopRatedMessage,
    List<TopRatedTvEntity>? listOfTopRated,
    RequestStates? topRatedTvRequestStates,
    bool? hasReachedMax,
  }) {
    return TopRatedTvStates(
      errorTopRatedMessage: errorTopRatedMessage ?? this.errorTopRatedMessage,
      listOfTopRated: listOfTopRated ?? this.listOfTopRated,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      topRatedTvRequestStates:
          topRatedTvRequestStates ?? this.topRatedTvRequestStates,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        hasReachedMax,
        topRatedTvRequestStates,
        listOfTopRated,
        errorTopRatedMessage
      ];
}
