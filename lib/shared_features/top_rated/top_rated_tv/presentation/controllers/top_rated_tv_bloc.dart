import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/shared_features/top_rated/top_rated_tv/presentation/controllers/top_rated_tv_events.dart';
import 'package:movie_app_clean_architecture/shared_features/top_rated/top_rated_tv/presentation/controllers/top_rated_tv_states.dart';
import '../../../../../core/utils/request_states.dart';
import '../../domain/usecases/get_top_rated_usecase.dart';

class TopRatedTvBloc extends Bloc<TopRatedTvEvents , TopRatedTvStates>{
  final GetTvTopRatedUseCase getTopRatedTvUseCase;
  TopRatedTvBloc({required this.getTopRatedTvUseCase}) : super(TopRatedTvStates()){
    on<GetTopRatedEvent>(
          (event, emit) async {
        if (state.hasReachedMax) return;
        try {
          if (state.topRatedTvRequestStates == RequestStates.loading) {
            final response = await getTopRatedTvUseCase(
                TopRatedParameters(page: event.page));
            response.fold((l) {
              emit(
                state.copyWith(
                  topRatedTvRequestStates: RequestStates.error,
                  errorTopRatedMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                state.copyWith(
                  topRatedTvRequestStates: RequestStates.success,
                  listOfTopRated: r,
                  hasReachedMax: false,
                ),
              );
            });
          } else {
            final response = await getTopRatedTvUseCase(
                TopRatedParameters(page: event.page));
            response.fold((l) {
              emit(
                state.copyWith(
                  topRatedTvRequestStates: RequestStates.error,
                  errorTopRatedMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                state.copyWith(
                  topRatedTvRequestStates: RequestStates.success,
                  listOfTopRated: [...state.listOfTopRated, ...r],
                  hasReachedMax: false,
                ),
              );
            });
          }
        } catch (e) {
          emit(
            state.copyWith(
              topRatedTvRequestStates: RequestStates.error,
              errorTopRatedMessage: e.toString(),
            ),
          );
        }
      },
      transformer: droppable(),
    );
  }
}