import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../entities/top_rated_tv_entity.dart';
import '../repository/base_top_rated_tv_repository.dart';


class GetTvTopRatedUseCase extends BaseUseCase<List<TopRatedTvEntity> , TopRatedParameters>{
  final BaseTopRatedTvRepository baseTopRatedTvRepository;

  GetTvTopRatedUseCase({required this.baseTopRatedTvRepository});
  @override
  Future<Either<Failure, List<TopRatedTvEntity>>> call(TopRatedParameters parameters) async{
    return await baseTopRatedTvRepository.getTopRatedTv(topRatedParameters: parameters);
  }
}

class TopRatedParameters extends Equatable{
  final int page;

  TopRatedParameters({required this.page});

  @override
  // TODO: implement props
  List<Object?> get props => [page];
}