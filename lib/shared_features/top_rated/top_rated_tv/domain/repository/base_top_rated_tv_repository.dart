import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import '../entities/top_rated_tv_entity.dart';
import '../usecases/get_top_rated_usecase.dart';

abstract class BaseTopRatedTvRepository {
  Future<Either<Failure, List<TopRatedTvEntity>>> getTopRatedTv(
      {required TopRatedParameters topRatedParameters});
}
