import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_tv/presentation/controller/tv_details_events.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_tv/presentation/controller/tv_details_states.dart';

import '../../domain/usecases/get_credits_tv_usecase.dart';
import '../../domain/usecases/get_tv_details_usecase.dart';
import '../../domain/usecases/get_tv_recommended_usecase.dart';
import '../../domain/usecases/get_tv_reviews_usecase.dart';
import '../../domain/usecases/get_tv_similar_usecase.dart';

class TvDetailsBloc extends Bloc<TvDetailsEvents, TvDetailsStates> {
  final GetTvDetailsUseCase getTvDetailsUseCase;
  final GetTvRecommendedUseCase getTvRecommendedUseCase;
  final GetTvSimilarUseCase getTvSimilarUseCase;
  final GetCreditsTvUseCase getCreditsTvUseCase;
  final GetTvReviewsUseCase getTvReviewsUseCase;

  TvDetailsBloc({
    required this.getTvDetailsUseCase,
    required this.getTvRecommendedUseCase,
    required this.getTvSimilarUseCase,
    required this.getCreditsTvUseCase,
    required this.getTvReviewsUseCase,
  }) : super(TvDetailsStates()) {
    on<GetTvDetailsEvent>((event, emit) async {
      final failureOrTvDetails =
          await getTvDetailsUseCase(TvDetailsParameters(tvId: event.tvId));
      failureOrTvDetails.fold((l) {
        emit(
          state.copyWith(
            errorTvDetailsMessage: l.message,
            requestStates: RequestStates.error,
          ),
        );
      }, (r) {
        emit(
          state.copyWith(
            requestStates: RequestStates.success,
            tvDetailsEntity: r,
          ),
        );
      });
    });
    on<GetTvRecommendedEvent>((event, emit) async {
      final failureOrTvRecommended = await getTvRecommendedUseCase(
          RecommendedTvParameters(tvId: event.tvId));
      failureOrTvRecommended.fold((l) {
        emit(
          state.copyWith(
            errorTvRecommendedMessage: l.message,
            tvRecommendedRequestStates: RequestStates.error,
          ),
        );
      }, (r) {
        emit(
          state.copyWith(
            tvRecommendedRequestStates: RequestStates.success,
            recommendationsOfTv: r,
          ),
        );
      });
    });
    on<GetTvSimilarEvent>((event, emit) async {
      final failureOrTvSimilar =
          await getTvSimilarUseCase(SimilarTvParameters(tvId: event.tvId));
      failureOrTvSimilar.fold((l) {
        emit(
          state.copyWith(
            errorTvSimilarMessage: l.message,
            tvSimilarRequestStates: RequestStates.error,
          ),
        );
      }, (r) {
        emit(
          state.copyWith(
            tvSimilarRequestStates: RequestStates.success,
            similarOfTv: r,
          ),
        );
      });
    });
    on<GetTvCreditsEvent>((event, emit) async {
      final failureOrTvCredits =
          await getCreditsTvUseCase(CreditsTvParameters(tvId: event.tvId));
      failureOrTvCredits.fold((l) {
        emit(
          state.copyWith(
            errorTvCreditsMessage: l.message,
            tvCreditsRequestStates: RequestStates.error,
          ),
        );
      }, (r) {
        emit(
          state.copyWith(
            tvCreditsRequestStates: RequestStates.success,
            creditsTvEntity: r,
          ),
        );
      });
    });
    on<GetTvReviewEvent>((event, emit) async {
      final failureOrTvReviews =
          await getTvReviewsUseCase(ReviewsTvParameters(tvId: event.tvId));
      failureOrTvReviews.fold((l) {
        emit(
          state.copyWith(
            errorTvReviewsMessage: l.message,
            tvRatingsRequestStates: RequestStates.error,
          ),
        );
      }, (r) {
        emit(
          state.copyWith(
            tvRatingsRequestStates: RequestStates.success,
            ratings: r,
          ),
        );
      });
    });
  }
}
