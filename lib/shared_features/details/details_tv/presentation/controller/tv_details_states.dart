import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import '../../domain/entities/credits_tv/credits_tv_entity.dart';
import '../../domain/entities/rating_tv/rating_tv_entity.dart';
import '../../domain/entities/recommended_tv/recommended_tv_entity.dart';
import '../../domain/entities/similar_tv/similar_tv_entity.dart';
import '../../domain/entities/tv_details/tv_details_entity.dart';

class TvDetailsStates extends Equatable {
  final String errorTvDetailsMessage;
  final TvDetailsEntity? tvDetailsEntity;
  final RequestStates requestStates;

  final String errorTvRecommendedMessage;
  final List<RecommendedTvEntity> recommendationsOfTv;
  final RequestStates tvRecommendedRequestStates;

  final String errorTvSimilarMessage;
  final List<SimilarTvEntity> similarOfTv;
  final RequestStates tvSimilarRequestStates;

  final String errorTvCreditsMessage;
  final CreditsTvEntity? creditsTvEntity;
  final RequestStates tvCreditsRequestStates;

  final String errorTvReviewsMessage;
  final List<RatingTvEntity> ratings;
  final RequestStates tvRatingsRequestStates;

  TvDetailsStates({
    this.errorTvDetailsMessage = '',
    this.tvDetailsEntity,
    this.requestStates = RequestStates.loading,
    this.errorTvRecommendedMessage = '',
    this.recommendationsOfTv = const [],
    this.tvRecommendedRequestStates = RequestStates.loading,
    this.errorTvSimilarMessage = '',
    this.similarOfTv = const [],
    this.tvSimilarRequestStates = RequestStates.loading,
    this.creditsTvEntity,
    this.errorTvCreditsMessage = '',
    this.tvCreditsRequestStates = RequestStates.loading,
    this.ratings = const [],
    this.errorTvReviewsMessage = '',
    this.tvRatingsRequestStates = RequestStates.loading,
  });

  TvDetailsStates copyWith({
    String? errorTvDetailsMessage,
    TvDetailsEntity? tvDetailsEntity,
    RequestStates? requestStates,
    String? errorTvRecommendedMessage,
    List<RecommendedTvEntity>? recommendationsOfTv,
    RequestStates? tvRecommendedRequestStates,
    String? errorTvSimilarMessage,
    List<SimilarTvEntity>? similarOfTv,
    RequestStates? tvSimilarRequestStates,
    String? errorTvCreditsMessage,
    CreditsTvEntity? creditsTvEntity,
    RequestStates? tvCreditsRequestStates,
    String? errorTvReviewsMessage,
    List<RatingTvEntity>? ratings,
    RequestStates? tvRatingsRequestStates,
  }) {
    return TvDetailsStates(
      errorTvDetailsMessage:
          errorTvDetailsMessage ?? this.errorTvDetailsMessage,
      tvDetailsEntity: tvDetailsEntity ?? this.tvDetailsEntity,
      requestStates: requestStates ?? this.requestStates,
      tvRecommendedRequestStates:
          tvRecommendedRequestStates ?? this.tvRecommendedRequestStates,
      recommendationsOfTv: recommendationsOfTv ?? this.recommendationsOfTv,
      errorTvRecommendedMessage:
          errorTvRecommendedMessage ?? this.errorTvRecommendedMessage,
      similarOfTv: similarOfTv ?? this.similarOfTv,
      tvSimilarRequestStates:
          tvSimilarRequestStates ?? this.tvSimilarRequestStates,
      errorTvSimilarMessage:
          errorTvSimilarMessage ?? this.errorTvSimilarMessage,
      creditsTvEntity: creditsTvEntity ?? this.creditsTvEntity,
      errorTvCreditsMessage:
          errorTvCreditsMessage ?? this.errorTvCreditsMessage,
      tvCreditsRequestStates:
          tvCreditsRequestStates ?? this.tvCreditsRequestStates,
      errorTvReviewsMessage:
          errorTvReviewsMessage ?? this.errorTvReviewsMessage,
      ratings: ratings ?? this.ratings,
      tvRatingsRequestStates:
          tvRatingsRequestStates ?? this.tvRatingsRequestStates,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        errorTvDetailsMessage,
        tvDetailsEntity,
        requestStates,
        tvRecommendedRequestStates,
        recommendationsOfTv,
        errorTvRecommendedMessage,
        errorTvSimilarMessage,
        similarOfTv,
        tvSimilarRequestStates,
        creditsTvEntity,
        errorTvCreditsMessage,
        tvCreditsRequestStates,
        ratings,
        errorTvReviewsMessage,
        tvRatingsRequestStates,
      ];
}
