import 'package:equatable/equatable.dart';

abstract class TvDetailsEvents extends Equatable{

}

class GetTvDetailsEvent extends TvDetailsEvents{
  final int tvId;

  GetTvDetailsEvent({required this.tvId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId];
}

class GetTvRecommendedEvent extends TvDetailsEvents{
  final int tvId;

  GetTvRecommendedEvent({required this.tvId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId];
}

class GetTvSimilarEvent extends TvDetailsEvents{
  final int tvId;

  GetTvSimilarEvent({required this.tvId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId];
}

class GetTvCreditsEvent extends TvDetailsEvents{
  final int tvId;

  GetTvCreditsEvent({required this.tvId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId];
}

class GetTvReviewEvent extends TvDetailsEvents{
  final int tvId;

  GetTvReviewEvent({required this.tvId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId];
}