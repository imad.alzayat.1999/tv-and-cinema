import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../component/credits_tv.dart';
import '../component/recommended_tv.dart';
import '../component/reviews_tv.dart';
import '../component/seasons.dart';
import '../component/similar_tv.dart';
import '../controller/tv_details_bloc.dart';
import '../controller/tv_details_events.dart';
import '../controller/tv_details_states.dart';

class TvDetailsScreen extends StatefulWidget {
  static const routeName = Routes.tvDetailsRoute;
  final int tvId;

  const TvDetailsScreen({Key? key, required this.tvId}) : super(key: key);
  @override
  State<TvDetailsScreen> createState() => _TvDetailsScreenState();
}

class _TvDetailsScreenState extends State<TvDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsManager.kEerieBlack,
      body: BlocProvider(
        create: (context) => getIt<TvDetailsBloc>()
          ..add(GetTvDetailsEvent(tvId: widget.tvId))
          ..add(GetTvRecommendedEvent(tvId: widget.tvId))
          ..add(GetTvSimilarEvent(tvId: widget.tvId))
          ..add(GetTvCreditsEvent(tvId: widget.tvId))
          ..add(GetTvReviewEvent(tvId: widget.tvId)),
        child: TvDetailsContent(tvId: widget.tvId),
      ),
    );
  }
}

class TvDetailsContent extends StatefulWidget {
  final int tvId;
  const TvDetailsContent({Key? key, required this.tvId}) : super(key: key);

  @override
  State<TvDetailsContent> createState() => _TvDetailsContentState();
}

class _TvDetailsContentState extends State<TvDetailsContent>
    with TickerProviderStateMixin {
  Alignment getAlignmentByLanguage() {
    if (AppLocalizations.of(context)!.isEnLocale) {
      return Alignment.topLeft;
    } else {
      return Alignment.topRight;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TvDetailsBloc, TvDetailsStates>(
      builder: (context, state) {
        switch (state.requestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return DefaultTabController(
              length: 3,
              child: NestedScrollView(
                physics: const NeverScrollableScrollPhysics(),
                key: const Key('movieDetailScrollView'),
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return [
                    SliverAppBar(
                      pinned: true,
                      backgroundColor: ColorsManager.kEerieBlack,
                      expandedHeight: getHeight(inputHeight: SizesManager.s250),
                      flexibleSpace: FlexibleSpaceBar(
                        background: FadeIn(
                          duration: const Duration(
                            milliseconds: ConstsManager.fadeAnimationDuration,
                          ),
                          child: ShaderMask(
                            shaderCallback: (rect) {
                              return const LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  ColorsManager.kTransparent,
                                  ColorsManager.kBlack,
                                  ColorsManager.kBlack,
                                  ColorsManager.kTransparent,
                                ],
                                stops: [0.0, 0.5, 1.0, 1.0],
                              ).createShader(
                                Rect.fromLTRB(
                                    0.0, 0.0, rect.width, rect.height),
                              );
                            },
                            blendMode: BlendMode.dstIn,
                            child: FadeInImage.assetNetwork(
                              width: MediaQuery.of(context).size.width,
                              image: FunctionsManager.showAPIImage(
                                  state.tvDetailsEntity!.tvImage),
                              placeholder: ImagesManager.logoImage,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: FadeInUp(
                        from: SizesManager.s20,
                        duration: const Duration(
                          milliseconds: ConstsManager.fadeAnimationDuration,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(PaddingManager.p16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                state.tvDetailsEntity!.name,
                                style: getBoldTextStyle(
                                  fontSize: FontSizeManager.size23,
                                  letterSpacing: SizesManager.s1_2,
                                  context: context,
                                ),
                              ),
                              SizedBox(
                                  height: getHeight(
                                inputHeight: SizesManager.s8,
                              )),
                              Row(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: PaddingManager.p2,
                                      horizontal: PaddingManager.p8,
                                    ),
                                    decoration: BoxDecoration(
                                      color: ColorsManager.kDimGray,
                                      borderRadius: BorderRadius.circular(
                                          SizesManager.s4),
                                    ),
                                    child: Text(
                                      state.tvDetailsEntity!.firstAirDate
                                          .split('-')[0],
                                      style: getMediumTextStyle(
                                        color: ColorsManager.kWhite,
                                        fontSize: FontSizeManager.size16,
                                        letterSpacing: SizesManager.s0,
                                        context: context,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                      width: getWidth(
                                          inputWidth: SizesManager.s16)),
                                  Row(
                                    children: [
                                      const BaseIcon(
                                        iconHeight: SizesManager.s16,
                                        iconPath: IconsManager.starIcon,
                                        iconColor: ColorsManager.voteColor,
                                      ),
                                      SizedBox(
                                        width: getWidth(
                                          inputWidth: SizesManager.s4,
                                        ),
                                      ),
                                      Text(
                                        (state.tvDetailsEntity!.tvVoteAvg / 2)
                                            .toStringAsFixed(1),
                                        style: getMediumTextStyle(
                                          fontSize: FontSizeManager.size16,
                                          letterSpacing: SizesManager.s1_2,
                                          context: context,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                      width: getWidth(
                                          inputWidth: SizesManager.s16)),
                                  Row(
                                    children: [
                                      const BaseIcon(
                                        iconHeight: SizesManager.s20,
                                        iconPath: IconsManager.clockIcon,
                                      ),
                                      SizedBox(
                                          width: getWidth(
                                              inputWidth: SizesManager.s4)),
                                      state.tvDetailsEntity!.episodeRunTime
                                              .isEmpty
                                          ? SizedBox.shrink()
                                          : Text(
                                              FunctionsManager.showDuration(
                                                state.tvDetailsEntity!
                                                    .episodeRunTime[0],
                                              ),
                                              style: getMediumTextStyle(
                                                fontSize:
                                                    FontSizeManager.size16,
                                                letterSpacing:
                                                    SizesManager.s1_2,
                                                context: context,
                                              ),
                                            ),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                  height:
                                      getHeight(inputHeight: SizesManager.s20)),
                              Text(
                                state.tvDetailsEntity!.overview,
                                style: getMediumTextStyle(
                                  fontSize: FontSizeManager.size14,
                                  letterSpacing: SizesManager.s1_2,
                                  context: context,
                                ),
                              ),
                              SizedBox(
                                  height:
                                      getHeight(inputHeight: SizesManager.s8)),
                              Text(
                                '${AppLocalizations.of(context)!.translate(StringsManager.genresString)} ${FunctionsManager.showGenres(state.tvDetailsEntity!.spokenLanguagesEntity)}',
                                style: getRegularTextStyle(
                                  fontSize: FontSizeManager.size12,
                                  letterSpacing: SizesManager.s1_2,
                                  context: context,
                                ),
                              ),
                              SizedBox(
                                  height:
                                      getHeight(inputHeight: SizesManager.s8)),
                              Text(
                                '${AppLocalizations.of(context)!.translate(StringsManager.spokenLanguagesString)} ${FunctionsManager.showSpokenLanguages(state.tvDetailsEntity!.spokenLanguagesEntity)}',
                                style: getRegularTextStyle(
                                  fontSize: FontSizeManager.size12,
                                  letterSpacing: SizesManager.s1_2,
                                  context: context,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: FadeInUp(
                        from: SizesManager.s20,
                        duration: const Duration(
                          milliseconds: ConstsManager.fadeAnimationDuration,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(
                            PaddingManager.p16,
                            PaddingManager.p0,
                            PaddingManager.p16,
                            PaddingManager.p24,
                          ),
                          child: CreditsTv(tvId: widget.tvId),
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: FadeInUp(
                        from: SizesManager.s20,
                        duration: const Duration(
                          milliseconds: ConstsManager.fadeAnimationDuration,
                        ),
                        child: ReviewsTv(tvId: widget.tvId),
                      ),
                    ),
                    SliverPersistentHeader(
                      delegate: MyDelegate(TabBar(
                        tabs: [
                          Tab(
                              text: AppLocalizations.of(context)!
                                  .translate(StringsManager.seasonsString)),
                          Tab(
                              text: AppLocalizations.of(context)!
                                  .translate(StringsManager.recommendedString)),
                          Tab(
                              text: AppLocalizations.of(context)!.translate(
                                  StringsManager.moreLikeThisString)),
                        ],
                        labelStyle: getRegularTextStyle(
                          fontSize: FontSizeManager.size14,
                          letterSpacing: SizesManager.s0,
                          context: context,
                        ),
                        indicatorColor: ColorsManager.kVeryLightBlue,
                        unselectedLabelColor: Colors.grey,
                        labelColor: ColorsManager.kWhite,
                      )),
                      floating: true,
                      pinned: true,
                    ),
                  ];
                },
                body: TabBarView(children: [
                  Seasons(
                      seasons: state.tvDetailsEntity!.tvSeasons,
                      tvId: widget.tvId),
                  RecommendedTv(tvId: widget.tvId),
                  SimilarTv(tvId: widget.tvId),
                ]),
              ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorTvDetailsMessage,
                onPressFunction: () {
                  BlocProvider.of<TvDetailsBloc>(context)
                    ..add(GetTvDetailsEvent(tvId: widget.tvId));
                });
        }
      },
    );
  }
}

class MyDelegate extends SliverPersistentHeaderDelegate {
  MyDelegate(this.tabBar);
  final TabBar tabBar;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: ColorsManager.kDarkGunmetal,
      child: tabBar,
    );
  }

  @override
  double get maxExtent => tabBar.preferredSize.height;

  @override
  double get minExtent => tabBar.preferredSize.height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
