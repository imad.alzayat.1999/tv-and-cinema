import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import '../../../../../core/config/routes_config/routes_config.dart';
import '../../domain/entities/tv_details/tv_season_entity.dart';

class Seasons extends StatelessWidget {
  final List<TvSeasonEntity> seasons;
  final int tvId;

  const Seasons({
    Key? key,
    required this.seasons,
    required this.tvId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return seasons.isEmpty
        ? SizedBox.shrink()
        : seasons.isEmpty
            ? BaseEmptyWidget(emptyMessage: StringsManager.noSeasonsTvString)
            : ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) =>
                    _buildSeasonItem(seasons[index], context),
                separatorBuilder: (context, index) => SizedBox(
                  height: getHeight(
                    inputHeight: SizesManager.s10,
                  ),
                ),
                itemCount: seasons.length,
              );
  }

  Widget _buildSeasonItem(TvSeasonEntity season, BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        Routes.seasonDetailsRoute,
        arguments: SeasonDetailsRouteParameters(
            tvId: tvId, seasonId: season.seasonNumber!),
      ),
      child: SizedBox(
        height: getHeight(inputHeight: SizesManager.s150),
        child: Card(
          color: ColorsManager.kDimGray,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SizesManager.s10),
          ),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(SizesManager.s10),
                child: FadeInImage.assetNetwork(
                  image: FunctionsManager.showAPIImage(season.posterPath),
                  fit: BoxFit.cover,
                  placeholder: ImagesManager.logoImage,
                  width: getWidth(inputWidth: SizesManager.s120),
                ),
              ),
              SizedBox(width: getWidth(inputWidth: SizesManager.s10)),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    season.name.toString(),
                    style: getBoldTextStyle(
                      fontSize: FontSizeManager.size16,
                      letterSpacing: SizesManager.s0,
                      context: context,
                    ),
                  ),
                  SizedBox(height: getHeight(inputHeight: SizesManager.s8)),
                  SizedBox(
                    width: getWidth(inputWidth: SizesManager.s200),
                    child: Text(
                      season.overview.toString(),
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: getLightTextStyle(
                        fontSize: FontSizeManager.size14,
                        letterSpacing: SizesManager.s0,
                        context: context,
                      ),
                    ),
                  ),
                  SizedBox(height: getHeight(inputHeight: SizesManager.s8)),
                  Row(
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: PaddingManager.p2,
                          horizontal: PaddingManager.p8,
                        ),
                        decoration: BoxDecoration(
                          color: ColorsManager.kDimGray,
                          borderRadius: BorderRadius.circular(SizesManager.s4),
                        ),
                        child: Text(
                          season.airDate!.split('-')[0],
                          style: getMediumTextStyle(
                            color: ColorsManager.kWhite,
                            fontSize: FontSizeManager.size12,
                            letterSpacing: SizesManager.s0,
                            context: context,
                          ),
                        ),
                      ),
                      SizedBox(width: getWidth(inputWidth: SizesManager.s50)),
                      FunctionsManager.richTextWidget(
                        title: "Number Of Episodes",
                        text: season.episodeNumber.toString(),
                        context: context,
                      )
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
