import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/utils/global_widgets/base_error.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../controller/tv_details_bloc.dart';
import '../controller/tv_details_events.dart';
import '../controller/tv_details_states.dart';

class SimilarTv extends StatelessWidget {
  final int tvId;
  const SimilarTv({Key? key, required this.tvId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TvDetailsBloc, TvDetailsStates>(
      builder: (context, state) {
        switch (state.tvSimilarRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return FadeInUp(
              from: SizesManager.s20,
              duration: const Duration(
                milliseconds: ConstsManager.fadeAnimationDuration,
              ),
              child: state.similarOfTv.isEmpty
                  ? BaseEmptyWidget(
                      emptyMessage: StringsManager.noSimilarTvString)
                  : GridView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.similarOfTv.length,
                      itemBuilder: (context, index) {
                        final similar = state.similarOfTv[index];
                        return FadeInUp(
                          from: SizesManager.s20,
                          duration: const Duration(
                            milliseconds: ConstsManager.fadeAnimationDuration,
                          ),
                          child: ClipRRect(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(SizesManager.s4),
                            ),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.pushNamed(
                                    context, Routes.tvDetailsRoute,
                                    arguments: DetailsRouteParameters(id: similar.id));
                              },
                              child: FadeInImage.assetNetwork(
                                image: FunctionsManager.showAPIImage(
                                  similar.backdropPath,
                                ),
                                placeholder: ImagesManager.logoImage,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        );
                      },
                      gridDelegate: ConstsManager.recommendationsGridDelegates,
                    ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorTvSimilarMessage,
                onPressFunction: () {
                  BlocProvider.of<TvDetailsBloc>(context)
                    ..add(GetTvSimilarEvent(tvId: tvId));
                });
        }
      },
    );
  }
}
