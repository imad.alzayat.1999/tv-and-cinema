import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';

import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../../domain/entities/credits_tv/tv_cast_entity.dart';
import '../controller/tv_details_bloc.dart';
import '../controller/tv_details_events.dart';
import '../controller/tv_details_states.dart';

class CreditsTv extends StatelessWidget {
  final int tvId;

  const CreditsTv({Key? key, required this.tvId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TvDetailsBloc, TvDetailsStates>(
      builder: (context, state) {
        switch (state.tvCreditsRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return SizedBox(
              height: getHeight(inputHeight: SizesManager.s250),
              child: state.creditsTvEntity!.tvCasts.isEmpty
                  ? BaseEmptyWidget(emptyMessage: StringsManager.noCreditsTvString)
                  : ListView.separated(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) => getCreditItem(
                          state.creditsTvEntity!.tvCasts[index], context),
                      separatorBuilder: (context, index) => SizedBox(
                          width: getWidth(inputWidth: SizesManager.s16)),
                      itemCount: state.creditsTvEntity!.tvCasts.length,
                    ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorTvCreditsMessage,
                onPressFunction: () {
                  BlocProvider.of<TvDetailsBloc>(context)
                      .add(GetTvCreditsEvent(tvId: tvId));
                });
        }
      },
    );
  }

  /// get credit item
  Widget getCreditItem(TvCastEntity cast, BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          Routes.actorDetailsRoute,
          arguments: DetailsRouteParameters(id: cast.id!),
        );
      },
      child: Column(
        children: [
          SizedBox(
            width: getHeight(inputHeight: SizesManager.s100),
            height: getHeight(inputHeight: SizesManager.s100),
            child: ClipOval(
              child: FadeInImage.assetNetwork(
                placeholder: ImagesManager.logoImage,
                fit: BoxFit.cover,
                image: FunctionsManager.showAPIImage(cast.profilePath),
              ),
            ),
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s10)),
          Text(
            cast.name.toString(),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size12,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s10)),
          SizedBox(
            width: getWidth(inputWidth: SizesManager.s100),
            child: Text(
              cast.character!,
              textAlign: TextAlign.center,
              style: getRegularTextStyle(
                fontSize: FontSizeManager.size10,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
