import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../../domain/entities/rating_tv/rating_tv_entity.dart';
import '../controller/tv_details_bloc.dart';
import '../controller/tv_details_events.dart';
import '../controller/tv_details_states.dart';

class ReviewsTv extends StatelessWidget {
  final int tvId;

  const ReviewsTv({Key? key, required this.tvId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TvDetailsBloc, TvDetailsStates>(
      builder: (context, states) {
        switch (states.tvRatingsRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return states.ratings.isEmpty
                ? BaseEmptyWidget(emptyMessage: StringsManager.noReviewsTvString)
                : ListView.separated(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, index) => getReviewItem(
                      review: states.ratings[index],
                      context: context,
                    ),
                    separatorBuilder: (context, index) => const Divider(
                      thickness: SizesManager.s1_2,
                      color: ColorsManager.kBlack,
                    ),
                    itemCount: states.ratings.length,
                  );
          case RequestStates.error:
            return BaseError(
              errorMessage: states.errorTvReviewsMessage,
              onPressFunction: () {
                BlocProvider.of<TvDetailsBloc>(context)
                  ..add(GetTvReviewEvent(tvId: tvId));
              },
            );
        }
      },
    );
  }

  /// review item
  Widget getReviewItem(
      {required RatingTvEntity review, required BuildContext context}) {
    return Column(
      children: [
        ListTile(
          leading: SizedBox(
            width: getHeight(inputHeight: SizesManager.s50),
            height: getHeight(inputHeight: SizesManager.s50),
            child: ClipOval(
              child: FadeInImage.assetNetwork(
                image: FunctionsManager.showAPIImage(
                    review.authorDetailsTvEntity.avatarPath),
                placeholder: ImagesManager.logoImage,
                fit: BoxFit.cover,
              ),
            ),
          ),
          title: Text(
            review.authorDetailsTvEntity.name.toString(),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size16,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
        ),
        getMovieRating(
            rating: review.authorDetailsTvEntity.rating, context: context),
        SizedBox(height: getHeight(inputHeight: SizesManager.s16)),
        SizedBox(
          width: getWidth(inputWidth: SizesManager.s250),
          child: Text(
            review.content.toString(),
            style: getRegularTextStyle(
              fontSize: FontSizeManager.size14,
              letterSpacing: 0,
              context: context,
            ),
          ),
        )
      ],
    );
  }

  /// get Movie Rating
  Widget getMovieRating({required num? rating, required BuildContext context}) {
    if (rating == null) {
      return const SizedBox.shrink();
    } else {
      return Column(
        children: [
          SizedBox(height: getHeight(inputHeight: SizesManager.s16)),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const BaseIcon(
                iconHeight: SizesManager.s16,
                iconPath: IconsManager.starIcon,
                iconColor: ColorsManager.voteColor,
              ),
              Text(
                rating.toString(),
                style: getBoldTextStyle(
                  fontSize: FontSizeManager.size16,
                  letterSpacing: SizesManager.s1_2,
                  context: context,
                ),
              ),
            ],
          ),
        ],
      );
    }
  }
}
