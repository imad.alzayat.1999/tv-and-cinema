import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import '../entities/credits_tv/credits_tv_entity.dart';
import '../entities/rating_tv/rating_tv_entity.dart';
import '../entities/recommended_tv/recommended_tv_entity.dart';
import '../entities/similar_tv/similar_tv_entity.dart';
import '../entities/tv_details/tv_details_entity.dart';
import '../usecases/get_credits_tv_usecase.dart';
import '../usecases/get_tv_details_usecase.dart';
import '../usecases/get_tv_recommended_usecase.dart';
import '../usecases/get_tv_reviews_usecase.dart';
import '../usecases/get_tv_similar_usecase.dart';

abstract class BaseTvDetailsRepository{
  Future<Either<Failure , TvDetailsEntity>> getTvDetails(TvDetailsParameters tvDetailsParameters);
  Future<Either<Failure , List<RecommendedTvEntity>>> getTvRecommended(RecommendedTvParameters recommendedTvParameters);
  Future<Either<Failure , List<SimilarTvEntity>>> getTvSimilar(SimilarTvParameters similarTvParameters);
  Future<Either<Failure , CreditsTvEntity>> getTvCredits(CreditsTvParameters creditsTvParameters);
  Future<Either<Failure , List<RatingTvEntity>>> getTvRating(ReviewsTvParameters reviewsTvParameters);
}