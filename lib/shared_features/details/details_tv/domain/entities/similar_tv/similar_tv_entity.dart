import 'package:equatable/equatable.dart';

class SimilarTvEntity extends Equatable{
  final int id;
  final String? backdropPath;

  SimilarTvEntity({required this.id, required this.backdropPath});

  @override
  // TODO: implement props
  List<Object?> get props => [id , backdropPath];
}