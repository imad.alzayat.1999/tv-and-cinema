import 'package:equatable/equatable.dart';

class AuthorDetailsTvEntity extends Equatable{
  final String? name;
  final String? avatarPath;
  final num? rating;

  AuthorDetailsTvEntity({required this.name, required this.avatarPath, required this.rating});

  @override
  // TODO: implement props
  List<Object?> get props => [name , avatarPath , rating];
}