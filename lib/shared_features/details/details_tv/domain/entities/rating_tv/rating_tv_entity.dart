import 'package:equatable/equatable.dart';
import 'author_details_tv_entity.dart';

class RatingTvEntity extends Equatable {
  final AuthorDetailsTvEntity authorDetailsTvEntity;
  final String? content;

  RatingTvEntity({required this.authorDetailsTvEntity, required this.content});

  @override
  // TODO: implement props
  List<Object?> get props => [authorDetailsTvEntity, content];
}
