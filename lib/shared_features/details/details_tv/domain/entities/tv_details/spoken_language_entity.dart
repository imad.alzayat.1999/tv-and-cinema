import 'package:equatable/equatable.dart';

class SpokenLanguageEntity extends Equatable{
  final String? name;

  SpokenLanguageEntity({required this.name});

  @override
  // TODO: implement props
  List<Object?> get props => [name];
}