import 'package:equatable/equatable.dart';

class CreatedByEntity extends Equatable{
  final int? id;
  final String? name;
  final String? profileImage;

  CreatedByEntity({required this.id, required this.name, required this.profileImage});

  @override
  // TODO: implement props
  List<Object?> get props => [id , name , profileImage];
}