import 'package:equatable/equatable.dart';

class LastEpisodeToAirEntity extends Equatable {
  final String? airDate;
  final int? episodeNumber;
  final int? seasonNumber;
  final int? id;
  final String? name;
  final String? overview;
  final String? stillPath;
  final num? voteAvg;

  LastEpisodeToAirEntity({
    required this.airDate,
    required this.episodeNumber,
    required this.seasonNumber,
    required this.id,
    required this.name,
    required this.overview,
    required this.stillPath,
    required this.voteAvg,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        airDate,
        episodeNumber,
        seasonNumber,
        id,
        name,
        overview,
        stillPath,
        voteAvg
      ];
}
