import 'package:equatable/equatable.dart';

class TvSeasonEntity extends Equatable {
  final String? airDate;
  final int? episodeNumber;
  final int? seasonNumber;
  final int? id;
  final String? name;
  final String? overview;
  final String? posterPath;

  TvSeasonEntity({
    required this.airDate,
    required this.episodeNumber,
    required this.seasonNumber,
    required this.id,
    required this.name,
    required this.overview,
    required this.posterPath,
  });

  @override
  // TODO: implement props
  List<Object?> get props =>
      [airDate, episodeNumber, id, name, overview, posterPath , seasonNumber];
}
