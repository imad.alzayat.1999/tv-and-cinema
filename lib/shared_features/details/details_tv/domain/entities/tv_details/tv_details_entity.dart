import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_tv/domain/entities/tv_details/tv_genre_entity.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_tv/domain/entities/tv_details/tv_season_entity.dart';
import 'created_by_entity.dart';
import 'last_episode_to_air_entity.dart';
import 'spoken_language_entity.dart';

class TvDetailsEntity extends Equatable {
  final String? tvImage;
  final String name;
  final int id;
  final List<int> episodeRunTime;
  final String firstAirDate;
  final String lastAirDate;
  final int numberOfEpisodes;
  final int numberOfSeasons;
  final String overview;
  final String tvType;
  final num tvVoteAvg;
  final List<TvGenreEntity> tvGenres;
  final List<TvSeasonEntity> tvSeasons;
  final List<CreatedByEntity> authors;
  final LastEpisodeToAirEntity lastEpisodeToAirEntity;
  final List<SpokenLanguageEntity> spokenLanguagesEntity;

  TvDetailsEntity({
    required this.tvImage,
    required this.name,
    required this.id,
    required this.episodeRunTime,
    required this.firstAirDate,
    required this.lastAirDate,
    required this.numberOfEpisodes,
    required this.numberOfSeasons,
    required this.overview,
    required this.tvType,
    required this.tvVoteAvg,
    required this.tvGenres,
    required this.tvSeasons,
    required this.authors,
    required this.lastEpisodeToAirEntity,
    required this.spokenLanguagesEntity,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        tvImage,
        name,
        id,
        episodeRunTime,
        firstAirDate,
        lastAirDate,
        numberOfSeasons,
        numberOfEpisodes,
        overview,
        tvType,
        tvVoteAvg,
        tvGenres,
        authors,
        tvSeasons,
        lastEpisodeToAirEntity,
        spokenLanguagesEntity,
      ];
}
