import 'package:equatable/equatable.dart';

class TvGenreEntity extends Equatable{
  final String? name;

  TvGenreEntity({required this.name});

  @override
  // TODO: implement props
  List<Object?> get props => [name];

}