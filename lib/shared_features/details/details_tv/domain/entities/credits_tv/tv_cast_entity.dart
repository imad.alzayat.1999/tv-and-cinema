import 'package:equatable/equatable.dart';

class TvCastEntity extends Equatable {
  final int? id;
  final String? name;
  final String? character;
  final String? profilePath;

  TvCastEntity({
    required this.id,
    required this.name,
    required this.character,
    required this.profilePath,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [id, name , character , profilePath];
}
