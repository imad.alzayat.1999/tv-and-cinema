import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_tv/domain/entities/credits_tv/tv_cast_entity.dart';

class CreditsTvEntity extends Equatable{
  final List<TvCastEntity> tvCasts;

  CreditsTvEntity({required this.tvCasts});

  @override
  // TODO: implement props
  List<Object?> get props => [tvCasts];
}