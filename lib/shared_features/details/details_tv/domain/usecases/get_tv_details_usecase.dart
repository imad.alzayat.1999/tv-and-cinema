import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../entities/tv_details/tv_details_entity.dart';
import '../repository/base_tv_details_repository.dart';


class GetTvDetailsUseCase extends BaseUseCase<TvDetailsEntity , TvDetailsParameters>{
  final BaseTvDetailsRepository baseTvDetailsRepository;

  GetTvDetailsUseCase({required this.baseTvDetailsRepository});
  @override
  Future<Either<Failure, TvDetailsEntity>> call(TvDetailsParameters parameters) async{
    return await baseTvDetailsRepository.getTvDetails(parameters);
  }
}

class TvDetailsParameters extends Equatable{
  final int tvId;

  TvDetailsParameters({required this.tvId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId];
}