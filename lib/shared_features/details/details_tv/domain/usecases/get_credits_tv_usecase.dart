import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../entities/credits_tv/credits_tv_entity.dart';
import '../repository/base_tv_details_repository.dart';

class GetCreditsTvUseCase extends BaseUseCase<CreditsTvEntity , CreditsTvParameters>{
  final BaseTvDetailsRepository baseTvDetailsRepository;

  GetCreditsTvUseCase({required this.baseTvDetailsRepository});

  @override
  Future<Either<Failure, CreditsTvEntity>> call(CreditsTvParameters parameters) async{
    return await baseTvDetailsRepository.getTvCredits(parameters);
  }
}

class CreditsTvParameters extends Equatable{
  final int tvId;

  CreditsTvParameters({required this.tvId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId];
}