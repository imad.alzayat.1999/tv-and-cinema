import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entities/similar_tv/similar_tv_entity.dart';
import '../repository/base_tv_details_repository.dart';

class GetTvSimilarUseCase extends BaseUseCase<List<SimilarTvEntity> , SimilarTvParameters>{
  final BaseTvDetailsRepository baseTvDetailsRepository;

  GetTvSimilarUseCase({required this.baseTvDetailsRepository});
  @override
  Future<Either<Failure, List<SimilarTvEntity>>> call(SimilarTvParameters parameters) async{
    return await baseTvDetailsRepository.getTvSimilar(parameters);
  }
}

class SimilarTvParameters extends Equatable{
  final int tvId;

  SimilarTvParameters({required this.tvId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId];
}