import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entities/recommended_tv/recommended_tv_entity.dart';
import '../repository/base_tv_details_repository.dart';

class GetTvRecommendedUseCase extends BaseUseCase<List<RecommendedTvEntity> , RecommendedTvParameters>{
  final BaseTvDetailsRepository baseTvDetailsRepository;

  GetTvRecommendedUseCase({required this.baseTvDetailsRepository});
  @override
  Future<Either<Failure, List<RecommendedTvEntity>>> call(RecommendedTvParameters parameters) async{
    return await baseTvDetailsRepository.getTvRecommended(parameters);
  }
}

class RecommendedTvParameters extends Equatable{
  final int tvId;

  RecommendedTvParameters({required this.tvId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId];
}