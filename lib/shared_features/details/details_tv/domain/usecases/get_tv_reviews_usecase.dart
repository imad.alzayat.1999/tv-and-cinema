import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entities/rating_tv/rating_tv_entity.dart';
import '../repository/base_tv_details_repository.dart';


class GetTvReviewsUseCase extends BaseUseCase<List<RatingTvEntity> , ReviewsTvParameters>{
  final BaseTvDetailsRepository baseTvDetailsRepository;

  GetTvReviewsUseCase({required this.baseTvDetailsRepository});

  @override
  Future<Either<Failure, List<RatingTvEntity>>> call(ReviewsTvParameters parameters) async{
    return await baseTvDetailsRepository.getTvRating(parameters);
  }
}

class ReviewsTvParameters extends Equatable{
  final int tvId;

  ReviewsTvParameters({required this.tvId});

  @override
  // TODO: implement props
  List<Object?> get props => [tvId];
}