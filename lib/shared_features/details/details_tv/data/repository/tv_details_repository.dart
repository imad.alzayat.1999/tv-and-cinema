import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';

import '../../domain/entities/credits_tv/credits_tv_entity.dart';
import '../../domain/entities/rating_tv/rating_tv_entity.dart';
import '../../domain/entities/recommended_tv/recommended_tv_entity.dart';
import '../../domain/entities/similar_tv/similar_tv_entity.dart';
import '../../domain/entities/tv_details/tv_details_entity.dart';
import '../../domain/repository/base_tv_details_repository.dart';
import '../../domain/usecases/get_credits_tv_usecase.dart';
import '../../domain/usecases/get_tv_details_usecase.dart';
import '../../domain/usecases/get_tv_recommended_usecase.dart';
import '../../domain/usecases/get_tv_reviews_usecase.dart';
import '../../domain/usecases/get_tv_similar_usecase.dart';
import '../datasources/tv_details_remote_datasource.dart';

class TvDetailsRepository extends BaseTvDetailsRepository {
  final BaseTvDetailsRemoteDataSource baseTvDetailsRemoteDataSource;

  TvDetailsRepository({required this.baseTvDetailsRemoteDataSource});

  @override
  Future<Either<Failure, TvDetailsEntity>> getTvDetails(
      TvDetailsParameters tvDetailsParameters) async {
    try {
      final result = await baseTvDetailsRemoteDataSource.getTvDetailsModel(
          tvDetailsParameters: tvDetailsParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<RecommendedTvEntity>>> getTvRecommended(
      RecommendedTvParameters recommendedTvParameters) async {
    try {
      final result = await baseTvDetailsRemoteDataSource.getTvRecommended(
          recommendedTvParameters: recommendedTvParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<SimilarTvEntity>>> getTvSimilar(
      SimilarTvParameters similarTvParameters) async {
    try {
      final result = await baseTvDetailsRemoteDataSource.getTvSimilar(
          similarTvParameters: similarTvParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, CreditsTvEntity>> getTvCredits(
      CreditsTvParameters creditsTvParameters) async {
    try {
      final result = await baseTvDetailsRemoteDataSource.getTvCredits(
          creditsTvParameters: creditsTvParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, List<RatingTvEntity>>> getTvRating(
      ReviewsTvParameters reviewsTvParameters) async {
    try {
      final result = await baseTvDetailsRemoteDataSource.getTvReview(
          reviewsTvParameters: reviewsTvParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }
}
