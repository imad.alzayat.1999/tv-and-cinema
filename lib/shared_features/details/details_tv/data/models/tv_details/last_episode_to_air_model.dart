import '../../../domain/entities/tv_details/last_episode_to_air_entity.dart';

class LastEpisodeToAirModel extends LastEpisodeToAirEntity {
  LastEpisodeToAirModel({
    required String? airDate,
    required int? episodeNumber,
    required int? seasonNumber,
    required int? id,
    required String? name,
    required String? overview,
    required String? stillPath,
    required num voteAvg,
  }) : super(
          airDate: airDate,
          episodeNumber: episodeNumber,
          seasonNumber: seasonNumber,
          id: id,
          name: name,
          overview: overview,
          stillPath: stillPath,
          voteAvg: voteAvg,
        );

  factory LastEpisodeToAirModel.fromJson(Map<String, dynamic> json) =>
      LastEpisodeToAirModel(
        airDate: json["air_date"],
        episodeNumber: json["episode_number"],
        seasonNumber: json["season_number"],
        id: json["id"],
        name: json["name"],
        overview: json["overview"],
        stillPath: json["still_path"],
        voteAvg: json["vote_average"],
      );

  Map<String , dynamic> toJson() => {
    "air_date": airDate,
    "episode_number": episodeNumber,
    "season_number": seasonNumber,
    "id": id,
    "name": name,
    "overview": overview,
    "still_path": stillPath,
    "vote_average": voteAvg
  };
}
