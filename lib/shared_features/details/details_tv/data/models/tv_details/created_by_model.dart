import '../../../domain/entities/tv_details/created_by_entity.dart';

class CreatedByModel extends CreatedByEntity {
  CreatedByModel(
      {required int? id, required String? name, required String? profileImage})
      : super(id: id, name: name, profileImage: profileImage);

  factory CreatedByModel.fromJson(Map<String, dynamic> json) =>
      CreatedByModel(id: json["id"], name: json["name"], profileImage: json["profile_path"]);

  Map<String , dynamic> toJson() => {
    "id": id,
    "name": name,
    "profile_path": profileImage,
  };
}
