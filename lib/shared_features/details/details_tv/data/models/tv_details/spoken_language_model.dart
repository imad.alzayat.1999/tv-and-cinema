import '../../../domain/entities/tv_details/spoken_language_entity.dart';

class SpokenLanguageModel extends SpokenLanguageEntity {
  SpokenLanguageModel({required String? name}) : super(name: name);

  factory SpokenLanguageModel.fromJson(Map<String, dynamic> json) =>
      SpokenLanguageModel(name: json["english_name"]);

  Map<String, dynamic> toJson() => {
        "english_name": name,
      };
}
