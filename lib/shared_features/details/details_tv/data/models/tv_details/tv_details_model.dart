import 'package:movie_app_clean_architecture/shared_features/details/details_tv/data/models/tv_details/spoken_language_model.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_tv/data/models/tv_details/tv_genre_model.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_tv/data/models/tv_details/tv_season_model.dart';

import '../../../domain/entities/tv_details/created_by_entity.dart';
import '../../../domain/entities/tv_details/last_episode_to_air_entity.dart';
import '../../../domain/entities/tv_details/spoken_language_entity.dart';
import '../../../domain/entities/tv_details/tv_details_entity.dart';
import '../../../domain/entities/tv_details/tv_genre_entity.dart';
import '../../../domain/entities/tv_details/tv_season_entity.dart';
import 'created_by_model.dart';
import 'last_episode_to_air_model.dart';

class TvDetailsModel extends TvDetailsEntity {
  TvDetailsModel({
    required String? tvImage,
    required String name,
    required int id,
    required List<int> episodeRunTime,
    required String firstAirDate,
    required String lastAirDate,
    required int numberOfEpisodes,
    required int numberOfSeasons,
    required String overview,
    required String tvType,
    required num tvVoteAvg,
    required List<TvGenreEntity> tvGenres,
    required List<TvSeasonEntity> tvSeasons,
    required List<CreatedByEntity> authors,
    required LastEpisodeToAirEntity lastEpisodeToAirEntity,
    required List<SpokenLanguageEntity> spokenLanguagesEntity,
  }) : super(
          tvImage: tvImage,
          name: name,
          id: id,
          episodeRunTime: episodeRunTime,
          firstAirDate: firstAirDate,
          lastAirDate: lastAirDate,
          numberOfEpisodes: numberOfEpisodes,
          numberOfSeasons: numberOfSeasons,
          overview: overview,
          tvType: tvType,
          tvVoteAvg: tvVoteAvg,
          tvGenres: tvGenres,
          tvSeasons: tvSeasons,
          authors: authors,
          lastEpisodeToAirEntity: lastEpisodeToAirEntity,
          spokenLanguagesEntity: spokenLanguagesEntity,
        );

  factory TvDetailsModel.fromJson(Map<String, dynamic> json) => TvDetailsModel(
        tvImage: json["backdrop_path"],
        name: json["name"],
        id: json["id"],
        episodeRunTime: List<int>.from(json["episode_run_time"].map((x) => x)),
        firstAirDate: json["first_air_date"],
        lastAirDate: json["last_air_date"],
        numberOfEpisodes: json["number_of_episodes"],
        numberOfSeasons: json["number_of_seasons"],
        overview: json["overview"],
        tvType: json["type"],
        tvVoteAvg: json["vote_average"]?.toDouble(),
        tvGenres: List<TvGenreModel>.from(json["genres"].map((x) => TvGenreModel.fromJson(x))),
        tvSeasons: List<TvSeasonModel>.from(json["seasons"].map((x) => TvSeasonModel.fromJson(x))),
        authors: List<CreatedByModel>.from(json["created_by"].map((x) => CreatedByModel.fromJson(x))),
        lastEpisodeToAirEntity: LastEpisodeToAirModel.fromJson(json["last_episode_to_air"]),
        spokenLanguagesEntity: List<SpokenLanguageModel>.from(json["spoken_languages"].map((x) => SpokenLanguageModel.fromJson(x))),
      );
}
