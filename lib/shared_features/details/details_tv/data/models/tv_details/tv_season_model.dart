import '../../../domain/entities/tv_details/tv_season_entity.dart';

class TvSeasonModel extends TvSeasonEntity {
  TvSeasonModel({
    required String? airDate,
    required int? episodeNumber,
    required int? id,
    required String? name,
    required String? overview,
    required String? posterPath,
    required int? seasonNumber,
  }) : super(
          airDate: airDate,
          episodeNumber: episodeNumber,
          id: id,
          name: name,
          overview: overview,
          posterPath: posterPath,
          seasonNumber: seasonNumber,
        );

  factory TvSeasonModel.fromJson(Map<String, dynamic> json) => TvSeasonModel(
        airDate: json["air_date"],
        episodeNumber: json["episode_count"],
        id: json["id"],
        name: json["name"],
        overview: json["overview"],
        posterPath: json["poster_path"],
        seasonNumber: json["season_number"],
      );

  Map<String, dynamic> toJson() => {
        "air_date": airDate,
        "episode_count": episodeNumber,
        "id": id,
        "name": name,
        "overview": overview,
        "poster_path": posterPath,
        "season_number": seasonNumber,
      };
}
