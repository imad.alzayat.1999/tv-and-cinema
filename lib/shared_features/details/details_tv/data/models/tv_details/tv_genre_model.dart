import '../../../domain/entities/tv_details/tv_genre_entity.dart';

class TvGenreModel extends TvGenreEntity {
  TvGenreModel({required String? name}) : super(name: name);

  factory TvGenreModel.fromJson(Map<String, dynamic> json) => TvGenreModel(
        name: json["name"],
      );

  Map<String , dynamic> toJson() => {
    "name": name,
  };
}
