import '../../../domain/entities/similar_tv/similar_tv_entity.dart';

class SimilarTvModel extends SimilarTvEntity {
  SimilarTvModel({required int id, required String? backdropPath})
      : super(id: id, backdropPath: backdropPath);

  factory SimilarTvModel.fromJson(Map<String, dynamic> json) =>
      SimilarTvModel(id: json["id"], backdropPath: json["backdrop_path"]);

  Map<String , dynamic> toJson() => {
    "id": id,
    "backdrop_path": backdropPath,
  };
}
