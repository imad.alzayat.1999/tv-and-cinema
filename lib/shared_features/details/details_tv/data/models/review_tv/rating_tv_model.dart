import '../../../domain/entities/rating_tv/author_details_tv_entity.dart';
import '../../../domain/entities/rating_tv/rating_tv_entity.dart';
import 'author_details_tv_model.dart';

class RatingTvModel extends RatingTvEntity {
  RatingTvModel({
    required AuthorDetailsTvEntity authorDetailsTvEntity,
    required String? content,
  }) : super(
          authorDetailsTvEntity: authorDetailsTvEntity,
          content: content,
        );

  factory RatingTvModel.fromJson(Map<String, dynamic> json) => RatingTvModel(
        authorDetailsTvEntity:
            AuthorDetailsTvModel.fromJson(json["author_details"]),
        content: json["content"],
      );
}
