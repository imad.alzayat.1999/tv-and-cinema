import '../../../domain/entities/rating_tv/author_details_tv_entity.dart';

class AuthorDetailsTvModel extends AuthorDetailsTvEntity {
  AuthorDetailsTvModel({
    required String? name,
    required String? avatarPath,
    required num? rating,
  }) : super(
          name: name,
          avatarPath: avatarPath,
          rating: rating,
        );

  factory AuthorDetailsTvModel.fromJson(Map<String, dynamic> json) =>
      AuthorDetailsTvModel(
        name: json["name"],
        avatarPath: json["avatar_path"],
        rating: json["rating"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "avatar_path": avatarPath,
        "rating": rating,
      };
}
