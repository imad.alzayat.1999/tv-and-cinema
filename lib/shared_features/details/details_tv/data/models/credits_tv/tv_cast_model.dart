import '../../../domain/entities/credits_tv/tv_cast_entity.dart';

class TvCastModel extends TvCastEntity {
  TvCastModel({
    required int? id,
    required String? name,
    required String? character,
    required String? profilePath,
  }) : super(
          id: id,
          name: name,
          character: character,
          profilePath: profilePath,
        );

  factory TvCastModel.fromJson(Map<String, dynamic> json) => TvCastModel(
        id: json["id"],
        name: json["name"],
        character: json["character"],
        profilePath: json["profile_path"],
      );

  Map<String , dynamic> toJson() => {
    "id": id,
    "name": name,
    "character": character,
    "profile_path": profilePath,
  };
}
