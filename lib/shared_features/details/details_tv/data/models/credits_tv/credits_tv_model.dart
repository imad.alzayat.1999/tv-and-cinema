import 'package:movie_app_clean_architecture/shared_features/details/details_tv/data/models/credits_tv/tv_cast_model.dart';

import '../../../domain/entities/credits_tv/credits_tv_entity.dart';
import '../../../domain/entities/credits_tv/tv_cast_entity.dart';

class CreditsTvModel extends CreditsTvEntity {
  CreditsTvModel({required List<TvCastEntity> tvCasts})
      : super(tvCasts: tvCasts);

  factory CreditsTvModel.fromJson(Map<String, dynamic> json) => CreditsTvModel(
        tvCasts: List<TvCastModel>.from(json["cast"].map((x) => TvCastModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
    "cast": List<dynamic>.from(tvCasts.map((x) => x)),
  };
}
