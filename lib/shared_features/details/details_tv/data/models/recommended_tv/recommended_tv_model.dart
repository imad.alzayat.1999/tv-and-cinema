import '../../../domain/entities/recommended_tv/recommended_tv_entity.dart';

class RecommendedTvModel extends RecommendedTvEntity {
  RecommendedTvModel({required int id, required String? backdropPath})
      : super(id: id, backdropPath: backdropPath);

  factory RecommendedTvModel.fromJson(Map<String, dynamic> json) =>
      RecommendedTvModel(
        id: json["id"],
        backdropPath: json["backdrop_path"]
      );

  Map<String , dynamic> toJson() => {
    "id" : id,
    "backdrop_path": backdropPath,
  };
}
