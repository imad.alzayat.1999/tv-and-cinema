import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';

import '../../domain/usecases/get_credits_tv_usecase.dart';
import '../../domain/usecases/get_tv_details_usecase.dart';
import '../../domain/usecases/get_tv_recommended_usecase.dart';
import '../../domain/usecases/get_tv_reviews_usecase.dart';
import '../../domain/usecases/get_tv_similar_usecase.dart';
import '../models/credits_tv/credits_tv_model.dart';
import '../models/recommended_tv/recommended_tv_model.dart';
import '../models/review_tv/rating_tv_model.dart';
import '../models/similar_tv/similar_tv_model.dart';
import '../models/tv_details/tv_details_model.dart';

abstract class BaseTvDetailsRemoteDataSource {
  Future<TvDetailsModel> getTvDetailsModel(
      {required TvDetailsParameters tvDetailsParameters});
  Future<List<RecommendedTvModel>> getTvRecommended(
      {required RecommendedTvParameters recommendedTvParameters});
  Future<List<SimilarTvModel>> getTvSimilar(
      {required SimilarTvParameters similarTvParameters});
  Future<CreditsTvModel> getTvCredits(
      {required CreditsTvParameters creditsTvParameters});
  Future<List<RatingTvModel>> getTvReview(
      {required ReviewsTvParameters reviewsTvParameters});
}

class TvDetailsRemoteDataSource extends BaseTvDetailsRemoteDataSource {
  final ApiConsumer apiConsumer;

  TvDetailsRemoteDataSource({required this.apiConsumer});

  @override
  Future<TvDetailsModel> getTvDetailsModel({
    required TvDetailsParameters tvDetailsParameters,
  }) async {
    final response = await apiConsumer.get(
        path: ApiConstance.getTvByIdEndPoint(tvDetailsParameters.tvId));
    return TvDetailsModel.fromJson(response);
  }

  @override
  Future<List<RecommendedTvModel>> getTvRecommended(
      {required RecommendedTvParameters recommendedTvParameters}) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getTvRecommendedByIdEndPoint(
        recommendedTvParameters.tvId,
      ),
    );
    return List<RecommendedTvModel>.from((response["results"] as List)
        .map((e) => RecommendedTvModel.fromJson(e)));
  }

  @override
  Future<List<SimilarTvModel>> getTvSimilar(
      {required SimilarTvParameters similarTvParameters}) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getTvSimilarByIdEndPoint(
        similarTvParameters.tvId,
      ),
    );
    return List<SimilarTvModel>.from(
        (response["results"] as List).map((e) => SimilarTvModel.fromJson(e)));
  }

  @override
  Future<CreditsTvModel> getTvCredits(
      {required CreditsTvParameters creditsTvParameters}) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getTvCreditsByIdEndPoint(
        creditsTvParameters.tvId,
      ),
    );
    return CreditsTvModel.fromJson(response);
  }

  @override
  Future<List<RatingTvModel>> getTvReview(
      {required ReviewsTvParameters reviewsTvParameters}) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getTvReviewByIdEndPoint(reviewsTvParameters.tvId),
    );
    return List<RatingTvModel>.from(
        (response["results"] as List).map((e) => RatingTvModel.fromJson(e)));
  }
}
