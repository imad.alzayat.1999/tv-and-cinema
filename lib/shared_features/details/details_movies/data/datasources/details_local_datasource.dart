import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/services/services_locator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/credits/credits_model.dart';
import '../models/movie_details_model.dart';
import '../models/movie_trailers/movie_trailers_model.dart';
import '../models/recommended/recommended_movies_model.dart';
import '../models/reviews/movie_reviews_model.dart';
import '../models/similar/similar_movies_model.dart';

abstract class BaseDetailsLocalDataSource {
  Future<Unit> cacheReviews(MovieReviewsModel movieReviews);
  Future<MovieReviewsModel> getCachedReviews();
  Future<Unit> cacheMovieDetails(MovieDetailsModel movieDetails);
  Future<MovieDetailsModel> getCachedMovieDetails();
  Future<Unit> cacheRecommendedMovies(RecommendedMoviesModel recommendedMovies);
  Future<RecommendedMoviesModel> getCachedRecommendedMovies();
  Future<Unit> cacheCredits(CreditsModel creditsModel);
  Future<CreditsModel> getCachedCredits();
  Future<Unit> cacheSimilarMovies(SimilarMoviesModel similarMovies);
  Future<SimilarMoviesModel> getCachedSimilarMovies();
  Future<Unit> cacheMovieTrailers(MovieTrailersModel trailers);
  Future<MovieTrailersModel> getCachedMovieTrailers();
}

class DetailsLocalDataSource extends BaseDetailsLocalDataSource {
  @override
  Future<Unit> cacheReviews(MovieReviewsModel movieReviews) {
    getIt<SharedPreferences>().setString(
      ConstsManager.CACHE_REVIEWS,
      jsonEncode(movieReviews.toJson()),
    );
    print(
        "Info Cached for ${ConstsManager.CACHE_REVIEWS} is ${jsonEncode(movieReviews.toJson())}");
    return Future.value(unit);
  }

  @override
  Future<MovieReviewsModel> getCachedReviews() {
    final cachedData =
        getIt<SharedPreferences>().getString(ConstsManager.CACHE_REVIEWS);
    final reviews = jsonDecode(cachedData!);
    return Future.value(MovieReviewsModel.fromJson(reviews));
  }

  @override
  Future<Unit> cacheMovieDetails(MovieDetailsModel movieDetails) {
    getIt<SharedPreferences>().setString(
        ConstsManager.CACHE_DETAILS, jsonEncode(movieDetails.toJson()));
    print(
        "Info Cached for ${ConstsManager.CACHE_DETAILS} is ${jsonEncode(movieDetails.toJson())}");
    return Future.value(unit);
  }

  @override
  Future<MovieDetailsModel> getCachedMovieDetails() {
    final cachedData =
        getIt<SharedPreferences>().getString(ConstsManager.CACHE_DETAILS);
    final details = jsonDecode(cachedData!);
    return Future.value(MovieDetailsModel.fromJson(details));
  }

  @override
  Future<Unit> cacheRecommendedMovies(
      RecommendedMoviesModel recommendedMovies) {
    getIt<SharedPreferences>().setString(ConstsManager.CACHE_RECOMMENDED,
        jsonEncode(recommendedMovies.toJson()));
    print(
        "Info Cached for ${ConstsManager.CACHE_RECOMMENDED} is ${jsonEncode(recommendedMovies.toJson())}");
    return Future.value(unit);
  }

  @override
  Future<RecommendedMoviesModel> getCachedRecommendedMovies() {
    final cachedData =
        getIt<SharedPreferences>().getString(ConstsManager.CACHE_RECOMMENDED);
    final recommended = jsonDecode(cachedData!);
    return Future.value(RecommendedMoviesModel.fromJson(recommended));
  }

  @override
  Future<Unit> cacheCredits(CreditsModel creditsModel) {
    getIt<SharedPreferences>().setString(
        ConstsManager.CACHE_CREDITS, jsonEncode(creditsModel.toJson()));
    print(
        "Info Cached for ${ConstsManager.CACHE_CREDITS} is ${jsonEncode(creditsModel.toJson())}");
    return Future.value(unit);
  }

  @override
  Future<CreditsModel> getCachedCredits() {
    final cachedData =
        getIt<SharedPreferences>().getString(ConstsManager.CACHE_CREDITS);
    final credits = jsonDecode(cachedData!);
    return Future.value(CreditsModel.fromJson(credits));
  }

  @override
  Future<Unit> cacheSimilarMovies(SimilarMoviesModel similarMovies) {
    getIt<SharedPreferences>().setString(
        ConstsManager.CACHE_SIMILAR, jsonEncode(similarMovies.toJson()));
    print(
        "Info Cached for ${ConstsManager.CACHE_SIMILAR} is ${jsonEncode(similarMovies.toJson())}");
    return Future.value(unit);
  }

  @override
  Future<SimilarMoviesModel> getCachedSimilarMovies() {
    final cachedData =
        getIt<SharedPreferences>().getString(ConstsManager.CACHE_SIMILAR);
    final similar = jsonDecode(cachedData!);
    return Future.value(SimilarMoviesModel.fromJson(similar));
  }

  @override
  Future<Unit> cacheMovieTrailers(MovieTrailersModel trailers) {
    getIt<SharedPreferences>().setString(
        ConstsManager.CACHE_TRAILER, jsonEncode(trailers.toJson()));
    print(
        "Info Cached for ${ConstsManager.CACHE_TRAILER} is ${jsonEncode(trailers.toJson())}");
    return Future.value(unit);
  }

  @override
  Future<MovieTrailersModel> getCachedMovieTrailers() {
    final cachedData =
    getIt<SharedPreferences>().getString(ConstsManager.CACHE_TRAILER);
    final trailer = jsonDecode(cachedData!);
    return Future.value(MovieTrailersModel.fromJson(trailer));
  }
}
