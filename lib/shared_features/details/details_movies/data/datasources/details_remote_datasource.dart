import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import '../../../../../core/network/api_constance.dart';
import '../../domain/usecases/get_movie_credits_usecase.dart';
import '../../domain/usecases/get_movie_details_usecase.dart';
import '../../domain/usecases/get_movie_reviews.dart';
import '../../domain/usecases/get_movie_trailers_usecase.dart';
import '../../domain/usecases/get_recommended_movies_usecase.dart';
import '../../domain/usecases/get_similar_movies_usecase.dart';
import '../models/credits/credits_model.dart';
import '../models/movie_details_model.dart';
import '../models/movie_trailers/movie_trailers_model.dart';
import '../models/recommended/recommended_movies_model.dart';
import '../models/reviews/movie_reviews_model.dart';
import '../models/similar/similar_movies_model.dart';

abstract class BaseDetailsRemoteDataSource {
  Future<MovieDetailsModel> getMovieDetails(
    MovieDetailsParameter parameter,
  );
  Future<RecommendedMoviesModel> getRecommendedMovies(
    RecommendedMoviesParameters parameter,
  );
  Future<MovieReviewsModel> getMovieReviews(
    MovieReviewsParameters movieReviewsParameters,
  );
  Future<CreditsModel> getMovieCredits(
    CreditsParameters creditsParameters,
  );
  Future<SimilarMoviesModel> getSimilarMovies(
    SimilarMoviesParameters similarMoviesParameters,
  );
  Future<MovieTrailersModel> getMovieTrailers(
    MovieTrailersParameters movieTrailersParameters,
  );
}

class DetailsRemoteDataSource extends BaseDetailsRemoteDataSource {
  final ApiConsumer apiConsumer;

  DetailsRemoteDataSource({required this.apiConsumer});
  @override
  Future<MovieDetailsModel> getMovieDetails(
      MovieDetailsParameter parameter) async {
    final response = await apiConsumer.get(
        path: ApiConstance.getMovieDetailsEndPoint(parameter.movieId),
        queryParameters: {
          "language": parameter.language,
        });
    return MovieDetailsModel.fromJson(response);
  }

  @override
  Future<RecommendedMoviesModel> getRecommendedMovies(
      RecommendedMoviesParameters parameter) async {
    final response = await apiConsumer.get(
        path: ApiConstance.getRecommendedMoviesEndPoint(parameter.movieId),
        queryParameters: {
          "language": parameter.language,
        });
    return RecommendedMoviesModel.fromJson(response);
  }

  @override
  Future<MovieReviewsModel> getMovieReviews(
      MovieReviewsParameters movieReviewsParameters) async {
    final response = await apiConsumer.get(
        path: ApiConstance.getMovieReviewsEndPoint(
          movieReviewsParameters.movieId,
        ),
        queryParameters: {
          "language": movieReviewsParameters.language,
        });
    return MovieReviewsModel.fromJson(response);
  }

  @override
  Future<CreditsModel> getMovieCredits(
      CreditsParameters creditsParameters) async {
    final response = await apiConsumer.get(
        path: ApiConstance.getMovieCreditsEndPoint(creditsParameters.movieId),
        queryParameters: {
          "language": creditsParameters.language,
        });
    return CreditsModel.fromJson(response);
  }

  @override
  Future<SimilarMoviesModel> getSimilarMovies(
      SimilarMoviesParameters similarMoviesParameters) async {
    final response = await apiConsumer.get(
        path: ApiConstance.getSimilarMoviesEndPoint(
          similarMoviesParameters.movieId,
        ),
        queryParameters: {
          "language": similarMoviesParameters.language,
        });
    return SimilarMoviesModel.fromJson(response);
  }

  @override
  Future<MovieTrailersModel> getMovieTrailers(
      MovieTrailersParameters movieTrailersParameters) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getMoviesTrailersEndPoint(
        movieTrailersParameters.movieId,
      ),
    );
    return MovieTrailersModel.fromJson(response);
  }
}
