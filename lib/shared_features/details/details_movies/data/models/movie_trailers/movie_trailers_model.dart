import 'package:movie_app_clean_architecture/shared_features/details/details_movies/data/models/movie_trailers/trailer_model.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_movies/domain/entities/movie_trailers/trailer.dart';

import '../../../domain/entities/movie_trailers/movie_trailers.dart';

class MovieTrailersModel extends MovieTrailers {
  const MovieTrailersModel({required List<Trailer> trailers})
      : super(trailers: trailers);

  factory MovieTrailersModel.fromJson(Map<String, dynamic> json) =>
      MovieTrailersModel(
        trailers:
            List<TrailerModel>.from(json["results"].map((x) => TrailerModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
    "results": List<dynamic>.from(trailers.map((x) => x)),
  };
}
