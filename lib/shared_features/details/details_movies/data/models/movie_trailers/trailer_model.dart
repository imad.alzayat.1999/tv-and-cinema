import '../../../domain/entities/movie_trailers/trailer.dart';

class TrailerModel extends Trailer {
  const TrailerModel(
      {required String trailerName,
      required String trailerKey,
      required String type})
      : super(trailerName: trailerName, trailerKey: trailerKey, type: type);

  factory TrailerModel.fromJson(Map<String, dynamic> json) => TrailerModel(
        trailerName: json["name"],
        trailerKey: json["key"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "name": trailerName,
        "key": trailerKey,
        "type": type,
      };
}
