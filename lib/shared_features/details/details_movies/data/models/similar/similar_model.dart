import 'package:intl/intl.dart';
import '../../../domain/entities/similar/similar.dart';

class SimilarModel extends Similar {
  const SimilarModel({
    required String? movieImage,
    required String? movieTitle,
    required DateTime? movieReleaseDate,
    required num? movieVote,
    required int? movieId,
  }) : super(
          movieImage: movieImage,
          movieTitle: movieTitle,
          movieReleaseDate: movieReleaseDate,
          movieVote: movieVote,
          movieId: movieId,
        );

  factory SimilarModel.fromJson(Map<String, dynamic> json) => SimilarModel(
        movieId: json["id"],
        movieImage: json["backdrop_path"],
        movieTitle: json["title"],
        movieReleaseDate: json["release_date"] == "" ? DateTime.now() : DateTime.parse(json["release_date"]),
        movieVote: json["vote_average"],
      );

  Map<String, dynamic> toJson() => {
        "id": movieId,
        "backdrop_path": movieImage,
        "title": movieTitle,
        "release_date": movieReleaseDate!.toIso8601String(),
        "vote_average": movieVote,
      };
}
