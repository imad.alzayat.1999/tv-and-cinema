import 'package:movie_app_clean_architecture/shared_features/details/details_movies/data/models/similar/similar_model.dart';

import '../../../domain/entities/similar/similar.dart';
import '../../../domain/entities/similar/similar_movies.dart';

class SimilarMoviesModel extends SimilarMovies {
  const SimilarMoviesModel({required List<Similar> similars})
      : super(similars: similars);

  factory SimilarMoviesModel.fromJson(Map<String, dynamic> json) =>
      SimilarMoviesModel(
        similars:
            List<SimilarModel>.from(json["results"].map((x) => SimilarModel.fromJson(x))),
      );

  Map<String , dynamic> toJson() => {
    "results" : List<dynamic>.from(similars.map((x) => x)),
  };
}
