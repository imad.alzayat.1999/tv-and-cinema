import 'package:movie_app_clean_architecture/shared_features/details/details_movies/data/models/spoken_languages_model.dart';

import '../../domain/entities/genre.dart';
import '../../domain/entities/movie_details.dart';
import '../../domain/entities/spoken_language.dart';
import 'genre_model.dart';

class MovieDetailsModel extends MovieDetails {
  const MovieDetailsModel({
    required String backdropPath,
    required int id,
    required String overview,
    required List<Genre> genres,
    required String title,
    required double voteAverage,
    required String releaseDate,
    required int runtime,
    required List<SpokenLanguage> spokenLanguages,
  }) : super(
          backdropPath: backdropPath,
          id: id,
          overview: overview,
          genres: genres,
          title: title,
          voteAverage: voteAverage,
          releaseDate: releaseDate,
          runtime: runtime,
          spokenLanguages: spokenLanguages,
        );

  factory MovieDetailsModel.fromJson(Map<String, dynamic> json) =>
      MovieDetailsModel(
        id: json["id"],
        title: json["title"],
        backdropPath: json["backdrop_path"],
        genres: List<GenreModel>.from(
            json["genres"].map((x) => GenreModel.fromJson(x))),
        spokenLanguages: List<SpokenLanguagesModel>.from(
            json["spoken_languages"]
                .map((x) => SpokenLanguagesModel.fromJson(x))),
        overview: json["overview"],
        voteAverage: json["vote_average"],
        releaseDate: json["release_date"],
        runtime: json["runtime"],
      );

  Map<String , dynamic> toJson() => {
    "id" : id,
    "title" : title,
    "backdrop_path" : backdropPath,
    "overview" : overview,
    "vote_average" : voteAverage,
    "release_date" : releaseDate,
    "runtime": runtime,
    "genres" : List<dynamic>.from(genres.map((x) => x)),
    "spoken_languages": List<dynamic>.from(spokenLanguages.map((x) => x)),
  };
}
