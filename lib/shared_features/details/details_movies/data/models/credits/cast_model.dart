import '../../../domain/entities/credits/cast.dart';

class CastModel extends Cast {
  const CastModel({
    required String? department,
    required String? character,
    required String? originalName,
    required String? actorImage,
    required int? id,
  }) : super(
          department: department,
          character: character,
          originalName: originalName,
          actorImage: actorImage,
          id: id,
        );

  factory CastModel.fromJson(Map<String, dynamic> json) => CastModel(
        department: json["known_for_department"],
        character: json["character"],
        originalName: json["original_name"],
        actorImage: json["profile_path"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "known_for_department": department,
        "character": character,
        "original_name": originalName,
        "profile_path": actorImage,
        "id": id,
      };
}
