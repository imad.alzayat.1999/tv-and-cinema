import '../../../domain/entities/credits/cast.dart';
import '../../../domain/entities/credits/credits.dart';
import 'cast_model.dart';

class CreditsModel extends Credits {
  const CreditsModel({required List<Cast> casts}) : super(casts: casts);

  factory CreditsModel.fromJson(Map<String, dynamic> json) => CreditsModel(
        casts: List<CastModel>.from(json["cast"].map((x) => CastModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "cast": List<dynamic>.from(casts.map((x) => x)),
      };
}
