import 'package:movie_app_clean_architecture/shared_features/details/details_movies/data/models/reviews/review_model.dart';

import '../../../domain/entities/reviews/movie_reviews.dart';
import '../../../domain/entities/reviews/review.dart';

class MovieReviewsModel extends MovieReviews {
  const MovieReviewsModel({required int page, required List<Review> reviews})
      : super(page: page, reviews: reviews);

  factory MovieReviewsModel.fromJson(Map<String, dynamic> json) =>
      MovieReviewsModel(
        page: json["page"],
        reviews: List<ReviewModel>.from(
          json["results"].map(
            (x) => ReviewModel.fromJson(x),
          ),
        ),
      );

  Map<String , dynamic> toJson() => {
    "page" : page,
    "results": List<dynamic>.from(reviews.map((x) => x)),
  };
}
