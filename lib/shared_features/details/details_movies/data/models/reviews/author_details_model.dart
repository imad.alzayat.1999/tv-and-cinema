import '../../../domain/entities/reviews/author_details.dart';

class AuthorDetailsModel extends AuthorDetails {
  const AuthorDetailsModel({
    required String? authorName,
    required String? authorImage,
    required num? rating,
  }) : super(
          authorName: authorName,
          authorImage: authorImage,
          rating: rating,
        );

  factory AuthorDetailsModel.fromJson(Map<String, dynamic> json) =>
      AuthorDetailsModel(
        authorName: json["username"],
        authorImage: json["avatar_path"],
        rating: json["rating"],
      );

  Map<String, dynamic> toJson() => {
        "username": authorName,
        "avatar_path": authorImage,
        "rating": rating,
      };
}
