import '../../../domain/entities/reviews/author_details.dart';
import '../../../domain/entities/reviews/review.dart';
import 'author_details_model.dart';

class ReviewModel extends Review {
  const ReviewModel({
    required AuthorDetails authorDetails,
    required String author,
    required String content,
  }) : super(
          authorDetails: authorDetails,
          author: author,
          content: content,
        );

  factory ReviewModel.fromJson(Map<String, dynamic> json) => ReviewModel(
        authorDetails: AuthorDetailsModel.fromJson(json["author_details"]),
        author: json["author"],
        content: json["content"],
      );

  Map<String, dynamic> toJson() => {
        "author": author,
        "author_details": AuthorDetailsModel(
          authorImage: authorDetails.authorImage,
          authorName: authorDetails.authorName,
          rating: authorDetails.rating,
        ).toJson(),
        "content": content,
      };
}
