import '../../../domain/entities/recommended/recommended.dart';

class RecommendedModel extends Recommended {
  const RecommendedModel({required int id, required String backdropPath})
      : super(id: id, backdropPath: backdropPath);

  factory RecommendedModel.fromJson(Map<String, dynamic> json) =>
      RecommendedModel(
          id: json["id"], backdropPath: json["backdrop_path"] ?? '');

  Map<String, dynamic> toJson() => {
        "id": id,
        "backdrop_path": backdropPath,
      };
}
