import 'package:movie_app_clean_architecture/shared_features/details/details_movies/data/models/recommended/recommended_model.dart';

import '../../../domain/entities/recommended/recommended.dart';
import '../../../domain/entities/recommended/recommended_movies.dart';

class RecommendedMoviesModel extends RecommendedMovies {
  const RecommendedMoviesModel({required List<Recommended> recommendations})
      : super(recommendations: recommendations);

  factory RecommendedMoviesModel.fromJson(Map<String, dynamic> json) =>
      RecommendedMoviesModel(
        recommendations: List<RecommendedModel>.from(
            json["results"].map((x) => RecommendedModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "results": List<dynamic>.from(recommendations.map((x) => x)),
      };
}
