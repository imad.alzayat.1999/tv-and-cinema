import '../../domain/entities/spoken_language.dart';

class SpokenLanguagesModel extends SpokenLanguage {
  const SpokenLanguagesModel({required String name}) : super(name: name);

  factory SpokenLanguagesModel.fromJson(Map<String, dynamic> json) =>
      SpokenLanguagesModel(name: json["english_name"]);

  Map<String , dynamic> toJson() => {
    "english_name" : name,
  };
}
