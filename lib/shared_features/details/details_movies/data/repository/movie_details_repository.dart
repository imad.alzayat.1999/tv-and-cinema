import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import '../../../../../core/error/exceptions.dart';
import '../../../../../core/network/network_info.dart';
import '../../domain/entities/credits/credits.dart';
import '../../domain/entities/movie_details.dart';
import '../../domain/entities/movie_trailers/movie_trailers.dart';
import '../../domain/entities/recommended/recommended_movies.dart';
import '../../domain/entities/reviews/movie_reviews.dart';
import '../../domain/entities/similar/similar_movies.dart';
import '../../domain/repository/base_movie_details_repository.dart';
import '../../domain/usecases/get_movie_credits_usecase.dart';
import '../../domain/usecases/get_movie_details_usecase.dart';
import '../../domain/usecases/get_movie_reviews.dart';
import '../../domain/usecases/get_movie_trailers_usecase.dart';
import '../../domain/usecases/get_recommended_movies_usecase.dart';
import '../../domain/usecases/get_similar_movies_usecase.dart';
import '../datasources/details_local_datasource.dart';
import '../datasources/details_remote_datasource.dart';

class MovieDetailsRepository extends BaseMovieDetailsRepository {
  final BaseDetailsRemoteDataSource baseDetailsRemoteDataSource;
  final BaseDetailsLocalDataSource baseDetailsLocalDataSource;
  final NetworkInfo networkInfo;

  MovieDetailsRepository({
    required this.baseDetailsRemoteDataSource,
    required this.networkInfo,
    required this.baseDetailsLocalDataSource,
  });

  @override
  Future<Either<Failure, MovieDetails>> getMovieDetails(
      MovieDetailsParameter parameter) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await baseDetailsRemoteDataSource.getMovieDetails(parameter);
        baseDetailsLocalDataSource.cacheMovieDetails(result);
        return Right(result);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message.toString()));
      }
    } else {
      try {
        final result = await baseDetailsLocalDataSource.getCachedMovieDetails();
        return Right(result);
      } on LocalDatabaseException catch (failure) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }

  @override
  Future<Either<Failure, RecommendedMovies>> getRecommendedMovies(
      RecommendedMoviesParameters parameter) async {
    if(await networkInfo.isConnected){
      try {
        final result = await baseDetailsRemoteDataSource.getRecommendedMovies(parameter);
        baseDetailsLocalDataSource.cacheRecommendedMovies(result);
        return Right(result);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message.toString()));
      }
    }else{
      try {
        final result = await baseDetailsLocalDataSource.getCachedRecommendedMovies();
        return Right(result);
      } on LocalDatabaseException catch (failure) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }

  @override
  Future<Either<Failure, MovieReviews>> getMovieReviews(
      MovieReviewsParameters parameter) async {
    if (await networkInfo.isConnected) {
      try {
        final result =
            await baseDetailsRemoteDataSource.getMovieReviews(parameter);
        baseDetailsLocalDataSource.cacheReviews(result);
        return Right(result);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message.toString()));
      }
    } else {
      try {
        final result = await baseDetailsLocalDataSource.getCachedReviews();
        return Right(result);
      } on LocalDatabaseException catch (failure) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }

  @override
  Future<Either<Failure, Credits>> getMovieCredits(
      CreditsParameters creditsParameters) async {
    if(await networkInfo.isConnected){
      try {
        final response =
        await baseDetailsRemoteDataSource.getMovieCredits(creditsParameters);
        baseDetailsLocalDataSource.cacheCredits(response);
        return Right(response);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message.toString()));
      }
    }else{
      try {
        final result = await baseDetailsLocalDataSource.getCachedCredits();
        return Right(result);
      } on LocalDatabaseException catch (failure) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }

  @override
  Future<Either<Failure, SimilarMovies>> getSimilarMovies(
      SimilarMoviesParameters similarMoviesParameters) async {
    if(await networkInfo.isConnected){
      try {
        final response = await baseDetailsRemoteDataSource
            .getSimilarMovies(similarMoviesParameters);
        baseDetailsLocalDataSource.cacheSimilarMovies(response);
        return Right(response);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message.toString()));
      }
    }else{
      try {
        final result = await baseDetailsLocalDataSource.getCachedSimilarMovies();
        return Right(result);
      } on LocalDatabaseException catch (failure) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }

  @override
  Future<Either<Failure, MovieTrailers>> getMovieTrailers(
      MovieTrailersParameters movieTrailersParameters) async {
    if(await networkInfo.isConnected){
      try {
        final response = await baseDetailsRemoteDataSource
            .getMovieTrailers(movieTrailersParameters);
        baseDetailsLocalDataSource.cacheMovieTrailers(response);
        return Right(response);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message.toString()));
      }
    }else{
      try {
        final result = await baseDetailsLocalDataSource.getCachedMovieTrailers();
        return Right(result);
      } on LocalDatabaseException catch (failure) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }
}
