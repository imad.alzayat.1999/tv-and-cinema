import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/services/services_locator.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_icon.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../components/credits.dart';
import '../components/recommendations.dart';
import '../components/reviews.dart';
import '../components/similar_movies.dart';
import '../components/trailers.dart';
import '../controllers/movie_details_bloc.dart';
import '../controllers/movie_details_events.dart';
import '../controllers/movie_details_states.dart';

class MovieDetailScreen extends StatelessWidget {
  final int id;

  const MovieDetailScreen({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsManager.kEerieBlack,
      body: BlocProvider(
        create: (_) => getIt<MovieDetailsBloc>()
          ..add(GetMovieDetailsEvent(
              id: id, language: FunctionsManager.getLangCode(context)))
          ..add(GetRecommendationMoviesEvent(
              id: id, language: FunctionsManager.getLangCode(context)))
          ..add(GetMovieReviewsEvent(
              id: id, language: FunctionsManager.getLangCode(context)))
          ..add(GetMovieCreditsEvent(
              id: id, language: FunctionsManager.getLangCode(context)))
          ..add(GetSimilarMoviesEvent(
              id: id, language: FunctionsManager.getLangCode(context)))
          ..add(GetMovieTrailersEvent(
              id: id, language: FunctionsManager.getLangCode(context))),
        child: MovieDetailContent(movieId: id),
      ),
    );
  }
}

class MovieDetailContent extends StatefulWidget {
  final int movieId;

  const MovieDetailContent({Key? key, required this.movieId}) : super(key: key);
  @override
  State<MovieDetailContent> createState() => _MovieDetailContentState();
}

class _MovieDetailContentState extends State<MovieDetailContent> {
  Alignment getAlignmentByLanguage() {
    if (AppLocalizations.of(context)!.isEnLocale) {
      return Alignment.topLeft;
    } else {
      return Alignment.topRight;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieDetailsBloc, MovieDetailsStates>(
        builder: (context, state) {
      switch (state.movieDetailsRequestState) {
        case RequestStates.loading:
          return BaseLoadingIndicator();
        case RequestStates.success:
          return CustomScrollView(
            key: const Key('movieDetailScrollView'),
            shrinkWrap: true,
            slivers: [
              SliverAppBar(
                leading: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: Icon(Icons.arrow_back),
                ),
                pinned: true,
                backgroundColor: ColorsManager.kEerieBlack,
                expandedHeight: getHeight(inputHeight: SizesManager.s250),
                flexibleSpace: FlexibleSpaceBar(
                  background: FadeIn(
                    duration: const Duration(
                      milliseconds: ConstsManager.fadeAnimationDuration,
                    ),
                    child: ShaderMask(
                      shaderCallback: (rect) {
                        return const LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            ColorsManager.kTransparent,
                            ColorsManager.kBlack,
                            ColorsManager.kBlack,
                            ColorsManager.kTransparent,
                          ],
                          stops: [0.0, 0.5, 1.0, 1.0],
                        ).createShader(
                          Rect.fromLTRB(0.0, 0.0, rect.width, rect.height),
                        );
                      },
                      blendMode: BlendMode.dstIn,
                      child: FadeInImage.assetNetwork(
                        width: MediaQuery.of(context).size.width,
                        image: FunctionsManager.imageUrl(
                            state.movieDetails!.backdropPath),
                        placeholder: ImagesManager.logoImage,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: FadeInUp(
                  from: SizesManager.s20,
                  duration: const Duration(
                    milliseconds: ConstsManager.fadeAnimationDuration,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(PaddingManager.p16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          state.movieDetails!.title,
                          style: getBoldTextStyle(
                            fontSize: FontSizeManager.size23,
                            letterSpacing: SizesManager.s1_2,
                            context: context,
                          ),
                        ),
                        SizedBox(
                            height: getHeight(
                          inputHeight: SizesManager.s8,
                        )),
                        Row(
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: PaddingManager.p2,
                                horizontal: PaddingManager.p8,
                              ),
                              decoration: BoxDecoration(
                                color: ColorsManager.kDimGray,
                                borderRadius:
                                    BorderRadius.circular(SizesManager.s4),
                              ),
                              child: Text(
                                state.movieDetails!.releaseDate.split('-')[0],
                                style: getMediumTextStyle(
                                  color: ColorsManager.kWhite,
                                  fontSize: FontSizeManager.size16,
                                  letterSpacing: SizesManager.s0,
                                  context: context,
                                ),
                              ),
                            ),
                            SizedBox(
                                width: getWidth(inputWidth: SizesManager.s16)),
                            Row(
                              children: [
                                const BaseIcon(
                                  iconHeight: SizesManager.s16,
                                  iconPath: IconsManager.starIcon,
                                  iconColor: ColorsManager.voteColor,
                                ),
                                SizedBox(
                                    width: getWidth(
                                  inputWidth: SizesManager.s4,
                                )),
                                Text(
                                  (state.movieDetails!.voteAverage / 2)
                                      .toStringAsFixed(1),
                                  style: getMediumTextStyle(
                                    fontSize: FontSizeManager.size16,
                                    letterSpacing: SizesManager.s1_2,
                                    context: context,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                                width: getWidth(inputWidth: SizesManager.s16)),
                            Row(
                              children: [
                                const BaseIcon(
                                  iconHeight: SizesManager.s20,
                                  iconPath: IconsManager.clockIcon,
                                ),
                                SizedBox(
                                    width:
                                        getWidth(inputWidth: SizesManager.s4)),
                                Text(
                                  FunctionsManager.showDuration(
                                      state.movieDetails!.runtime),
                                  style: getMediumTextStyle(
                                    fontSize: FontSizeManager.size16,
                                    letterSpacing: SizesManager.s1_2,
                                    context: context,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                            height: getHeight(inputHeight: SizesManager.s20)),
                        Text(
                          state.movieDetails!.overview,
                          style: getMediumTextStyle(
                            fontSize: FontSizeManager.size14,
                            letterSpacing: SizesManager.s1_2,
                            context: context,
                          ),
                        ),
                        SizedBox(
                            height: getHeight(inputHeight: SizesManager.s8)),
                        Text(
                          '${FunctionsManager.translateText(text: StringsManager.genresString, context: context)} ${FunctionsManager.showGenres(state.movieDetails!.genres)}',
                          style: getRegularTextStyle(
                            fontSize: FontSizeManager.size12,
                            letterSpacing: SizesManager.s1_2,
                            context: context,
                          ),
                        ),
                        SizedBox(
                            height: getHeight(inputHeight: SizesManager.s8)),
                        Text(
                          '${FunctionsManager.translateText(text: StringsManager.spokenLanguagesString, context: context)} ${FunctionsManager.showSpokenLanguages(state.movieDetails!.spokenLanguages)}',
                          style: getRegularTextStyle(
                            fontSize: FontSizeManager.size12,
                            letterSpacing: SizesManager.s1_2,
                            context: context,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(
                    PaddingManager.p16,
                    PaddingManager.p0,
                    PaddingManager.p16,
                    PaddingManager.p24,
                  ),
                  child: Column(
                    children: [
                      Align(
                        alignment: getAlignmentByLanguage(),
                        child: Text(
                          FunctionsManager.translateText(
                              text: StringsManager.recommendedString,
                              context: context),
                          style: getBoldTextStyle(
                            fontSize: FontSizeManager.size20,
                            letterSpacing: SizesManager.s0,
                            context: context,
                          ),
                        ),
                      ),
                      Recommendations(movieId: widget.movieId),
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(
                    PaddingManager.p16,
                    PaddingManager.p0,
                    PaddingManager.p16,
                    PaddingManager.p24,
                  ),
                  child: Column(
                    children: [
                      Align(
                        alignment: getAlignmentByLanguage(),
                        child: Text(
                          FunctionsManager.translateText(
                              text: StringsManager.similarMoviesString,
                              context: context),
                          style: getBoldTextStyle(
                            fontSize: FontSizeManager.size20,
                            letterSpacing: SizesManager.s0,
                            context: context,
                          ),
                        ),
                      ),
                      SimilarMovies(movieId: widget.movieId),
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(
                    PaddingManager.p16,
                    PaddingManager.p0,
                    PaddingManager.p16,
                    PaddingManager.p24,
                  ),
                  child: Column(
                    children: [
                      Align(
                        alignment: getAlignmentByLanguage(),
                        child: Text(
                          FunctionsManager.translateText(
                              text: StringsManager.reviewString,
                              context: context),
                          style: getBoldTextStyle(
                            fontSize: FontSizeManager.size20,
                            letterSpacing: SizesManager.s0,
                            context: context,
                          ),
                        ),
                      ),
                      Reviews(movieId: widget.movieId),
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(
                    PaddingManager.p16,
                    PaddingManager.p0,
                    PaddingManager.p16,
                    PaddingManager.p24,
                  ),
                  child: Column(
                    children: [
                      Align(
                        alignment: getAlignmentByLanguage(),
                        child: Text(
                          FunctionsManager.translateText(
                              text: StringsManager.trailersString,
                              context: context),
                          style: getBoldTextStyle(
                            fontSize: FontSizeManager.size20,
                            letterSpacing: SizesManager.s0,
                            context: context,
                          ),
                        ),
                      ),
                      Trailers(
                        moviePoster: state.movieDetails!.backdropPath,
                        movieId: widget.movieId,
                      ),
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(
                    PaddingManager.p16,
                    PaddingManager.p0,
                    PaddingManager.p16,
                    PaddingManager.p24,
                  ),
                  child: Column(
                    children: [
                      Align(
                        alignment: getAlignmentByLanguage(),
                        child: Text(
                          FunctionsManager.translateText(
                              text: StringsManager.creditsString,
                              context: context),
                          style: getBoldTextStyle(
                            fontSize: FontSizeManager.size20,
                            letterSpacing: SizesManager.s0,
                            context: context,
                          ),
                        ),
                      ),
                      Credits(movieId: widget.movieId),
                    ],
                  ),
                ),
              ),
            ],
          );
        case RequestStates.error:
          return BaseError(
              errorMessage: state.movieDetailsErrorMessage,
              onPressFunction: () {
                BlocProvider.of<MovieDetailsBloc>(context)
                  ..add(
                    GetMovieDetailsEvent(
                      id: widget.movieId,
                      language: FunctionsManager.getLangCode(context),
                    ),
                  );
              });
      }
    });
  }
}
