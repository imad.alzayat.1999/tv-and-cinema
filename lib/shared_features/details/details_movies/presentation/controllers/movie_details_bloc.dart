import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../../core/utils/request_states.dart';
import '../../domain/usecases/get_movie_credits_usecase.dart';
import '../../domain/usecases/get_movie_details_usecase.dart';
import '../../domain/usecases/get_movie_reviews.dart';
import '../../domain/usecases/get_movie_trailers_usecase.dart';
import '../../domain/usecases/get_recommended_movies_usecase.dart';
import '../../domain/usecases/get_similar_movies_usecase.dart';
import 'movie_details_events.dart';
import 'movie_details_states.dart';

class MovieDetailsBloc extends Bloc<MovieDetailsEvents, MovieDetailsStates> {
  final GetMovieDetailsUseCase getMovieDetailsUseCase;
  final GetRecommendedMoviesUseCase getRecommendedMoviesUseCase;
  final GetMovieReviews getMovieReviews;
  final GetMovieCreditsUseCase getMovieCreditsUseCase;
  final GetSimilarMoviesUseCase getSimilarMoviesUseCase;
  final GetMovieTrailersUseCase getMovieTrailersUseCase;

  MovieDetailsBloc({
    required this.getMovieDetailsUseCase,
    required this.getRecommendedMoviesUseCase,
    required this.getMovieReviews,
    required this.getMovieCreditsUseCase,
    required this.getSimilarMoviesUseCase,
    required this.getMovieTrailersUseCase,
  }) : super(const MovieDetailsStates()) {
    on<GetMovieDetailsEvent>(
      (event, emit) async {
        final failureOrDetails = await getMovieDetailsUseCase(
            MovieDetailsParameter(movieId: event.id , language: event.language));
        failureOrDetails.fold(
          (l) => emit(
            state.copyWith(
              movieDetailsRequestState: RequestStates.error,
              movieDetailsErrorMessage: l.message,
            ),
          ),
          (r) => emit(
            state.copyWith(
              movieDetailsRequestState: RequestStates.success,
              movieDetails: r,
            ),
          ),
        );
      },
    );

    on<GetRecommendationMoviesEvent>(
      (event, emit) async {
        final failureOrRecommended = await getRecommendedMoviesUseCase(
          RecommendedMoviesParameters(movieId: event.id , language: event.language),
        );
        failureOrRecommended.fold(
          (l) => emit(
            state.copyWith(
              recommendedState: RequestStates.error,
              recommendedErrorMessage: l.message,
            ),
          ),
          (r) => emit(
            state.copyWith(
              recommendedState: RequestStates.success,
              recommended: r,
            ),
          ),
        );
      },
    );

    on<GetMovieReviewsEvent>(
      (event, emit) async {
        final failureOrReviews = await getMovieReviews(
          MovieReviewsParameters(movieId: event.id , language: event.language),
        );
        failureOrReviews.fold(
          (l) => emit(
            state.copyWith(
              reviewsState: RequestStates.error,
              errorReviewsMessage: l.message,
            ),
          ),
          (r) => emit(
            state.copyWith(
              reviewsState: RequestStates.success,
              movieReviews: r,
            ),
          ),
        );
      },
    );

    on<GetMovieCreditsEvent>(
      (event, emit) async {
        final failureOrCredits = await getMovieCreditsUseCase(
          CreditsParameters(movieId: event.id , language: event.language),
        );
        failureOrCredits.fold(
          (l) => emit(
            state.copyWith(
              creditsState: RequestStates.error,
              errorCreditsMessage: l.message,
            ),
          ),
          (r) => emit(
            state.copyWith(
              creditsState: RequestStates.success,
              credits: r,
            ),
          ),
        );
      },
    );

    on<GetSimilarMoviesEvent>(
      (event, emit) async {
        final failureOrSimilarMovies = await getSimilarMoviesUseCase(
          SimilarMoviesParameters(movieId: event.id , language: event.language),
        );
        failureOrSimilarMovies.fold(
          (l) => emit(
            state.copyWith(
              similarStates: RequestStates.error,
              errorSimilarMessage: l.message,
            ),
          ),
          (r) => emit(
            state.copyWith(
              similarStates: RequestStates.success,
              similarMovies: r,
            ),
          ),
        );
      },
    );

    on<GetMovieTrailersEvent>(
          (event, emit) async {
        final failureOrTrailers = await getMovieTrailersUseCase(
          MovieTrailersParameters(movieId: event.id , language: event.language),
        );
        failureOrTrailers.fold(
              (l) => emit(
            state.copyWith(
              trailerStates: RequestStates.error,
              errorTrailerMessage: l.message,
            ),
          ),
              (r) => emit(
            state.copyWith(
              trailerStates: RequestStates.success,
              movieTrailers: r,
            ),
          ),
        );
      },
    );
  }
}
