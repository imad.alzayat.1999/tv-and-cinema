import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import '../../domain/entities/credits/credits.dart';
import '../../domain/entities/movie_details.dart';
import '../../domain/entities/movie_trailers/movie_trailers.dart';
import '../../domain/entities/recommended/recommended_movies.dart';
import '../../domain/entities/reviews/movie_reviews.dart';
import '../../domain/entities/similar/similar_movies.dart';

class MovieDetailsStates extends Equatable {
  final String movieDetailsErrorMessage;
  final MovieDetails? movieDetails;
  final RequestStates movieDetailsRequestState;

  final String recommendedErrorMessage;
  final RecommendedMovies? recommended;
  final RequestStates recommendedState;

  final MovieReviews? movieReviews;
  final String errorReviewsMessage;
  final RequestStates reviewsState;

  final Credits? credits;
  final String errorCreditsMessage;
  final RequestStates creditsState;

  final SimilarMovies? similarMovies;
  final String errorSimilarMessage;
  final RequestStates similarStates;

  final MovieTrailers? movieTrailers;
  final String errorTrailerMessage;
  final RequestStates trailerStates;

  const MovieDetailsStates({
    this.movieDetailsErrorMessage = '',
    this.movieDetails,
    this.movieDetailsRequestState = RequestStates.loading,
    this.recommendedState = RequestStates.loading,
    this.recommended,
    this.recommendedErrorMessage = '',
    this.movieReviews,
    this.errorReviewsMessage = '',
    this.reviewsState = RequestStates.loading,
    this.creditsState = RequestStates.loading,
    this.errorCreditsMessage = '',
    this.credits,
    this.errorSimilarMessage = '',
    this.similarStates = RequestStates.loading,
    this.similarMovies,
    this.movieTrailers,
    this.errorTrailerMessage = '',
    this.trailerStates = RequestStates.loading,
  });

  MovieDetailsStates copyWith({
    String? movieDetailsErrorMessage,
    MovieDetails? movieDetails,
    RequestStates? movieDetailsRequestState,
    String? recommendedErrorMessage,
    RecommendedMovies? recommended,
    RequestStates? recommendedState,
    MovieReviews? movieReviews,
    String? errorReviewsMessage,
    RequestStates? reviewsState,
    String? errorCreditsMessage,
    RequestStates? creditsState,
    Credits? credits,
    SimilarMovies? similarMovies,
    String? errorSimilarMessage,
    RequestStates? similarStates,
    MovieTrailers? movieTrailers,
    String? errorTrailerMessage,
    RequestStates? trailerStates,
  }) {
    return MovieDetailsStates(
      movieDetailsErrorMessage:
          movieDetailsErrorMessage ?? this.movieDetailsErrorMessage,
      movieDetails: movieDetails ?? this.movieDetails,
      movieDetailsRequestState:
          movieDetailsRequestState ?? this.movieDetailsRequestState,
      recommended: recommended ?? this.recommended,
      recommendedErrorMessage:
          recommendedErrorMessage ?? this.recommendedErrorMessage,
      recommendedState: recommendedState ?? this.recommendedState,
      errorReviewsMessage: errorReviewsMessage ?? this.errorReviewsMessage,
      movieReviews: movieReviews ?? this.movieReviews,
      reviewsState: reviewsState ?? this.reviewsState,
      credits: credits ?? this.credits,
      creditsState: creditsState ?? this.creditsState,
      errorCreditsMessage: errorCreditsMessage ?? this.errorCreditsMessage,
      similarMovies: similarMovies ?? this.similarMovies,
      similarStates: similarStates ?? this.similarStates,
      errorSimilarMessage: errorSimilarMessage ?? this.errorSimilarMessage,
      trailerStates: trailerStates ?? this.trailerStates,
      errorTrailerMessage: errorTrailerMessage ?? this.errorTrailerMessage,
      movieTrailers: movieTrailers ?? this.movieTrailers,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        movieDetails,
        movieDetailsErrorMessage,
        movieDetailsRequestState,
        recommended,
        recommendedState,
        recommendedErrorMessage,
        movieReviews,
        errorReviewsMessage,
        reviewsState,
        creditsState,
        credits,
        errorCreditsMessage,
        similarStates,
        similarMovies,
        errorSimilarMessage,
        trailerStates,
        errorTrailerMessage,
        movieTrailers,
      ];
}
