import 'package:equatable/equatable.dart';

abstract class MovieDetailsEvents extends Equatable {}

class GetMovieDetailsEvent extends MovieDetailsEvents {
  final int id;
  final String language;
  GetMovieDetailsEvent({required this.id , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [id , language];
}

class GetRecommendationMoviesEvent extends MovieDetailsEvents {
  final int id;
  final String language;
  GetRecommendationMoviesEvent({required this.id , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [id , language];
}

class GetMovieReviewsEvent extends MovieDetailsEvents {
  final int id;
  final String language;
  GetMovieReviewsEvent({required this.id , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [id , language];
}

class GetMovieCreditsEvent extends MovieDetailsEvents {
  final int id;
  final String language;
  GetMovieCreditsEvent({required this.id , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [id , language];
}

class GetSimilarMoviesEvent extends MovieDetailsEvents {
  final int id;
  final String language;
  GetSimilarMoviesEvent({required this.id , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [id , language];
}

class GetMovieTrailersEvent extends MovieDetailsEvents {
  final int id;
  final String language;
  GetMovieTrailersEvent({required this.id , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [id , language];
}
