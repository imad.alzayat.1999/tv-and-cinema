import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_icon.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../domain/entities/movie_trailers/trailer.dart';
import '../controllers/movie_details_bloc.dart';
import '../controllers/movie_details_events.dart';
import '../controllers/movie_details_states.dart';

class Trailers extends StatelessWidget {
  final String moviePoster;
  final int movieId;

  const Trailers({
    Key? key,
    required this.moviePoster,
    required this.movieId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieDetailsBloc, MovieDetailsStates>(
      builder: (context, state) {
        switch (state.trailerStates) {
          case RequestStates.loading:
            return const BaseLoadingIndicator();
          case RequestStates.success:
            return FadeInUp(
              from: SizesManager.s20,
              duration: const Duration(
                milliseconds: ConstsManager.fadeAnimationDuration,
              ),
              child: SizedBox(
                height: getHeight(inputHeight: SizesManager.s120),
                child: state.movieTrailers!.trailers.isEmpty
                    ? BaseEmptyWidget(emptyMessage: StringsManager.noTrailersFilmString)
                    : ListView.separated(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) => getTrailer(
                          trailer: state.movieTrailers!.trailers[index],
                        ),
                        separatorBuilder: (context, index) => SizedBox(
                            width: getWidth(inputWidth: SizesManager.s16)),
                        itemCount: state.movieTrailers!.trailers.length,
                      ),
              ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorTrailerMessage,
                onPressFunction: () {
                  BlocProvider.of<MovieDetailsBloc>(context)
                    ..add(
                      GetMovieTrailersEvent(
                        id: movieId,
                        language: FunctionsManager.getLangCode(context),
                      ),
                    );
                });
        }
      },
    );
  }

  Widget getTrailer({required Trailer trailer}) {
    return GestureDetector(
      onTap: () {
        FunctionsManager.launchExternalUrl(
          ConstsManager.getYoutubeUrl(trailer.trailerKey),
        );
      },
      child: Stack(
        alignment: Alignment.center,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(SizesManager.s10),
            child: Container(
              color: ColorsManager.kBlack,
              width: getWidth(inputWidth: SizesManager.s200),
              child: Opacity(
                opacity: 0.6,
                child: FadeInImage.assetNetwork(
                  image: FunctionsManager.imageUrl(moviePoster),
                  placeholder: ImagesManager.logoImage,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          const BaseIcon(
            iconHeight: SizesManager.s50,
            iconPath: IconsManager.playButtonIcon,
            iconColor: ColorsManager.kWhite,
          )
        ],
      ),
    );
  }
}
