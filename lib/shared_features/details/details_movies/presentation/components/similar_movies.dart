import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_icon.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../domain/entities/similar/similar.dart';
import '../controllers/movie_details_bloc.dart';
import '../controllers/movie_details_events.dart';
import '../controllers/movie_details_states.dart';

class SimilarMovies extends StatelessWidget {
  final int movieId;
  const SimilarMovies({Key? key, required this.movieId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieDetailsBloc, MovieDetailsStates>(
      builder: (context, state) {
        switch (state.similarStates) {
          case RequestStates.loading:
            return const BaseLoadingIndicator();
          case RequestStates.success:
            return FadeInUp(
              from: SizesManager.s20,
              duration: const Duration(
                milliseconds: ConstsManager.fadeAnimationDuration,
              ),
              child: SizedBox(
                height: getHeight(inputHeight: SizesManager.s240),
                child: state.similarMovies!.similars.isEmpty
                    ? BaseEmptyWidget(emptyMessage: StringsManager.noSimilarFilmString)
                    : ListView.separated(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemBuilder: (context, index) => GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(
                              context,
                              Routes.movieDetailsRoute,
                              arguments: DetailsRouteParameters(
                                  id: state
                                      .similarMovies!.similars[index].movieId!),
                            );
                          },
                          child: getSimilarMovie(
                            similar: state.similarMovies!.similars[index],
                            context: context,
                          ),
                        ),
                        separatorBuilder: (context, index) => SizedBox(
                            width: getWidth(inputWidth: SizesManager.s25)),
                        itemCount: state.similarMovies!.similars.length,
                      ),
              ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorSimilarMessage,
                onPressFunction: () {
                  BlocProvider.of<MovieDetailsBloc>(context)
                    ..add(
                      GetSimilarMoviesEvent(
                        id: movieId,
                        language: FunctionsManager.getLangCode(context),
                      ),
                    );
                });
        }
      },
    );
  }

  /// similar movie item
  Widget getSimilarMovie(
      {required Similar similar, required BuildContext context}) {
    return SizedBox(
      width: getWidth(inputWidth: SizesManager.s200),
      child: Column(
        children: [
          SizedBox(
            height: getHeight(inputHeight: SizesManager.s150),
            width: getWidth(inputWidth: SizesManager.s200),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(SizesManager.s10),
              child: FadeInImage.assetNetwork(
                image: FunctionsManager.showAPIImage(similar.movieImage),
                placeholder: ImagesManager.logoImage,
                height: SizesManager.s170,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(height: getHeight(inputHeight: SizesManager.s10)),
          Expanded(
            child: Text(
              similar.movieTitle!,
              style: getBoldTextStyle(
                fontSize: FontSizeManager.size16,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  const BaseIcon(
                    iconHeight: SizesManager.s20,
                    iconPath: IconsManager.calendarIcon,
                  ),
                  SizedBox(width: getWidth(inputWidth: SizesManager.s4)),
                  Text(
                    FunctionsManager.formatDate(similar.movieReleaseDate),
                    style: getRegularTextStyle(
                      fontSize: FontSizeManager.size14,
                      letterSpacing: SizesManager.s0,
                      context: context,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  const BaseIcon(
                    iconHeight: SizesManager.s20,
                    iconPath: IconsManager.starIcon,
                    iconColor: ColorsManager.voteColor,
                  ),
                  SizedBox(width: getWidth(inputWidth: SizesManager.s4)),
                  Text(
                    (similar.movieVote!).toStringAsFixed(1),
                    style: getRegularTextStyle(
                      fontSize: FontSizeManager.size14,
                      letterSpacing: SizesManager.s0,
                      context: context,
                    ),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
