import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';

import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../domain/entities/reviews/review.dart';
import '../controllers/movie_details_bloc.dart';
import '../controllers/movie_details_events.dart';
import '../controllers/movie_details_states.dart';

class Reviews extends StatelessWidget {
  final int movieId;
  const Reviews({Key? key, required this.movieId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieDetailsBloc, MovieDetailsStates>(
        builder: (context, state) {
      switch (state.reviewsState) {
        case RequestStates.loading:
          return const BaseLoadingIndicator();
        case RequestStates.success:
          return FadeInUp(
            from: SizesManager.s20,
            duration: const Duration(
              milliseconds: ConstsManager.fadeAnimationDuration,
            ),
            child: state.movieReviews!.reviews.isEmpty
                ? BaseEmptyWidget(emptyMessage: StringsManager.noReviewsFilmString)
                : ListView.separated(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, index) => getReviewItem(
                        review: state.movieReviews!.reviews[index],
                        context: context),
                    separatorBuilder: (context, index) => const Divider(
                      thickness: SizesManager.s1_2,
                      color: ColorsManager.kBlack,
                    ),
                    itemCount: state.movieReviews!.reviews.length,
                  ),
          );
        case RequestStates.error:
          return BaseError(
              errorMessage: state.errorReviewsMessage,
              onPressFunction: () {
                BlocProvider.of<MovieDetailsBloc>(context)
                  ..add(
                    GetMovieReviewsEvent(
                      id: movieId,
                      language: FunctionsManager.getLangCode(context),
                    ),
                  );
              });
      }
    });
  }

  /// get Movie Rating
  Widget getMovieRating({required num? rating, required BuildContext context}) {
    if (rating == null) {
      return const SizedBox.shrink();
    } else {
      return Column(
        children: [
          SizedBox(height: getHeight(inputHeight: SizesManager.s16)),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const BaseIcon(
                iconHeight: SizesManager.s16,
                iconPath: IconsManager.starIcon,
                iconColor: ColorsManager.voteColor,
              ),
              Text(
                rating.toString(),
                style: getBoldTextStyle(
                  fontSize: FontSizeManager.size16,
                  letterSpacing: SizesManager.s1_2,
                  context: context,
                ),
              ),
            ],
          ),
        ],
      );
    }
  }

  /// review item
  Widget getReviewItem(
      {required Review review, required BuildContext context}) {
    return Column(
      children: [
        ListTile(
          leading: SizedBox(
            width: getHeight(inputHeight: SizesManager.s50),
            height: getHeight(inputHeight: SizesManager.s50),
            child: ClipOval(
              child: FadeInImage.assetNetwork(
                image: FunctionsManager.showAPIImage(
                    review.authorDetails.authorImage),
                placeholder: ImagesManager.logoImage,
                fit: BoxFit.cover,
              ),
            ),
          ),
          title: Text(
            review.authorDetails.authorName!,
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size16,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
        ),
        getMovieRating(rating: review.authorDetails.rating, context: context),
        SizedBox(height: getHeight(inputHeight: SizesManager.s16)),
        SizedBox(
          width: getWidth(inputWidth: SizesManager.s250),
          child: Text(
            review.content,
            style: getRegularTextStyle(
              fontSize: FontSizeManager.size14,
              letterSpacing: 0,
              context: context,
            ),
          ),
        )
      ],
    );
  }
}
