import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';

import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../controllers/movie_details_bloc.dart';
import '../controllers/movie_details_events.dart';
import '../controllers/movie_details_states.dart';

class Recommendations extends StatelessWidget {
  final int movieId;
  const Recommendations({Key? key, required this.movieId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieDetailsBloc, MovieDetailsStates>(
      builder: (context, state) {
        switch (state.recommendedState) {
          case RequestStates.loading:
            return const BaseLoadingIndicator();
          case RequestStates.success:
            return FadeInUp(
              from: SizesManager.s20,
              duration: const Duration(
                milliseconds: ConstsManager.fadeAnimationDuration,
              ),
              child: state.recommended!.recommendations.isEmpty
                  ? BaseEmptyWidget(
                      emptyMessage: StringsManager.noRecommendationsFilmString,
                    )
                  : GridView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.recommended!.recommendations.length,
                      itemBuilder: (context, index) {
                        final recommendation =
                            state.recommended!.recommendations[index];
                        return FadeInUp(
                          from: SizesManager.s20,
                          duration: const Duration(
                            milliseconds: ConstsManager.fadeAnimationDuration,
                          ),
                          child: ClipRRect(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(SizesManager.s4),
                            ),
                            child: GestureDetector(
                              onTap: () => Navigator.pushNamed(
                                context,
                                Routes.movieDetailsRoute,
                                arguments: DetailsRouteParameters(
                                    id: recommendation.id),
                              ),
                              child: FadeInImage.assetNetwork(
                                image: FunctionsManager.showAPIImage(
                                  recommendation.backdropPath!,
                                ),
                                placeholder: ImagesManager.logoImage,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        );
                      },
                      gridDelegate: ConstsManager.recommendationsGridDelegates,
                    ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.recommendedErrorMessage,
                onPressFunction: () {
                  BlocProvider.of<MovieDetailsBloc>(context)
                    ..add(
                      GetRecommendationMoviesEvent(
                        id: movieId,
                        language: FunctionsManager.getLangCode(context),
                      ),
                    );
                });
        }
      },
    );
  }
}
