import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../domain/entities/credits/cast.dart';
import '../controllers/movie_details_bloc.dart';
import '../controllers/movie_details_events.dart';
import '../controllers/movie_details_states.dart';

class Credits extends StatelessWidget {
  final int movieId;
  const Credits({Key? key, required this.movieId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieDetailsBloc, MovieDetailsStates>(
        builder: (context, state) {
      switch (state.creditsState) {
        case RequestStates.loading:
          return const BaseLoadingIndicator();
        case RequestStates.success:
          return FadeInUp(
            from: SizesManager.s20,
            duration: const Duration(
              milliseconds: ConstsManager.fadeAnimationDuration,
            ),
            child: SizedBox(
              height: getHeight(inputHeight: SizesManager.s250),
              child: state.credits!.casts.isEmpty
                  ? BaseEmptyWidget(emptyMessage: StringsManager.noCreditsFilmString)
                  : ListView.separated(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) =>
                          getCreditItem(state.credits!.casts[index], context),
                      separatorBuilder: (context, index) => SizedBox(
                          width: getWidth(inputWidth: SizesManager.s16)),
                      itemCount: state.credits!.casts.length,
                    ),
            ),
          );
        case RequestStates.error:
          return BaseError(
              errorMessage: state.errorCreditsMessage,
              onPressFunction: () {
                BlocProvider.of<MovieDetailsBloc>(context)
                  ..add(
                    GetMovieCreditsEvent(
                      id: movieId,
                      language: FunctionsManager.getLangCode(context),
                    ),
                  );
              });
      }
    });
  }

  /// get credit item
  Widget getCreditItem(Cast cast, BuildContext context) {
    if (cast.department! == "Acting") {
      return GestureDetector(
        onTap: () {
          Navigator.pushNamed(
            context,
            Routes.actorDetailsRoute,
            arguments: DetailsRouteParameters(id: cast.id!),
          );
        },
        child: Column(
          children: [
            SizedBox(
              width: getHeight(inputHeight: SizesManager.s100),
              height: getHeight(inputHeight: SizesManager.s100),
              child: ClipOval(
                child: FadeInImage.assetNetwork(
                  placeholder: ImagesManager.logoImage,
                  fit: BoxFit.cover,
                  image: FunctionsManager.showAPIImage(cast.actorImage),
                ),
              ),
            ),
            SizedBox(height: getHeight(inputHeight: SizesManager.s10)),
            Text(
              cast.originalName!,
              style: getBoldTextStyle(
                fontSize: FontSizeManager.size12,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
            SizedBox(height: getHeight(inputHeight: SizesManager.s10)),
            SizedBox(
              width: getWidth(inputWidth: SizesManager.s100),
              child: Text(
                cast.character!,
                textAlign: TextAlign.center,
                style: getRegularTextStyle(
                  fontSize: FontSizeManager.size10,
                  letterSpacing: SizesManager.s0,
                  context: context,
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return const SizedBox.shrink();
    }
  }
}
