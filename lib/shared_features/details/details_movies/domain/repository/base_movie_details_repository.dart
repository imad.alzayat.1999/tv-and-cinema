import 'package:dartz/dartz.dart';
import '../../../../../core/error/failure.dart';
import '../entities/credits/credits.dart';
import '../entities/movie_details.dart';
import '../entities/movie_trailers/movie_trailers.dart';
import '../entities/recommended/recommended_movies.dart';
import '../entities/reviews/movie_reviews.dart';
import '../entities/similar/similar_movies.dart';
import '../usecases/get_movie_credits_usecase.dart';
import '../usecases/get_movie_details_usecase.dart';
import '../usecases/get_movie_reviews.dart';
import '../usecases/get_movie_trailers_usecase.dart';
import '../usecases/get_recommended_movies_usecase.dart';
import '../usecases/get_similar_movies_usecase.dart';

abstract class BaseMovieDetailsRepository{
  Future<Either<Failure , MovieDetails>> getMovieDetails(MovieDetailsParameter parameter);
  Future<Either<Failure , RecommendedMovies>> getRecommendedMovies(RecommendedMoviesParameters parameter);
  Future<Either<Failure , MovieReviews>> getMovieReviews(MovieReviewsParameters parameter);
  Future<Either<Failure , Credits>> getMovieCredits(CreditsParameters creditsParameters);
  Future<Either<Failure , SimilarMovies>> getSimilarMovies(SimilarMoviesParameters similarMoviesParameters);
  Future<Either<Failure , MovieTrailers>> getMovieTrailers(MovieTrailersParameters movieTrailersParameters);
}