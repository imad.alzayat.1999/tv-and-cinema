import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_movies/domain/entities/movie_trailers/trailer.dart';

class MovieTrailers extends Equatable{
  final List<Trailer> trailers;

  const MovieTrailers({required this.trailers});

  @override
  // TODO: implement props
  List<Object?> get props => [trailers];
}