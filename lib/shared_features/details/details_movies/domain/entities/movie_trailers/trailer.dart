import 'package:equatable/equatable.dart';

class Trailer extends Equatable{
  final String trailerName;
  final String trailerKey;
  final String type;

  const Trailer({required this.trailerName, required this.trailerKey , required this.type});

  @override
  // TODO: implement props
  List<Object?> get props => [trailerName , trailerKey , type];
}