import 'package:equatable/equatable.dart';
import 'author_details.dart';

class Review extends Equatable {
  final AuthorDetails authorDetails;
  final String author;
  final String content;

  const Review({
    required this.authorDetails,
    required this.author,
    required this.content,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [authorDetails, author, content];
}
