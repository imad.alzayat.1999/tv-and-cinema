import 'package:equatable/equatable.dart';

class AuthorDetails extends Equatable {
  final String? authorName;
  final String? authorImage;
  final num? rating;

  const AuthorDetails({
    required this.authorName,
    required this.authorImage,
    required this.rating,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [authorName, authorImage, rating];
}
