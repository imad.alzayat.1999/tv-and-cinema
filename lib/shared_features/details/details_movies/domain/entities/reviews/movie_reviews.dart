import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_movies/domain/entities/reviews/review.dart';
class MovieReviews extends Equatable{
  final int page;
  final List<Review> reviews;

  const MovieReviews({required this.page, required this.reviews});

  @override
  // TODO: implement props
  List<Object?> get props => [page , reviews];
}