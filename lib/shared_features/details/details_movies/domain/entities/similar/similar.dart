import 'package:equatable/equatable.dart';

class Similar extends Equatable {
  final int? movieId;
  final String? movieImage;
  final String? movieTitle;
  final DateTime? movieReleaseDate;
  final num? movieVote;

  const Similar({
    required this.movieId,
    required this.movieImage,
    required this.movieTitle,
    required this.movieReleaseDate,
    required this.movieVote,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        movieId,
        movieImage,
        movieTitle,
        movieReleaseDate,
        movieVote,
      ];
}
