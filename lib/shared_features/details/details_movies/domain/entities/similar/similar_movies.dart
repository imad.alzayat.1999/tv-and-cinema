import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_movies/domain/entities/similar/similar.dart';

class SimilarMovies extends Equatable{
  final List<Similar> similars;

  const SimilarMovies({required this.similars});

  @override
  // TODO: implement props
  List<Object?> get props => [similars];
}