import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/shared_features/details/details_movies/domain/entities/spoken_language.dart';

import 'genre.dart';

class MovieDetails extends Equatable {
  final String backdropPath;
  final int id;
  final String overview;
  final List<Genre> genres;
  final List<SpokenLanguage> spokenLanguages;
  final String title;
  final double voteAverage;
  final String releaseDate;
  final int runtime;

  const MovieDetails({
    required this.backdropPath,
    required this.id,
    required this.overview,
    required this.genres,
    required this.spokenLanguages,
    required this.title,
    required this.voteAverage,
    required this.releaseDate,
    required this.runtime,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
    backdropPath,
    id,
    overview,
    genres,
    spokenLanguages,
    title,
    voteAverage,
    releaseDate,
    runtime
  ];
}
