import 'package:equatable/equatable.dart';

class Recommended extends Equatable{
  final String? backdropPath;
  final int id;

  const Recommended({required this.id , this.backdropPath});

  @override
  // TODO: implement props
  List<Object?> get props => [id , backdropPath];
}