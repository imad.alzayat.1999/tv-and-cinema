import 'package:equatable/equatable.dart';

import 'recommended.dart';

class RecommendedMovies extends Equatable{
  final List<Recommended> recommendations;

  const RecommendedMovies({required this.recommendations});

  @override
  // TODO: implement props
  List<Object?> get props => [recommendations];
}