import 'package:equatable/equatable.dart';

import 'cast.dart';

class Credits extends Equatable{
  final List<Cast> casts;

  const Credits({required this.casts});

  @override
  // TODO: implement props
  List<Object?> get props => [casts];
}