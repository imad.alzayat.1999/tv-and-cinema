import 'package:equatable/equatable.dart';

class Cast extends Equatable {
  final String? department;
  final String? character;
  final String? originalName;
  final String? actorImage;
  final int? id;

  const Cast({
    required this.department,
    required this.character,
    required this.originalName,
    required this.actorImage,
    required this.id,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        department,
        character,
        originalName,
        actorImage,
        id,
      ];
}
