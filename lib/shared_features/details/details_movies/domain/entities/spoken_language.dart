import 'package:equatable/equatable.dart';

class SpokenLanguage extends Equatable{
  final String name;

  const SpokenLanguage({required this.name});

  @override
  // TODO: implement props
  List<Object?> get props => [name];
}