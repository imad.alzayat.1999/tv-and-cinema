import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../entities/movie_trailers/movie_trailers.dart';
import '../repository/base_movie_details_repository.dart';

class GetMovieTrailersUseCase extends BaseUseCase<MovieTrailers , MovieTrailersParameters>{
  final BaseMovieDetailsRepository baseMovieDetailsRepository;

  GetMovieTrailersUseCase({required this.baseMovieDetailsRepository});
  @override
  Future<Either<Failure, MovieTrailers>> call(MovieTrailersParameters parameters) async{
    return await baseMovieDetailsRepository.getMovieTrailers(parameters);
  }
}

class MovieTrailersParameters extends Equatable{
  final int movieId;
  final String language;

  const MovieTrailersParameters({required this.movieId , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [movieId , language];
}