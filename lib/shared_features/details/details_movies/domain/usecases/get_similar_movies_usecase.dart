import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entities/similar/similar_movies.dart';
import '../repository/base_movie_details_repository.dart';

class GetSimilarMoviesUseCase extends BaseUseCase<SimilarMovies , SimilarMoviesParameters>{
  final BaseMovieDetailsRepository baseMovieDetailsRepository;

  GetSimilarMoviesUseCase({required this.baseMovieDetailsRepository});
  @override
  Future<Either<Failure, SimilarMovies>> call(SimilarMoviesParameters parameters) async{
    return await baseMovieDetailsRepository.getSimilarMovies(parameters);
  }
}
class SimilarMoviesParameters extends Equatable{
  final int movieId;
  final String language;

  const SimilarMoviesParameters({required this.movieId , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [movieId , language];
}