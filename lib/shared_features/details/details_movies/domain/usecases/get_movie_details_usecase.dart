import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entities/movie_details.dart';
import '../repository/base_movie_details_repository.dart';

class GetMovieDetailsUseCase extends BaseUseCase<MovieDetails , MovieDetailsParameter>{
  final BaseMovieDetailsRepository baseMovieDetailsRepository;

  GetMovieDetailsUseCase({required this.baseMovieDetailsRepository});

  @override
  Future<Either<Failure, MovieDetails>> call(MovieDetailsParameter parameters) async {
    return await baseMovieDetailsRepository.getMovieDetails(parameters);
  }

}

class MovieDetailsParameter extends Equatable{
  final int movieId;
  final String language;

  const MovieDetailsParameter({required this.movieId , required this.language});
  @override
  // TODO: implement props
  List<Object?> get props => [movieId , language];

}