import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entities/credits/credits.dart';
import '../repository/base_movie_details_repository.dart';

class GetMovieCreditsUseCase extends BaseUseCase<Credits , CreditsParameters>{
  final BaseMovieDetailsRepository baseMovieDetailsRepository;

  GetMovieCreditsUseCase({required this.baseMovieDetailsRepository});
  @override
  Future<Either<Failure, Credits>> call(CreditsParameters parameters) async{
    return await baseMovieDetailsRepository.getMovieCredits(parameters);
  }
}

class CreditsParameters extends Equatable{
  final int movieId;
  final String language;

  const CreditsParameters({required this.movieId , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [movieId , language];
}