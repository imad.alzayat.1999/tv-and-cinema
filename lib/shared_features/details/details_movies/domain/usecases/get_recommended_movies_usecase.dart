import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../entities/recommended/recommended_movies.dart';
import '../repository/base_movie_details_repository.dart';

class GetRecommendedMoviesUseCase
    extends BaseUseCase<RecommendedMovies, RecommendedMoviesParameters> {
  final BaseMovieDetailsRepository baseMovieDetailsRepository;

  GetRecommendedMoviesUseCase({required this.baseMovieDetailsRepository});
  @override
  Future<Either<Failure, RecommendedMovies>> call(
      RecommendedMoviesParameters parameters) async {
    return await baseMovieDetailsRepository.getRecommendedMovies(parameters);
  }
}

class RecommendedMoviesParameters extends Equatable {
  final int movieId;
  final String language;

  const RecommendedMoviesParameters({required this.movieId , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [movieId , language];
}
