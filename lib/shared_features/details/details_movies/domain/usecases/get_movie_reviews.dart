import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entities/reviews/movie_reviews.dart';
import '../repository/base_movie_details_repository.dart';

class GetMovieReviews extends BaseUseCase<MovieReviews , MovieReviewsParameters>{
  final BaseMovieDetailsRepository baseMovieDetailsRepository;

  GetMovieReviews({required this.baseMovieDetailsRepository});
  @override
  Future<Either<Failure, MovieReviews>> call(MovieReviewsParameters parameters) async{
    return await baseMovieDetailsRepository.getMovieReviews(parameters);
  }
}

class MovieReviewsParameters extends Equatable{
  final int movieId;
  final String language;

  const MovieReviewsParameters({required this.movieId , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [movieId , language];
}