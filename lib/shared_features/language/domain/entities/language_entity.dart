class LanguageEntity {
  final String langTitle;
  final String langKey;
  final String langIcon;
  bool isSelected;

  LanguageEntity({
    required this.langTitle,
    required this.langKey,
    required this.langIcon,
    required this.isSelected,
  });
}
