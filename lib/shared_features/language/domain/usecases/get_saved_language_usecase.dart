import 'package:dartz/dartz.dart';

import 'package:movie_app_clean_architecture/core/error/failure.dart';
import '../../../../../core/usecase/base_usecase.dart';
import '../repository/base_language_repository.dart';

class GetSavedLanguageUseCase implements BaseUseCase<String , NoParameters>{
  final BaseLanguageRepository baseLanguageRepository;

  GetSavedLanguageUseCase({required this.baseLanguageRepository});
  @override
  Future<Either<Failure, String>> call(NoParameters parameters) async{
    return await baseLanguageRepository.getSavedLang();
  }
}