import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../repository/base_language_repository.dart';

class ChangeLanguageUseCase implements BaseUseCase<bool , String>{
  final BaseLanguageRepository baseLanguageRepository;

  ChangeLanguageUseCase({required this.baseLanguageRepository});
  @override
  Future<Either<Failure, bool>> call(String parameters) async{
    return await baseLanguageRepository.changeLang(langCode: parameters);
  }
}