import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../domain/entities/language_entity.dart';
import '../component/language_item.dart';
import '../controller/language_cubit.dart';

class LanguageScreen extends StatefulWidget {
  final bool isEnLocale;
  const LanguageScreen({Key? key, required this.isEnLocale}) : super(key: key);

  @override
  State<LanguageScreen> createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  List<LanguageEntity> langs = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    langs.add(
      LanguageEntity(
        langTitle: StringsManager.englishString,
        langKey: ConstsManager.en,
        langIcon: IconsManager.englishIcon,
        isSelected: widget.isEnLocale ? true : false,
      ),
    );
    langs.add(
      LanguageEntity(
        langTitle: StringsManager.arabicString,
        langKey: ConstsManager.ar,
        langIcon: IconsManager.arabicIcon,
        isSelected: widget.isEnLocale ? false : true,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorsManager.kEerieBlack,
      appBar: AppBar(
        elevation: SizesManager.s0,
        backgroundColor: ColorsManager.kEerieBlack,
        title: Text(
          FunctionsManager.translateText(
              text: StringsManager.languageString, context: context),
          style: getBoldTextStyle(
            fontSize: FontSizeManager.size16,
            letterSpacing: SizesManager.s0,
            context: context,
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              FunctionsManager.translateText(
                  text: StringsManager.chooseLangString, context: context),
              style: getBoldTextStyle(
                fontSize: FontSizeManager.size20,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
            SizedBox(
              height: getHeight(inputHeight: SizesManager.s5),
            ),
            ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return LanguageItem(
                  languageEntity: langs[index],
                  onPressFunction: () {
                    setState(() {
                      langs.forEach((element) => element.isSelected = false);
                      langs[index].isSelected = true;
                      BlocProvider.of<LanguageCubit>(context)
                          .changeLang(langs[index].langKey, context);
                    });
                  },
                );
              },
              separatorBuilder: (context, index) => SizedBox(
                height: getHeight(inputHeight: SizesManager.s5),
              ),
              itemCount: langs.length,
            ),
          ],
        ),
      ),
    );
  }
}
