import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../../domain/entities/language_entity.dart';

class LanguageItem extends StatelessWidget {
  final void Function()? onPressFunction;
  final LanguageEntity languageEntity;

  const LanguageItem({
    Key? key,
    required this.onPressFunction,
    required this.languageEntity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressFunction,
      child: Padding(
        padding: const EdgeInsets.all(PaddingManager.p12),
        child: Container(
          decoration: BoxDecoration(
            color: languageEntity.isSelected
                ? ColorsManager.kDarkGunmetal
                : ColorsManager.kVeryLightBlue,
            borderRadius: BorderRadius.circular(SizesManager.s10),
          ),
          child: ListTile(
            leading: SizedBox(
              width: getHeight(inputHeight: SizesManager.s50),
              height: getHeight(inputHeight: SizesManager.s50),
              child: SvgPicture.asset(languageEntity.langIcon),
            ),
            title: Text(
              FunctionsManager.translateText(
                  text: languageEntity.langTitle, context: context),
              style: getRegularTextStyle(
                fontSize: FontSizeManager.size16,
                letterSpacing: SizesManager.s0,
                color: languageEntity.isSelected
                    ? ColorsManager.kWhite
                    : ColorsManager.kBlack,
                context: context,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
