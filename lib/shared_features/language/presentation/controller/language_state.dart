import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class LanguageState extends Equatable {
  final Locale locale;
  const LanguageState(this.locale);

  @override
  List<Object> get props => [locale];
}

class ChangeLanguageState extends LanguageState {
  const ChangeLanguageState(Locale selectedLocale) : super(selectedLocale);
}