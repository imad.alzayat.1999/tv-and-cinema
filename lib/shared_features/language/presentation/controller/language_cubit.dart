import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:flutter/material.dart';

import '../../domain/usecases/change_language_usecase.dart';
import '../../domain/usecases/get_saved_language_usecase.dart';
import 'language_state.dart';

class LanguageCubit extends Cubit<LanguageState> {
  final GetSavedLanguageUseCase getSavedLanguageUseCase;
  final ChangeLanguageUseCase changeLanguageUseCase;
  LanguageCubit({
    required this.getSavedLanguageUseCase,
    required this.changeLanguageUseCase,
  }) : super(const ChangeLanguageState(Locale(ConstsManager.en)));

  String currentLangCode = ConstsManager.en;

  Future<void> getSavedLang() async {
    final response = await getSavedLanguageUseCase.call(NoParameters());
    response.fold((failure) => debugPrint(StringsManager.localDataBaseFailureString), (value) {
      currentLangCode = value;
      emit(ChangeLanguageState(Locale(currentLangCode)));
    });
  }

  Future<void> _changeLang(String langCode , BuildContext context) async {
    final response = await changeLanguageUseCase.call(langCode);
    response.fold((failure) => debugPrint(StringsManager.localDataBaseFailureString), (value) {
      print(value);
      currentLangCode = langCode;
      print(currentLangCode);
      Phoenix.rebirth(context);
      emit(ChangeLanguageState(Locale(currentLangCode)));
    });
  }

  void changeLang(String key , BuildContext context) => _changeLang(key , context);

}
