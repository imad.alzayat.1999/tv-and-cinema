import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import '../../domain/repository/base_language_repository.dart';
import '../datasources/language_local_datasource.dart';

class LanguageRepository implements BaseLanguageRepository{
  final BaseLanguageLocalDataSource baseLanguageLocalDataSource;

  LanguageRepository({required this.baseLanguageLocalDataSource});

  @override
  Future<Either<Failure, bool>> changeLang({required String langCode}) async{
    try{
      final result = await baseLanguageLocalDataSource.changeLang(langCode: langCode);
      return Right(result);
    }on LocalDatabaseException catch(e){
      return Left(DatabaseFailure(e.message));
    }
  }

  @override
  Future<Either<Failure, String>> getSavedLang() async{
    try{
      final result = await baseLanguageLocalDataSource.getSavedLang();
      return Right(result);
    }on LocalDatabaseException catch(e){
      return Left(DatabaseFailure(e.message));
    }
  }
}