import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class BaseLanguageLocalDataSource{
  Future<bool> changeLang({required String langCode});
  Future<String> getSavedLang();
}
class LanguageLocalDataSource implements BaseLanguageLocalDataSource{
  final SharedPreferences sharedPreferences;

  LanguageLocalDataSource({required this.sharedPreferences});
  @override
  Future<bool> changeLang({required String langCode}) async{
    return await sharedPreferences.setString(ConstsManager.LANG_KEY, langCode);
  }

  @override
  Future<String> getSavedLang() async{
    if(sharedPreferences.containsKey(ConstsManager.LANG_KEY)){
      return sharedPreferences.getString(ConstsManager.LANG_KEY)!;
    }else{
      return ConstsManager.en;
    }
  }

}