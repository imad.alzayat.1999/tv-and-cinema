import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../components/popular_component.dart';
import '../controllers/popular_bloc.dart';
import '../controllers/popular_events.dart';
import '../controllers/popular_states.dart';

class PopularMoviesScreen extends StatelessWidget {
  const PopularMoviesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<PopularBloc>()
        ..add(GetAllPopularMoviesEvent(
            page: 1, language: FunctionsManager.getLangCode(context))),
      child: Scaffold(
          backgroundColor: ColorsManager.kEerieBlack,
          appBar: AppBar(
            elevation: SizesManager.s0,
            backgroundColor: ColorsManager.kEerieBlack,
            title: Text(
              FunctionsManager.translateText(
                  text: StringsManager.popularString, context: context),
              style: getBoldTextStyle(
                fontSize: FontSizeManager.size16,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
          ),
          body: const PopularMoviesContent()),
    );
  }
}

class PopularMoviesContent extends StatefulWidget {
  const PopularMoviesContent({Key? key}) : super(key: key);

  @override
  State<PopularMoviesContent> createState() => _PopularMoviesContentState();
}

class _PopularMoviesContentState extends State<PopularMoviesContent> {
  final scrollController = ScrollController();
  int page = 1;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(_listener);
  }

  _listener() {
    if (scrollController.position.maxScrollExtent ==
        scrollController.position.pixels) {
      page = page + 1;
      BlocProvider.of<PopularBloc>(context).add(GetAllPopularMoviesEvent(
          page: page, language: FunctionsManager.getLangCode(context)));
    }
  }

  @override
  void dispose() {
    scrollController
      ..removeListener(_listener)
      ..dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PopularBloc, PopularStates>(
      builder: (context, state) {
        switch (state.popularRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return state.popularMovies.isEmpty
                ? BaseEmptyWidget(
                    emptyMessage: StringsManager.noPopularFilmsString)
                : SingleChildScrollView(
                    key: const Key('topRatedScrollView'),
                    controller: scrollController,
                    child: Column(
                      children: [
                        FadeInUp(
                          from: SizesManager.s20,
                          duration: const Duration(
                            milliseconds: ConstsManager.fadeAnimationDuration,
                          ),
                          child: ListView.separated(
                            padding: EdgeInsets.symmetric(
                              vertical:
                                  getHeight(inputHeight: PaddingManager.p16),
                              horizontal:
                                  getWidth(inputWidth: PaddingManager.p8),
                            ),
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              if (index >= state.popularMovies.length) {
                                return BaseLoadingIndicator();
                              }
                              return PopularComponent(
                                  popular: state.popularMovies[index]);
                            },
                            separatorBuilder: (context, index) => SizedBox(
                                height:
                                    getHeight(inputHeight: SizesManager.s20)),
                            itemCount: state.hasReachedMax
                                ? state.popularMovies.length
                                : state.popularMovies.length + 1,
                          ),
                        ),
                      ],
                    ),
                  );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorPopularMessage,
                onPressFunction: () {
                  BlocProvider.of<PopularBloc>(context).add(
                      GetAllPopularMoviesEvent(
                          page: page,
                          language: FunctionsManager.getLangCode(context)));
                });
        }
      },
    );
  }
}
