import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';

import '../../domain/entites/popular.dart';

class PopularStates extends Equatable {
  final bool hasReachedMax;
  final RequestStates popularRequestStates;
  final List<Popular> popularMovies;
  final String errorPopularMessage;

  PopularStates({
    this.hasReachedMax = false,
    this.popularRequestStates = RequestStates.loading,
    this.popularMovies = const [],
    this.errorPopularMessage = '',
  });

  PopularStates copyWith({
    bool? hasReachedMax,
    RequestStates? popularRequestStates,
    List<Popular>? popularMovies,
    String? errorPopularMessage,
  }) {
    return PopularStates(
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      errorPopularMessage: errorPopularMessage ?? this.errorPopularMessage,
      popularMovies: popularMovies ?? this.popularMovies,
      popularRequestStates: popularRequestStates ?? this.popularRequestStates,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        hasReachedMax,
        popularRequestStates,
        popularMovies,
        errorPopularMessage,
      ];
}