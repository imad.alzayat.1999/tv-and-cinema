import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/shared_features/popular/popular_movies/presentation/controllers/popular_events.dart';
import 'package:movie_app_clean_architecture/shared_features/popular/popular_movies/presentation/controllers/popular_states.dart';

import '../../../../../core/utils/request_states.dart';
import '../../domain/usecase/get_popular_movies_usecase.dart';

class PopularBloc extends Bloc<PopularEvents, PopularStates> {
  final GetPopularUseCase getPopularMoviesUseCase;

  PopularBloc({
    required this.getPopularMoviesUseCase,
  }) : super(PopularStates()) {
    on<GetAllPopularMoviesEvent>((event, emit) async {
      if (state.hasReachedMax) return;
      try {
        if (state.popularRequestStates == RequestStates.loading) {
          final response = await getPopularMoviesUseCase(
            PopularMoviesParameters(page: event.page , language: event.language),
          );
          response.fold((l) {
            emit(
              state.copyWith(
                popularRequestStates: RequestStates.error,
                errorPopularMessage: l.message,
              ),
            );
          }, (r) {
            r.isEmpty
                ? emit(state.copyWith(hasReachedMax: true))
                : emit(
              state.copyWith(
                popularRequestStates: RequestStates.success,
                popularMovies: r,
                hasReachedMax: false,
              ),
            );
          });
        } else {
          final response = await getPopularMoviesUseCase(
            PopularMoviesParameters(page: event.page , language: event.language),
          );
          print("length is ${state.popularMovies.length}");
          response.fold((l) {
            emit(
              state.copyWith(
                popularRequestStates: RequestStates.error,
                errorPopularMessage: l.message,
              ),
            );
          }, (r) {
            r.isEmpty
                ? emit(state.copyWith(hasReachedMax: true))
                : emit(
              state.copyWith(
                popularRequestStates: RequestStates.success,
                popularMovies: [...state.popularMovies, ...r],
                hasReachedMax: false,
              ),
            );
          });
        }
      } catch (e) {
        emit(
          state.copyWith(
            popularRequestStates: RequestStates.error,
            errorPopularMessage: e.toString(),
          ),
        );
      }
    } , transformer: droppable());
  }
}
