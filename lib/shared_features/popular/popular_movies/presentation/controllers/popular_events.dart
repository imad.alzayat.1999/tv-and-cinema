import 'package:equatable/equatable.dart';

abstract class PopularEvents extends Equatable{

}
class GetAllPopularMoviesEvent extends PopularEvents{
  final int page;
  final String language;

  GetAllPopularMoviesEvent({required this.page , required this.language});
  @override
  // TODO: implement props
  List<Object?> get props => [page , language];
}