import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';

import '../../../../../core/network/network_info.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../domain/entites/popular.dart';
import '../../domain/repository/base_popular_repository.dart';
import '../../domain/usecase/get_popular_movies_usecase.dart';
import '../datasource/popular_movies_local_datasource.dart';
import '../datasource/popular_movies_remote_datasource.dart';

class PopularRepository extends BasePopularRepository {
  final BasePopularMoviesRemoteDataSource basePopularMoviesRemoteDataSource;
  final BasePopularMoviesLocalDataSource basePopularMoviesLocalDataSource;
  final NetworkInfo networkInfo;

  PopularRepository({
    required this.basePopularMoviesRemoteDataSource,
    required this.basePopularMoviesLocalDataSource,
    required this.networkInfo,
  });
  @override
  Future<Either<Failure, List<Popular>>> getPopularMovies(
      PopularMoviesParameters popularMoviesParameters) async {
    if(await networkInfo.isConnected){
      try {
        final response = await basePopularMoviesRemoteDataSource
            .getPopularMovies(popularMoviesParameters);
        basePopularMoviesLocalDataSource.cacheInfo(response, ConstsManager.CACHE_POPULAR_LIST);
        return Right(response);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message));
      }
    }else{
      try {
        final localData = await basePopularMoviesLocalDataSource
            .getCachedInfo(ConstsManager.CACHE_POPULAR_LIST);
        return Right(localData);
      } on LocalDatabaseException catch (e) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }
}
