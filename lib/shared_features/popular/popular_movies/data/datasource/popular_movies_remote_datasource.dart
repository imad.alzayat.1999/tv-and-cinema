import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import '../../domain/usecase/get_popular_movies_usecase.dart';
import '../models/popular_model.dart';

abstract class BasePopularMoviesRemoteDataSource {
  Future<List<PopularModel>> getPopularMovies(
      PopularMoviesParameters popularMoviesParameters);
}

class PopularMoviesRemoteDataSource extends BasePopularMoviesRemoteDataSource {
  final ApiConsumer apiConsumer;

  PopularMoviesRemoteDataSource({required this.apiConsumer});
  @override
  Future<List<PopularModel>> getPopularMovies(
      PopularMoviesParameters popularMoviesParameters) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getPopularMoviesEndPoint,
      queryParameters: {
        "page": popularMoviesParameters.page,
        "language": popularMoviesParameters.language,
      },
    );
    return List<PopularModel>.from((response["results"] as List).map(
      (e) => PopularModel.fromJson(e),
    ));
  }
}
