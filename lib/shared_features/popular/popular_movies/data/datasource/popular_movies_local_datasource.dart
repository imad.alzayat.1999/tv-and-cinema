import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/services/services_locator.dart';
import '../models/popular_model.dart';

abstract class BasePopularMoviesLocalDataSource{
  Future<List<PopularModel>> getCachedInfo(String infoKey);
  Future<Unit> cacheInfo(List<PopularModel> popularMovies, String infoKey);
}

class PopularMoviesLocalDataSource extends BasePopularMoviesLocalDataSource{
  @override
  Future<Unit> cacheInfo(List<PopularModel> popularMovies, String infoKey) {
    List popularModelsToJson = popularMovies
        .map<Map<String, dynamic>>((popularMovies) => popularMovies.toJson())
        .toList();
    getIt<SharedPreferences>().setString(infoKey, json.encode(popularModelsToJson));
    print("Info Cached for is:  " + json.encode(popularModelsToJson));
    return Future.value(unit);
  }

  @override
  Future<List<PopularModel>> getCachedInfo(String infoKey) {
    final jsonString = getIt<SharedPreferences>().getString(infoKey);
    if (jsonString != null) {
      List decodeJsonData = json.decode(jsonString);
      List<PopularModel> jsonToPopularModels = decodeJsonData
          .map<PopularModel>((jsonPopularModel) => PopularModel.fromJson(jsonPopularModel))
          .toList();
      return Future.value(jsonToPopularModels);
    } else {
      throw LocalDatabaseException(message: '');
    }
  }

}