import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';

import '../entites/popular.dart';
import '../usecase/get_popular_movies_usecase.dart';

abstract class BasePopularRepository{
  Future<Either<Failure , List<Popular>>> getPopularMovies(PopularMoviesParameters popularMoviesParameters);
}