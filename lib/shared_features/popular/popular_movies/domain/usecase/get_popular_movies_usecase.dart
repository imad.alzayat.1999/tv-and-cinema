import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entites/popular.dart';
import '../repository/base_popular_repository.dart';

class GetPopularUseCase
    extends BaseUseCase<List<Popular>, PopularMoviesParameters> {
  final BasePopularRepository basePopularRepository;

  GetPopularUseCase({required this.basePopularRepository});
  @override
  Future<Either<Failure, List<Popular>>> call(
      PopularMoviesParameters parameters) async {
    return await basePopularRepository.getPopularMovies(parameters);
  }
}

class PopularMoviesParameters extends Equatable {
  final int page;
  final String language;

  const PopularMoviesParameters({required this.page , required this.language});

  @override
  // TODO: implement props
  List<Object?> get props => [page , language];
}
