import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';

import '../../domain/entities/popular_tv_entity.dart';

class TvPopularStates extends Equatable {
  final String errorTvPopularMessage;
  final List<PopularTvEntity> listOfPopularTv;
  final RequestStates popularTvRequestStates;
  final bool hasReachedMax;

  TvPopularStates({
    this.errorTvPopularMessage = '',
    this.listOfPopularTv = const [],
    this.popularTvRequestStates = RequestStates.loading,
    this.hasReachedMax = false,
  });

  TvPopularStates copyWith({
    String? errorTvPopularMessage,
    List<PopularTvEntity>? listOfPopularTv,
    bool? hasReachedMax,
    RequestStates? popularTvRequestStates,
  }) {
    return TvPopularStates(
      errorTvPopularMessage:
          errorTvPopularMessage ?? this.errorTvPopularMessage,
      listOfPopularTv: listOfPopularTv ?? this.listOfPopularTv,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      popularTvRequestStates:
          popularTvRequestStates ?? this.popularTvRequestStates,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        hasReachedMax,
        popularTvRequestStates,
        listOfPopularTv,
        errorTvPopularMessage
      ];
}
