import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/shared_features/popular/popular_tv/presentation/controller/tv_popular_events.dart';
import 'package:movie_app_clean_architecture/shared_features/popular/popular_tv/presentation/controller/tv_popular_states.dart';

import '../../../../../core/utils/request_states.dart';
import '../../domain/usecases/get_popular_tv_usecase.dart';


class TvPopularBloc extends Bloc<TvPopularEvents, TvPopularStates> {
  final GetTvPopularUseCase getTvPopularUseCase;
  TvPopularBloc({required this.getTvPopularUseCase})
      : super(TvPopularStates()) {
    on<GetTvPopularEvent>(
      (event, emit) async {
        if (state.hasReachedMax) return;
        try {
          if (state.popularTvRequestStates == RequestStates.loading) {
            final response = await getTvPopularUseCase(
                PopularTvParameters(page: event.page));
            response.fold((l) {
              emit(
                state.copyWith(
                  popularTvRequestStates: RequestStates.error,
                  errorTvPopularMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                      state.copyWith(
                        popularTvRequestStates: RequestStates.success,
                        listOfPopularTv: r,
                        hasReachedMax: false,
                      ),
                    );
            });
          } else {
            final response = await getTvPopularUseCase(
                PopularTvParameters(page: event.page));
            response.fold((l) {
              emit(
                state.copyWith(
                  popularTvRequestStates: RequestStates.error,
                  errorTvPopularMessage: l.message,
                ),
              );
            }, (r) {
              r.isEmpty
                  ? emit(state.copyWith(hasReachedMax: true))
                  : emit(
                      state.copyWith(
                        popularTvRequestStates: RequestStates.success,
                        listOfPopularTv: [...state.listOfPopularTv, ...r],
                        hasReachedMax: false,
                      ),
                    );
            });
          }
        } catch (e) {
          emit(
            state.copyWith(
              popularTvRequestStates: RequestStates.error,
              errorTvPopularMessage: e.toString(),
            ),
          );
        }
      },
      transformer: droppable(),
    );
  }
}
