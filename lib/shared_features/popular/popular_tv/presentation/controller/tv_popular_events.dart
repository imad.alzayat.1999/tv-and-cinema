import 'package:equatable/equatable.dart';

abstract class TvPopularEvents extends Equatable{

}

class GetTvPopularEvent extends TvPopularEvents{
  final int page;

  GetTvPopularEvent({required this.page});
  @override
  // TODO: implement props
  List<Object?> get props => [page];
}