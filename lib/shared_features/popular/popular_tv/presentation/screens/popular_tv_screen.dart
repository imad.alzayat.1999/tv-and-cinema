import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_error.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../components/tv_popular_item.dart';
import '../controller/tv_popular_bloc.dart';
import '../controller/tv_popular_events.dart';
import '../controller/tv_popular_states.dart';

class PopularTvScreen extends StatelessWidget {
  const PopularTvScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          getIt<TvPopularBloc>()..add(GetTvPopularEvent(page: 1)),
      child: Scaffold(
        backgroundColor: ColorsManager.kEerieBlack,
        appBar: AppBar(
          elevation: SizesManager.s0,
          backgroundColor: ColorsManager.kEerieBlack,
          title: Text(
            AppLocalizations.of(context)!
                .translate(StringsManager.popularString)
                .toString(),
            style: getBoldTextStyle(
              fontSize: FontSizeManager.size16,
              letterSpacing: SizesManager.s0,
              context: context,
            ),
          ),
        ),
        body: PopularTvContent(),
      ),
    );
  }
}

class PopularTvContent extends StatefulWidget {
  const PopularTvContent({Key? key}) : super(key: key);

  @override
  State<PopularTvContent> createState() => _PopularTvContentState();
}

class _PopularTvContentState extends State<PopularTvContent> {
  final scrollController = ScrollController();
  int page = 1;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(_listener);
  }

  _listener() {
    if (scrollController.position.maxScrollExtent ==
        scrollController.position.pixels) {
      page = page + 1;
      BlocProvider.of<TvPopularBloc>(context)
          .add(GetTvPopularEvent(page: page));
    }
  }

  @override
  void dispose() {
    scrollController
      ..removeListener(_listener)
      ..dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TvPopularBloc, TvPopularStates>(
      builder: (context, state) {
        switch (state.popularTvRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return SingleChildScrollView(
              key: const Key('topRatedScrollView'),
              controller: scrollController,
              child: Column(
                children: [
                  FadeInUp(
                    from: SizesManager.s20,
                    duration: const Duration(
                      milliseconds: ConstsManager.fadeAnimationDuration,
                    ),
                    child: ListView.separated(
                      padding: EdgeInsets.symmetric(
                        vertical: getHeight(inputHeight: PaddingManager.p16),
                        horizontal: getWidth(inputWidth: PaddingManager.p8),
                      ),
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return index >= state.listOfPopularTv.length
                            ? BaseLoadingIndicator()
                            : TvPopularItem(
                                tvPopular: state.listOfPopularTv[index]);
                      },
                      separatorBuilder: (context, index) => SizedBox(
                          height: getHeight(inputHeight: SizesManager.s20)),
                      itemCount: state.hasReachedMax
                          ? state.listOfPopularTv.length
                          : state.listOfPopularTv.length + 1,
                    ),
                  ),
                ],
              ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorTvPopularMessage,
                onPressFunction: () {
                  BlocProvider.of<TvPopularBloc>(context)
                    ..add(GetTvPopularEvent(page: page));
                });
        }
      },
    );
  }
}
