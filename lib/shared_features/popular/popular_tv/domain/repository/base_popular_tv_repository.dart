import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';

import '../entities/popular_tv_entity.dart';
import '../usecases/get_popular_tv_usecase.dart';

abstract class BasePopularTvRepository {
  Future<Either<Failure, List<PopularTvEntity>>> getPopularTv(
      {required PopularTvParameters popularTvParameters});
}
