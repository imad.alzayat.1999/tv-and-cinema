import 'package:equatable/equatable.dart';

class PopularTvEntity extends Equatable {
  final String? backdropPath;
  final String? name;
  final String? overview;
  final String? firstAirDate;
  final num? voteAvg;
  final int id;

  PopularTvEntity({
    required this.backdropPath,
    required this.name,
    required this.overview,
    required this.firstAirDate,
    required this.voteAvg,
    required this.id,
  });

  @override
  // TODO: implement props
  List<Object?> get props =>
      [backdropPath, name, overview, firstAirDate, voteAvg , id];
}
