import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../entities/popular_tv_entity.dart';
import '../repository/base_popular_tv_repository.dart';


class GetTvPopularUseCase extends BaseUseCase<List<PopularTvEntity> , PopularTvParameters>{
  final BasePopularTvRepository basePopularTvRepository;

  GetTvPopularUseCase({required this.basePopularTvRepository});

  @override
  Future<Either<Failure, List<PopularTvEntity>>> call(PopularTvParameters parameters) async{
    return await basePopularTvRepository.getPopularTv(popularTvParameters: parameters);
  }
}

class PopularTvParameters extends Equatable{
  final int page;

  PopularTvParameters({required this.page});

  @override
  // TODO: implement props
  List<Object?> get props => [page];
}