import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';

import '../../domain/entities/popular_tv_entity.dart';
import '../../domain/repository/base_popular_tv_repository.dart';
import '../../domain/usecases/get_popular_tv_usecase.dart';
import '../datasources/popular_tv_remote_datasource.dart';

class PopularTvRepository extends BasePopularTvRepository {
  final BasePopularTvRemoteDataSource basePopularTvRemoteDataSource;

  PopularTvRepository({required this.basePopularTvRemoteDataSource});
  @override
  Future<Either<Failure, List<PopularTvEntity>>> getPopularTv(
      {required PopularTvParameters popularTvParameters}) async {
    try {
      final result = await basePopularTvRemoteDataSource.getPopularModel(
          popularTvParameters: popularTvParameters);
      return Right(result);
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    }
  }
}
