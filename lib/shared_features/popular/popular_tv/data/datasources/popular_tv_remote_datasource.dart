import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';

import '../../domain/usecases/get_popular_tv_usecase.dart';
import '../models/popular_tv_model.dart';

abstract class BasePopularTvRemoteDataSource{
  Future<List<PopularTvModel>> getPopularModel({required PopularTvParameters popularTvParameters});
}

class PopularTvRemoteDataSource extends BasePopularTvRemoteDataSource{
  final ApiConsumer apiConsumer;

  PopularTvRemoteDataSource({required this.apiConsumer});

  @override
  Future<List<PopularTvModel>> getPopularModel({required PopularTvParameters popularTvParameters}) async{
    final response = await apiConsumer.get(path: ApiConstance.popularTvEndPoint , queryParameters: {
      "page" : popularTvParameters.page,
    });
    return List<PopularTvModel>.from((response["results"] as List).map(
          (e) => PopularTvModel.fromJson(e),
    ));
  }
}