import '../../domain/entities/popular_tv_entity.dart';

class PopularTvModel extends PopularTvEntity {
  PopularTvModel({
    required String? backdropPath,
    required String? name,
    required String? overview,
    required String? firstAirDate,
    required num? voteAvg,
    required int id,
  }) : super(
          backdropPath: backdropPath,
          name: name,
          overview: overview,
          firstAirDate: firstAirDate,
          voteAvg: voteAvg,
          id: id,
        );

  factory PopularTvModel.fromJson(Map<String, dynamic> json) => PopularTvModel(
        backdropPath: json["backdrop_path"],
        name: json["name"],
        overview: json["overview"],
        firstAirDate: json["first_air_date"],
        voteAvg: json["vote_average"],
        id: json["id"],
      );
}
