import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';

import '../../domain/entities/tv_popular_entity.dart';

class TvStates extends Equatable {
  final RequestStates popularRequestStates;
  final List<TvEntity> tvPopularEntities;
  final String errorTvPopularMessage;

  final RequestStates onTheAirRequestStates;
  final List<TvEntity> onTheAirTvEntities;
  final String errorOnTheAirTvMessage;

  final RequestStates airingTodayRequestStates;
  final List<TvEntity> airingTodayEntities;
  final String errorAiringTodayMessage;

  final RequestStates topRatedRequestStates;
  final List<TvEntity> topRatedTvEntities;
  final String errorTopRatedTvMessage;

  const TvStates({
    this.popularRequestStates = RequestStates.loading,
    this.tvPopularEntities = const [],
    this.errorTvPopularMessage = '',
    this.onTheAirRequestStates = RequestStates.loading,
    this.onTheAirTvEntities = const [],
    this.errorOnTheAirTvMessage = '',
    this.airingTodayRequestStates = RequestStates.loading,
    this.airingTodayEntities = const [],
    this.errorAiringTodayMessage = '',
    this.topRatedRequestStates = RequestStates.loading,
    this.topRatedTvEntities = const [],
    this.errorTopRatedTvMessage = '',
  });

  TvStates copyWith({
    RequestStates? popularRequestStates,
    List<TvEntity>? tvPopularEntities,
    String? errorTvPopularMessage,
    RequestStates? onTheAirRequestStates,
    List<TvEntity>? onTheAirTvEntities,
    String? errorOnTheAirTvMessage,
    RequestStates? airingTodayRequestStates,
    List<TvEntity>? airingTodayEntities,
    String? errorAiringTodayMessage,
    RequestStates? topRatedRequestStates,
    List<TvEntity>? topRatedTvEntities,
    String? errorTopRatedTvMessage,
  }) {
    return TvStates(
      popularRequestStates: popularRequestStates ?? this.popularRequestStates,
      tvPopularEntities: tvPopularEntities ?? this.tvPopularEntities,
      errorTvPopularMessage:
          errorTvPopularMessage ?? this.errorTvPopularMessage,
      onTheAirRequestStates:
          onTheAirRequestStates ?? this.onTheAirRequestStates,
      onTheAirTvEntities: onTheAirTvEntities ?? this.onTheAirTvEntities,
      errorOnTheAirTvMessage:
          errorOnTheAirTvMessage ?? this.errorOnTheAirTvMessage,
      airingTodayEntities: airingTodayEntities ?? this.airingTodayEntities,
      airingTodayRequestStates:
          airingTodayRequestStates ?? this.airingTodayRequestStates,
      errorAiringTodayMessage:
          errorAiringTodayMessage ?? this.errorAiringTodayMessage,
      topRatedRequestStates:
          topRatedRequestStates ?? this.topRatedRequestStates,
      topRatedTvEntities: topRatedTvEntities ?? this.topRatedTvEntities,
      errorTopRatedTvMessage:
          errorTopRatedTvMessage ?? this.errorTopRatedTvMessage,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        popularRequestStates,
        tvPopularEntities,
        errorTvPopularMessage,
        onTheAirTvEntities,
        onTheAirRequestStates,
        errorOnTheAirTvMessage,
        airingTodayEntities,
        airingTodayRequestStates,
        errorAiringTodayMessage,
        topRatedRequestStates,
        topRatedTvEntities,
        errorTopRatedTvMessage,
      ];
}
