import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import 'package:movie_app_clean_architecture/shared_features/home/home_tv/presentation/controller/tv_events.dart';
import 'package:movie_app_clean_architecture/shared_features/home/home_tv/presentation/controller/tv_states.dart';
import '../../domain/usecases/get_airing_today_tv_usecase.dart';
import '../../domain/usecases/get_on_the_air_tv_usecase.dart';
import '../../domain/usecases/get_popular_tv_usecase.dart';
import '../../domain/usecases/get_top_rated_tv_usecase.dart';

class TvBloc extends Bloc<TvEvents, TvStates> {
  final GetOnTheAirTvUseCase getOnTheAirTvUseCase;
  final GetPopularTvUseCase getPopularTvUseCase;
  final GetAiringTodayTvUseCase getAiringTodayTvUseCase;
  final GetTopRatedTvUseCase getTopRatedTvUseCase;
  TvBloc({
    required this.getOnTheAirTvUseCase,
    required this.getPopularTvUseCase,
    required this.getAiringTodayTvUseCase,
    required this.getTopRatedTvUseCase,
  }) : super(TvStates()) {
    on<GetOnTheAirEvent>((event, emit) async {
      final failureOrOnTheAirTv =
          await getOnTheAirTvUseCase(const NoParameters());
      failureOrOnTheAirTv.fold(
        (l) => emit(
          state.copyWith(
            onTheAirRequestStates: RequestStates.error,
            errorOnTheAirTvMessage: l.message,
          ),
        ),
        (r) => emit(
          state.copyWith(
              onTheAirRequestStates: RequestStates.success,
              onTheAirTvEntities: r),
        ),
      );
    });
    on<GetTvPopularEvent>((event, emit) async {
      emit(state.copyWith(popularRequestStates: RequestStates.loading));
      final failureOrTvPopular =
          await getPopularTvUseCase(const NoParameters());
      failureOrTvPopular.fold(
        (l) => emit(
          state.copyWith(
            popularRequestStates: RequestStates.error,
            errorTvPopularMessage: l.message,
          ),
        ),
        (r) => emit(
          state.copyWith(
              popularRequestStates: RequestStates.success,
              tvPopularEntities: r),
        ),
      );
    });
    on<GetAiringTodayEvent>((event, emit) async {
      emit(state.copyWith(airingTodayRequestStates: RequestStates.loading));
      final failureOrAiringToday =
          await getAiringTodayTvUseCase(const NoParameters());
      failureOrAiringToday.fold(
        (l) => emit(
          state.copyWith(
            airingTodayRequestStates: RequestStates.error,
            errorAiringTodayMessage: l.message,
          ),
        ),
        (r) => emit(
          state.copyWith(
              airingTodayRequestStates: RequestStates.success,
              airingTodayEntities: r),
        ),
      );
    });
    on<GetTopRatedTvEvent>((event, emit) async {
      emit(state.copyWith(topRatedRequestStates: RequestStates.loading));
      final failureOrTopRated =
          await getTopRatedTvUseCase(const NoParameters());
      failureOrTopRated.fold(
        (l) => emit(
          state.copyWith(
            topRatedRequestStates: RequestStates.error,
            errorTopRatedTvMessage: l.message,
          ),
        ),
        (r) => emit(
          state.copyWith(
              topRatedRequestStates: RequestStates.success,
              topRatedTvEntities: r),
        ),
      );
    });
  }
}
