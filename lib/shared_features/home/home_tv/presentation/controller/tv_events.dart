import 'package:equatable/equatable.dart';

abstract class TvEvents extends Equatable{

}

class GetOnTheAirEvent extends TvEvents{
  GetOnTheAirEvent();
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class GetTvPopularEvent extends TvEvents{
  GetTvPopularEvent();
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class GetAiringTodayEvent extends TvEvents{
  GetAiringTodayEvent();
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class GetTopRatedTvEvent extends TvEvents{
  GetTopRatedTvEvent();
  @override
  // TODO: implement props
  List<Object?> get props => [];
}
