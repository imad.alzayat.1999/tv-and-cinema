import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../controller/tv_bloc.dart';
import '../controller/tv_events.dart';
import '../controller/tv_states.dart';

class TopRatedTvComponent extends StatelessWidget {
  const TopRatedTvComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TvBloc, TvStates>(
      buildWhen: (previousState, currentState) =>
          previousState.topRatedRequestStates !=
          currentState.topRatedRequestStates,
      builder: (context, state) {
        switch (state.topRatedRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            print(
                "the length of the array is ${state.topRatedTvEntities.length}");
            return FadeIn(
              duration: const Duration(
                  milliseconds: ConstsManager.fadeAnimationDuration),
              child: SizedBox(
                height: getHeight(inputHeight: SizesManager.s170),
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.symmetric(
                    horizontal: getWidth(inputWidth: PaddingManager.p16),
                  ),
                  itemCount: state.topRatedTvEntities.length,
                  itemBuilder: (context, index) {
                    final tv = state.topRatedTvEntities[index];
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: PaddingManager.p8,
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, Routes.tvDetailsRoute,
                                  arguments: DetailsRouteParameters(id: tv.id))
                              .then((value) {
                            BlocProvider.of<TvBloc>(context)
                              ..add(GetTopRatedTvEvent());
                          });
                        },
                        child: ClipRRect(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(SizesManager.s8),
                          ),
                          child: FadeInImage.assetNetwork(
                            width: getWidth(inputWidth: SizesManager.s120),
                            fit: BoxFit.cover,
                            image: FunctionsManager.showAPIImage(
                                tv.tvPopularImage),
                            placeholder: ImagesManager.logoImage,
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            );
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorTopRatedTvMessage,
                onPressFunction: () {
                  BlocProvider.of<TvBloc>(context)..add(GetTopRatedTvEvent());
                });
        }
      },
    );
  }
}
