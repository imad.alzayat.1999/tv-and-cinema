import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../controller/tv_bloc.dart';
import '../controller/tv_events.dart';
import '../controller/tv_states.dart';

class AiringTodayTvComponent extends StatelessWidget {
  const AiringTodayTvComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TvBloc, TvStates>(
      buildWhen: (previous, current) =>
          previous.airingTodayRequestStates != current.airingTodayRequestStates,
      builder: (context, state) {
        switch (state.airingTodayRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return FadeIn(
              duration: const Duration(
                  milliseconds: ConstsManager.fadeAnimationDuration),
              child: SizedBox(
                height:
                    getHeight(inputHeight: SizesManager.s170),
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.symmetric(
                    horizontal: getWidth(inputWidth: PaddingManager.p16),
                  ),
                  itemCount: state.airingTodayEntities.length,
                  itemBuilder: (context, index) {
                    final tv = state.airingTodayEntities[index];
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: PaddingManager.p8,
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, Routes.tvDetailsRoute,
                                  arguments: DetailsRouteParameters(id: tv.id))
                              .then((value) {
                            BlocProvider.of<TvBloc>(context)
                              ..add(GetAiringTodayEvent());
                          });
                        },
                        child: SizedBox(
                          width: getWidth(inputWidth: SizesManager.s120),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(SizesManager.s8),
                            child: FadeInImage.assetNetwork(
                              fit: BoxFit.cover,
                              placeholder: ImagesManager.logoImage,
                              image: FunctionsManager.showAPIImage(tv.tvPopularImage),
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            );
          case RequestStates.error:
            return BaseError(
              errorMessage: state.errorAiringTodayMessage,
              onPressFunction: () {
                BlocProvider.of<TvBloc>(context)..add(GetAiringTodayEvent());
              },
            );
        }
      },
    );
  }
}
