import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';

import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../controller/tv_bloc.dart';
import '../controller/tv_events.dart';
import '../controller/tv_states.dart';

class OnTheAirTvComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TvBloc, TvStates>(
      buildWhen: (previous, current) =>
          previous.onTheAirRequestStates != current.onTheAirRequestStates,
      builder: (context, state) {
        switch (state.onTheAirRequestStates) {
          case RequestStates.loading:
            return BaseLoadingIndicator();
          case RequestStates.success:
            return CarouselSlider(
                items: state.onTheAirTvEntities
                    .map(
                      (e) => Card(
                        borderOnForeground: true,
                        semanticContainer: true,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(SizesManager.s20),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(SizesManager.s20),
                          child: FadeInImage.assetNetwork(
                            fit: BoxFit.cover,
                            image:
                                FunctionsManager.showAPIImage(e.tvPopularImage),
                            placeholder: ImagesManager.logoImage,
                          ),
                        ),
                      ),
                    )
                    .toList(),
                options: CarouselOptions(
                  height: getHeight(inputHeight: SizesManager.s300),
                  viewportFraction: 0.8,
                  initialPage: 0,
                  enableInfiniteScroll: true,
                  reverse: false,
                  autoPlay: true,
                  autoPlayInterval: const Duration(
                    seconds: ConstsManager.carouselAutoPlayIntervalDuration,
                  ),
                  autoPlayAnimationDuration: const Duration(
                    milliseconds: ConstsManager.autoPlayAnimationDuration,
                  ),
                  autoPlayCurve: Curves.fastOutSlowIn,
                  enlargeCenterPage: true,
                  scrollDirection: Axis.horizontal,
                ));
          case RequestStates.error:
            return BaseError(
                errorMessage: state.errorOnTheAirTvMessage,
                onPressFunction: () {
                  BlocProvider.of<TvBloc>(context)..add(GetOnTheAirEvent());
                });
        }
      },
    );
  }
}
