import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_search_bar_component.dart';
import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/config/size_config/size_config.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../components/airing_today_tv_component.dart';
import '../components/on_the_air_tv_component.dart';
import '../components/top_rated_tv_component.dart';
import '../components/tv_popular_component.dart';
import '../controller/tv_bloc.dart';
import '../controller/tv_events.dart';

class TvScreen extends StatefulWidget {
  static const String routeName = Routes.tvRoute;
  @override
  State<TvScreen> createState() => _TvScreenState();
}

class _TvScreenState extends State<TvScreen> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<TvBloc>()
        ..add(GetOnTheAirEvent())
        ..add(GetTvPopularEvent())
        ..add(GetAiringTodayEvent())
        ..add(GetTopRatedTvEvent()),
      child: SingleChildScrollView(
        key: const Key('movieScrollView'),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(
                MarginManager.m16,
                getHeight(inputHeight: MarginManager.m24),
                MarginManager.m16,
                MarginManager.m8,
              ),
              child: BaseSearchBarComponent(
                onPressed: () =>
                    Navigator.pushNamed(context, Routes.searchSeriesRoute),
              ),
            ),
            OnTheAirTvComponent(),
            Container(
              margin: EdgeInsets.fromLTRB(
                MarginManager.m16,
                getHeight(inputHeight: MarginManager.m24),
                MarginManager.m16,
                MarginManager.m8,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .translate(StringsManager.popularString)
                        .toString(),
                    style: getMediumTextStyle(
                      fontSize: FontSizeManager.size19,
                      letterSpacing: SizesManager.s0_15,
                      context: context,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed(Routes.tvPopularRoute);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(PaddingManager.p8),
                      child: Row(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .translate(StringsManager.seeMoreString)
                                .toString(),
                            style: getMediumTextStyle(
                                fontSize: FontSizeManager.size12,
                                letterSpacing: SizesManager.s0,
                                context: context),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const TvPopularComponent(),
            Container(
              margin: EdgeInsets.fromLTRB(
                MarginManager.m16,
                getHeight(inputHeight: MarginManager.m24),
                MarginManager.m16,
                MarginManager.m8,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .translate(StringsManager.airingTodayString)
                        .toString(),
                    style: getMediumTextStyle(
                      fontSize: FontSizeManager.size19,
                      letterSpacing: SizesManager.s0_15,
                      context: context,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, Routes.airingTodayRoute);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(PaddingManager.p8),
                      child: Row(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .translate(StringsManager.seeMoreString)
                                .toString(),
                            style: getMediumTextStyle(
                                fontSize: FontSizeManager.size12,
                                letterSpacing: SizesManager.s0,
                                context: context),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const AiringTodayTvComponent(),
            Container(
              margin: EdgeInsets.fromLTRB(
                MarginManager.m16,
                getHeight(inputHeight: MarginManager.m24),
                MarginManager.m16,
                MarginManager.m8,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .translate(StringsManager.topRatedString)
                        .toString(),
                    style: getMediumTextStyle(
                      fontSize: FontSizeManager.size19,
                      letterSpacing: SizesManager.s0_15,
                      context: context,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, Routes.topRatedTvRoute);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(PaddingManager.p8),
                      child: Row(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .translate(StringsManager.seeMoreString)
                                .toString(),
                            style: getMediumTextStyle(
                                fontSize: FontSizeManager.size12,
                                letterSpacing: SizesManager.s0,
                                context: context),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const TopRatedTvComponent(),
            SizedBox(
              height: getHeight(inputHeight: SizesManager.s50),
            ),
          ],
        ),
      ),
    );
  }
}
