import '../../domain/entities/tv_popular_entity.dart';

class TvModel extends TvEntity {
  TvModel({required int id, required String? tvPopularImage})
      : super(id: id, tvPopularImage: tvPopularImage);

  factory TvModel.fromJson(Map<String, dynamic> json) => TvModel(
        id: json["id"],
        tvPopularImage: json["backdrop_path"],
      );

  Map<String , dynamic> toJson() => {
    "id" : id,
    "backdrop_path": tvPopularImage,
  };
}
