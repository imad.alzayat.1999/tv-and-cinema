import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import '../../domain/entities/tv_popular_entity.dart';
import '../../domain/repository/base_home_tv_repository.dart';
import '../datasource/home_tv_remote_datasource.dart';

class HomeTvRepository extends BaseTvHomeRepository{
  final BaseTvHomeRemoteDataSource baseTvHomeRemoteDataSource;

  HomeTvRepository({required this.baseTvHomeRemoteDataSource});

  @override
  Future<Either<Failure, List<TvEntity>>> getOnTheAirTv() async{
    try{
      final result = await baseTvHomeRemoteDataSource.getOnTheAirTv();
      return Right(result);
    }on ServerException catch(e){
      return Left(ServerFailure(StringsManager.serverFailureString));
    }
  }

  @override
  Future<Either<Failure, List<TvEntity>>> getPopularTv() async{
    try{
      final result = await baseTvHomeRemoteDataSource.getPopularTv();
      return Right(result);
    }on ServerException catch(e){
      return Left(ServerFailure(StringsManager.serverFailureString));
    }
  }

  @override
  Future<Either<Failure, List<TvEntity>>> getAiringTodayTv() async{
    try{
      final result = await baseTvHomeRemoteDataSource.getAiringTodayTv();
      return Right(result);
    }on ServerException catch(e){
      return Left(ServerFailure(StringsManager.serverFailureString));
    }
  }

  @override
  Future<Either<Failure, List<TvEntity>>> getTopRatedTv() async{
    try{
      final result = await baseTvHomeRemoteDataSource.getTopRatedTv();
      return Right(result);
    }on ServerException catch(e){
      return Left(ServerFailure(StringsManager.serverFailureString));
    }
  }
}