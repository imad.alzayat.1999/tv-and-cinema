import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import '../models/tv_popular_model.dart';

abstract class BaseTvHomeRemoteDataSource {
  Future<List<TvModel>> getOnTheAirTv();
  Future<List<TvModel>> getPopularTv();
  Future<List<TvModel>> getAiringTodayTv();
  Future<List<TvModel>> getTopRatedTv();
}

class HomeTvRemoteDataSource extends BaseTvHomeRemoteDataSource {
  final ApiConsumer apiConsumer;

  HomeTvRemoteDataSource({required this.apiConsumer});

  @override
  Future<List<TvModel>> getOnTheAirTv() async {
    final response =
        await apiConsumer.get(path: ApiConstance.getOnTheAirTvEndPoint);
    return List<TvModel>.from(
        (response["results"] as List).map((e) => TvModel.fromJson(e)));
  }

  @override
  Future<List<TvModel>> getPopularTv() async {
    final response =
        await apiConsumer.get(path: ApiConstance.geTvPopularEndPoint);
    return List<TvModel>.from(
        (response["results"] as List).map((e) => TvModel.fromJson(e)));
  }

  @override
  Future<List<TvModel>> getAiringTodayTv() async{
    final response =
        await apiConsumer.get(path: ApiConstance.getAiringTodayTvEndPoint);
    return List<TvModel>.from(
        (response["results"] as List).map((e) => TvModel.fromJson(e)));
  }

  @override
  Future<List<TvModel>> getTopRatedTv() async{
    final response =
        await apiConsumer.get(path: ApiConstance.getTopRatedTvEndPoint);
    return List<TvModel>.from(
        (response["results"] as List).map((e) => TvModel.fromJson(e)));
  }
}
