import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import '../entities/tv_popular_entity.dart';

abstract class BaseTvHomeRepository{
  Future<Either<Failure , List<TvEntity>>> getOnTheAirTv();
  Future<Either<Failure , List<TvEntity>>> getPopularTv();
  Future<Either<Failure , List<TvEntity>>> getAiringTodayTv();
  Future<Either<Failure , List<TvEntity>>> getTopRatedTv();
}