import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../entities/tv_popular_entity.dart';
import '../repository/base_home_tv_repository.dart';

class GetPopularTvUseCase extends BaseUseCase<List<TvEntity> , NoParameters>{
  final BaseTvHomeRepository baseTvHomeRepository;

  GetPopularTvUseCase({required this.baseTvHomeRepository});
  @override
  Future<Either<Failure, List<TvEntity>>> call(NoParameters parameters) async{
    return await baseTvHomeRepository.getPopularTv();
  }
}