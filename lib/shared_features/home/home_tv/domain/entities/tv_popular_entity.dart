import 'package:equatable/equatable.dart';

class TvEntity extends Equatable{
  final int id;
  final String? tvPopularImage;

  TvEntity({required this.id , required this.tvPopularImage});

  @override
  // TODO: implement props
  List<Object?> get props => [id , tvPopularImage];
}