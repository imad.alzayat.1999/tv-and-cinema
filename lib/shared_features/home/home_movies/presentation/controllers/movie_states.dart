import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import '../../domain/entities/all_movies.dart';
import '../../domain/entities/movie.dart';

class MovieStates extends Equatable {
  final String errorNowPlayingMessage;
  final AllMovies? nowPlayingMovies;
  final RequestStates nowPlayingRequestStates;

  final String errorPopularMessage;
  final AllMovies? popularMovies;
  final RequestStates popularRequestStates;

  final String errorTopRatedMessage;
  final AllMovies? topRatedMovies;
  final RequestStates topRatedRequestStates;

  final String errorComingSoonMessage;
  final AllMovies? comingSoonMovies;
  final RequestStates comingSoonRequestStates;

  MovieStates({
    this.errorNowPlayingMessage = '',
    this.nowPlayingMovies,
    this.nowPlayingRequestStates = RequestStates.loading,
    this.errorPopularMessage = '',
    this.popularMovies,
    this.popularRequestStates = RequestStates.loading,
    this.errorTopRatedMessage = '',
    this.topRatedMovies,
    this.topRatedRequestStates = RequestStates.loading,
    this.comingSoonMovies,
    this.comingSoonRequestStates = RequestStates.loading,
    this.errorComingSoonMessage = '',
  });

  MovieStates copyWith({
    String? errorNowPlayingMessage,
    AllMovies? nowPlayingMovies,
    RequestStates? nowPlayingRequestStates,
    String? errorPopularMessage,
    AllMovies? popularMovies,
    RequestStates? popularRequestStates,
    String? errorTopRatedMessage,
    AllMovies? topRatedMovies,
    RequestStates? topRatedRequestStates,
    String? errorComingSoonMessage,
    AllMovies? comingSoonMovies,
    RequestStates? comingSoonRequestStates,
  }) {
    return MovieStates(
      errorNowPlayingMessage:
          errorNowPlayingMessage ?? this.errorNowPlayingMessage,
      nowPlayingRequestStates:
          nowPlayingRequestStates ?? this.nowPlayingRequestStates,
      errorPopularMessage: errorPopularMessage ?? this.errorPopularMessage,
      nowPlayingMovies: nowPlayingMovies ?? this.nowPlayingMovies,
      popularMovies: popularMovies ?? this.popularMovies,
      errorTopRatedMessage: errorTopRatedMessage ?? this.errorTopRatedMessage,
      topRatedMovies: topRatedMovies ?? this.topRatedMovies,
      popularRequestStates: popularRequestStates ?? this.popularRequestStates,
      topRatedRequestStates:
          topRatedRequestStates ?? this.topRatedRequestStates,
      errorComingSoonMessage:
          errorComingSoonMessage ?? this.errorComingSoonMessage,
      comingSoonMovies: comingSoonMovies ?? this.comingSoonMovies,
      comingSoonRequestStates:
          comingSoonRequestStates ?? this.comingSoonRequestStates,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [
        errorNowPlayingMessage,
        nowPlayingMovies,
        topRatedRequestStates,
        popularRequestStates,
        nowPlayingRequestStates,
        errorPopularMessage,
        popularMovies,
        errorTopRatedMessage,
        topRatedMovies,
        errorComingSoonMessage,
        comingSoonRequestStates,
        comingSoonMovies,
      ];
}
