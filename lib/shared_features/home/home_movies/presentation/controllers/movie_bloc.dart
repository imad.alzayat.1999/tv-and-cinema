import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import '../../domain/usecases/get_coming_soon_usecase.dart';
import '../../domain/usecases/get_now_playing_usecase.dart';
import '../../domain/usecases/get_popular_movies_usecase.dart';
import '../../domain/usecases/get_top_rated_movies_usecase.dart';
import 'movie_events.dart';
import 'movie_states.dart';

class MovieBloc extends Bloc<MovieEvents, MovieStates> {
  final GetTopPlayingUseCase getTopPlayingUseCase;
  final GetPopularMoviesUseCase getPopularMoviesUseCase;
  final GetTopRatedMoviesUseCase getTopRatedMoviesUseCase;
  final GetComingSoonUseCase getComingSoonUseCase;

  MovieBloc({
    required this.getTopPlayingUseCase,
    required this.getPopularMoviesUseCase,
    required this.getTopRatedMoviesUseCase,
    required this.getComingSoonUseCase,
  }) : super(MovieStates()) {
    on<GetNowPlayingMoviesEvent>((event, emit) async {
      final failureOrTopPlayingMovies =
          await getTopPlayingUseCase(NowPlayingHomeParameters(langCode: event.langCode));
      failureOrTopPlayingMovies.fold(
        (l) => emit(
          state.copyWith(
            nowPlayingRequestStates: RequestStates.error,
            errorNowPlayingMessage: l.message,
          ),
        ),
        (r) => emit(
          state.copyWith(
            nowPlayingRequestStates: RequestStates.success,
            nowPlayingMovies: r,
          ),
        ),
      );
    });
    on<GetPopularMoviesEvent>((event, emit) async {
      final failureOrPopularMovies =
          await getPopularMoviesUseCase(PopularHomeParameters(langCode: event.langCode));
      failureOrPopularMovies.fold(
        (l) => emit(state.copyWith(
          errorPopularMessage: l.message,
          popularRequestStates: RequestStates.error,
        )),
        (r) => emit(
          state.copyWith(
            popularRequestStates: RequestStates.success,
            popularMovies: r,
          ),
        ),
      );
    });
    on<GetTopRatedMoviesEvent>((event, emit) async {
      final failureOrTopRatedMovies =
          await getTopRatedMoviesUseCase(TopRatedHomeParameters(langCode: event.langCode));
      failureOrTopRatedMovies.fold(
        (l) => emit(state.copyWith(
          errorTopRatedMessage: l.message,
          topRatedRequestStates: RequestStates.error,
        )),
        (r) => emit(
          state.copyWith(
            topRatedRequestStates: RequestStates.success,
            topRatedMovies: r,
          ),
        ),
      );
    });
    on<GetComingSoonMoviesEvent>((event, emit) async {
      final failureOrComingSoonMovies =
          await getComingSoonUseCase(ComingSoonHomeParameters(langCode: event.langCode));
      failureOrComingSoonMovies.fold(
        (l) => emit(state.copyWith(
          errorComingSoonMessage: l.message,
          comingSoonRequestStates: RequestStates.error,
        )),
        (r) => emit(
          state.copyWith(
            comingSoonRequestStates: RequestStates.success,
            comingSoonMovies: r,
          ),
        ),
      );
    });
  }
}
