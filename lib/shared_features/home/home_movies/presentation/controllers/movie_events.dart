import 'package:equatable/equatable.dart';

abstract class MovieEvents extends Equatable{

}

class GetTopRatedMoviesEvent extends MovieEvents{
  final String langCode;
  GetTopRatedMoviesEvent({required this.langCode});
  @override
  // TODO: implement props
  List<Object?> get props => [langCode];
}

class GetPopularMoviesEvent extends MovieEvents{
  final String langCode;
  GetPopularMoviesEvent({required this.langCode});

  @override
  // TODO: implement props
  List<Object?> get props => [langCode];
}

class GetNowPlayingMoviesEvent extends MovieEvents{
  final String langCode;
  GetNowPlayingMoviesEvent({required this.langCode});

  @override
  // TODO: implement props
  List<Object?> get props => [langCode];
}
class GetComingSoonMoviesEvent extends MovieEvents{
  final String langCode;
  GetComingSoonMoviesEvent({required this.langCode});
  @override
  // TODO: implement props
  List<Object?> get props => [langCode];
}