import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_config.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import '../../../../../core/utils/global_widgets/base_error.dart';
import '../../../../../core/utils/request_states.dart';
import '../controllers/movie_bloc.dart';
import '../controllers/movie_events.dart';
import '../controllers/movie_states.dart';

class TopRatedMovies extends StatelessWidget {
  const TopRatedMovies({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieBloc, MovieStates>(
        buildWhen: (previous, current) =>
            previous.topRatedRequestStates != current.topRatedRequestStates,
        builder: (context, state) {
          switch (state.topRatedRequestStates) {
            case RequestStates.loading:
              return const BaseLoadingIndicator();
            case RequestStates.success:
              return FadeIn(
                duration: const Duration(
                  milliseconds: ConstsManager.fadeAnimationDuration,
                ),
                child: SizedBox(
                  height: getHeight(inputHeight: SizesManager.s170),
                  child: state.topRatedMovies!.movies.isEmpty
                      ? BaseEmptyWidget(
                          emptyMessage: StringsManager.noTopRatedFilmsString)
                      : ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          padding: EdgeInsets.symmetric(
                            horizontal:
                                getWidth(inputWidth: PaddingManager.p16),
                          ),
                          itemCount: state.topRatedMovies!.movies.length,
                          itemBuilder: (context, index) {
                            final movie = state.topRatedMovies!.movies[index];
                            return Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: PaddingManager.p8,
                              ),
                              child: InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, Routes.movieDetailsRoute,
                                      arguments:
                                          DetailsRouteParameters(id: movie.id));
                                },
                                child: ClipRRect(
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(SizesManager.s8),
                                  ),
                                  child: FadeInImage.assetNetwork(
                                    width:
                                        getWidth(inputWidth: SizesManager.s120),
                                    fit: BoxFit.cover,
                                    image: FunctionsManager.showAPIImage(
                                        movie.backdropPath),
                                    placeholder: ImagesManager.logoImage,
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                ),
              );
            case RequestStates.error:
              return BaseError(
                  errorMessage: state.errorTopRatedMessage,
                  onPressFunction: () {
                    BlocProvider.of<MovieBloc>(context)
                      ..add(GetTopRatedMoviesEvent(
                          langCode: FunctionsManager.getLangCode(context)));
                  });
          }
        });
  }
}
