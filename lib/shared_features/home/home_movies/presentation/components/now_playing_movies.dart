import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import '../../../../../core/utils/global_widgets/base_error.dart';
import '../../../../../core/utils/request_states.dart';
import '../controllers/movie_bloc.dart';
import '../controllers/movie_events.dart';
import '../controllers/movie_states.dart';

class NowPlayingMovies extends StatelessWidget {
  const NowPlayingMovies({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MovieBloc, MovieStates>(
      buildWhen: (previous, current) =>
          previous.nowPlayingRequestStates != current.nowPlayingRequestStates,
      builder: (context, state) {
        switch (state.nowPlayingRequestStates) {
          case RequestStates.loading:
            return const BaseLoadingIndicator();
          case RequestStates.success:
            return state.nowPlayingMovies!.movies.isEmpty
                ? BaseEmptyWidget(emptyMessage: StringsManager.noNowPlayingFilmsString)
                : CarouselSlider(
                    items: state.nowPlayingMovies!.movies
                        .map(
                          (e) => Card(
                            borderOnForeground: true,
                            semanticContainer: true,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(SizesManager.s20),
                            ),
                            child: ClipRRect(
                              borderRadius:
                                  BorderRadius.circular(SizesManager.s20),
                              child: FadeInImage.assetNetwork(
                                fit: BoxFit.cover,
                                image:
                                    FunctionsManager.imageUrl(e.backdropPath!),
                                placeholder: ImagesManager.logoImage,
                              ),
                            ),
                          ),
                        )
                        .toList(),
                    options: CarouselOptions(
                      height: getHeight(inputHeight: SizesManager.s300),
                      viewportFraction: 0.8,
                      initialPage: 0,
                      enableInfiniteScroll: true,
                      reverse: false,
                      autoPlay: true,
                      autoPlayInterval: const Duration(
                        seconds: ConstsManager.carouselAutoPlayIntervalDuration,
                      ),
                      autoPlayAnimationDuration: const Duration(
                        milliseconds: ConstsManager.autoPlayAnimationDuration,
                      ),
                      autoPlayCurve: Curves.fastOutSlowIn,
                      enlargeCenterPage: true,
                      scrollDirection: Axis.horizontal,
                    ));
          case RequestStates.error:
            return SizedBox(
                height: getHeight(inputHeight: SizesManager.s300),
                child: BaseError(
                    errorMessage: state.errorNowPlayingMessage,
                    onPressFunction: () {
                      BlocProvider.of<MovieBloc>(context)
                        ..add(GetNowPlayingMoviesEvent(
                            langCode: FunctionsManager.getLangCode(context)));
                    }));
        }
      },
    );
  }
}
