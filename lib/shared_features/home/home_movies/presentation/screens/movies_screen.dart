import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/services/services_locator.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/functions_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_search_bar_component.dart';
import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../components/coming_soon_movies.dart';
import '../components/now_playing_movies.dart';
import '../components/popular_movies.dart';
import '../components/top_rated_movies.dart';
import '../controllers/movie_bloc.dart';
import '../controllers/movie_events.dart';

class MainMoviesScreen extends StatefulWidget {
  static const String routeName = Routes.moviesRoute;
  @override
  State<MainMoviesScreen> createState() => _MainMoviesScreenState();
}

class _MainMoviesScreenState extends State<MainMoviesScreen> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<MovieBloc>()
        ..add(GetNowPlayingMoviesEvent(
            langCode: FunctionsManager.getLangCode(context)))
        ..add(GetPopularMoviesEvent(
            langCode: FunctionsManager.getLangCode(context)))
        ..add(GetTopRatedMoviesEvent(
            langCode: FunctionsManager.getLangCode(context)))
        ..add(GetComingSoonMoviesEvent(
            langCode: FunctionsManager.getLangCode(context))),
      child: SingleChildScrollView(
        key: const Key('movieScrollView'),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(
                MarginManager.m16,
                getHeight(inputHeight: MarginManager.m24),
                MarginManager.m16,
                MarginManager.m8,
              ),
              child: BaseSearchBarComponent(
                onPressed: () =>
                    Navigator.pushNamed(context, Routes.searchMoviesRoute),
              ),
            ),
            const NowPlayingMovies(),
            Container(
              margin: EdgeInsets.fromLTRB(
                MarginManager.m16,
                getHeight(inputHeight: MarginManager.m24),
                MarginManager.m16,
                MarginManager.m8,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    FunctionsManager.translateText(
                        text: StringsManager.popularString, context: context),
                    style: getMediumTextStyle(
                      fontSize: FontSizeManager.size19,
                      letterSpacing: SizesManager.s0_15,
                      context: context,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, Routes.popularRoute);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(PaddingManager.p8),
                      child: Row(
                        children: [
                          Text(
                            FunctionsManager.translateText(
                                text: StringsManager.seeMoreString,
                                context: context),
                            style: getMediumTextStyle(
                                fontSize: FontSizeManager.size12,
                                letterSpacing: SizesManager.s0,
                                context: context),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const PopularMovies(),
            Container(
              margin: EdgeInsets.fromLTRB(
                MarginManager.m16,
                getHeight(inputHeight: MarginManager.m24),
                MarginManager.m16,
                MarginManager.m8,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    FunctionsManager.translateText(
                        text: StringsManager.topRatedString, context: context),
                    style: getMediumTextStyle(
                      fontSize: FontSizeManager.size19,
                      letterSpacing: SizesManager.s0_15,
                      context: context,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, Routes.topRatedRoute);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(PaddingManager.p8),
                      child: Row(
                        children: [
                          Text(
                            FunctionsManager.translateText(
                                text: StringsManager.seeMoreString,
                                context: context),
                            style: getMediumTextStyle(
                                fontSize: FontSizeManager.size12,
                                letterSpacing: SizesManager.s0,
                                context: context),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const TopRatedMovies(),
            Container(
              margin: EdgeInsets.fromLTRB(
                MarginManager.m16,
                getHeight(inputHeight: MarginManager.m24),
                MarginManager.m16,
                MarginManager.m8,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    FunctionsManager.translateText(
                        text: StringsManager.comingSoonString,
                        context: context),
                    style: getMediumTextStyle(
                      fontSize: FontSizeManager.size19,
                      letterSpacing: SizesManager.s0_15,
                      context: context,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, Routes.comingSoonRoute);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(PaddingManager.p8),
                      child: Row(
                        children: [
                          Text(
                            FunctionsManager.translateText(
                                text: StringsManager.seeMoreString,
                                context: context),
                            style: getMediumTextStyle(
                                fontSize: FontSizeManager.size12,
                                letterSpacing: SizesManager.s0,
                                context: context),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const ComingSoonMovies(),
            SizedBox(height: getHeight(inputHeight: SizesManager.s50)),
          ],
        ),
      ),
    );
  }
}
