import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import '../../../../../core/error/failure.dart';
import '../../../../../core/network/network_info.dart';
import '../../domain/entities/all_movies.dart';
import '../../domain/repository/base_movie_repository.dart';
import '../../domain/usecases/get_coming_soon_usecase.dart';
import '../../domain/usecases/get_now_playing_usecase.dart';
import '../../domain/usecases/get_popular_movies_usecase.dart';
import '../../domain/usecases/get_top_rated_movies_usecase.dart';
import '../datasources/home_local_datasource.dart';
import '../datasources/home_remote_datasource.dart';

class MovieRepository extends BaseMovieRepository {
  final BaseHomeRemoteDataSource baseHomeRemoteDataSource;
  final BaseHomeLocalDataSource baseHomeLocalDataSource;
  final NetworkInfo networkInfo;

  MovieRepository({
    required this.baseHomeRemoteDataSource,
    required this.networkInfo,
    required this.baseHomeLocalDataSource,
  });

  @override
  Future<Either<Failure, AllMovies>> getNowPlaying({
    required NowPlayingHomeParameters nowPlayingHomeParameters,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await baseHomeRemoteDataSource.getNowPlayingMovies(
          nowPlayingHomeParameters: nowPlayingHomeParameters,
        );
        baseHomeLocalDataSource.cacheInfo(
            result, ConstsManager.CACHE_NOW_PLAYING);
        return Right(result);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message.toString()));
      }
    } else {
      try {
        final localData = await baseHomeLocalDataSource
            .getCachedInfo(ConstsManager.CACHE_NOW_PLAYING);
        return Right(localData);
      } on LocalDatabaseException catch (e) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }

  @override
  Future<Either<Failure, AllMovies>> getPopularMovies({
    required PopularHomeParameters popularHomeParameters,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await baseHomeRemoteDataSource.getPopularMovies(
          popularHomeParameters: popularHomeParameters,
        );
        baseHomeLocalDataSource.cacheInfo(result, ConstsManager.CACHE_POPULAR);
        return Right(result);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message.toString()));
      }
    } else {
      try {
        final localData = await baseHomeLocalDataSource
            .getCachedInfo(ConstsManager.CACHE_POPULAR);
        return Right(localData);
      } on LocalDatabaseException catch (e) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }

  @override
  Future<Either<Failure, AllMovies>> getTopRatedMovies({
    required TopRatedHomeParameters topRatedHomeParameters,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await baseHomeRemoteDataSource.getTopRatedMovies(
          topRatedHomeParameters: topRatedHomeParameters,
        );
        baseHomeLocalDataSource.cacheInfo(
            result, ConstsManager.CACHE_TOP_RATED);
        return Right(result);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message.toString()));
      }
    } else {
      try {
        final localData = await baseHomeLocalDataSource
            .getCachedInfo(ConstsManager.CACHE_TOP_RATED);
        return Right(localData);
      } on LocalDatabaseException catch (e) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }

  @override
  Future<Either<Failure, AllMovies>> getComingSoonMovies({
    required ComingSoonHomeParameters comingSoonHomeParameters,
  }) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await baseHomeRemoteDataSource.getComingSoonMovies(
          comingSoonHomeParameters: comingSoonHomeParameters,
        );
        baseHomeLocalDataSource.cacheInfo(
            result, ConstsManager.CACHE_COMING_SOON);
        return Right(result);
      } on ServerException catch (failure) {
        return Left(ServerFailure(failure.message.toString()));
      }
    } else {
      try {
        final localData = await baseHomeLocalDataSource
            .getCachedInfo(ConstsManager.CACHE_COMING_SOON);
        return Right(localData);
      } on LocalDatabaseException catch (e) {
        return const Left(
            DatabaseFailure(StringsManager.localDataBaseFailureString));
      }
    }
  }
}
