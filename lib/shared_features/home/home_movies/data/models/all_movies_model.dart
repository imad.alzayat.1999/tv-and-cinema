
import '../../domain/entities/all_movies.dart';
import '../../domain/entities/movie.dart';
import 'movie_model.dart';

class AllMoviesModel extends AllMovies {
  const AllMoviesModel({required List<Movie> movies}) : super(movies: movies);

  factory AllMoviesModel.fromJson(Map<String, dynamic> json) => AllMoviesModel(
        movies: List<MovieModel>.from(
          json["results"].map(
            (x) => MovieModel.fromJson(x),
          ),
        ),
      );

  Map<String , dynamic> toJson() => {
    "results": List<dynamic>.from(movies.map((x) => x)),
  };
}
