import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import '../../../../../core/network/api_constance.dart';
import '../../domain/usecases/get_coming_soon_usecase.dart';
import '../../domain/usecases/get_now_playing_usecase.dart';
import '../../domain/usecases/get_popular_movies_usecase.dart';
import '../../domain/usecases/get_top_rated_movies_usecase.dart';
import '../models/all_movies_model.dart';

abstract class BaseHomeRemoteDataSource {
  Future<AllMoviesModel> getNowPlayingMovies(
      {required NowPlayingHomeParameters nowPlayingHomeParameters});
  Future<AllMoviesModel> getPopularMovies(
      {required PopularHomeParameters popularHomeParameters});
  Future<AllMoviesModel> getTopRatedMovies(
      {required TopRatedHomeParameters topRatedHomeParameters});
  Future<AllMoviesModel> getComingSoonMovies(
      {required ComingSoonHomeParameters comingSoonHomeParameters});
}

class HomeRemoteDataSource extends BaseHomeRemoteDataSource {
  final ApiConsumer apiConsumer;

  HomeRemoteDataSource({required this.apiConsumer});
  @override
  Future<AllMoviesModel> getNowPlayingMovies({
    required NowPlayingHomeParameters nowPlayingHomeParameters,
  }) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getNowPlayingEndPoint,
      queryParameters: {
        "language": nowPlayingHomeParameters.langCode,
      },
    );
    print("now ${response["results"]}");
    return AllMoviesModel.fromJson(response);
  }

  @override
  Future<AllMoviesModel> getPopularMovies({
    required PopularHomeParameters popularHomeParameters,
  }) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getPopularMoviesEndPoint,
      queryParameters: {
        "language": popularHomeParameters.langCode,
      },
    );
    return AllMoviesModel.fromJson(response);
  }

  @override
  Future<AllMoviesModel> getTopRatedMovies({
    required TopRatedHomeParameters topRatedHomeParameters,
  }) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getTopRatedMoviesEndPoint,
      queryParameters: {
        "language": topRatedHomeParameters.langCode,
      },
    );
    return AllMoviesModel.fromJson(response);
  }

  @override
  Future<AllMoviesModel> getComingSoonMovies({
    required ComingSoonHomeParameters comingSoonHomeParameters,
  }) async {
    final response = await apiConsumer.get(
      path: ApiConstance.getComingSoonMoviesEndPoint,
      queryParameters: {
        "language": comingSoonHomeParameters.langCode,
      },
    );
    return AllMoviesModel.fromJson(response);
  }
}
