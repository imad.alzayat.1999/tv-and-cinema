import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../../../core/error/exceptions.dart';
import '../../../../../core/services/services_locator.dart';
import '../models/all_movies_model.dart';
import '../models/movie_model.dart';

abstract class BaseHomeLocalDataSource{
  Future<AllMoviesModel> getCachedInfo(String infoKey);
  Future<Unit> cacheInfo(AllMoviesModel movies, String infoKey);
}
class HomeLocalDataSource extends BaseHomeLocalDataSource{
  @override
  Future<Unit> cacheInfo(AllMoviesModel movies, String infoKey) {
    final encodedMovies = movies.toJson();
    getIt<SharedPreferences>()
        .setString(infoKey, json.encode(encodedMovies));
    print("Info Cached is for $infoKey is: " + json.encode(encodedMovies));
    return Future.value(unit);
  }

  @override
  Future<AllMoviesModel> getCachedInfo(String infoKey) {
    final jsonString = getIt<SharedPreferences>().getString(infoKey);
    if (jsonString != null) {
      final decodeJsonData = json.decode(jsonString);
      return Future.value(AllMoviesModel.fromJson(decodeJsonData));
    } else {
      throw LocalDatabaseException(message: '');
    }
  }

}