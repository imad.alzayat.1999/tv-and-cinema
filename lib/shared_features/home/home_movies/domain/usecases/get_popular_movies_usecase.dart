import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import '../../../../../core/error/failure.dart';
import '../../../../../core/usecase/base_usecase.dart';
import '../entities/all_movies.dart';
import '../repository/base_movie_repository.dart';

class GetPopularMoviesUseCase  extends BaseUseCase<AllMovies , PopularHomeParameters>{
  final BaseMovieRepository baseMovieRepository;

  GetPopularMoviesUseCase(this.baseMovieRepository);

  @override
  Future<Either<Failure, AllMovies>> call(PopularHomeParameters parameters) async{
    return await baseMovieRepository.getPopularMovies(popularHomeParameters: parameters);
  }
}


class PopularHomeParameters extends Equatable{
  final String langCode;

  PopularHomeParameters({required this.langCode});

  @override
  // TODO: implement props
  List<Object?> get props => [langCode];
}