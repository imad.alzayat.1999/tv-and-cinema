import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../entities/all_movies.dart';
import '../repository/base_movie_repository.dart';

class GetComingSoonUseCase extends BaseUseCase<AllMovies , ComingSoonHomeParameters>{
  final BaseMovieRepository baseMovieRepository;

  GetComingSoonUseCase({required this.baseMovieRepository});
  @override
  Future<Either<Failure, AllMovies>> call(ComingSoonHomeParameters parameters) async{
    return await baseMovieRepository.getComingSoonMovies(comingSoonHomeParameters: parameters);
  }
}

class ComingSoonHomeParameters extends Equatable{
  final String langCode;

  ComingSoonHomeParameters({required this.langCode});

  @override
  // TODO: implement props
  List<Object?> get props => [langCode];
}