import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import '../../../../../core/error/failure.dart';
import '../../../../../core/usecase/base_usecase.dart';
import '../entities/all_movies.dart';
import '../repository/base_movie_repository.dart';

class GetTopRatedMoviesUseCase extends BaseUseCase<AllMovies , TopRatedHomeParameters>{
  final BaseMovieRepository baseMovieRepository;

  GetTopRatedMoviesUseCase(this.baseMovieRepository);

  @override
  Future<Either<Failure, AllMovies>> call(TopRatedHomeParameters parameters) async{
    return await baseMovieRepository.getTopRatedMovies(topRatedHomeParameters: parameters);
  }
}


class TopRatedHomeParameters extends Equatable{
  final String langCode;

  TopRatedHomeParameters({required this.langCode});

  @override
  // TODO: implement props
  List<Object?> get props => [langCode];
}