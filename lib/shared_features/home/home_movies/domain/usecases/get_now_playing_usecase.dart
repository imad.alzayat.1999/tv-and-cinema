import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import '../../../../../core/error/failure.dart';
import '../../../../../core/usecase/base_usecase.dart';
import '../entities/all_movies.dart';
import '../repository/base_movie_repository.dart';

class GetTopPlayingUseCase extends BaseUseCase<AllMovies , NowPlayingHomeParameters>{

  final BaseMovieRepository baseMovieRepository;

  GetTopPlayingUseCase(this.baseMovieRepository);

  @override
  Future<Either<Failure, AllMovies>> call(NowPlayingHomeParameters parameters) async{
    return await baseMovieRepository.getNowPlaying(nowPlayingHomeParameters: parameters);
  }
}

class NowPlayingHomeParameters extends Equatable{
  final String langCode;

  NowPlayingHomeParameters({required this.langCode});

  @override
  // TODO: implement props
  List<Object?> get props => [langCode];
}