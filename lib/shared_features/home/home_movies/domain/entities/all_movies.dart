import 'package:equatable/equatable.dart';

import 'movie.dart';

class AllMovies extends Equatable{
  final List<Movie> movies;

  const AllMovies({required this.movies});

  @override
  // TODO: implement props
  List<Object?> get props => [movies];
}