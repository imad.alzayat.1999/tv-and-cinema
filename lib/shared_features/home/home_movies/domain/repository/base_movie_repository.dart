import 'package:dartz/dartz.dart';
import '../../../../../core/error/failure.dart';
import '../entities/all_movies.dart';
import '../usecases/get_coming_soon_usecase.dart';
import '../usecases/get_now_playing_usecase.dart';
import '../usecases/get_popular_movies_usecase.dart';
import '../usecases/get_top_rated_movies_usecase.dart';

abstract class BaseMovieRepository {
  Future<Either<Failure, AllMovies>> getNowPlaying({
    required NowPlayingHomeParameters nowPlayingHomeParameters,
  });
  Future<Either<Failure, AllMovies>> getPopularMovies({
    required PopularHomeParameters popularHomeParameters,
  });
  Future<Either<Failure, AllMovies>> getTopRatedMovies({
    required TopRatedHomeParameters topRatedHomeParameters,
  });
  Future<Either<Failure, AllMovies>> getComingSoonMovies({
    required ComingSoonHomeParameters comingSoonHomeParameters,
  });
}
