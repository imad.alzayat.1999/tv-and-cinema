import '../../domain/entities/genres_entity.dart';

class GenresModel extends GenresEntity {
  GenresModel({required int? id, required String? name})
      : super(id: id, name: name);

  factory GenresModel.fromJson(Map<String, dynamic> json) =>
      GenresModel(id: json["id"], name: json["name"]);

  Map<String , dynamic> toJson() => {
    "id" : id,
    "name" : name,
  };
}
