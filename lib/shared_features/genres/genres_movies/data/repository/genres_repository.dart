import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';

import '../../domain/entities/genres_entity.dart';
import '../../domain/repository/base_genres_repository.dart';
import '../datasources/genres_remote_datasource.dart';

class GenresRepository extends BaseGenresRepository{
  final BaseGenresRemoteDataSources baseGenresRemoteDataSources;

  GenresRepository({required this.baseGenresRemoteDataSources});
  @override
  Future<Either<Failure, List<GenresEntity>>> getMovieGenres() async{
    try{
      final result = await baseGenresRemoteDataSources.getMovieGenres();
      return Right(result);
    }on ServerException catch(failure){
      return Left(ServerFailure(failure.message));
    }
  }
}