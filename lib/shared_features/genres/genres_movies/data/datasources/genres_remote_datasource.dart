import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';

import '../models/genres_model.dart';

abstract class BaseGenresRemoteDataSources{
  Future<List<GenresModel>> getMovieGenres();
}
class GenresRemoteDataSources extends BaseGenresRemoteDataSources{
  final ApiConsumer apiConsumer;

  GenresRemoteDataSources({required this.apiConsumer});

  @override
  Future<List<GenresModel>> getMovieGenres() async{
    final response = await apiConsumer.get(path: ApiConstance.getMovieGenresEndPoint);
    return List<GenresModel>.from(response["genres"]!.map((x) => GenresModel.fromJson(x)));
  }
}