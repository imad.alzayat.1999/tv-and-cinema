import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';

import '../entities/genres_entity.dart';
import '../repository/base_genres_repository.dart';

class GetMovieGenresUseCase extends BaseUseCase<List<GenresEntity> , NoParameters>{
  final BaseGenresRepository baseGenresRepository;

  GetMovieGenresUseCase({required this.baseGenresRepository});
  @override
  Future<Either<Failure, List<GenresEntity>>> call(NoParameters parameters) async{
    return await baseGenresRepository.getMovieGenres();
  }
}