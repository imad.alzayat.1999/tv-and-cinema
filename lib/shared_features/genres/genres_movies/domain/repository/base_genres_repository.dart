import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import '../entities/genres_entity.dart';

abstract class BaseGenresRepository{
  Future<Either<Failure , List<GenresEntity>>> getMovieGenres();
}