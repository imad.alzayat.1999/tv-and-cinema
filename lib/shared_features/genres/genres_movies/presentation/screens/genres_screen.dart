import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/config/size_config/size_config.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_empty_widget.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_error.dart';
import 'package:movie_app_clean_architecture/core/utils/global_widgets/base_loading_indicator.dart';
import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/services/services_locator.dart';
import '../../../../../core/utils/global_widgets/base_icon.dart';
import '../../../../../core/utils/request_states.dart';
import '../../../../../core/utils/resources/assets_manager.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../../domain/entities/genres_entity.dart';
import '../components/genre_component.dart';
import '../controllers/genres_bloc.dart';
import '../controllers/genres_events.dart';
import '../controllers/genres_states.dart';

class GenresScreen extends StatelessWidget {
  const GenresScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<GenresBloc>()..add(GetAllGenres()),
      child: Scaffold(
        backgroundColor: ColorsManager.kEerieBlack,
          appBar: AppBar(
            elevation: SizesManager.s0,
            backgroundColor: ColorsManager.kEerieBlack,
            title: Text(
              AppLocalizations.of(context)!.translate(StringsManager.genreString)!,
              style: getBoldTextStyle(
                fontSize: FontSizeManager.size16,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
          ),
          body: GenresContent()),
    );
  }
}

class GenresContent extends StatefulWidget {
  const GenresContent({Key? key}) : super(key: key);

  @override
  State<GenresContent> createState() => _GenresContentState();
}

class _GenresContentState extends State<GenresContent> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GenresBloc, GenresStates>(
      builder: (context, state) {
        switch (state.genresRequestStates) {
          case RequestStates.loading:
            return const BaseLoadingIndicator();
          case RequestStates.success:
            return _getGenresGrid(state.genres);
          case RequestStates.error:
            return BaseError(errorMessage: state.errorGenresMessage, onPressFunction: (){
              BlocProvider.of<GenresBloc>(context).add(GetAllGenres());
            });
        }
      },
    );
  }

  Widget _getGenresGrid(List<GenresEntity> genres) {
    return genres.isEmpty ? BaseEmptyWidget(emptyMessage: StringsManager.noGenresString) : GridView.builder(
      shrinkWrap: true,
      padding: EdgeInsets.symmetric(
        vertical: getHeight(inputHeight: PaddingManager.p16),
        horizontal: getWidth(inputWidth: PaddingManager.p8),
      ),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
        childAspectRatio: 1,
      ),
      itemCount: genres.length,
      itemBuilder: (context, index) =>
          GenreComponent(genresEntity: genres[index]),
    );
  }
}
