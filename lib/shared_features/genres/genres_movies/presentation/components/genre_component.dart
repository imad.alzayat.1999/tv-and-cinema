import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/config/routes_config/routes_parameters.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/colors_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/consts_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/fonts_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/strings_mananger.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/style_manager.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/values_mananger.dart';
import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/utils/resources/functions_manager.dart';
import '../../domain/entities/genres_entity.dart';

class GenreComponent extends StatelessWidget {
  final GenresEntity genresEntity;
  const GenreComponent({Key? key, required this.genresEntity})
      : super(key: key);

  String _getMovieGenreImage(String? name) {
    switch (name) {
      case "Action":
        return ConstsManager.actionImage;
      case "Adventure":
        return ConstsManager.adventureImage;
      case "Animation":
        return ConstsManager.animationImage;
      case "Comedy":
        return ConstsManager.comedyImage;
      case "Crime":
        return ConstsManager.crimeImage;
      case "Documentary":
        return ConstsManager.documentaryImage;
      case "Drama":
        return ConstsManager.dramaImage;
      case "Family":
        return ConstsManager.familyImage;
      case "Fantasy":
        return ConstsManager.fantasyImage;
      case "History":
        return ConstsManager.historyImage;
      case "Horror":
        return ConstsManager.horrorImage;
      case "Music":
        return ConstsManager.musicImage;
      case "Mystrey":
        return ConstsManager.mysteryImage;
      case "Romance":
        return ConstsManager.romanceImage;
      case "Science Fiction":
        return ConstsManager.scienceFictionImage;
      case "TV Movie":
        return ConstsManager.tvMovieImage;
      case "Thriller":
        return ConstsManager.thrillerImage;
      case "War":
        return ConstsManager.warImage;
      case "Mystery":
        return ConstsManager.mysteryImage;
      default:
        return ConstsManager.dummyReviewProfileName;
    }
  }

  String _getGenreName(String? name, BuildContext context) {
    switch (name) {
      case "Action":
        return FunctionsManager.translateText(
            text: StringsManager.actionString, context: context);
      case "Adventure":
        return FunctionsManager.translateText(
            text: StringsManager.adventureString, context: context);
      case "Animation":
        return FunctionsManager.translateText(
            text: StringsManager.animationString, context: context);
      case "Comedy":
        return FunctionsManager.translateText(
            text: StringsManager.comedyString, context: context);
      case "Crime":
        return FunctionsManager.translateText(
            text: StringsManager.crimeString, context: context);
      case "Documentary":
        return FunctionsManager.translateText(
            text: StringsManager.documentaryString, context: context);
      case "Drama":
        return FunctionsManager.translateText(
            text: StringsManager.dramaString, context: context);
      case "Family":
        return FunctionsManager.translateText(
            text: StringsManager.familyString, context: context);
      case "Fantasy":
        return FunctionsManager.translateText(
            text: StringsManager.fantasyString, context: context);
      case "History":
        return FunctionsManager.translateText(
            text: StringsManager.historyString, context: context);
      case "Horror":
        return FunctionsManager.translateText(
            text: StringsManager.horrorString, context: context);
      case "Music":
        return FunctionsManager.translateText(
            text: StringsManager.musicString, context: context);
      case "Mystery":
        return FunctionsManager.translateText(
            text: StringsManager.mystreyString, context: context);
      case "Romance":
        return FunctionsManager.translateText(
            text: StringsManager.romanceString, context: context);
      case "Science Fiction":
        return FunctionsManager.translateText(
            text: StringsManager.scienceFictionString, context: context);
      case "TV Movie":
        return FunctionsManager.translateText(
            text: StringsManager.tvMoviesString, context: context);
      case "Thriller":
        return FunctionsManager.translateText(
            text: StringsManager.thrillerString, context: context);
      case "War":
        return FunctionsManager.translateText(
            text: StringsManager.warString, context: context);
      default:
        return FunctionsManager.translateText(
            text: StringsManager.otherString, context: context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        Routes.movieByGenreRoute,
        arguments: ByGenreRouteParameters(
            id: genresEntity.id!, name: genresEntity.name!),
      ),
      child: Card(
        margin: const EdgeInsets.all(MarginManager.m4),
        color: ColorsManager.kWhite,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SizesManager.s20)),
        child: Stack(
          alignment: Alignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(SizesManager.s20),
              child: AspectRatio(
                aspectRatio: 1,
                child: Container(
                  color: ColorsManager.kBlack,
                  child: Opacity(
                    opacity: 0.6,
                    child: FadeInImage.assetNetwork(
                      image: _getMovieGenreImage(genresEntity.name),
                      placeholder: ImagesManager.logoImage,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            Text(
              _getGenreName(genresEntity.name!, context),
              style: getBoldTextStyle(
                fontSize: FontSizeManager.size14,
                color: ColorsManager.kWhite,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
