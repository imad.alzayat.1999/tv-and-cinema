import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import '../../domain/usecases/get_movie_genres_usecase.dart';
import 'genres_events.dart';
import 'genres_states.dart';

class GenresBloc extends Bloc<GenresEvents, GenresStates> {
  final GetMovieGenresUseCase getMovieGenresUseCase;
  GenresBloc({required this.getMovieGenresUseCase}) : super(GenresStates()) {
    on<GenresEvents>((event, emit) async {
      final failureOrGenres = await getMovieGenresUseCase(const NoParameters());
      failureOrGenres.fold(
        (l) => emit(
          state.copyWith(
            errorGenresMessage: l.message,
            genresRequestStates: RequestStates.error,
          ),
        ),
        (r) => emit(
          state.copyWith(
            genres: r,
            genresRequestStates: RequestStates.success,
          ),
        ),
      );
    });
  }
}
