import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';

import '../../domain/entities/genres_entity.dart';

class GenresStates extends Equatable {
  final String errorGenresMessage;
  final List<GenresEntity> genres;
  final RequestStates genresRequestStates;

  GenresStates({
    this.errorGenresMessage = '',
    this.genres = const [],
    this.genresRequestStates = RequestStates.loading,
  });

  GenresStates copyWith({
    String? errorGenresMessage,
    List<GenresEntity>? genres,
    RequestStates? genresRequestStates,
  }) {
    return GenresStates(
      errorGenresMessage: errorGenresMessage ?? this.errorGenresMessage,
      genres: genres ?? this.genres,
      genresRequestStates: genresRequestStates ?? this.genresRequestStates,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [errorGenresMessage , genres , genresRequestStates];
}
