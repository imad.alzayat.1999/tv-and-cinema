import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import '../entities/tv_genres_entity.dart';

abstract class BaseTvGenresRepository{
  Future<Either<Failure , List<TvGenresEntity>>> getTvGenres();
}