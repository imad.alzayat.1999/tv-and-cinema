import 'package:equatable/equatable.dart';

class TvGenresEntity extends Equatable{
  final int? id;
  final String? name;

  TvGenresEntity({required this.id, required this.name});

  @override
  // TODO: implement props
  List<Object?> get props => [id , name];
}