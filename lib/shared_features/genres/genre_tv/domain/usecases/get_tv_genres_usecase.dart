import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import '../entities/tv_genres_entity.dart';
import '../repository/base_tv_genres_repository.dart';

class GetTvGenresUseCase extends BaseUseCase<List<TvGenresEntity> , NoParameters>{
  final BaseTvGenresRepository baseTvGenresRepository;

  GetTvGenresUseCase({required this.baseTvGenresRepository});

  @override
  Future<Either<Failure, List<TvGenresEntity>>> call(NoParameters parameters) async{
    return await baseTvGenresRepository.getTvGenres();
  }
}