import 'package:flutter/material.dart';
import 'package:movie_app_clean_architecture/core/utils/resources/assets_manager.dart';
import '../../../../../core/config/language_config/app_localizations.dart';
import '../../../../../core/config/routes_config/routes_config.dart';
import '../../../../../core/utils/resources/colors_manager.dart';
import '../../../../../core/utils/resources/consts_manager.dart';
import '../../../../../core/utils/resources/fonts_mananger.dart';
import '../../../../../core/utils/resources/strings_mananger.dart';
import '../../../../../core/utils/resources/style_manager.dart';
import '../../../../../core/utils/resources/values_mananger.dart';
import '../../domain/entities/tv_genres_entity.dart';
import '../controllers/routing_parameters/tv_genre_routing_parameters.dart';

class TvGenreItem extends StatelessWidget {
  final TvGenresEntity tvGenre;
  const TvGenreItem({Key? key, required this.tvGenre}) : super(key: key);

  String _getMovieGenreImage(String? name) {
    switch (name) {
      case "Action & Adventure":
        return ConstsManager.actionImage;
      case "Animation":
        return ConstsManager.animationImage;
      case "Comedy":
        return ConstsManager.comedyImage;
      case "Crime":
        return ConstsManager.crimeImage;
      case "Documentary":
        return ConstsManager.documentaryImage;
      case "Drama":
        return ConstsManager.dramaImage;
      case "Family":
        return ConstsManager.familyImage;
      case "Sci-Fi & Fantasy":
        return ConstsManager.fantasyImage;
      case "Kids":
        return ConstsManager.kidsImage;
      case "News":
        return ConstsManager.newsImage;
      case "Reality":
        return ConstsManager.realityImage;
      case "Soap":
        return ConstsManager.soapImage;
      case "Talk":
        return ConstsManager.talkImage;
      case "War & Politics":
        return ConstsManager.warAndPoliticsImage;
      case "Mystery":
        return ConstsManager.mysteryImage;
      default:
        return ConstsManager.dummyReviewProfileName;
    }
  }

  String _getGenreName(String? name, BuildContext context) {
    switch (name) {
      case "Action & Adventure":
        return AppLocalizations.of(context)!
            .translate(StringsManager.actionAndAdventureString)!;
      case "Animation":
        return AppLocalizations.of(context)!
            .translate(StringsManager.animationString)!;
      case "Comedy":
        return AppLocalizations.of(context)!
            .translate(StringsManager.comedyString)!;
      case "Crime":
        return AppLocalizations.of(context)!
            .translate(StringsManager.crimeString)!;
      case "Documentary":
        return AppLocalizations.of(context)!
            .translate(StringsManager.documentaryString)!;
      case "Drama":
        return AppLocalizations.of(context)!
            .translate(StringsManager.dramaString)!;
      case "Family":
        return AppLocalizations.of(context)!
            .translate(StringsManager.familyString)!;
      case "Sci-Fi & Fantasy":
        return AppLocalizations.of(context)!
            .translate(StringsManager.scifiAndFantasyString)!;
      case "Kids":
        return AppLocalizations.of(context)!
            .translate(StringsManager.kidsString)!;
      case "News":
        return AppLocalizations.of(context)!
            .translate(StringsManager.newsString)!;
      case "Reality":
        return AppLocalizations.of(context)!
            .translate(StringsManager.realityString)!;
      case "Mystery":
        return AppLocalizations.of(context)!
            .translate(StringsManager.mystreyString)!;
      case "Soap":
        return AppLocalizations.of(context)!
            .translate(StringsManager.soapString)!;
      case "Talk":
        return AppLocalizations.of(context)!
            .translate(StringsManager.talkString)!;
      case "War & Politics":
        return AppLocalizations.of(context)!
            .translate(StringsManager.warAndPoliticsString)!;
      default:
        return AppLocalizations.of(context)!
            .translate(StringsManager.otherString)!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          Routes.genresTvRoute,
          arguments: SeriesByGenreRoutingParameter(
              genre: tvGenre.name.toString(), genreId: tvGenre.id!),
        );
      },
      child: Card(
        margin: const EdgeInsets.all(MarginManager.m4),
        color: ColorsManager.kWhite,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SizesManager.s20)),
        child: Stack(
          alignment: Alignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(SizesManager.s20),
              child: AspectRatio(
                aspectRatio: 1,
                child: Container(
                  color: ColorsManager.kBlack,
                  child: Opacity(
                    opacity: 0.6,
                    child: FadeInImage.assetNetwork(
                      image: _getMovieGenreImage(tvGenre.name),
                      placeholder: ImagesManager.logoImage,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            Text(
              _getGenreName(tvGenre.name!, context),
              style: getBoldTextStyle(
                fontSize: FontSizeManager.size14,
                color: ColorsManager.kWhite,
                letterSpacing: SizesManager.s0,
                context: context,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
