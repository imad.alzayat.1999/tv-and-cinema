import 'package:equatable/equatable.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';

import '../../../domain/entities/tv_genres_entity.dart';

class TvGenresStates extends Equatable {
  final String tvGenresErrorMessage;
  final List<TvGenresEntity> tvGenres;
  final RequestStates tvGenresRequestStates;

  TvGenresStates({
    this.tvGenresErrorMessage = '',
    this.tvGenres = const [],
    this.tvGenresRequestStates = RequestStates.loading,
  });

  TvGenresStates copyWith({
    String? tvGenresErrorMessage,
    List<TvGenresEntity>? tvGenres,
    RequestStates? tvGenresRequestStates,
  }) {
    return TvGenresStates(
      tvGenres: tvGenres ?? this.tvGenres,
      tvGenresErrorMessage: tvGenresErrorMessage ?? this.tvGenresErrorMessage,
      tvGenresRequestStates:
          tvGenresRequestStates ?? this.tvGenresRequestStates,
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props =>
      [tvGenresRequestStates, tvGenres, tvGenresErrorMessage];
}
