import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_clean_architecture/core/usecase/base_usecase.dart';
import 'package:movie_app_clean_architecture/core/utils/request_states.dart';
import 'package:movie_app_clean_architecture/shared_features/genres/genre_tv/presentation/controllers/bloc/tv_genres_events.dart';
import 'package:movie_app_clean_architecture/shared_features/genres/genre_tv/presentation/controllers/bloc/tv_genres_states.dart';
import '../../../domain/usecases/get_tv_genres_usecase.dart';

class TvGenresBloc extends Bloc<TvGenresEvents, TvGenresStates> {
  final GetTvGenresUseCase getTvGenresUseCase;
  TvGenresBloc({required this.getTvGenresUseCase}) : super(TvGenresStates()) {
    on<GetTvGenresEvent>((event, emit) async {
      final failureOrTvGenres = await getTvGenresUseCase(const NoParameters());
      failureOrTvGenres.fold((l) {
        emit(
          state.copyWith(
            tvGenresRequestStates: RequestStates.error,
            tvGenresErrorMessage: l.message,
          ),
        );
      }, (r) {
        emit(
          state.copyWith(
            tvGenresRequestStates: RequestStates.success,
            tvGenres: r,
          ),
        );
      });
    });
  }
}
