import 'package:equatable/equatable.dart';

class SeriesByGenreRoutingParameter extends Equatable{
  final int genreId;
  final String genre;

  SeriesByGenreRoutingParameter({required this.genreId, required this.genre});

  @override
  // TODO: implement props
  List<Object?> get props => [genreId , genre];
}