import 'package:movie_app_clean_architecture/core/api/api_consumer.dart';
import 'package:movie_app_clean_architecture/core/network/api_constance.dart';
import '../models/tv_genres_model.dart';

abstract class BaseTvGenresRemoteDataSource {
  Future<List<TvGenresModel>> getTvGenres();
}

class TvGenresRemoteDataSource extends BaseTvGenresRemoteDataSource {
  final ApiConsumer apiConsumer;

  TvGenresRemoteDataSource({required this.apiConsumer});

  @override
  Future<List<TvGenresModel>> getTvGenres() async {
    final response =
        await apiConsumer.get(path: ApiConstance.getTvGenresEndPoint);
    return List<TvGenresModel>.from(
        (response["genres"] as List).map((e) => TvGenresModel.fromJson(e)));
  }
}
