import '../../domain/entities/tv_genres_entity.dart';

class TvGenresModel extends TvGenresEntity {
  TvGenresModel({required int? id, required String? name})
      : super(id: id, name: name);

  factory TvGenresModel.fromJson(Map<String, dynamic> json) =>
      TvGenresModel(id: json["id"], name: json["name"]);

  Map<String , dynamic> toJson() => {
    "id" : id,
    "name": name,
  };
}
