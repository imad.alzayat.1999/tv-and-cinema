import 'package:dartz/dartz.dart';
import 'package:movie_app_clean_architecture/core/error/exceptions.dart';
import 'package:movie_app_clean_architecture/core/error/failure.dart';
import '../../domain/entities/tv_genres_entity.dart';
import '../../domain/repository/base_tv_genres_repository.dart';
import '../datasources/tv_genres_remote_datasource.dart';

class TvGenresRepository extends BaseTvGenresRepository{
  final BaseTvGenresRemoteDataSource baseTvGenresRemoteDataSource;

  TvGenresRepository({required this.baseTvGenresRemoteDataSource});

  @override
  Future<Either<Failure, List<TvGenresEntity>>> getTvGenres() async{
    try{
      final result = await baseTvGenresRemoteDataSource.getTvGenres();
      return Right(result);
    }on ServerException catch(e){
      return Left(ServerFailure(e.message));
    }
  }
}